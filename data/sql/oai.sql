-- TODO: This variable should be obtained from the system ENV
\set DB dnet_oai
CREATE DATABASE :DB;
\c :DB

BEGIN;

CREATE TABLE oai_md_formats (
       prefix    text PRIMARY KEY,
       schema    text NOT NULL,
       namespace text NOT NULL,
       xslt      text
);

CREATE TABLE oai_sets (
       set_spec    text PRIMARY KEY,
       set_name    text NOT NULL,
       description text,
       ds_id       text
);

CREATE TABLE oai_conf (
	id              int PRIMARY KEY DEFAULT 0,
	repository_name text NOT NULL,
	public_baseurl  text NOT NULL,
	admin_email     text NOT NULL,
	native_format   text NOT NULL REFERENCES oai_md_formats(prefix)
);


CREATE TABLE oai_records (
       id       text PRIMARY KEY,
       body     text NOT NULL,
       date     timestamp NOT NULL DEFAULT now(),
       oai_set  text NOT NULL REFERENCES oai_set(set_spec)
); 

COMMIT;
