-- TODO: This variable should be obtained from the system ENV
\set DB dnet_vocabularies
CREATE DATABASE :DB;
\c :DB

BEGIN;

CREATE TABLE vocabularies (
	id          text PRIMARY KEY,
	name        text NOT NULL,
	description text
);

CREATE TABLE vocabulary_terms (
	vocabulary  text NOT NULL REFERENCES vocabularies(id) ON UPDATE CASCADE ON DELETE CASCADE,
	code        text NOT NULL,
	name        text NOT NULL,
	encoding    text DEFAULT 'OPENAIRE',
	synonyms    jsonb,
	PRIMARY KEY (vocabulary, code)
);

CREATE INDEX ON vocabulary_terms (vocabulary);

COMMIT;
