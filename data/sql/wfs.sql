-- TODO: This variable should be obtained from the system ENV
\set DB dnet_wfs
CREATE DATABASE :DB;
\c :DB

BEGIN;

-- WF History

CREATE TABLE wf_runtime (
       process_id     text PRIMARY KEY,
       wf_conf_id     text,
       wf_executor    text,
       priority int NOT NULL DEFAULT 100,
	name           text NOT NULL,
	family         text NOT NULL,
	status         text NOT NULL,
	start_date     timestamp,
	end_date       timestamp,
	last_update    timestamp DEFAULT now(),
	ds_id          text,
	ds_name        text,
	ds_api         text,
	graph          jsonb NOT NULL DEFAULT '{}',
	input         jsonb NOT NULL DEFAULT '{}',
	output        jsonb NOT NULL DEFAULT '{}'
);


CREATE TABLE wf_history (
 process_id     text PRIMARY KEY,
       wf_conf_id     text,
        name           text NOT NULL,
        family         text NOT NULL,
        status         text NOT NULL,
        start_date     timestamp,
        end_date       timestamp,
	ds_id          text,
        ds_name        text,
        ds_api         text,
	input         jsonb NOT NULL DEFAULT '{}',
        output        jsonb NOT NULL DEFAULT '{}'
);

CREATE VIEW wf_journal_view AS SELECT
       coalesce(r.process_id, h.process_id) AS process_id,
       coalesce(r.wf_conf_id, h.wf_conf_id) AS wf_conf_id,
       coalesce(r.name, h.name)             AS name,
       coalesce(r.family, h.family)         AS family,
       coalesce(r.status, h.status)         AS status,
       coalesce(r.start_date, h.start_date) AS start_date,
       coalesce(r.end_date, h.end_date)     AS end_date,
       coalesce(r.last_update, h.end_date)  AS last_update,
       coalesce(r.ds_id, h.ds_id)           AS ds_id,
       coalesce(r.ds_name, h.ds_name)       AS ds_name,
       coalesce(r.ds_api, h.ds_api)         AS ds_api,
       coalesce(r.input, h.input)           AS input,
       coalesce(r.output, h.output)         AS output,
       wf_executor                          AS wf_executor,
       graph                                AS graph
FROM wf_runtime r FULL OUTER JOIN wf_history h ON (r.process_id = h.process_id);

-- Workflows

CREATE TABLE wf_sections (
	id                      text PRIMARY KEY,
	name                    text NOT NULL
);

INSERT INTO wf_sections(id, name) VALUES
('GC', 'Garbage Collection'),
('CONSISTENCY', 'InfoSpace Consistency'),
('DEDUP', 'InfoSpace Deduplication'),
('INFERENCE', 'InfoSpace Inference'),
('MONITOR', 'InfoSpace Monitoring'),
('PROVISION', 'InfoSpace Provision'),
('IS', 'Information Service'),
('BROKER', 'Notification Broker');

CREATE TABLE wf_configurations (
	id                      text PRIMARY KEY,
	name                    text NOT NULL,
	section                 text REFERENCES wf_sections(id),
	details                 jsonb NOT NULL DEFAULT '{}',
	priority                int,
	dsid                    text,
	dsname                  text,
	apiid                   text,
	enabled                 boolean NOT NULL DEFAULT false,
	configured              boolean NOT NULL DEFAULT false,
	scheduling_enabled      boolean NOT NULL DEFAULT false, 
	scheduling_cron         text, 
	scheduling_min_interval int,
	workflows               text[] NOT NULL DEFAULT array[]::text[],
	destroy_wf              text,
	system_params           jsonb NOT NULL DEFAULT '{}',
	user_params             jsonb NOT NULL DEFAULT '{}'
);

CREATE TABLE wf_subscriptions (
	wf_conf_id              text NOT NULL REFERENCES wf_configurations(id),
	condition               text NOT NULL,
	email                   text NOT NULL,
	message_id              text NOT NULL,
	PRIMARY KEY (wf_conf_id, condition, email)
);

CREATE VIEW wf_sections_view AS SELECT
       s.id       AS id,
       s.name     AS name,
       count(c.*) AS count
FROM wf_sections s LEFT OUTER JOIN wf_configurations c ON (s.id = c.section)
GROUP BY s.id, s.name
ORDER BY s.name;

COMMIT;
