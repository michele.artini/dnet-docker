-- TODO: This variable should be obtained from the system ENV
\set DB dnet_dsm
CREATE DATABASE :DB;
\c :DB

BEGIN;

-- PROTOCOLS 
CREATE TABLE protocols (
	id text PRIMARY KEY
);
INSERT INTO protocols(id) VALUES ('oai'),('oai_sets'),('http'),('file'),('classpath'),('fileCSV'),('httpCSV'),('ftp'),('sftp'),('filesystem'),('files_from_metadata'),('files_from_mdstore'),('mongoDump'),('targz'),('zip'),('fileGzip'),('httpList'),('remoteMdstore');

CREATE TABLE protocol_params (
	protocol         text    NOT NULL REFERENCES protocols(id) ON UPDATE CASCADE ON DELETE CASCADE,
	param_name       text    NOT NULL,
	param_label      text    NOT NULL,
	param_type       text    NOT NULL DEFAULT 'TEXT',
	optional         boolean NOT NULL default false,
	has_sel_function boolean NOT NULL default false,
	PRIMARY KEY (protocol, param_name)
);
INSERT INTO protocol_params(protocol, param_name, param_label, param_type, optional, has_sel_function) VALUES
('oai', 'set', 'OAI set', 'LIST', true, true),
('oai', 'format', 'OAI Metadata Format', 'TEXT', false, false),
('http', 'splitOnElement', 'splitOnElement', 'TEXT', false, false),
('file', 'splitOnElement', 'splitOnElement', 'TEXT', false, false),
('classpath', 'splitOnElement', 'splitOnElement', 'TEXT', false, false),
('fileCSV', 'header', 'header', 'TEXT', false, false),
('fileCSV', 'separator', 'separator', 'TEXT', false, false),
('fileCSV', 'identifier', 'identifier', 'TEXT', false, false),
('fileCSV', 'quote', 'quote', 'TEXT', false, false),
('httpCSV', 'separator', 'separator', 'TEXT', false, false),
('httpCSV', 'identifier', 'identifier', 'TEXT', false, false),
('httpCSV', 'quote', 'quote', 'TEXT', false, false),
('ftp', 'username', 'username', 'TEXT', false, false),
('ftp', 'password', 'password', 'TEXT', false, false),
('ftp', 'recursive', 'recursive', 'BOOLEAN', false, false),
('ftp', 'extensions', 'extensions', 'LIST', false, false),
('sftp', 'username', 'username', 'TEXT', false, false),
('sftp', 'password', 'password', 'TEXT', true, false),
('sftp', 'authMethod', 'authMethod', 'TEXT', true, false),
('sftp', 'privateKeyPath', 'privateKeyPath', 'TEXT', true, false),
('sftp', 'port', 'port', 'TEXT', true, false),
('sftp', 'recursive', 'recursive', 'BOOLEAN', false, false),
('sftp', 'extensions', 'extensions', 'LIST', false, false);

-- END PROTOCOLS 

-- SERVICES 
CREATE TABLE dsm_services (
	id text NOT NULL PRIMARY KEY,
	namespaceprefix character(12) NOT NULL UNIQUE,
	officialname text NOT NULL,
	englishname text,
	websiteurl text,
	logourl text,
	contactemail text,
	latitude double precision DEFAULT 0.0,
	longitude double precision DEFAULT 0.0,
	timezone text DEFAULT '0.0',
	languages text,
	collectedfrom text REFERENCES dsm_services(id),
	_typology_to_remove_ text,
	provenanceaction text DEFAULT 'UNKNOWN',
	dateofcollection date DEFAULT now() NOT NULL,
	platform text,
	activationid text,
	description text,
	aggregator text DEFAULT 'OPENAIRE' NOT NULL,
	subjects text,
	managed boolean DEFAULT false NOT NULL,
	registeredby text,
	registrationdate date,
	consenttermsofuse boolean,
	consenttermsofusedate date,
	fulltextdownload boolean,
	dateofvalidation date,
	releasestartdate date,
	releaseenddate date,
	missionstatementurl text,
	databaseaccesstype text,
	datauploadtype text,
	databaseaccessrestriction text,
	datauploadrestriction text,
	citationguidelineurl text,
	certificates text,
	issn text,
	eissn text,
	lissn text,
	content_policies text[],
	submission_policy_url text,
	preservation_policy_url text,
	version_control boolean,
	jurisdiction text,
	eosc_type text DEFAULT 'Service',
	eosc_datasource_type text,
	research_entity_types text[],
	thematic boolean,
	research_product_access_policies text[],
	research_product_metadata_access_policies text[],
	dedup_main_service boolean DEFAULT true NOT NULL,
	optional1 text,
	optional2 text,
	status text DEFAULT 'ready',
	lastconsenttermsofusedate date
);

CREATE TABLE dsm_api (
	id text NOT NULL PRIMARY KEY,
	protocol text,
	service text REFERENCES dsm_services(id),
	contentdescription text DEFAULT 'metadata',
	active boolean DEFAULT false,
	removable boolean DEFAULT false,
	compatibility text DEFAULT 'UNKNOWN',
	compatibility_override text DEFAULT NULL,
	metadata_identifier_path text DEFAULT NULL,
	last_collection_total integer,
	last_collection_date timestamp without time zone,
	last_collection_mdid text DEFAULT NULL,
	last_aggregation_total integer,
	last_aggregation_date timestamp without time zone,
	last_aggregation_mdid text DEFAULT NULL,
	last_download_total integer,
	last_download_date timestamp without time zone,
	last_download_objid text DEFAULT NULL,
	last_validation_job text DEFAULT NULL,
	baseurl text,
	first_collection_date timestamp without time zone
);

CREATE TABLE dsm_apiparams (
	param text NOT NULL,
	value text DEFAULT '' NOT NULL,
	api text NOT NULL REFERENCES dsm_api(id),
	PRIMARY KEY (api, param)
);

CREATE TABLE dsm_organizations (
	id text NOT NULL PRIMARY KEY,
	legalshortname text,
	legalname text,
	websiteurl text,
	logourl text,
	country text,
	collectedfrom text REFERENCES dsm_services(id),
	optional1 text,
	optional2 text,
	dateofcollection date DEFAULT now() NOT NULL,
	provenanceaction text DEFAULT 'UNKNOWN',
	ec_legalperson boolean DEFAULT false,
	ec_enterprise boolean DEFAULT false,
	ec_legalbody boolean DEFAULT false,
	ec_nonprofit boolean DEFAULT false,
	ec_researchorganization boolean DEFAULT false,
	ec_highereducation boolean DEFAULT false,
	ec_internationalorganizationeurinterests boolean DEFAULT false,
	ec_internationalorganization boolean DEFAULT false,
	ec_smevalidated boolean DEFAULT false,
	ec_nutscode boolean DEFAULT false,
	lastupdate date DEFAULT now() NOT NULL,
	trust double precision DEFAULT 0.9
);

CREATE TABLE dsm_service_organization (
    service text NOT NULL REFERENCES dsm_services(id) ON UPDATE CASCADE,
    organization text NOT NULL REFERENCES dsm_organizations(id) ON DELETE CASCADE,
    dedup_inferred boolean DEFAULT false NOT NULL,
    PRIMARY KEY (service, organization)
);

CREATE TABLE dsm_pid_systems (
	service text NOT NULL REFERENCES dsm_services(id),
	type text NOT NULL,
	scheme text NOT NULL,
	PRIMARY KEY (service, type, scheme)
);

CREATE TABLE dsm_identities (
	pid text NOT NULL PRIMARY KEY,
	issuertype text
);

CREATE TABLE dsm_servicepids (
    service text NOT NULL REFERENCES dsm_services(id),
    pid text NOT NULL REFERENCES dsm_identities(pid),
    PRIMARY KEY (service, pid)
);

CREATE VIEW dsm_datasources_view AS SELECT 
	ds.id,
	ds.officialname AS name,
	ds.englishname AS other_name,
	ds.namespaceprefix AS nsprefix,
	ds.websiteurl AS url,
	ds.consenttermsofuse,
	ds.fulltextdownload,
	ds.eosc_datasource_type AS type,
	scf.officialname AS collectedfrom,
	json_agg(DISTINCT jsonb_build_object('name', o.legalname, 'country', o.country)) AS organizations,
	json_agg(DISTINCT jsonb_build_object('id', a.id, 'protocol', a.protocol, 'compliance', COALESCE(a.compatibility_override, a.compatibility), 'active', a.active, 'aggrDate', COALESCE((a.last_aggregation_date)::text, (a.last_download_date)::text, ''::text), 'aggrTotal', COALESCE(a.last_aggregation_total, a.last_download_total, 0))) AS apis,
	array_agg(DISTINCT o.country) AS hidden_countries,
	array_agg(DISTINCT a.protocol) AS hidden_protocols,
	array_agg(DISTINCT COALESCE(a.compatibility_override, a.compatibility)) AS hidden_compliances,
	array_agg(DISTINCT a.active) AS hidden_actives
FROM 
	dsm_services ds
	LEFT JOIN dsm_api a ON (a.service = ds.id)
	LEFT JOIN dsm_service_organization dsorg ON (ds.id = dsorg.service)
	LEFT JOIN dsm_organizations o ON (dsorg.organization = o.id)
	LEFT JOIN dsm_services scf ON (ds.collectedfrom = scf.id)
WHERE (ds.dedup_main_service = true) AND (ds.eosc_type = 'Data Source')
GROUP BY 
	ds.id,
	ds.officialname,
	ds.namespaceprefix,
	ds.websiteurl,
	ds.consenttermsofuse,
	ds.fulltextdownload,
	ds.eosc_datasource_type,
	scf.officialname;

CREATE VIEW dsm_datasource_api_view AS SELECT 
	row_number() OVER (ORDER BY a.id) AS rowid,
	s.id,
	s.officialname,
	s.englishname,
	s.websiteurl,
	s.contactemail,
	s.collectedfrom,
	s.eosc_datasource_type,
	s.platform,
	s.registeredby,
	s.managed,
	a.protocol,
	a.contentdescription,
	a.active,
	a.removable,
	a.compatibility,
	a.baseurl
FROM 
	dsm_services s
	LEFT JOIN dsm_api a ON (s.id = a.service);

-- END SERVICES 

COMMIT;
