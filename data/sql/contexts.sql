-- TODO: This variable should be obtained from the system ENV
\set DB dnet_contexts
CREATE DATABASE :DB;
\c :DB

BEGIN;

CREATE TABLE contexts (
	id     text PRIMARY KEY,
	label  text NOT NULL,
	type   text NOT NULL,
	params jsonb
);

CREATE TABLE context_categories (
	id     text    NOT NULL PRIMARY KEY,
	parent text    NOT NULL REFERENCES contexts(id) ON UPDATE CASCADE ON DELETE CASCADE,
	label  text    NOT NULL,
	claim  boolean NOT NULL,
	params jsonb
);

CREATE TABLE context_cat_concepts_lvl_0 (
	id     text    NOT NULL PRIMARY KEY,
	parent text    NOT NULL REFERENCES context_categories(id) ON UPDATE CASCADE ON DELETE CASCADE,
	label  text    NOT NULL,
	claim  boolean NOT NULL,
	params jsonb
);

CREATE TABLE context_cat_concepts_lvl_1 (
	id     text    NOT NULL PRIMARY KEY,
	parent text    NOT NULL REFERENCES context_cat_concepts_lvl_0(id) ON UPDATE CASCADE ON DELETE CASCADE,
	label  text    NOT NULL,
	claim  boolean NOT NULL,
	params jsonb
);

CREATE TABLE context_cat_concepts_lvl_2 (
	id     text    NOT NULL PRIMARY KEY,
	parent text    NOT NULL REFERENCES context_cat_concepts_lvl_1(id) ON UPDATE CASCADE ON DELETE CASCADE,
	label  text    NOT NULL,
	claim  boolean NOT NULL,
	params jsonb
);

CREATE INDEX ON context_categories         (parent);
CREATE INDEX ON context_cat_concepts_lvl_0 (parent);
CREATE INDEX ON context_cat_concepts_lvl_1 (parent);
CREATE INDEX ON context_cat_concepts_lvl_2 (parent);

COMMIT;
