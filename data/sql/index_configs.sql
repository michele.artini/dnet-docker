-- TODO: This variable should be obtained from the system ENV
\set DB dnet_index_configs
CREATE DATABASE :DB;
\c :DB

BEGIN;

CREATE TABLE index_configurations (
        id              text PRIMARY KEY,
        solr_config     text NOT NULL,
	description     text,
	n_shards        int NOT NULL DEFAULT 1,
	n_replicas      int NOT NULL DEFAULT 1
);

CREATE TABLE index_fields (
       idx_id           text NOT NULL,
       name             text NOT NULL,
       constant         text,
       xpath            text,
       result           boolean NOT NULL DEFAULT true,
       PRIMARY KEY (idx_id, name)
);

COMMIT;
