
-- TODO: This variable should be obtained from the system ENV
\set DB dnet_mdstores
CREATE DATABASE :DB;
\c :DB

BEGIN;

CREATE TABLE mdstores (
	id              text PRIMARY KEY,
	format          text NOT NULL,
	layout          text NOT NULL,
	interpretation  text NOT NULL,
	type            text NOT NULL,
	datasource_name text,
	datasource_id   text,
	api_id          text,
	creation_date   timestamp NOT NULL DEFAULT now(),
	params	        jsonb
);

CREATE TABLE mdstore_versions (
	id              text PRIMARY KEY,
	mdstore         text      NOT NULL REFERENCES mdstores(id),
	writing         boolean   NOT NULL,
	readcount       int       NOT NULL DEFAULT 0,
	lastupdate      timestamp NOT NULL DEFAULT now(),
	size            bigint    NOT NULL DEFAULT 0,
	params	        jsonb
);

CREATE TABLE mdstore_current_versions (
	mdstore         text PRIMARY KEY REFERENCES mdstores(id),
	current_version text NOT NULL    REFERENCES mdstore_versions(id)
);

CREATE VIEW mdstores_with_info AS SELECT
	md.id               AS id,
	md.format           AS format,
	md.layout           AS layout,
	md.type             AS type,
	md.interpretation   AS interpretation,
	md.datasource_name  AS datasource_name,
	md.datasource_id    AS datasource_id,
	md.api_id           AS api_id, 
	md.params           AS params,
	md.creation_date    as creation_date,
	cv.current_version  AS current_version,
	v1.lastupdate       AS lastupdate,
	v1.size             AS size,
	count(v2.id)        AS n_versions
FROM
	mdstores md
	LEFT OUTER JOIN mdstore_current_versions cv ON (md.id = cv.mdstore)
	LEFT OUTER JOIN mdstore_versions         v1 ON (cv.current_version = v1.id)
	LEFT OUTER JOIN mdstore_versions         v2 ON (md.id = v2.mdstore)
GROUP BY md.id,
	md.format,
	md.layout,
	md.interpretation,
	md.type,
	md.datasource_name,
	md.datasource_id,
	md.params,
	md.creation_date,
	md.api_id, 
	cv.current_version,
	v1.lastupdate,
	v1.size;

	COMMIT;
