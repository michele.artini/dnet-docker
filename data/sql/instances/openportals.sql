---------
-- DSM --
---------

\c dnet_dsm
BEGIN;

-- Repositories
INSERT INTO dsm_services (id, namespaceprefix, officialname) VALUES ('open_portals::people' , 'people______', 'CNR ExploRA' );
INSERT INTO dsm_services (id, namespaceprefix, officialname) VALUES ('open_portals::puma' , 'puma________', 'PUblication MAnagement (IEI and CNUCE)');
UPDATE dsm_services SET englishname = officialname;
UPDATE dsm_services SET	contactemail = 'michele.artini@isti.cnr.it';
UPDATE dsm_services SET managed = true;
UPDATE dsm_services SET	eosc_type = 'Data Source';
UPDATE dsm_services SET eosc_datasource_type = 'Repository';

-- CNR organizations
INSERT INTO dsm_organizations(id, legalshortname, legalname, websiteurl, country) VALUES ('open_portals::cnr', 'CNR', 'Consiglio Nazionale delle Ricerche', 'http://www.cnr.it', 'IT');
INSERT INTO dsm_service_organization(service, organization) VALUES ('open_portals::people' ,'open_portals::cnr');

-- OAI APIs
INSERT INTO dsm_api(id, service) VALUES ('open_portals::people::isti::oai', 'open_portals::people');
INSERT INTO dsm_api(id, service) VALUES	('open_portals::people::ispc::oai', 'open_portals::people');
INSERT INTO dsm_api(id, service) VALUES	('open_portals::people::ino::oai', 'open_portals::people');
INSERT INTO dsm_api(id, service) VALUES	('open_portals::people::ismar::oai', 'open_portals::people');
UPDATE dsm_api SET protocol = 'oai';
UPDATE dsm_api SET metadata_identifier_path = '//*[local-name()=''header'']/*[local-name()=''identifier'']';
UPDATE dsm_api SET baseurl = 'https://publications.cnr.it/api/v1/oai-pmh';

INSERT INTO dsm_apiparams(api, param, value) VALUES ('open_portals::people::isti::oai','format','oai_dc');
INSERT INTO dsm_apiparams(api, param, value) VALUES ('open_portals::people::isti::oai','set','CDS074');
INSERT INTO dsm_apiparams(api, param, value) VALUES ('open_portals::people::ispc::oai','format','oai_dc');
INSERT INTO dsm_apiparams(api, param, value) VALUES ('open_portals::people::ispc::oai','set','CDS123');
INSERT INTO dsm_apiparams(api, param, value) VALUES ('open_portals::people::ino::oai','format','oai_dc');
INSERT INTO dsm_apiparams(api, param, value) VALUES ('open_portals::people::ino::oai','set','CDS111');
INSERT INTO dsm_apiparams(api, param, value) VALUES ('open_portals::people::ismar::oai','format','oai_dc');
INSERT INTO dsm_apiparams(api, param, value) VALUES ('open_portals::people::ismar::oai','set','CDS080');

-- FILESYSTEM API for IEI and CNUCE
INSERT INTO dsm_api(id, service, protocol, baseurl, metadata_identifier_path) VALUES ('open_portals::puma::filesystem', 'open_portals::puma', 'filesystem', 'file:///var/lib/dnet/puma_import', '//*[local-name()=''localidentifier'']');

INSERT INTO dsm_apiparams(api, param, value) VALUES ('open_portals::puma::filesystem','extensions','xml');

COMMIT;

------------------
-- Vocabularies --
------------------

\c dnet_vocabularies;
BEGIN;
INSERT INTO vocabularies (id, name, description) VALUES ('isti:groups', 'isti:groups', 'Laboratori, servizi e gruppi di lavoro (ISTI-CNR)');
INSERT INTO vocabularies (id, name, description) VALUES	('iop:typologies', 'iop:typologies', 'Tipologie di documenti');


--INSERT INTO vocabulary_terms (vocabulary, code, name, synonyms) VALUES ()



COMMIT;

