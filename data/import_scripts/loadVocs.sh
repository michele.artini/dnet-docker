#!/bin/sh

VOC_URL="http://localhost:8888/proxy/byType/vocabulary_manager/api/import/vocabulary"
find ./voc_profiles/ -name "*.xml" -exec curl -X POST "$VOC_URL" -H "accept: */*" -H "Content-Type: text/plain; charset=UTF-8" --data-binary @{} \;


#find ./DedupConfigurationDSResources/ -name "*.xml" -exec curl -X POST "http://localhost:8280/api/import/resource" -H "accept: */*" -H "Content-Type: text/plain; charset=UTF-8" --data-binary @{} \;
