package eu.dnetlib.manager.wf.model;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.domain.wfs.conf.WfConfiguration;

class WorkflowConfigurationTest {

	private final ObjectMapper mapper = new ObjectMapper();

	@BeforeEach
	private void setUp() throws Exception {}

	@Test
	final void test() throws IOException {
		final WfConfiguration conf =
				mapper.readValue(IOUtils.toString(getClass().getResourceAsStream("test-conf.json"), "UTF-8"), WfConfiguration.class);

		System.out.println(conf.getName());
		assertNotNull(conf.getName());
	}

}
