package eu.dnetlib.manager.wf.workflows.graph;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.function.Function;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.expression.MapAccessor;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import eu.dnetlib.wfs.procs.RuntimeEnv;

public class GraphLoaderTest {

	private RuntimeEnv env;

	@BeforeEach
	void setUp() throws Exception {
		this.env = new RuntimeEnv();
		this.env.setAttribute("author", "Michele Artini");
		this.env.setAttribute("age", 47);
	}

	@Test
	final void testExpressions() {
		assertTrue(evalFunction("age == 47").apply(this.env));
		assertTrue(evalFunction("age > 40").apply(this.env));
		assertTrue(evalFunction("author == 'Michele Artini'").apply(this.env));
		assertTrue(evalFunction("age == 47 && author == 'Michele Artini'").apply(this.env));
		assertTrue(evalFunction("age == 47 || author == 'Michele Artini'").apply(this.env));
		assertTrue(evalFunction("age == 47 || author == 'Claudio Atzori'").apply(this.env));
		assertTrue(evalFunction("age == 22 || author == 'Michele Artini'").apply(this.env));
		assertFalse(evalFunction("age != 47").apply(this.env));
		assertFalse(evalFunction("age < 40").apply(this.env));
		assertFalse(evalFunction("author != 'Michele Artini'").apply(this.env));
		assertFalse(evalFunction("age == 47 && author == 'Claudio Atzori'").apply(this.env));
	}

	private Function<RuntimeEnv, Boolean> evalFunction(final String f) {
		return env -> {
			final ExpressionParser parser = new SpelExpressionParser();

			final StandardEvaluationContext context = new StandardEvaluationContext(env.getAttributes());
			context.addPropertyAccessor(new MapAccessor());

			return parser.parseExpression(f).getValue(context, Boolean.class);
		};
	}

}
