package eu.dnetlib.manager.wf.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.domain.wfs.graph.Arc;
import eu.dnetlib.domain.wfs.graph.Node;
import eu.dnetlib.domain.wfs.graph.NodeInputParam;
import eu.dnetlib.domain.wfs.templates.WfParam;
import eu.dnetlib.domain.wfs.templates.WfParamType;
import eu.dnetlib.domain.wfs.templates.WfTemplate;

class WorkflowTemplateTest {

	private final ObjectMapper mapper = new ObjectMapper();

	private WfTemplate tmpl;

	private WfParam p1;
	private WfParam p2;
	private WfParam p3;

	private Node n1;
	private Node n2;
	private Node n3;

	private NodeInputParam np1;

	private Arc a12;
	private Arc a13;
	private Arc a23;
	private Arc a3succ;

	@BeforeEach
	private void setUp() throws Exception {

		this.a12 = new Arc("N2");
		this.a13 = new Arc("N3");
		this.a23 = new Arc("N3");
		this.a3succ = new Arc("success");

		this.p1 = new WfParam();
		this.p1.setName("Name");

		this.p2 = new WfParam();
		this.p2.setName("Age");
		this.p2.setType(WfParamType.NUMBER);

		this.p3 = new WfParam();
		this.p3.setName("Birthday");
		this.p3.setDescription("Your Birthday");
		this.p3.setType(WfParamType.DATE);
		this.p3.setDefaultValue("1900-01-01");
		this.p3.setRequired(false);

		this.np1 = NodeInputParam.newRefParam("Age", "Age");
		this.np1 = NodeInputParam.newSimpleParam("Test", "test");

		this.n1 = new Node("N1", true);
		this.n1.setArcs(Arrays.asList(this.a12, this.a13));

		this.n2 = new Node("N2", "T2");
		this.n2.setArcs(Arrays.asList(this.a23));
		this.n2.setInput(Arrays.asList(this.np1));

		this.n3 = new Node("N3");
		this.n2.setArcs(Arrays.asList(this.a3succ));

		this.tmpl = new WfTemplate();
		this.tmpl.setParameters(Arrays.asList(this.p1, this.p2, this.p3));
		this.tmpl.setGraph(Arrays.asList(this.n1, this.n2, this.n3));
	}

	@Test
	final void test() throws JsonProcessingException {
		final String json = this.mapper.writeValueAsString(this.tmpl);

		System.out.println(json);

		final WfTemplate tmpl2 = this.mapper.readValue(json, WfTemplate.class);

		assertEquals(this.tmpl.getGraph().size(), tmpl2.getGraph().size());
		assertEquals(this.tmpl.getParameters().size(), tmpl2.getParameters().size());
	}

}
