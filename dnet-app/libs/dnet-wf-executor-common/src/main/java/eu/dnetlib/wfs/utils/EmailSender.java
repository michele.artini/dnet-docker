package eu.dnetlib.wfs.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.EmailClient;
import eu.dnetlib.domain.wfs.jobs.JobStatus;
import eu.dnetlib.domain.wfs.subscriptions.NotificationCondition;
import eu.dnetlib.wfs.procs.WorkflowProcess;
import eu.dnetlib.wfs.repository.WfSubscriptionRepository;

@Service
public class EmailSender {

	private static final Log log = LogFactory.getLog(EmailSender.class);

	@Value("${wf.manager.email.address}")
	private String fromMail;

	@Value("${wf.manager.email.name}")
	private String fromName;

	@Autowired
	private WfSubscriptionRepository wfSubscriptionRepository;

	@Autowired
	private DnetServiceClientFactory clientFactory;

	public void sendMails(final String wfConfId, final WorkflowProcess proc) {

		this.wfSubscriptionRepository.findByWfConfigurationId(wfConfId).forEach(s -> {
			if ((s.getCondition() == NotificationCondition.ALWAYS) ||
					((s.getCondition() == NotificationCondition.ONLY_FAILED) && (proc.getJobDetails().getStatus() == JobStatus.failure)) ||
					((s.getCondition() == NotificationCondition.ONLY_SUCCESS) && (proc.getJobDetails().getStatus() == JobStatus.success))) {
				try {
					final Map<String, Object> params = new HashMap<>();

					this.clientFactory.getClient(EmailClient.class).sendStoredMail(s.getEmail(), this.fromMail, this.fromName, s.getMessageId(), params);

				} catch (final Exception e) {
					log.error("Error sending mail to " + s.getEmail(), e);
				}
			}

		});
	}
}
