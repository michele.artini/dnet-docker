package eu.dnetlib.wfs.nodes.store;

import org.springframework.beans.factory.annotation.Autowired;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.MDStoreManagerClient;
import eu.dnetlib.wfs.annotations.WfInputParam;
import eu.dnetlib.wfs.annotations.WfNode;
import eu.dnetlib.wfs.nodes.ProcessNode;

@WfNode("deleteMdStore")
public class DeleteMdStoreNode extends ProcessNode {

	@WfInputParam
	private String mdId;

	@Autowired
	private DnetServiceClientFactory clientFactory;

	@Override
	protected void execute() throws Exception {
		final MDStoreManagerClient client = this.clientFactory.getClient(MDStoreManagerClient.class);

		if (client.existsMdSstore(this.mdId)) {
			client.deleteMdStore(this.mdId);
		}
	}

}
