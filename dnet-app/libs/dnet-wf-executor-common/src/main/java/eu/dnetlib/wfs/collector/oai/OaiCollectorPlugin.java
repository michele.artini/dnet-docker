package eu.dnetlib.wfs.collector.oai;

import java.time.LocalDateTime;

import java.util.Iterator;
import java.util.Map;
import java.util.stream.Stream;

import eu.dnetlib.common.oai.OaiIterator;
import eu.dnetlib.utils.DnetStreamSupport;
import eu.dnetlib.wfs.annotations.CollectorPlugin;
import eu.dnetlib.wfs.collector.DnetCollectorPlugin;

@CollectorPlugin("oai")
public class OaiCollectorPlugin implements DnetCollectorPlugin {

	@Override
	public Stream<String> collect(final String baseUrl, final Map<String, String> apiParams, final LocalDateTime from, final LocalDateTime until)
			throws Exception {
		final Iterator<String> iter = new OaiIterator(baseUrl, apiParams.getOrDefault("format", "oai_dc"), apiParams.get("set"), from, until);
		return DnetStreamSupport.stream(iter);
	}

}
