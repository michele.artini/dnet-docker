package eu.dnetlib.wfs.nodes;

public final class DefaultJobNode extends ProcessNode {

	public DefaultJobNode(final String name) {
		setNodeName(name);
	}

	@Override
	public final void execute() {}

}
