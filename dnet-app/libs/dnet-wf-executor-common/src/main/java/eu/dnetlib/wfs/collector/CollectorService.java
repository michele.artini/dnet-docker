package eu.dnetlib.wfs.collector;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.domain.dsm.ApiParam;
import eu.dnetlib.errors.DnetRuntimeException;
import eu.dnetlib.wfs.annotations.CollectorPlugin;

@Service
public class CollectorService {

	@Autowired
	private List<DnetCollectorPlugin> plugins;

	public Stream<String> collect(final Api api) {
		try {
			return findPlugin(api.getProtocol()).collect(api.getBaseurl(), toMap(api.getApiParams()), null, null);
		} catch (final Exception e) {
			throw new DnetRuntimeException("Error collecting api", e);
		}
	}

	public Stream<String> collect(final Api api, final LocalDateTime from, final LocalDateTime until) {
		try {
			return findPlugin(api.getProtocol()).collect(api.getBaseurl(), toMap(api.getApiParams()), from, until);
		} catch (final Exception e) {
			throw new DnetRuntimeException("Error collecting api", e);
		}
	}

	private DnetCollectorPlugin findPlugin(final String protocol) {
		return plugins.stream()
				.filter(p -> p.getClass().isAnnotationPresent(CollectorPlugin.class))
				.filter(p -> p.getClass().getAnnotation(CollectorPlugin.class).value().equalsIgnoreCase(protocol))
				.findFirst()
				.orElseThrow();
	}

	private Map<String, String> toMap(final Set<ApiParam> apiParams) {
		return apiParams.stream().collect(Collectors.toMap(ApiParam::getParam, ApiParam::getValue));
	}

}
