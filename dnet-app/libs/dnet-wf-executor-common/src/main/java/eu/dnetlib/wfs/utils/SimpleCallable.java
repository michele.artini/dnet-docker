package eu.dnetlib.wfs.utils;

@FunctionalInterface
public interface SimpleCallable {

	void call() throws Exception;
}
