package eu.dnetlib.wfs.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import eu.dnetlib.common.controller.DnetRestController;
import eu.dnetlib.domain.wfs.jobs.WfRunningJob;
import eu.dnetlib.wfs.procs.ProcessEngine;
import eu.dnetlib.wfs.procs.ProcessRegistry;

public abstract class AbstractWfExecutorApiController extends DnetRestController {

	@Autowired
	private ProcessEngine engine;

	@Autowired
	private ProcessRegistry registry;

	@GetMapping("/process/{id}")
	public WfRunningJob findProcess(@PathVariable final String id) throws Exception {
		return this.registry.findProcess(id).getJobDetails();
	}

	@GetMapping("/procs")
	public Set<String> findProcess() throws Exception {
		return this.registry.listProcIds();
	}

	@DeleteMapping("/process/{id}")
	public void killProcess(@PathVariable final String id) throws Exception {
		this.engine.killProcess(id);
	}

	@GetMapping("/node-types")
	public List<String> listAvailableNodes() {
		return this.engine.getValidNodeTypes().stream().sorted().toList();
	}
}
