package eu.dnetlib.wfs.procs;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.lang3.math.NumberUtils;

import eu.dnetlib.domain.wfs.jobs.JobStatus;
import eu.dnetlib.domain.wfs.jobs.WfRunningJob;

/**
 * Created by michele on 19/11/15.
 */
public class WorkflowProcess implements Comparable<WorkflowProcess> {

	public enum StartMode {
		AUTO,
		MANUAL,
		DISABLED
	}

	private final String id;
	private final WfRunningJob jobDetails;
	private List<RuntimeEnv> envs = new CopyOnWriteArrayList<>();
	private final Map<String, List<RuntimeEnv>> pausedJoinNodeEnvs = new HashMap<>();
	private final Map<String, String> outputParams = new HashMap<>();

	private Throwable error;

	public WorkflowProcess(final WfRunningJob jobDetails) {
		this.id = jobDetails.getProcessId();
		this.jobDetails = jobDetails;
	}

	public String getId() {
		return this.id;
	}

	public WfRunningJob getJobDetails() {
		return this.jobDetails;
	}

	public Map<String, List<RuntimeEnv>> getPausedJoinNodeEnvs() {
		return this.pausedJoinNodeEnvs;
	}

	public List<RuntimeEnv> getEnvs() {
		return this.envs;
	}

	public void kill() {
		// TODO (MEDIUM PRIORITY)
		this.jobDetails.setStatus(JobStatus.killed);
	}

	public boolean isTerminated() {
		return (this.error != null) || switch (this.jobDetails.getStatus()) {
		case success, failure, killed -> true;
		default -> false;
		};
	}

	@Override
	public String toString() {
		return String.format("[process id='%s']", this.id);
	}

	@Override
	public int compareTo(final WorkflowProcess other) {
		final int n1 = this.jobDetails.getPriority();
		final int n2 = other.jobDetails.getPriority();
		return NumberUtils.compare(n1, n2);
	}

	public Throwable getError() {
		return this.error;
	}

	public void setError(final Throwable error) {
		this.error = error;
	}

	public Map<String, String> getOutputParams() {
		return this.outputParams;
	}

	public RuntimeEnv newEnv(final Map<String, Object> attrs) {
		final RuntimeEnv env = new RuntimeEnv();
		env.addAttributes(attrs);
		this.envs.add(env);
		return env;
	}

	public void complete(final RuntimeEnv env, final Throwable err) {
		final LocalDateTime now = LocalDateTime.now();
		this.jobDetails.setLastUpdate(now);
		this.jobDetails.setEndDate(now);
		this.jobDetails.setStatus(err != null ? JobStatus.failure : JobStatus.success);
		this.error = err; // NULL value is permitted
	}

	public void setEnvs(final List<RuntimeEnv> envs) {
		this.envs = envs;
	}

}
