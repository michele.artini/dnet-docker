package eu.dnetlib.wfs.collector.filesystem;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Class enabling lazy & recursive iteration of a filesystem tree. The iterator iterates over file paths.
 *
 * @author Andrea
 */
public class FileSystemIterator implements Iterator<String> {

	/**
	 * The logger
	 */
	private static final Log log = LogFactory.getLog(FileSystemIterator.class);

	private final Set<String> extensions;
	private Iterator<Path> pathIterator;
	private String current;

	public FileSystemIterator(final String baseDir, final String extensions) {
		this.extensions = new HashSet<>(Arrays.asList(extensions.split(",")));
		try {
			this.pathIterator = Files.newDirectoryStream(Paths.get(baseDir)).iterator();
			this.current = walkTillNext();
		} catch (final IOException e) {
			log.error("Cannot initialize File System Iterator. Is this path correct? " + baseDir);
			throw new RuntimeException("Filesystem collection error.", e);
		}
	}

	@Override
	public boolean hasNext() {
		return current != null;
	}

	@Override
	public synchronized String next() {
		final String pivot = new String(current);
		current = walkTillNext();
		log.debug("Returning: " + pivot);
		return pivot;
	}

	@Override
	public void remove() {}

	/**
	 * Walk the filesystem recursively until it finds a candidate. Strategies: a) For any directory found during the walk, an iterator is
	 * built and concat to the main one; b) Any file is checked against admitted extensions
	 *
	 * @return the next element to be returned by next call of this.next()
	 */
	private synchronized String walkTillNext() {
		while (pathIterator.hasNext()) {
			final Path nextFilePath = pathIterator.next();
			if (Files.isDirectory(nextFilePath)) {
				// concat
				try {
					pathIterator = IteratorUtils.chainedIterator(pathIterator, Files.newDirectoryStream(nextFilePath).iterator());
					log.debug("Adding folder iterator: " + nextFilePath.toString());
				} catch (final IOException e) {
					log.error("Cannot create folder iterator! Is this path correct? " + nextFilePath.toString());
					return null;
				}
			} else {
				if (extensions.contains(FilenameUtils.getExtension(nextFilePath.toString()))) {
					log.debug("Returning: " + nextFilePath.toString());
					return nextFilePath.toString();
				}
			}
		}
		return null;
	}
}
