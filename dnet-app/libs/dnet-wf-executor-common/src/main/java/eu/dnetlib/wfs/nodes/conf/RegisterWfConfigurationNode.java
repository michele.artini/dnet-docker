package eu.dnetlib.wfs.nodes.conf;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.SimpleResourceClient;
import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.domain.dsm.Datasource;
import eu.dnetlib.domain.wfs.WorkflowsConstants;
import eu.dnetlib.domain.wfs.conf.WfConfiguration;
import eu.dnetlib.domain.wfs.templates.WfTemplate;
import eu.dnetlib.wfs.annotations.WfInputParam;
import eu.dnetlib.wfs.annotations.WfNode;
import eu.dnetlib.wfs.annotations.WfOutputParam;
import eu.dnetlib.wfs.nodes.ProcessNode;
import eu.dnetlib.wfs.repository.WfConfigurationRepository;
import eu.dnetlib.wfs.utils.WfConfigurationUtils;

@WfNode("registerWfConfiguration")
public class RegisterWfConfigurationNode extends ProcessNode {

	private static final int DEFAULT_AGGR_PRIORITY = 75;

	@WfInputParam
	private String workflows;

	@WfInputParam
	private String destroyWf;

	@WfInputParam
	private Datasource ds;

	@WfInputParam
	private Api api;

	@WfInputParam
	private String nativeMdStoreId;

	@WfInputParam
	private String cleanedMdStoreId;

	@WfOutputParam
	private String wfConfId;

	@Autowired
	private WfConfigurationRepository wfConfigurationRepository;

	@Autowired
	private DnetServiceClientFactory clientFactory;

	@Override
	protected void execute() throws Exception {

		this.wfConfId = "wf-aggr-" + UUID.randomUUID();

		final List<String> wfList = Arrays.stream(StringUtils.split(this.workflows, ","))
				.map(String::trim)
				.filter(StringUtils::isNotBlank)
				.toList();

		final WfConfiguration conf = new WfConfiguration();
		conf.setId(this.wfConfId);
		conf.setSection(WorkflowsConstants.AGGREGATION_WF_CONFS_SECTION);
		conf.setParentId(null);

		conf.setName(String.format("Aggregation of '%s' (api: %s)", this.ds.getOfficialname(), this.api.getId()));
		conf.setDetails(new HashMap<>());

		conf.setDsId(this.ds.getId());
		conf.setDsName(this.ds.getOfficialname());
		conf.setApiId(this.api.getId());

		conf.setWorkflows(wfList);
		conf.setDestroyWf(this.destroyWf);
		conf.setEnabled(true);
		conf.setPriority(DEFAULT_AGGR_PRIORITY);

		conf.setSchedulingEnabled(false);
		conf.setCronExpression("0 0 0 ? * *");
		conf.setCronMinInterval(10080);

		conf.setSystemParams(prepareSystemParams());

		final SimpleResourceClient client = this.clientFactory.getClient(SimpleResourceClient.class);

		final List<ImmutablePair<String, WfTemplate>> wfTemplates = wfList.stream()
				.map(wf -> new ImmutablePair<>(wf, client.findResourceContent(SimpleResourceClient.ResourceType.wf_template, wf, WfTemplate.class)))
				.toList();

		conf.setConfigured(WfConfigurationUtils.isConfigured(wfTemplates, conf));

		this.wfConfigurationRepository.save(conf);

	}

	private Map<String, Object> prepareSystemParams() {
		final Map<String, Object> map = new LinkedHashMap<>();
		map.put("nativeMdStoreId", this.nativeMdStoreId);
		map.put("cleanedMdStoreId", this.cleanedMdStoreId);
		return map;
	}

}
