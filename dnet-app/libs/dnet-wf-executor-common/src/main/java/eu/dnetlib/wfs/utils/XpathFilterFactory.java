package eu.dnetlib.wfs.utils;

import java.util.function.Predicate;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;

public class XpathFilterFactory {

	public static Predicate<Document> createFilter(final String xpath) {
		if (StringUtils.isBlank(xpath)) { return doc -> true; }

		return doc -> !doc.selectNodes(xpath).isEmpty();
	}

}
