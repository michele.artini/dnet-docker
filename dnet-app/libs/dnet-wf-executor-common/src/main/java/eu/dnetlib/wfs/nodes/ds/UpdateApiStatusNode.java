package eu.dnetlib.wfs.nodes.ds;

import org.springframework.beans.factory.annotation.Autowired;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.DsmClient;
import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.domain.dsm.Datasource;
import eu.dnetlib.wfs.annotations.WfInputParam;
import eu.dnetlib.wfs.annotations.WfNode;
import eu.dnetlib.wfs.nodes.ProcessNode;

@WfNode("updateApiStatus")
public class UpdateApiStatusNode extends ProcessNode {

	@WfInputParam
	private Datasource ds;

	@WfInputParam
	private Api api;

	@Autowired
	private DnetServiceClientFactory clientFactory;

	@Override
	protected void execute() throws Exception {
		final DsmClient dsm = clientFactory.getClient(DsmClient.class);
		dsm.setManaged(ds, true);
		dsm.setApiActive(api, true);
	}

}
