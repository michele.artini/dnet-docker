package eu.dnetlib.wfs.nodes;

public class SuccessNode extends ProcessNode {

	public SuccessNode() {
		setNodeName("success");
	}

	@Override
	public final void execute() {}

}
