package eu.dnetlib.wfs.nodes.conf;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import eu.dnetlib.errors.WorkflowManagerException;
import eu.dnetlib.wfs.annotations.WfNode;
import eu.dnetlib.wfs.nodes.ProcessNode;
import eu.dnetlib.wfs.repository.WfConfigurationRepository;

@WfNode("deleteWfConfiguration")
public class DeleteWfConfigurationNode extends ProcessNode {

	@Autowired
	private WfConfigurationRepository wfConfigurationRepository;

	@Override
	protected void execute() throws Exception {

		final String wfConfId = getProcess().getJobDetails().getWfConfId();

		if (StringUtils.isBlank(wfConfId)) { throw new WorkflowManagerException("wfConfId is null"); }

		this.wfConfigurationRepository.deleteById(wfConfId);
	}

}
