package eu.dnetlib.wfs.collector;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.stream.Stream;

public interface DnetCollectorPlugin {

	Stream<String> collect(String baseUrl, Map<String, String> apiParams, LocalDateTime from, LocalDateTime until) throws Exception;

}
