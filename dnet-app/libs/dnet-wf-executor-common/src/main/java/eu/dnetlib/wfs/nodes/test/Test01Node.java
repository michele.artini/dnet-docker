package eu.dnetlib.wfs.nodes.test;

import eu.dnetlib.wfs.annotations.WfInputParam;
import eu.dnetlib.wfs.annotations.WfNode;
import eu.dnetlib.wfs.nodes.ProcessNode;

@WfNode("test01")
public class Test01Node extends ProcessNode {

	@WfInputParam
	private long times;

	@Override
	protected void execute() throws Exception {
		System.out.println("************************");

		for (long i = 0; i < this.times; i++) {
			System.out.println("* " + i);
			updateProgressMessage(i + "/" + this.times);
			Thread.sleep(2000);
		}

		System.out.println("************************");
	}

}
