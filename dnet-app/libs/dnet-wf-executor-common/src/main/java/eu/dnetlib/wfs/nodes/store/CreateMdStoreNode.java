package eu.dnetlib.wfs.nodes.store;

import org.springframework.beans.factory.annotation.Autowired;

import eu.dnetlib.common.clients.MDStoreManagerClient;
import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.domain.dsm.Datasource;
import eu.dnetlib.domain.mdstore.MDStoreType;
import eu.dnetlib.domain.mdstore.MDStoreWithInfo;
import eu.dnetlib.wfs.annotations.WfInputParam;
import eu.dnetlib.wfs.annotations.WfNode;
import eu.dnetlib.wfs.annotations.WfOutputParam;
import eu.dnetlib.wfs.nodes.ProcessNode;

@WfNode("createMdStore")
public class CreateMdStoreNode extends ProcessNode {

	@WfInputParam
	private String format;

	@WfInputParam
	private String layout;

	@WfInputParam
	private String interpretation;

	@WfInputParam
	private String backend;

	@WfInputParam
	private Datasource ds;

	@WfInputParam
	private Api api;

	@WfOutputParam
	private String mdId;

	@Autowired
	private DnetServiceClientFactory clientFactory;

	@Override
	protected void execute() throws Exception {
		final MDStoreWithInfo mdstore = clientFactory.getClient(MDStoreManagerClient.class)
				.createMDStore(format, layout, interpretation, MDStoreType.valueOf(backend), ds.getOfficialname(), ds.getId(), api.getId());

		mdId = mdstore.getId();
	}

}
