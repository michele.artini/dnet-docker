package eu.dnetlib.wfs.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.dnetlib.errors.WorkflowManagerException;
import eu.dnetlib.wfs.utils.ValidNodeValuesFetcher.DnetParamValue;

public abstract class ValidNodeValuesFetcher implements Function<Map<String, String>, List<DnetParamValue>> {

	private String name;

	private static final Log log = LogFactory.getLog(ValidNodeValuesFetcher.class);

	public class DnetParamValue implements Comparable<DnetParamValue> {

		private final String id;
		private final String name;

		public DnetParamValue(final String id, final String name) {
			this.id = id;
			this.name = name;
		}

		public String getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		@Override
		public int compareTo(final DnetParamValue o) {
			return getName().compareTo(o.getName());
		}
	}

	@Override
	final public List<DnetParamValue> apply(final Map<String, String> params) {
		try {
			return obtainValues(params);
		} catch (final Throwable e) {
			log.error("Error obtaing values", e);
			return new ArrayList<>();
		}
	}

	abstract protected List<DnetParamValue> obtainValues(Map<String, String> params) throws Exception;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	protected void verifyParams(final Map<String, String> params, final String... pnames) throws WorkflowManagerException {
		for (final String s : pnames) {
			if (!params.containsKey(s)) { throw new WorkflowManagerException("Parameter not found: " + s); }
		}
	}
}
