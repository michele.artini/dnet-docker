package eu.dnetlib.wfs.utils;

import java.time.LocalDateTime;
import java.util.function.Function;

import org.dom4j.Document;

import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.domain.dsm.Datasource;
import eu.dnetlib.domain.mdstore.records.MetadataRecord;
import eu.dnetlib.domain.mdstore.records.MetadataRecord.MetadataRecordEncoding;
import eu.dnetlib.domain.mdstore.records.Provenance;

public class MdBuilderFactory {

	public static Function<Document, MetadataRecord> createMdBuilder(final Datasource ds, final Api api) {

		final String dsId = ds.getId();
		final String dsName = ds.getOfficialname();
		final String nsPrefix = ds.getNamespaceprefix();
		final String xpath = api.getMetadataIdentifierPath();

		final Provenance provenance = new Provenance();
		provenance.setDatasourceId(dsId);
		provenance.setDatasourceName(dsName);
		provenance.setNsPrefix(nsPrefix);

		return doc -> {

			final String origId = doc.valueOf(xpath);

			// TODO (LOW PRIORITY) populate the xml record with missing oaf fields, see:

			// https://svn.driver.research-infrastructures.eu/driver/dnet45/modules/dnet-openaireplus-workflows/trunk/src/main/resources/eu/dnetlib/msro/openaireplus/workflows/repo-hi/xslt/openaireMdBuilder.xslt.st

			/*
			 * <xsl:template name="about"> <about> <provenance xmlns="http://www.openarchives.org/OAI/2.0/provenance"
			 * xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			 * xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/provenance http://www.openarchives.org/OAI/2.0/provenance.xsd">
			 * <originDescription harvestDate="{\$datestamp}" altered="true"> <baseURL>$baseurl$</baseURL> <identifier><xsl:value-of
			 * select="//*[local-name()='header']/*[local-name()='identifier']"/></identifier> <datestamp><xsl:value-of
			 * select="//*[local-name()='header']/*[local-name()='datestamp']"/></datestamp>
			 * <metadataNamespace>$metadatanamespace$</metadataNamespace> <xsl:copy-of
			 * select="//*[local-name()='provenance']/*[local-name() = 'originDescription']"/> </originDescription> </provenance>
			 * <oaf:datainfo> <oaf:inferred>$inferred$</oaf:inferred> <oaf:deletedbyinference>$deletedbyinference$</oaf:deletedbyinference>
			 * <oaf:trust>$trust$</oaf:trust> <oaf:inferenceprovenance>$inferenceprovenance$</oaf:inferenceprovenance> <oaf:provenanceaction
			 * schemename="dnet:provenanceActions" schemeid="dnet:provenanceActions" classname="$provenanceactionclassname$"
			 * classid="$provenanceactionclassid$"/> </oaf:datainfo> </about> </xsl:template>
			 */

			final MetadataRecord md = new MetadataRecord();

			md.setId(MetadataRecord.generateIdentifier(origId, nsPrefix));
			md.setOriginalId(origId);
			md.setBody(doc.asXML());
			md.setEncoding(MetadataRecordEncoding.XML);
			md.setDateOfCollection(LocalDateTime.now());
			md.setDateOfTransformation(null);
			md.setProvenance(provenance);

			return md;
		};
	}
}
