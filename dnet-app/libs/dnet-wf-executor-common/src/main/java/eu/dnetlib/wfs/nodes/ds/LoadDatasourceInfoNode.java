package eu.dnetlib.wfs.nodes.ds;

import org.springframework.beans.factory.annotation.Autowired;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.DsmClient;
import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.domain.dsm.Datasource;
import eu.dnetlib.wfs.annotations.WfNode;
import eu.dnetlib.wfs.annotations.WfOutputParam;
import eu.dnetlib.wfs.nodes.ProcessNode;

@WfNode("loadDatasourceInfo")
public class LoadDatasourceInfoNode extends ProcessNode {

	@WfOutputParam
	private Datasource ds;

	@WfOutputParam
	private Api api;

	@Autowired
	private DnetServiceClientFactory clientFactory;

	@Override
	protected void execute() throws Exception {
		final String dsId = getProcess().getJobDetails().getDsId();
		final String apiId = getProcess().getJobDetails().getApiId();

		final DsmClient dsm = this.clientFactory.getClient(DsmClient.class);

		this.ds = dsm.findDs(dsId);
		this.api = dsm.findApi(apiId);
	}

}
