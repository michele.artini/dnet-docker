package eu.dnetlib.wfs.nodes.index;

import org.springframework.beans.factory.annotation.Autowired;

import eu.dnetlib.common.index.solr.SolrService;
import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.wfs.annotations.WfInputParam;
import eu.dnetlib.wfs.annotations.WfNode;
import eu.dnetlib.wfs.nodes.ProcessNode;

@WfNode("deleteIndexRecords")
public class DeleteIndexRecordsNode extends ProcessNode {

	@WfInputParam
	private String indexConfigurationId;

	@WfInputParam
	private Api api;

	@Autowired
	private SolrService solrService;

	@Override
	protected void execute() throws Exception {
		solrService.deleteByQuery(indexConfigurationId, prepareQuery(api.getDatasource(), api.getId()));
	}

	private String prepareQuery(final String dsId, final String apiId) {
		// TODO (MEDIUM PRIORITY) write the query
		return null;
	}

}
