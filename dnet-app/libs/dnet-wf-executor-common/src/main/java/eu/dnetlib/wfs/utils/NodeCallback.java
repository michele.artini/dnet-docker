package eu.dnetlib.wfs.utils;

import eu.dnetlib.utils.DnetCallback;
import eu.dnetlib.wfs.procs.RuntimeEnv;

public interface NodeCallback extends DnetCallback<RuntimeEnv> {

}
