package eu.dnetlib.wfs.procs;

import java.io.Serializable;

public class ExecutionStatus implements Serializable {

	private static final long serialVersionUID = 9033354768530468698L;

	private final String processId;

	public ExecutionStatus(final String processId) {
		this.processId = processId;
	}

	public String getProcessId() {
		return processId;
	}

}
