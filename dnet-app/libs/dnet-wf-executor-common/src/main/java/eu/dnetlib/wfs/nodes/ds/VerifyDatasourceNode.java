package eu.dnetlib.wfs.nodes.ds;

import java.util.Arrays;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.domain.dsm.Datasource;
import eu.dnetlib.wfs.annotations.WfInputParam;
import eu.dnetlib.wfs.annotations.WfNode;
import eu.dnetlib.wfs.nodes.ProcessNode;

@WfNode("verifyDatasource")
public class VerifyDatasourceNode extends ProcessNode {

	@WfInputParam
	private String expectedEoscDsTypes;

	@WfInputParam
	private String expectedCompliances;

	@WfInputParam
	private Datasource ds;

	@WfInputParam
	private Api api;

	@Override
	protected void execute() throws Exception {
		final String type = ds.getEoscDatasourceType();
		final String compatibility = ObjectUtils.firstNonNull(api.getCompatibilityOverride(), api.getCompatibility());

		if (StringUtils.isNotBlank(expectedEoscDsTypes)) {
			Arrays.stream(expectedEoscDsTypes.split(","))
					.filter(StringUtils::isNotBlank)
					.filter(s -> s.equalsIgnoreCase(type))
					.findFirst()
					.orElseThrow(() -> new Exception("Invalid type: " + type));
		}

		if (StringUtils.isNotBlank(expectedCompliances)) {
			Arrays.stream(expectedCompliances.split(","))
					.filter(StringUtils::isNotBlank)
					.filter(s -> s.equalsIgnoreCase(compatibility))
					.findFirst()
					.orElseThrow(() -> new Exception("Invalid type: " + compatibility));
		}

	}

}
