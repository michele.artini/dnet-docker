package eu.dnetlib.wfs.procs;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import eu.dnetlib.errors.WorkflowManagerException;

@Service
public class ProcessRegistry {

	private static final Log log = LogFactory.getLog(ProcessRegistry.class);
	private final Map<String, WorkflowProcess> procs = new HashMap<>();

	synchronized public int countRunningWfs() {
		int count = 0;
		for (final Map.Entry<String, WorkflowProcess> e : this.procs.entrySet()) {
			final WorkflowProcess proc = e.getValue();
			if (!proc.isTerminated()) {
				count++;
			}
		}
		return count;
	}

	public Set<String> listProcIds() {
		return this.procs.keySet();
	}

	public WorkflowProcess findProcess(final String procId) {
		return this.procs.get(procId);
	}

	public Collection<WorkflowProcess> listProcesses() {
		return this.procs.values();
	}

	public void registerProcess(final WorkflowProcess process) throws WorkflowManagerException {
		if (this.procs.containsValue(process) || this.procs.containsKey(process.getId())) {
			log.error("Already registerd process: " + process);
			throw new WorkflowManagerException("Already registered process: " + process);
		}
		this.procs.put(process.getId(), process);
	}

	public void unregisterProcess(final String procId) {
		this.procs.remove(procId);
	}

}
