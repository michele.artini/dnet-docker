package eu.dnetlib.wfs.nodes;

public enum NodeStatus {
	CONFIGURED, NOT_CONFIGURED, DISABLED, SYSTEM
}
