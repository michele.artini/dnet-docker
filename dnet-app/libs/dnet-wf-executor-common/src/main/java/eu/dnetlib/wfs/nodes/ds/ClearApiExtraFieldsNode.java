package eu.dnetlib.wfs.nodes.ds;

import org.springframework.beans.factory.annotation.Autowired;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.DsmClient;
import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.wfs.annotations.WfInputParam;
import eu.dnetlib.wfs.annotations.WfNode;
import eu.dnetlib.wfs.nodes.ProcessNode;

@WfNode("clearApiExtraFields")
public class ClearApiExtraFieldsNode extends ProcessNode {

	@WfInputParam
	private String infoType; // COLLECT, TRANSFORM, DOWNLOAD

	@WfInputParam
	private Api api;

	@Autowired
	private DnetServiceClientFactory clientFactory;

	@Override
	protected void execute() throws Exception {

		final DsmClient dsm = this.clientFactory.getClient(DsmClient.class);

		switch (this.infoType.toUpperCase()) {
		case "COLLECT":
			dsm.updateApiCollectionInfo(this.api.getId(), null, 0);
			break;
		case "AGGREGATOR":
		case "TRANSFORM":
			dsm.updateApiAggregationInfo(this.api.getId(), null, 0);
			break;
		case "DOWNLOAD":
			dsm.updateApiDownloadInfo(this.api.getId(), null, 0);
			break;
		default:
			throw new RuntimeException("Invalid infoType: " + this.infoType);
		}

	}

}
