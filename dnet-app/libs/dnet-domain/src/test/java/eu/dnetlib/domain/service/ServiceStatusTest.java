package eu.dnetlib.domain.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.MalformedURLException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ServiceStatusTest {

	@BeforeEach
	void setUp() throws Exception {}

	@Test
	void test() throws MalformedURLException {
		final ServiceStatus status = new ServiceStatus(ServiceType.vocabulary_manager, "http://192.168.0.5:8080/vocs");
		assertEquals("vocs-192-168-000-005-8080", status.getName());
	}

}
