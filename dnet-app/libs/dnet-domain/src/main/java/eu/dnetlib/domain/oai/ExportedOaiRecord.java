package eu.dnetlib.domain.oai;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "oai_records")
public class ExportedOaiRecord implements OaiRecord<LocalDateTime, String> {

	private static final long serialVersionUID = 9178403183427337500L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "body")
	private String body;

	@Column(name = "date")
	private LocalDateTime date;

	@Column(name = "oai_set")
	private String oaiSet;

	@Override
	public String getId() {
		return this.id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	@Override
	public String getBody() {
		return this.body;
	}

	public void setBody(final String body) {
		this.body = body;
	}

	@Override
	public LocalDateTime getDate() {
		return this.date;
	}

	public void setDate(final LocalDateTime date) {
		this.date = date;
	}

	public String getOaiSet() {
		return this.oaiSet;
	}

	public void setOaiSet(final String oaiSet) {
		this.oaiSet = oaiSet;
	}

	@Override
	public List<String> getSets() {
		return Arrays.asList(this.oaiSet);
	}

	@Override
	public boolean isDeleted() {
		return false;
	}

}
