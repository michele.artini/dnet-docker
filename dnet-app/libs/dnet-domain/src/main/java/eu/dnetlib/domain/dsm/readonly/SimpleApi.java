package eu.dnetlib.domain.dsm.readonly;

import java.io.Serializable;
import java.util.Objects;

public class SimpleApi implements Serializable {

	private static final long serialVersionUID = -5452907197475338214L;

	private String id;
	private String protocol;
	private String compliance;
	private Boolean active;
	private String aggrDate;
	private Long aggrTotal;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(final String protocol) {
		this.protocol = protocol;
	}

	public String getCompliance() {
		return compliance;
	}

	public void setCompliance(final String compliance) {
		this.compliance = compliance;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(final Boolean active) {
		this.active = active;
	}

	public String getAggrDate() {
		return aggrDate;
	}

	public void setAggrDate(final String aggrDate) {
		this.aggrDate = aggrDate;
	}

	public Long getAggrTotal() {
		return aggrTotal;
	}

	public void setAggrTotal(final Long aggrTotal) {
		this.aggrTotal = aggrTotal;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof SimpleApi)) { return false; }
		final SimpleApi other = (SimpleApi) obj;
		return Objects.equals(id, other.id);
	}

}
