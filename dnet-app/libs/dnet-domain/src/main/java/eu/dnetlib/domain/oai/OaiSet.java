package eu.dnetlib.domain.oai;

import java.io.Serializable;

public interface OaiSet extends Serializable {

	String getSetSpec();

	String getSetName();

	String getDescription();

}
