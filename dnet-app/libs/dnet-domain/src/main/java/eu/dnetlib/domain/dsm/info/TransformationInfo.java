package eu.dnetlib.domain.dsm.info;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * Created by claudio on 29/11/2016.
 */
@JsonAutoDetect
public class TransformationInfo extends AggregationInfo {

}
