package eu.dnetlib.domain.dsm.info;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class DatasourceResponse<T> extends Response {

	private List<T> datasourceInfo = new ArrayList<>();

	public DatasourceResponse<T> addDatasourceInfo(final T datasourceInfo) {
		getDatasourceInfo().add(datasourceInfo);
		return this;
	}

	public List<T> getDatasourceInfo() {
		return datasourceInfo;
	}

	public DatasourceResponse<T> setDatasourceInfo(final List<T> datasourceInfo) {
		this.datasourceInfo = datasourceInfo;
		return this;
	}

}
