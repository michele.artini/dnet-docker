package eu.dnetlib.domain.dsm;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@Entity
@Table(name = "dsm_api")
public class Api implements Comparable<Api>, Serializable {

	private static final long serialVersionUID = 2083931880495828376L;

	@Id
	private String id = null;

	@Column(name = "protocol")
	private String protocol = null;

	@Column(name = "service")
	private String datasource = null;

	@Column(name = "contentdescription")
	private String contentdescription = null;

	@Column(name = "active")
	private Boolean active = false;

	@Column(name = "removable")
	private Boolean removable = true;

	@Column(name = "compatibility")
	private String compatibility;

	@Transient
	private boolean compatibilityOverrided = false;

	@Column(name = "metadata_identifier_path")
	private String metadataIdentifierPath;

	@Column(name = "last_collection_total")
	private Integer lastCollectionTotal;

	@Column(name = "last_collection_date")
	private Timestamp lastCollectionDate;

	@Column(name = "last_collection_mdid")
	private String lastCollectionMdid;

	@Column(name = "last_aggregation_total")
	private Integer lastAggregationTotal;

	@Column(name = "last_aggregation_date")
	private Timestamp lastAggregationDate;

	@Column(name = "last_aggregation_mdid")
	private String lastAggregationMdid;

	@Column(name = "last_download_total")
	private Integer lastDownloadTotal;

	@Column(name = "last_download_date")
	private Timestamp lastDownloadDate;

	@Column(name = "last_download_objid")
	private String lastDownloadObjid;

	@Column(name = "last_validation_job")
	private String lastValidationJob;

	@Column(name = "baseurl")
	private String baseurl = null;

	@OneToMany(mappedBy = "api", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<ApiParam> apiParams = new LinkedHashSet<>();

	@Column(name = "compatibility_override")
	private String compatibilityOverride;

	@Override
	public boolean equals(final Object o) {
		if (this == o) { return true; }
		if ((o == null) || (getClass() != o.getClass())) { return false; }
		final Api api = (Api) o;
		return Objects.equals(this.id, api.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}

	@Override
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (final JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int compareTo(final Api a) {
		return getId().compareTo(a.getId());
	}

	public String getId() {
		return this.id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getProtocol() {
		return this.protocol;
	}

	public void setProtocol(final String protocol) {
		this.protocol = protocol;
	}

	public String getDatasource() {
		return this.datasource;
	}

	public void setDatasource(final String datasource) {
		this.datasource = datasource;
	}

	public String getContentdescription() {
		return this.contentdescription;
	}

	public void setContentdescription(final String contentdescription) {
		this.contentdescription = contentdescription;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(final Boolean active) {
		this.active = active;
	}

	public Boolean getRemovable() {
		return this.removable;
	}

	public void setRemovable(final Boolean removable) {
		this.removable = removable;
	}

	public String getCompatibility() {
		return this.compatibility;
	}

	public void setCompatibility(final String compatibility) {
		this.compatibility = compatibility;
	}

	public boolean isCompatibilityOverrided() {
		return this.compatibilityOverrided;
	}

	public void setCompatibilityOverrided(final boolean compatibilityOverrided) {
		this.compatibilityOverrided = compatibilityOverrided;
	}

	public String getMetadataIdentifierPath() {
		return this.metadataIdentifierPath;
	}

	public void setMetadataIdentifierPath(final String metadataIdentifierPath) {
		this.metadataIdentifierPath = metadataIdentifierPath;
	}

	public Integer getLastCollectionTotal() {
		return this.lastCollectionTotal;
	}

	public void setLastCollectionTotal(final Integer lastCollectionTotal) {
		this.lastCollectionTotal = lastCollectionTotal;
	}

	public Timestamp getLastCollectionDate() {
		return this.lastCollectionDate;
	}

	public void setLastCollectionDate(final Timestamp lastCollectionDate) {
		this.lastCollectionDate = lastCollectionDate;
	}

	public String getLastCollectionMdid() {
		return this.lastCollectionMdid;
	}

	public void setLastCollectionMdid(final String lastCollectionMdid) {
		this.lastCollectionMdid = lastCollectionMdid;
	}

	public Integer getLastAggregationTotal() {
		return this.lastAggregationTotal;
	}

	public void setLastAggregationTotal(final Integer lastAggregationTotal) {
		this.lastAggregationTotal = lastAggregationTotal;
	}

	public Timestamp getLastAggregationDate() {
		return this.lastAggregationDate;
	}

	public void setLastAggregationDate(final Timestamp lastAggregationDate) {
		this.lastAggregationDate = lastAggregationDate;
	}

	public String getLastAggregationMdid() {
		return this.lastAggregationMdid;
	}

	public void setLastAggregationMdid(final String lastAggregationMdid) {
		this.lastAggregationMdid = lastAggregationMdid;
	}

	public Integer getLastDownloadTotal() {
		return this.lastDownloadTotal;
	}

	public void setLastDownloadTotal(final Integer lastDownloadTotal) {
		this.lastDownloadTotal = lastDownloadTotal;
	}

	public Timestamp getLastDownloadDate() {
		return this.lastDownloadDate;
	}

	public void setLastDownloadDate(final Timestamp lastDownloadDate) {
		this.lastDownloadDate = lastDownloadDate;
	}

	public String getLastDownloadObjid() {
		return this.lastDownloadObjid;
	}

	public void setLastDownloadObjid(final String lastDownloadObjid) {
		this.lastDownloadObjid = lastDownloadObjid;
	}

	public String getLastValidationJob() {
		return this.lastValidationJob;
	}

	public void setLastValidationJob(final String lastValidationJob) {
		this.lastValidationJob = lastValidationJob;
	}

	public String getBaseurl() {
		return this.baseurl;
	}

	public void setBaseurl(final String baseurl) {
		this.baseurl = baseurl;
	}

	public Set<ApiParam> getApiParams() {
		return this.apiParams;
	}

	public void setApiParams(final Set<ApiParam> apiParams) {
		this.apiParams = apiParams;
	}

	public String getCompatibilityOverride() {
		return this.compatibilityOverride;
	}

	public void setCompatibilityOverride(final String compatibilityOverride) {
		this.compatibilityOverride = compatibilityOverride;
	}

}
