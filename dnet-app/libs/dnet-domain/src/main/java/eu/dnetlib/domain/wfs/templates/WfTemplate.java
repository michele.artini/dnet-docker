package eu.dnetlib.domain.wfs.templates;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import eu.dnetlib.domain.wfs.graph.Node;

@JsonInclude(Include.NON_NULL)
public class WfTemplate implements Serializable {

	private static final long serialVersionUID = 5919290887480115842L;

	private List<WfParam> parameters = new ArrayList<>();
	private List<Node> graph = new ArrayList<>();
	private Map<String, String> info = new LinkedHashMap<>();

	public List<Node> getGraph() {
		return this.graph;
	}

	public void setGraph(final List<Node> graph) {
		this.graph = graph;
	}

	public List<WfParam> getParameters() {
		return this.parameters;
	}

	public void setParameters(final List<WfParam> parameters) {
		this.parameters = parameters;
	}

	public Map<String, String> getInfo() {
		return this.info;
	}

	public void setInfo(final Map<String, String> info) {
		this.info = info;
	}

}
