package eu.dnetlib.domain.dsm;

public interface BrowseTerm {

	String getCode();

	void setCode(final String code);

	String getName();

	void setName(final String name);

	long getTotal();

	void setTotal(final long total);
}
