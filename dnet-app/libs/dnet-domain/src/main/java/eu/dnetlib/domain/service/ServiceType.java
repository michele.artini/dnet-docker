package eu.dnetlib.domain.service;

public enum ServiceType {

	is_manager("is"),
	context_manager("ctx"),
	datasource_manager("dsm"),
	mail_sender("mail"),
	index_manager("idx"),
	mdstore_manager("mdstore"),
	oai_manager("oai"),
	resource_manager("res"),
	vocabulary_manager("vocs"),
	wf_executor("wf-exec"),
	wf_manager("wf-manager");

	private final String prefix;

	private ServiceType(final String prefix) {
		this.prefix = prefix;
	}

	public String getPrefix() {
		return prefix;
	}

}
