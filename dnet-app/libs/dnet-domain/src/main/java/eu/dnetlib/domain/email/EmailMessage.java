package eu.dnetlib.domain.email;

import java.io.Serializable;
import java.util.List;

public class EmailMessage implements Serializable {

	private static final long serialVersionUID = 2012889148529575960L;

	private String fromName;
	private String fromMail;
	private String to;
	private List<String> ccs;
	private String subject;
	private String content;

	public EmailMessage() {}

	public EmailMessage(final String fromName, final String fromMail, final String to, final List<String> ccs, final String subject, final String content) {
		this.fromName = fromName;
		this.fromMail = fromMail;
		this.to = to;
		this.ccs = ccs;
		this.subject = subject;
		this.content = content;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(final String fromName) {
		this.fromName = fromName;
	}

	public String getFromMail() {
		return fromMail;
	}

	public void setFromMail(final String fromMail) {
		this.fromMail = fromMail;
	}

	public String getTo() {
		return to;
	}

	public void setTo(final String to) {
		this.to = to;
	}

	public List<String> getCcs() {
		return ccs;
	}

	public void setCcs(final List<String> ccs) {
		this.ccs = ccs;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(final String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(final String content) {
		this.content = content;
	}

}
