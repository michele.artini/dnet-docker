package eu.dnetlib.domain.dsm;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "dsm_organizations")
public class Organization implements Serializable {

	private static final long serialVersionUID = -8517323814472121142L;

	@Id
	private String id;
	private String legalshortname;
	private String legalname;
	private String websiteurl;
	private String logourl;

	private String country;
	private String collectedfrom;

	private LocalDateTime dateofcollection;
	private String provenanceaction;

	@ManyToMany(cascade = {
		CascadeType.PERSIST, CascadeType.MERGE
	}, fetch = FetchType.EAGER, mappedBy = "organizations")
	private Set<Datasource> datasources;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getLegalshortname() {
		return legalshortname;
	}

	public void setLegalshortname(final String legalshortname) {
		this.legalshortname = legalshortname;
	}

	public String getLegalname() {
		return legalname;
	}

	public void setLegalname(final String legalname) {
		this.legalname = legalname;
	}

	public String getWebsiteurl() {
		return websiteurl;
	}

	public void setWebsiteurl(final String websiteurl) {
		this.websiteurl = websiteurl;
	}

	public String getLogourl() {
		return logourl;
	}

	public void setLogourl(final String logourl) {
		this.logourl = logourl;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	public String getCollectedfrom() {
		return collectedfrom;
	}

	public void setCollectedfrom(final String collectedfrom) {
		this.collectedfrom = collectedfrom;
	}

	public LocalDateTime getDateofcollection() {
		return dateofcollection;
	}

	public void setDateofcollection(final LocalDateTime dateofcollection) {
		this.dateofcollection = dateofcollection;
	}

	public String getProvenanceaction() {
		return provenanceaction;
	}

	public void setProvenanceaction(final String provenanceaction) {
		this.provenanceaction = provenanceaction;
	}

	@JsonIgnore
	public Set<Datasource> getDatasources() {
		return datasources;
	}

	public void setDatasources(final Set<Datasource> datasources) {
		this.datasources = datasources;
	}

}
