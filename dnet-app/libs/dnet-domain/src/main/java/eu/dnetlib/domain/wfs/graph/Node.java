package eu.dnetlib.domain.wfs.graph;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Node implements Serializable {

	private static final long serialVersionUID = -3695762832959801906L;

	private static final String regExRef = "\\$\\{(\\w*)\\}";

	private String name;
	private String type;
	private boolean start = false;
	private boolean join = false;
	private List<Arc> arcs = new ArrayList<>();
	private List<NodeInputParam> input = new ArrayList<>();
	private List<NodeOutputParam> output = new ArrayList<>();

	public Node() {}

	public Node(final String name, final boolean start) {
		this.name = name;
		this.start = start;
	}

	public Node(final String name) {
		this.name = name;
	}

	public Node(final String name, final String type) {
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public boolean isStart() {
		return this.start;
	}

	public void setStart(final boolean start) {
		this.start = start;
	}

	public boolean isJoin() {
		return this.join;
	}

	public void setJoin(final boolean join) {
		this.join = join;
	}

	public List<Arc> getArcs() {
		return this.arcs;
	}

	public void setArcs(final List<Arc> arcs) {
		this.arcs = arcs;
	}

	public List<NodeInputParam> getInput() {
		return this.input;
	}

	public void setInput(final List<NodeInputParam> input) {
		this.input = input;
	}

	public List<NodeOutputParam> getOutput() {
		return this.output;
	}

	public void setOutput(final List<NodeOutputParam> output) {
		this.output = output;
	}

	public Map<String, Object> findEnvParams() {
		return this.input.stream()
				.filter(p -> StringUtils.isNotBlank(p.getEnv()))
				.collect(Collectors.toMap(NodeInputParam::getName, NodeInputParam::getEnv));
	}

	public Map<String, Object> calculateInitialParams(final Map<String, Object> globalParams, final Environment environment) {

		final Map<String, Object> map = new HashMap<>();
		for (final NodeInputParam p : this.input) {
			if (StringUtils.isBlank(p.getEnv())) {
				map.put(p.getName(), calculateSimpleValue(p, globalParams, environment));
			}
		}

		return map;
	}

	private Object calculateSimpleValue(final NodeInputParam p, final Map<String, Object> globalParams, final Environment environment) {
		String value = p.getValue();
		final String ref = p.getRef();
		final String prop = p.getProperty();

		if (StringUtils.isNotBlank(ref) && (globalParams.get(ref) != null)) { return globalParams.get(ref); }

		if (StringUtils.isNotBlank(value)) {
			final Matcher matcher = Pattern.compile(regExRef, Pattern.MULTILINE).matcher(value);
			while (matcher.find()) {
				final String rName = matcher.group(1);
				final Object rValue = globalParams.get(rName);

				value = value.replaceAll(Pattern.quote(matcher.group(0)), rValue.toString());
			}
			return value;
		}
		if (StringUtils.isNotBlank(prop)) { return environment.getProperty(prop); }

		return null;
	}

	public Map<String, String> outputToEnvMap() {
		final Map<String, String> map = new HashMap<>();
		for (final NodeOutputParam p : this.output) {
			if (StringUtils.isNotBlank(p.getEnv())) {
				map.put(p.getName(), p.getEnv());
			}
		}
		return map;
	}
}
