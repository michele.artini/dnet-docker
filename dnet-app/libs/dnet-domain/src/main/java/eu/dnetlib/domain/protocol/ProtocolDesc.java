package eu.dnetlib.domain.protocol;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProtocolDesc implements Serializable {

	private static final long serialVersionUID = -5697339401309977230L;

	private String id;

	private List<ProtocolParam> params = new ArrayList<>();

	public ProtocolDesc() {}

	public ProtocolDesc(final String id, final List<ProtocolParam> params) {
		this.id = id;
		this.params = params;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public List<ProtocolParam> getParams() {
		return params;
	}

	public void setParams(final List<ProtocolParam> params) {
		this.params = params;
	}

}
