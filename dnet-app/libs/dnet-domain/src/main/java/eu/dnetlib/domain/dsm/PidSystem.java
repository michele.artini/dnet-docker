package eu.dnetlib.domain.dsm;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@IdClass(PidSystemPK.class)
@Table(name = "dsm_pid_systems")
public class PidSystem implements Serializable {

	private static final long serialVersionUID = 7413964485674583561L;

	@Id
	private String service;
	@Id
	private String type;
	@Id
	private String scheme;

	public PidSystem() {}

	public PidSystem(final String service, final String type, final String scheme) {
		this.service = service;
		this.type = type;
		this.scheme = scheme;
	}

	public String getService() {
		return service;
	}

	public void setService(final String service) {
		this.service = service;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getScheme() {
		return scheme;
	}

	public void setScheme(final String scheme) {
		this.scheme = scheme;
	}

}
