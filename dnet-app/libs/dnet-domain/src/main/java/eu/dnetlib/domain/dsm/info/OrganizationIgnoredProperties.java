package eu.dnetlib.domain.dsm.info;

import java.time.LocalDateTime;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class OrganizationIgnoredProperties {

	@JsonIgnore
	protected String id;

	@JsonIgnore
	protected String collectedfrom;

	@JsonIgnore
	protected LocalDateTime dateofcollection;

	@JsonIgnore
	protected String provenanceaction;

	@JsonIgnore
	protected Set<?> datasources;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getCollectedfrom() {
		return collectedfrom;
	}

	public void setCollectedfrom(final String collectedfrom) {
		this.collectedfrom = collectedfrom;
	}

	public LocalDateTime getDateofcollection() {
		return dateofcollection;
	}

	public void setDateofcollection(final LocalDateTime dateofcollection) {
		this.dateofcollection = dateofcollection;
	}

	public String getProvenanceaction() {
		return provenanceaction;
	}

	public void setProvenanceaction(final String provenanceaction) {
		this.provenanceaction = provenanceaction;
	}

	public Set<?> getDatasources() {
		return datasources;
	}

	public void setDatasources(final Set<?> datasources) {
		this.datasources = datasources;
	}
}
