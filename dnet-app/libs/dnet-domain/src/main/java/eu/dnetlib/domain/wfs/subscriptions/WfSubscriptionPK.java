package eu.dnetlib.domain.wfs.subscriptions;

import java.io.Serializable;
import java.util.Objects;

public class WfSubscriptionPK implements Serializable {

	private static final long serialVersionUID = -7569690774071644848L;

	private String wfConfigurationId;

	private NotificationCondition condition;

	private String email;

	public String getWfConfigurationId() {
		return wfConfigurationId;
	}

	public void setWfConfigurationId(final String wfConfigurationId) {
		this.wfConfigurationId = wfConfigurationId;
	}

	public NotificationCondition getCondition() {
		return condition;
	}

	public void setCondition(final NotificationCondition condition) {
		this.condition = condition;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	@Override
	public int hashCode() {
		return Objects.hash(condition, email, wfConfigurationId);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if ((obj == null) || (getClass() != obj.getClass())) { return false; }
		final WfSubscriptionPK other = (WfSubscriptionPK) obj;
		return (condition == other.condition) && Objects.equals(email, other.email) && Objects.equals(wfConfigurationId, other.wfConfigurationId);
	}
}
