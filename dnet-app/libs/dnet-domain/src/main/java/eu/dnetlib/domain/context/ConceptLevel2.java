package eu.dnetlib.domain.context;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "context_cat_concepts_lvl_2")
public class ConceptLevel2 extends CtxChildInfo {

	private static final long serialVersionUID = 906131339592862096L;

}
