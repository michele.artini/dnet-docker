package eu.dnetlib.domain.resource;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "resource_types_view")
public class ResourceType implements Serializable {

	private static final long serialVersionUID = -152368127918595773L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "content_type")
	private String contentType;

	@Column(name = "count")
	private long count;

	@Column(name = "simple")
	private boolean simple = true;

	public ResourceType() {}

	public ResourceType(final String id, final String name, final String contentType, final long count) {
		this.id = id;
		this.name = name;
		this.count = count;
		this.contentType = contentType;
		this.simple = true;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(final String contentType) {
		this.contentType = contentType;
	}

	public long getCount() {
		return count;
	}

	public void setCount(final long count) {
		this.count = count;
	}

	public boolean isSimple() {
		return simple;
	}

	public void setSimple(final boolean simple) {
		this.simple = simple;
	}

}
