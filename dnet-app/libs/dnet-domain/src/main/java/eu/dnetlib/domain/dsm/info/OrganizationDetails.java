package eu.dnetlib.domain.dsm.info;

import jakarta.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
@Schema(name = "Organization info model", description = "provides information about the organization")
public class OrganizationDetails extends OrganizationIgnoredProperties {

	private String legalshortname;

	@NotBlank
	private String legalname;

	private String websiteurl;

	private String logourl;

	@NotBlank
	private String country;

	public String getLegalshortname() {
		return legalshortname;
	}

	public String getLegalname() {
		return legalname;
	}

	public String getWebsiteurl() {
		return websiteurl;
	}

	public String getLogourl() {
		return logourl;
	}

	public String getCountry() {
		return country;
	}

	public OrganizationDetails setLegalshortname(final String legalshortname) {
		this.legalshortname = legalshortname;
		return this;
	}

	public OrganizationDetails setLegalname(final String legalname) {
		this.legalname = legalname;
		return this;
	}

	public OrganizationDetails setWebsiteurl(final String websiteurl) {
		this.websiteurl = websiteurl;
		return this;
	}

	public OrganizationDetails setLogourl(final String logourl) {
		this.logourl = logourl;
		return this;
	}

	public OrganizationDetails setCountry(final String country) {
		this.country = country;
		return this;
	}

}
