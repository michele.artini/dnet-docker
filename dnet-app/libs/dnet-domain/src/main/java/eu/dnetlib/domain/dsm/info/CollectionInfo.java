package eu.dnetlib.domain.dsm.info;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * Created by claudio on 29/11/2016.
 */
@JsonAutoDetect
public class CollectionInfo extends AggregationInfo {

	private CollectionMode collectionMode;

	public CollectionMode getCollectionMode() {
		return collectionMode;
	}

	public void setCollectionMode(final CollectionMode collectionMode) {
		this.collectionMode = collectionMode;
	}

}
