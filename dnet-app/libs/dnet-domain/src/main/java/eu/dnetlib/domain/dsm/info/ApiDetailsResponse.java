package eu.dnetlib.domain.dsm.info;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class ApiDetailsResponse extends Response {

	private List<ApiDetails> api;

	public List<ApiDetails> getApi() {
		return api;
	}

	public ApiDetailsResponse setApi(final List<ApiDetails> api) {
		this.api = api;
		return this;
	}
}
