package eu.dnetlib.domain.common;

public class KeyValue<T> {

	private final String k;
	private final T v;

	public KeyValue(final String k, final T v) {
		this.k = k;
		this.v = v;
	}

	public String getK() {
		return k;
	}

	public T getV() {
		return v;
	}

}
