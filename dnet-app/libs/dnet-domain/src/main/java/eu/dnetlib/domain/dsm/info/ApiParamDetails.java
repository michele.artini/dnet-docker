package eu.dnetlib.domain.dsm.info;

public class ApiParamDetails {

	protected String param;

	protected String value;

	public String getParam() {
		return param;
	}

	public void setParam(final String param) {
		this.param = param;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}
}
