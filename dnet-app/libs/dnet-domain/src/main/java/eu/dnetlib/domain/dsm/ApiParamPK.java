package eu.dnetlib.domain.dsm;

import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Embeddable
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiParamPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1640636806392015938L;

	private Api api = null;

	private String param;

	public String getParam() {
		return param;
	}

	public ApiParamPK setParam(final String param) {
		this.param = param;
		return this;
	}

	public Api getApi() {
		return api;
	}

	public void setApi(final Api api) {
		this.api = api;
	}

	@Override
	public int hashCode() {
		return Objects.hash(api, param);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof ApiParamPK)) { return false; }
		final ApiParamPK other = (ApiParamPK) obj;
		return Objects.equals(api, other.api) && Objects.equals(param, other.param);
	}

	@Override
	public String toString() {
		return String.format("ApiParamPK [api=%s, param=%s]", api, param);
	}

}
