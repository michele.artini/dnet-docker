package eu.dnetlib.domain.index;

import java.io.Serializable;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@Entity
@Table(name = "index_configurations")
public class IndexConfiguration implements Serializable {

	private static final long serialVersionUID = -7273938084794386017L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "solr_config")
	private String solrConfig;

	@Column(name = "description")
	private String description;

	@Column(name = "n_shards")
	private int numShards;

	@Column(name = "n_replicas")
	private int numReplicas;

	@Transient
	private List<IndexField> fields;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public List<IndexField> getFields() {
		return fields;
	}

	public void setFields(final List<IndexField> fields) {
		this.fields = fields;
	}

	public String getSolrConfig() {
		return solrConfig;
	}

	public void setSolrConfig(final String solrConfig) {
		this.solrConfig = solrConfig;
	}

	public int getNumShards() {
		return numShards;
	}

	public void setNumShards(final int numShards) {
		this.numShards = numShards;
	}

	public int getNumReplicas() {
		return numReplicas;
	}

	public void setNumReplicas(final int numReplicas) {
		this.numReplicas = numReplicas;
	}

}
