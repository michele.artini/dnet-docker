package eu.dnetlib.domain.vocabulary;

import java.io.Serializable;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

public class Synonym implements Serializable, Comparable<Synonym> {

	private static final long serialVersionUID = 194743347016156197L;

	public Synonym() {}

	public Synonym(final String term, final String encoding) {
		this.term = term;
		this.encoding = encoding;
	}

	private String term;

	private String encoding;

	public String getTerm() {
		return term;
	}

	public void setTerm(final String term) {
		this.term = term;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(final String encoding) {
		this.encoding = encoding;
	}

	@Override
	public int hashCode() {
		return Objects.hash(encoding, term);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof Synonym)) { return false; }
		final Synonym other = (Synonym) obj;
		return Objects.equals(encoding, other.encoding) && Objects.equals(term, other.term);
	}

	@Override
	public String toString() {
		return String.format("Synonym [term=%s, encoding=%s]", term, encoding);
	}

	@Override
	public int compareTo(final Synonym o) {
		return StringUtils.compare(term, o.getTerm());
	}

}
