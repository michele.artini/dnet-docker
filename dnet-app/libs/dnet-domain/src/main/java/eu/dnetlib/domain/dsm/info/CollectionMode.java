package eu.dnetlib.domain.dsm.info;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * Created by claudio on 12/09/16.
 */
@JsonAutoDetect
public enum CollectionMode {
	REFRESH,
	INCREMENTAL
}
