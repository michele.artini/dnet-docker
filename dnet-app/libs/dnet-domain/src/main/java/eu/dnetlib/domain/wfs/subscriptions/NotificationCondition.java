package eu.dnetlib.domain.wfs.subscriptions;

public enum NotificationCondition {
	ALWAYS, NEVER, ONLY_SUCCESS, ONLY_FAILED
}
