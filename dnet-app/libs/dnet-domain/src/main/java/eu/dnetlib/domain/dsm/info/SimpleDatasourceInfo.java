package eu.dnetlib.domain.dsm.info;

import java.util.LinkedHashMap;
import java.util.Map;

public class SimpleDatasourceInfo {

	private String id;
	private String officialName;
	private String englishName;
	private Map<String, String> organizations = new LinkedHashMap<>();
	@Deprecated
	private String typology;
	private String eoscType;
	private String eoscDatasourceType;
	private String registeredBy;
	private String registrationDate;
	private String compatibility;
	private String firstCollectionDate;
	private String lastCollectionDate;
	private long lastCollectionTotal;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getOfficialName() {
		return officialName;
	}

	public void setOfficialName(final String officialName) {
		this.officialName = officialName;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(final String englishName) {
		this.englishName = englishName;
	}

	public Map<String, String> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(final Map<String, String> organizations) {
		this.organizations = organizations;
	}

	@Deprecated
	public String getTypology() {
		return typology;
	}

	@Deprecated
	public void setTypology(final String typology) {
		this.typology = typology;
	}

	public String getEoscType() {
		return eoscType;
	}

	public void setEoscType(final String eoscType) {
		this.eoscType = eoscType;
	}

	public String getEoscDatasourceType() {
		return eoscDatasourceType;
	}

	public void setEoscDatasourceType(final String eoscDatasourceType) {
		this.eoscDatasourceType = eoscDatasourceType;
	}

	public String getRegisteredBy() {
		return registeredBy;
	}

	public void setRegisteredBy(final String registeredBy) {
		this.registeredBy = registeredBy;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(final String registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getCompatibility() {
		return compatibility;
	}

	public void setCompatibility(final String compatibility) {
		this.compatibility = compatibility;
	}

	public String getFirstCollectionDate() {
		return firstCollectionDate;
	}

	public void setFirstCollectionDate(final String firstCollectionDate) {
		this.firstCollectionDate = firstCollectionDate;
	}

	public String getLastCollectionDate() {
		return lastCollectionDate;
	}

	public void setLastCollectionDate(final String lastCollectionDate) {
		this.lastCollectionDate = lastCollectionDate;
	}

	public long getLastCollectionTotal() {
		return lastCollectionTotal;
	}

	public void setLastCollectionTotal(final long lastCollectionTotal) {
		this.lastCollectionTotal = lastCollectionTotal;
	}

}
