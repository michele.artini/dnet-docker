package eu.dnetlib.domain.email;

import java.io.Serializable;

public class EmailTemplate implements Serializable {

	private static final long serialVersionUID = -8424437958140000626L;

	private String subject;

	private String message;

	public String getSubject() {
		return subject;
	}

	public void setSubject(final String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

}
