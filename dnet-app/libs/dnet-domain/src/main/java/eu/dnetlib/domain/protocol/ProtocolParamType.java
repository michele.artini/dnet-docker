package eu.dnetlib.domain.protocol;

public enum ProtocolParamType {
	TEXT,
	LIST,
	BOOLEAN,
	INT
}
