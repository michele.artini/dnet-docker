package eu.dnetlib.domain.vocabulary;

import java.io.Serializable;

import org.hibernate.annotations.Type;

import io.hypersistence.utils.hibernate.type.json.JsonType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;

@Entity
@Table(name = "vocabulary_terms")
@IdClass(VocabularyTermPK.class)
public class VocabularyTerm implements Serializable {

	private static final long serialVersionUID = -3995855638130512114L;

	@Id
	@Column(name = "code")
	private String code;

	@Id
	@Column(name = "vocabulary")
	private String vocabulary;

	@Column(name = "name")
	private String name;

	@Column(name = "encoding")
	private String encoding;

	@Type(JsonType.class)
	@Column(name = "synonyms")
	private Synonym[] synonyms;

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public String getVocabulary() {
		return vocabulary;
	}

	public void setVocabulary(final String vocabulary) {
		this.vocabulary = vocabulary;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(final String encoding) {
		this.encoding = encoding;
	}

	public Synonym[] getSynonyms() {
		return synonyms;
	}

	public void setSynonyms(final Synonym[] synonyms) {
		this.synonyms = synonyms;
	}

}
