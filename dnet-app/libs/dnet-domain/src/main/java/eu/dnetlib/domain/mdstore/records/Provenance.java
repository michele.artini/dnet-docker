package eu.dnetlib.domain.mdstore.records;

import java.io.Serializable;

/**
 * @author Sandro La Bruzzo
 *         <p>
 *         Provenace class models the provenance of the record in the metadataStore It contains the identifier and the name of the
 *         datasource that gives the record
 */
public class Provenance implements Serializable {

	private static final long serialVersionUID = 4582298196263695093L;

	private String datasourceId;

	private String datasourceName;

	private String nsPrefix;

	public Provenance() {}

	public Provenance(final String datasourceId, final String datasourceName, final String nsPrefix) {
		this.datasourceId = datasourceId;
		this.datasourceName = datasourceName;
		this.nsPrefix = nsPrefix;
	}

	public String getDatasourceId() {
		return datasourceId;
	}

	public void setDatasourceId(final String datasourceId) {
		this.datasourceId = datasourceId;
	}

	public String getDatasourceName() {
		return datasourceName;
	}

	public void setDatasourceName(final String datasourceName) {
		this.datasourceName = datasourceName;
	}

	public String getNsPrefix() {
		return nsPrefix;
	}

	public void setNsPrefix(final String nsPrefix) {
		this.nsPrefix = nsPrefix;
	}
}
