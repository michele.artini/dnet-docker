package eu.dnetlib.domain.wfs.graph.runtime;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import eu.dnetlib.domain.wfs.WorkflowsConstants;

public class RuntimeNode implements Serializable {

	private static final long serialVersionUID = 2182058793660686197L;

	private String name;
	private String type;
	private boolean isStart;
	private boolean isJoin;
	private boolean isSuccessNode;
	private Map<String, Object> params;
	private Map<String, Object> envParams;
	private Map<String, String> outputEnvMap;

	private RuntimeNodeStatus status = RuntimeNodeStatus.ready;
	private int nExecutions = 0;
	private String progressMessage;

	public RuntimeNode() {}

	private RuntimeNode(final String name,
			final String type,
			final boolean isStart,
			final boolean isJoin,
			final boolean isSuccessNode,
			final Map<String, Object> params,
			final Map<String, Object> envParams,
			final Map<String, String> outputEnvMap) {
		this.name = name;
		this.type = type;
		this.isStart = isStart;
		this.isJoin = isJoin;
		this.isSuccessNode = isSuccessNode;
		this.params = params;
		this.envParams = envParams;
		this.outputEnvMap = outputEnvMap;
	}

	public static RuntimeNode newNode(final String name,
			final String type,
			final Map<String, Object> params,
			final Map<String, Object> envParams,
			final Map<String, String> outputEnvMap) {
		return new RuntimeNode(name, type, false, false, false, params, envParams, outputEnvMap);
	}

	public static RuntimeNode newStartNode(final String name,
			final String type,
			final Map<String, Object> params,
			final Map<String, Object> envParams,
			final Map<String, String> outputEnvMap) {
		return new RuntimeNode(name, type, true, false, false, params, envParams, outputEnvMap);
	}

	public static RuntimeNode newJoinNode(final String name,
			final String type,
			final Map<String, Object> params,
			final Map<String, Object> envParams,
			final Map<String, String> outputEnvMap) {
		return new RuntimeNode(name, type, false, true, false, params, envParams, outputEnvMap);
	}

	public static RuntimeNode newSuccessNode() {
		return new RuntimeNode(WorkflowsConstants.SUCCESS_NODE, null, false, true, true, new HashMap<>(), new HashMap<>(), new HashMap<>());
	}

	@Override
	public String toString() {
		final StringWriter sw = new StringWriter();
		sw.append("[ name: ");
		sw.append(this.name);
		if (StringUtils.isNotBlank(this.type)) {
			sw.append(", type: ");
			sw.append(this.type);
		}
		if (isStart()) {
			sw.append(" - isStart");
		}
		if (isJoin()) {
			sw.append(" - isJoin");
		}
		sw.append(" ]");
		return sw.toString();
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public boolean isStart() {
		return this.isStart;
	}

	public void setStart(final boolean isStart) {
		this.isStart = isStart;
	}

	public boolean isJoin() {
		return this.isJoin;
	}

	public void setJoin(final boolean isJoin) {
		this.isJoin = isJoin;
	}

	public boolean isSuccessNode() {
		return this.isSuccessNode;
	}

	public void setSuccessNode(final boolean isSuccessNode) {
		this.isSuccessNode = isSuccessNode;
	}

	public Map<String, Object> getParams() {
		return this.params;
	}

	public void setParams(final Map<String, Object> params) {
		this.params = params;
	}

	public Map<String, Object> getEnvParams() {
		return this.envParams;
	}

	public void setEnvParams(final Map<String, Object> envParams) {
		this.envParams = envParams;
	}

	public Map<String, String> getOutputEnvMap() {
		return this.outputEnvMap;
	}

	public void setOutputEnvMap(final Map<String, String> outputEnvMap) {
		this.outputEnvMap = outputEnvMap;
	}

	public int getnExecutions() {
		return this.nExecutions;
	}

	public void setnExecutions(final int nExecutions) {
		this.nExecutions = nExecutions;
	}

	public String getProgressMessage() {
		return this.progressMessage;
	}

	public void setProgressMessage(final String progressMessage) {
		this.progressMessage = progressMessage;
	}

	public RuntimeNodeStatus getStatus() {
		return this.status;
	}

	public void setStatus(final RuntimeNodeStatus status) {
		this.status = status;
	}

}
