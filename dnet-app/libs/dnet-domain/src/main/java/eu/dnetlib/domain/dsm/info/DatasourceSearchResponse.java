package eu.dnetlib.domain.dsm.info;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class DatasourceSearchResponse extends Response {

	private List<DatasourceInfo> datasourceInfo;

	public DatasourceSearchResponse(final List<DatasourceInfo> datasourceInfo) {
		super();
		this.datasourceInfo = datasourceInfo;
	}

	public List<DatasourceInfo> getDatasourceInfo() {
		return datasourceInfo;
	}

	public void setDatasourceInfo(final List<DatasourceInfo> datasourceInfo) {
		this.datasourceInfo = datasourceInfo;
	}
}
