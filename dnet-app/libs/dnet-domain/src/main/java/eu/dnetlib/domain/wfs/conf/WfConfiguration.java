package eu.dnetlib.domain.wfs.conf;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.annotations.Type;

import io.hypersistence.utils.hibernate.type.array.ListArrayType;
import io.hypersistence.utils.hibernate.type.json.JsonType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@Entity
@Table(name = "wf_configurations")
public class WfConfiguration implements Serializable {

	private static final long serialVersionUID = 7503841966138333044L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "section")
	private String section;

	@Type(JsonType.class)
	@Column(name = "details")
	private Map<String, String> details = new LinkedHashMap<>();

	@Column(name = "priority")
	private Integer priority;

	@Column(name = "dsid")
	private String dsId;

	@Column(name = "dsname")
	private String dsName;

	@Column(name = "apiid")
	private String apiId;

	@Column(name = "enabled")
	private boolean enabled;

	@Column(name = "configured")
	private boolean configured;

	@Column(name = "scheduling_enabled")
	private boolean schedulingEnabled;

	@Column(name = "scheduling_cron")
	private String cronExpression;

	@Column(name = "scheduling_min_interval")
	private int cronMinInterval;

	@Type(ListArrayType.class)
	@Column(name = "workflows", columnDefinition = "text[]")
	private List<String> workflows;

	@Column(name = "destroy_wf")
	private String destroyWf;

	@Type(JsonType.class)
	@Column(name = "system_params")
	private Map<String, Object> systemParams = new LinkedHashMap<>();

	@Type(JsonType.class)
	@Column(name = "user_params")
	private Map<String, Object> userParams = new LinkedHashMap<>();

	@Transient
	private String parentId;

	public String getId() {
		return this.id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getSection() {
		return this.section;
	}

	public void setSection(final String section) {
		this.section = section;
	}

	public Map<String, String> getDetails() {
		return this.details;
	}

	public void setDetails(final Map<String, String> details) {
		this.details = details;
	}

	public Integer getPriority() {
		return this.priority;
	}

	public void setPriority(final Integer priority) {
		this.priority = priority;
	}

	public String getDsId() {
		return this.dsId;
	}

	public void setDsId(final String dsId) {
		this.dsId = dsId;
	}

	public String getDsName() {
		return this.dsName;
	}

	public void setDsName(final String dsName) {
		this.dsName = dsName;
	}

	public String getApiId() {
		return this.apiId;
	}

	public void setApiId(final String apiId) {
		this.apiId = apiId;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(final boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isConfigured() {
		return this.configured;
	}

	public void setConfigured(final boolean configured) {
		this.configured = configured;
	}

	public boolean isSchedulingEnabled() {
		return this.schedulingEnabled;
	}

	public void setSchedulingEnabled(final boolean schedulingEnabled) {
		this.schedulingEnabled = schedulingEnabled;
	}

	public String getCronExpression() {
		return this.cronExpression;
	}

	public void setCronExpression(final String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public int getCronMinInterval() {
		return this.cronMinInterval;
	}

	public void setCronMinInterval(final int cronMinInterval) {
		this.cronMinInterval = cronMinInterval;
	}

	public List<String> getWorkflows() {
		return this.workflows;
	}

	public void setWorkflows(final List<String> workflows) {
		this.workflows = workflows;
	}

	public String getDestroyWf() {
		return this.destroyWf;
	}

	public void setDestroyWf(final String destroyWf) {
		this.destroyWf = destroyWf;
	}

	public Map<String, Object> getSystemParams() {
		return this.systemParams;
	}

	public void setSystemParams(final Map<String, Object> systemParams) {
		this.systemParams = systemParams;
	}

	public Map<String, Object> getUserParams() {
		return this.userParams;
	}

	public void setUserParams(final Map<String, Object> userParams) {
		this.userParams = userParams;
	}

	public String getParentId() {
		return this.parentId;
	}

	public void setParentId(final String parentId) {
		this.parentId = parentId;
	}

}
