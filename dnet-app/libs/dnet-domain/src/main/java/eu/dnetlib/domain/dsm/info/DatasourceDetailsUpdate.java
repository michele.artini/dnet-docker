package eu.dnetlib.domain.dsm.info;

import java.time.LocalDate;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

/**
 * Created by claudio on 12/09/16.
 */
@JsonAutoDetect
@Schema(name = "Datasource updatable fields model", description = "provides information about the datasource field that can be updated")
public class DatasourceDetailsUpdate {

	@NotBlank
	private String officialname;

	@NotBlank
	private String englishname;

	private String websiteurl;

	private String logourl;

	@Email
	private String contactemail;

	private Double latitude;

	private Double longitude;

	private String timezone;

	@Deprecated
	private String typology;

	private String eoscDatasourceType;

	private String platform;

	private String description;

	@Email
	private String registeredby;

	private Boolean managed;

	private Set<IdentitiesDetails> identities;

	private Boolean consentTermsOfUse;

	private LocalDate consentTermsOfUseDate;

	private LocalDate lastConsentTermsOfUseDate;

	private Boolean fullTextDownload;

	public String getOfficialname() {
		return officialname;
	}

	public String getEnglishname() {
		return englishname;
	}

	public String getWebsiteurl() {
		return websiteurl;
	}

	public String getLogourl() {
		return logourl;
	}

	public String getContactemail() {
		return contactemail;
	}

	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public String getTimezone() {
		return timezone;
	}

	public String getEoscDatasourceType() {
		return eoscDatasourceType;
	}

	public String getPlatform() {
		return platform;
	}

	public String getDescription() {
		return description;
	}

	public String getRegisteredby() {
		return registeredby;
	}

	public Boolean getManaged() {
		return managed;
	}

	public Set<IdentitiesDetails> getIdentities() {
		return identities;
	}

	public DatasourceDetailsUpdate setOfficialname(final String officialname) {
		this.officialname = officialname;
		return this;
	}

	public DatasourceDetailsUpdate setEnglishname(final String englishname) {
		this.englishname = englishname;
		return this;
	}

	public DatasourceDetailsUpdate setWebsiteurl(final String websiteurl) {
		this.websiteurl = websiteurl;
		return this;
	}

	public DatasourceDetailsUpdate setLogourl(final String logourl) {
		this.logourl = logourl;
		return this;
	}

	public DatasourceDetailsUpdate setContactemail(final String contactemail) {
		this.contactemail = contactemail;
		return this;
	}

	public DatasourceDetailsUpdate setLatitude(final Double latitude) {
		this.latitude = latitude;
		return this;
	}

	public DatasourceDetailsUpdate setLongitude(final Double longitude) {
		this.longitude = longitude;
		return this;
	}

	public DatasourceDetailsUpdate setTimezone(final String timezone) {
		this.timezone = timezone;
		return this;
	}

	public DatasourceDetailsUpdate setEoscDatasourceType(final String eoscDatasourceType) {
		this.eoscDatasourceType = eoscDatasourceType;
		return this;
	}

	public DatasourceDetailsUpdate setPlatform(final String platform) {
		this.platform = platform;
		return this;
	}

	public DatasourceDetailsUpdate setDescription(final String description) {
		this.description = description;
		return this;
	}

	public DatasourceDetailsUpdate setRegisteredby(final String registeredby) {
		this.registeredby = registeredby;
		return this;
	}

	public DatasourceDetailsUpdate setManaged(final Boolean managed) {
		this.managed = managed;
		return this;
	}

	public DatasourceDetailsUpdate setIdentities(final Set<IdentitiesDetails> identities) {
		this.identities = identities;
		return this;
	}

	public Boolean getConsentTermsOfUse() {
		return consentTermsOfUse;
	}

	public DatasourceDetailsUpdate setConsentTermsOfUse(final Boolean consentTermsOfUse) {
		this.consentTermsOfUse = consentTermsOfUse;
		return this;
	}

	public LocalDate getConsentTermsOfUseDate() {
		return consentTermsOfUseDate;
	}

	public DatasourceDetailsUpdate setConsentTermsOfUseDate(final LocalDate consentTermsOfUseDate) {
		this.consentTermsOfUseDate = consentTermsOfUseDate;
		return this;
	}

	public Boolean getFullTextDownload() {
		return fullTextDownload;
	}

	public DatasourceDetailsUpdate setFullTextDownload(final Boolean fullTextDownload) {
		this.fullTextDownload = fullTextDownload;
		return this;
	}

	public LocalDate getLastConsentTermsOfUseDate() {
		return lastConsentTermsOfUseDate;
	}

	public DatasourceDetailsUpdate setLastConsentTermsOfUseDate(final LocalDate lastConsentTermsOfUseDate) {
		this.lastConsentTermsOfUseDate = lastConsentTermsOfUseDate;
		return this;
	}

	@Deprecated
	public String getTypology() {
		return typology;
	}

	@Deprecated
	public DatasourceDetailsUpdate setTypology(final String typology) {
		this.typology = typology;
		return this;
	}

}
