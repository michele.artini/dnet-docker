package eu.dnetlib.domain.wfs.graph.runtime;

public enum RuntimeNodeStatus {
	ready,
	running,
	completed,
	failed
}
