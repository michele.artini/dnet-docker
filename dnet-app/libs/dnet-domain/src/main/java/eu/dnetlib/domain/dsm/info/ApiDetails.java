package eu.dnetlib.domain.dsm.info;

import java.time.LocalDateTime;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
@Schema(name = "Api model", description = "provides information about the datasource API")
public class ApiDetails extends ApiIgnoredProperties {

	private String id = null;

	private String protocol = null;

	private String datasource = null;

	private String contentdescription = null;

	private String eoscDatasourceType = null;

	private String compatibility;

	private String compatibilityOverride;

	private Integer lastCollectionTotal;

	private LocalDateTime lastCollectionDate;

	private Integer lastAggregationTotal;

	private LocalDateTime lastAggregationDate;

	private Integer lastDownloadTotal;

	private LocalDateTime lastDownloadDate;

	private String baseurl;

	protected Boolean removable = false;

	private Set<ApiParamDetails> apiParams;

	private String metadataIdentifierPath = "";

	private String typology = null;

	public String getId() {
		return id;
	}

	public String getProtocol() {
		return protocol;
	}

	public String getDatasource() {
		return datasource;
	}

	public String getContentdescription() {
		return contentdescription;
	}

	public String getCompatibility() {
		return compatibility;
	}

	public Integer getLastCollectionTotal() {
		return lastCollectionTotal;
	}

	public LocalDateTime getLastCollectionDate() {
		return lastCollectionDate;
	}

	public Integer getLastAggregationTotal() {
		return lastAggregationTotal;
	}

	public LocalDateTime getLastAggregationDate() {
		return lastAggregationDate;
	}

	public Integer getLastDownloadTotal() {
		return lastDownloadTotal;
	}

	public LocalDateTime getLastDownloadDate() {
		return lastDownloadDate;
	}

	public String getBaseurl() {
		return baseurl;
	}

	public ApiDetails setId(final String id) {
		this.id = id;
		return this;
	}

	public ApiDetails setProtocol(final String protocol) {
		this.protocol = protocol;
		return this;
	}

	public ApiDetails setDatasource(final String datasource) {
		this.datasource = datasource;
		return this;
	}

	public ApiDetails setContentdescription(final String contentdescription) {
		this.contentdescription = contentdescription;
		return this;
	}

	public ApiDetails setCompatibility(final String compatibility) {
		this.compatibility = compatibility;
		return this;
	}

	public ApiDetails setLastCollectionTotal(final Integer lastCollectionTotal) {
		this.lastCollectionTotal = lastCollectionTotal;
		return this;
	}

	public ApiDetails setLastCollectionDate(final LocalDateTime lastCollectionDate) {
		this.lastCollectionDate = lastCollectionDate;
		return this;
	}

	public ApiDetails setLastAggregationTotal(final Integer lastAggregationTotal) {
		this.lastAggregationTotal = lastAggregationTotal;
		return this;
	}

	public ApiDetails setLastAggregationDate(final LocalDateTime lastAggregationDate) {
		this.lastAggregationDate = lastAggregationDate;
		return this;
	}

	public ApiDetails setLastDownloadTotal(final Integer lastDownloadTotal) {
		this.lastDownloadTotal = lastDownloadTotal;
		return this;
	}

	public ApiDetails setLastDownloadDate(final LocalDateTime lastDownloadDate) {
		this.lastDownloadDate = lastDownloadDate;
		return this;
	}

	public ApiDetails setBaseurl(final String baseurl) {
		this.baseurl = baseurl;
		return this;
	}

	public Set<ApiParamDetails> getApiParams() {
		return apiParams;
	}

	public void setApiParams(final Set<ApiParamDetails> apiParams) {
		this.apiParams = apiParams;
	}

	public String getCompatibilityOverride() {
		return compatibilityOverride;
	}

	public ApiDetails setCompatibilityOverride(final String compatibilityOverride) {
		this.compatibilityOverride = compatibilityOverride;
		return this;
	}

	public Boolean getRemovable() {
		return removable;
	}

	public ApiDetails setRemovable(final Boolean removable) {
		this.removable = removable;
		return this;
	}

	public String getMetadataIdentifierPath() {
		return metadataIdentifierPath;
	}

	public ApiDetails setMetadataIdentifierPath(final String metadataIdentifierPath) {
		this.metadataIdentifierPath = metadataIdentifierPath;
		return this;
	}

	public String getEoscDatasourceType() {
		return eoscDatasourceType;
	}

	public ApiDetails setEoscDatasourceType(final String eoscDatasourceType) {
		this.eoscDatasourceType = eoscDatasourceType;
		return this;
	}

	public String getTypology() {
		return typology;
	}

	public ApiDetails setTypology(final String typology) {
		this.typology = typology;
		return this;
	}
}
