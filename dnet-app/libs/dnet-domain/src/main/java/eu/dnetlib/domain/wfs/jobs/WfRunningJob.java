package eu.dnetlib.domain.wfs.jobs;

import java.time.LocalDateTime;

import org.hibernate.annotations.Type;

import eu.dnetlib.domain.wfs.graph.runtime.RuntimeGraph;
import io.hypersistence.utils.hibernate.type.json.JsonType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "wf_runtime")
public class WfRunningJob extends AbstractJob {

	private static final long serialVersionUID = -3163064347762609210L;

	@Column(name = "wf_executor")
	private String wfExecutor;

	@Column(name = "priority")
	private int priority;

	@Column(name = "last_update")
	private LocalDateTime lastUpdate;

	@Type(JsonType.class)
	@Column(name = "graph")
	private RuntimeGraph graph;

	public String getWfExecutor() {
		return this.wfExecutor;
	}

	public void setWfExecutor(final String wfExecutor) {
		this.wfExecutor = wfExecutor;
	}

	public int getPriority() {
		return this.priority;
	}

	public void setPriority(final int priority) {
		this.priority = priority;
	}

	public LocalDateTime getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(final LocalDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public RuntimeGraph getGraph() {
		return this.graph;
	}

	public void setGraph(final RuntimeGraph graph) {
		this.graph = graph;
	}

}
