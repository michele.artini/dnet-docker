package eu.dnetlib.domain.mdstore;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import org.hibernate.annotations.Type;

import io.hypersistence.utils.hibernate.type.json.JsonType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "mdstore_versions")
public class MDStoreVersion implements Serializable {

	/** */
	private static final long serialVersionUID = -4763494442274298339L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "mdstore")
	private String mdstore;

	@Column(name = "writing")
	private boolean writing;

	@Column(name = "readcount")
	private int readCount = 0;

	@Column(name = "lastupdate")
	private LocalDateTime lastUpdate;

	@Column(name = "size")
	private long size = 0;

	@Type(JsonType.class)
	@Column(name = "params")
	private Map<String, Object> params = new LinkedHashMap<>();

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getMdstore() {
		return mdstore;
	}

	public void setMdstore(final String mdstore) {
		this.mdstore = mdstore;
	}

	public boolean isWriting() {
		return writing;
	}

	public void setWriting(final boolean writing) {
		this.writing = writing;
	}

	public int getReadCount() {
		return readCount;
	}

	public void setReadCount(final int readCount) {
		this.readCount = readCount;
	}

	public LocalDateTime getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(final LocalDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public long getSize() {
		return size;
	}

	public void setSize(final long size) {
		this.size = size;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(final Map<String, Object> params) {
		this.params = params;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof MDStoreVersion)) { return false; }
		final MDStoreVersion other = (MDStoreVersion) obj;
		return Objects.equals(id, other.id);
	}

}
