package eu.dnetlib.domain.dsm.readonly;

import java.io.Serializable;
import java.util.Set;

import org.hibernate.annotations.Type;

import io.hypersistence.utils.hibernate.type.json.JsonType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "dsm_datasources_view")
public class SimpleDsWithApis implements Serializable {

	private static final long serialVersionUID = 278825021454427898L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "other_name")
	private String otherName;

	@Column(name = "nsprefix")
	private String nsprefix;

	@Column(name = "url")
	private String websiteUrl;

	@Column(name = "type")
	private String type;

	@Column(name = "consenttermsofuse")
	private Boolean consenttermsofuse;

	@Column(name = "fulltextdownload")
	private Boolean fulltextdownload;

	@Column(name = "collectedfrom")
	private String collectedFrom;

	@Type(JsonType.class)
	@Column(name = "organizations")
	private Set<SimpleOrganization> organizations;

	@Type(JsonType.class)
	@Column(name = "apis")
	private Set<SimpleApi> apis;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getOtherName() {
		return otherName;
	}

	public void setOtherName(final String otherName) {
		this.otherName = otherName;
	}

	public String getNsprefix() {
		return nsprefix;
	}

	public void setNsprefix(final String nsprefix) {
		this.nsprefix = nsprefix;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(final String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public Boolean getConsenttermsofuse() {
		return consenttermsofuse;
	}

	public void setConsenttermsofuse(final Boolean consenttermsofuse) {
		this.consenttermsofuse = consenttermsofuse;
	}

	public Boolean getFulltextdownload() {
		return fulltextdownload;
	}

	public void setFulltextdownload(final Boolean fulltextdownload) {
		this.fulltextdownload = fulltextdownload;
	}

	public String getCollectedFrom() {
		return collectedFrom;
	}

	public void setCollectedFrom(final String collectedFrom) {
		this.collectedFrom = collectedFrom;
	}

	public Set<SimpleOrganization> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(final Set<SimpleOrganization> organizations) {
		this.organizations = organizations;
	}

	public Set<SimpleApi> getApis() {
		return apis;
	}

	public void setApis(final Set<SimpleApi> apis) {
		this.apis = apis;
	}

}
