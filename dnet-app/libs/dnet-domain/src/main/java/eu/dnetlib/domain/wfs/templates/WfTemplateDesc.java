package eu.dnetlib.domain.wfs.templates;

import java.io.Serializable;

public class WfTemplateDesc extends WfTemplate implements Serializable {

	private static final long serialVersionUID = -7651246011953954481L;

	private String id;
	private String name;
	private String description;

	public String getId() {
		return this.id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

}
