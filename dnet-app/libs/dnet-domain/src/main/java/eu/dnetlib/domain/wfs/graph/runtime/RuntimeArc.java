package eu.dnetlib.domain.wfs.graph.runtime;

public class RuntimeArc {

	private String from;
	private String to;
	private String condition;
	private boolean completed = false;

	public RuntimeArc() {}

	public RuntimeArc(final String from, final String to, final String condition) {
		this.from = from;
		this.to = to;
		this.condition = condition;
	}

	public String getFrom() {
		return this.from;
	}

	public void setFrom(final String from) {
		this.from = from;
	}

	public String getTo() {
		return this.to;
	}

	public void setTo(final String to) {
		this.to = to;
	}

	public String getCondition() {
		return this.condition;
	}

	public void setCondition(final String condition) {
		this.condition = condition;
	}

	@Override
	public String toString() {
		return String.format("[ %s -> %s ]", this.from, this.to);
	}

	public boolean isCompleted() {
		return this.completed;
	}

	public void setCompleted(final boolean completed) {
		this.completed = completed;
	}

}
