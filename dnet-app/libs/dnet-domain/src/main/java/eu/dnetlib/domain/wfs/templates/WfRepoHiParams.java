package eu.dnetlib.domain.wfs.templates;

import java.io.Serializable;

public class WfRepoHiParams implements Serializable {

	private static final long serialVersionUID = 4130982720455226772L;

	private String dsId;
	private String dsName;
	private String apiId;

	public String getDsId() {
		return dsId;
	}

	public void setDsId(final String dsId) {
		this.dsId = dsId;
	}

	public String getDsName() {
		return dsName;
	}

	public void setDsName(final String dsName) {
		this.dsName = dsName;
	}

	public String getApiId() {
		return apiId;
	}

	public void setApiId(final String apiId) {
		this.apiId = apiId;
	}

}
