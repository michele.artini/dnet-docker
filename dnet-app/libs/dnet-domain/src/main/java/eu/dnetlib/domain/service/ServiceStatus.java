package eu.dnetlib.domain.service;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;

import org.apache.commons.lang3.StringUtils;

public class ServiceStatus implements Serializable {

	private static final long serialVersionUID = -4913228769231373286L;

	private String name;

	private ServiceType type;

	private String baseUrl;

	private LocalDateTime date;

	public ServiceStatus() {}

	public ServiceStatus(final ServiceType type, final String baseUrl) throws MalformedURLException {
		this.type = type;
		this.baseUrl = baseUrl;

		date = LocalDateTime.now();

		final URL url = new URL(baseUrl);

		final StringBuilder sb = new StringBuilder();
		sb.append(type.getPrefix());

		for (final String p : url.getHost().split("\\.")) {
			sb.append("-");
			sb.append(StringUtils.leftPad(p, 3, "0"));
		}
		sb.append("-");
		sb.append(url.getPort());

		name = sb.toString();
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public ServiceType getType() {
		return type;
	}

	public void setType(final ServiceType type) {
		this.type = type;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setBaseUrl(final String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public void setDate(final LocalDateTime date) {
		this.date = date;
	}

}
