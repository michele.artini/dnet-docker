package eu.dnetlib.domain.mdstore;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import org.hibernate.annotations.Type;

import io.hypersistence.utils.hibernate.type.json.JsonType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "mdstores_with_info")
public class MDStoreWithInfo implements Serializable {

	/** */
	private static final long serialVersionUID = -8445784770687571492L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "format")
	private String format;

	@Column(name = "layout")
	private String layout;

	@Column(name = "interpretation")
	private String interpretation;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private MDStoreType type;

	@Column(name = "datasource_name")
	private String datasourceName;

	@Column(name = "datasource_id")
	private String datasourceId;

	@Column(name = "api_id")
	private String apiId;

	@Column(name = "current_version")
	private String currentVersion;

	@Column(name = "creation_date")
	private LocalDateTime creationDate;

	@Column(name = "lastupdate")
	private LocalDateTime lastUpdate;

	@Column(name = "size")
	private long size = 0;

	@Column(name = "n_versions")
	private long numberOfVersions = 0;

	@Type(JsonType.class)
	@Column(name = "params")
	private Map<String, Object> params = new LinkedHashMap<>();

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(final String format) {
		this.format = format;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(final String layout) {
		this.layout = layout;
	}

	public String getInterpretation() {
		return interpretation;
	}

	public void setInterpretation(final String interpretation) {
		this.interpretation = interpretation;
	}

	public MDStoreType getType() {
		return type;
	}

	public void setType(final MDStoreType type) {
		this.type = type;
	}

	public String getDatasourceName() {
		return datasourceName;
	}

	public void setDatasourceName(final String datasourceName) {
		this.datasourceName = datasourceName;
	}

	public String getDatasourceId() {
		return datasourceId;
	}

	public void setDatasourceId(final String datasourceId) {
		this.datasourceId = datasourceId;
	}

	public String getApiId() {
		return apiId;
	}

	public void setApiId(final String apiId) {
		this.apiId = apiId;
	}

	public String getCurrentVersion() {
		return currentVersion;
	}

	public void setCurrentVersion(final String currentVersion) {
		this.currentVersion = currentVersion;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(final LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(final LocalDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public long getSize() {
		return size;
	}

	public void setSize(final long size) {
		this.size = size;
	}

	public long getNumberOfVersions() {
		return numberOfVersions;
	}

	public void setNumberOfVersions(final long numberOfVersions) {
		this.numberOfVersions = numberOfVersions;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(final Map<String, Object> params) {
		this.params = params;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof MDStoreWithInfo)) { return false; }
		final MDStoreWithInfo other = (MDStoreWithInfo) obj;
		return Objects.equals(id, other.id);
	}

}
