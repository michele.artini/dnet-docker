package eu.dnetlib.domain.dsm.info;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonAutoDetect
public class Header {

	private long total;

	private int page;

	private int size;

	private long time;

	private int statusCode;

	private List<String> errors = new ArrayList<>();

	@JsonIgnore
	private Queue<Throwable> exceptions = new LinkedList<>();

	public static Header newInsance() {
		return new Header();
	}

	public Header() {}

	public long getTime() {
		return time;
	}

	public Header setTime(final long time) {
		this.time = time;
		return this;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public Header setStatusCode(final int statusCode) {
		this.statusCode = statusCode;
		return this;
	}

	public long getTotal() {
		return total;
	}

	public int getPage() {
		return page;
	}

	public int getSize() {
		return size;
	}

	public Header setPage(final int page) {
		this.page = page;
		return this;
	}

	public Header setSize(final int size) {
		this.size = size;
		return this;
	}

	public Header setTotal(final long total) {
		this.total = total;
		return this;
	}

	public Queue<Throwable> getExceptions() {
		return exceptions;
	}

	public Header setExceptions(final Queue<Throwable> exceptions) {
		this.exceptions = exceptions;
		return this;
	}

	public List<String> getErrors() {
		return getExceptions().stream()
			.map(Throwable::getMessage)
			.collect(Collectors.toList());
	}

	public Header setErrors(final List<String> errors) {
		this.errors = errors;
		return this;
	}

	public String toJson() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (final JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

}
