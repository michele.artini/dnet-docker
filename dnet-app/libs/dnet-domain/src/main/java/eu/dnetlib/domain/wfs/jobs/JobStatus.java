package eu.dnetlib.domain.wfs.jobs;

public enum JobStatus {
	created,
	accepted,
	running,
	success,
	failure,
	killed;
}
