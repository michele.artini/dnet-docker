package eu.dnetlib.domain.vocabulary;

import java.io.Serializable;
import java.util.Objects;

public class VocabularyTermPK implements Serializable {

	private static final long serialVersionUID = -7726004241883017275L;

	private String code;

	private String vocabulary;

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public String getVocabulary() {
		return vocabulary;
	}

	public void setVocabulary(final String vocabulary) {
		this.vocabulary = vocabulary;
	}

	@Override
	public int hashCode() {
		return Objects.hash(code, vocabulary);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof VocabularyTermPK)) { return false; }
		final VocabularyTermPK other = (VocabularyTermPK) obj;
		return Objects.equals(code, other.code) && Objects.equals(vocabulary, other.vocabulary);
	}

	@Override
	public String toString() {
		return String.format("VocabularyTermPK [code=%s, vocabulary=%s]", code, vocabulary);
	}

}
