package eu.dnetlib.domain.mdstore;

import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "mdstore_current_versions")
public class MDStoreCurrentVersion implements Serializable {

	/** */
	private static final long serialVersionUID = -4757725888593745773L;

	@Id
	@Column(name = "mdstore")
	private String mdstore;

	@Column(name = "current_version")
	private String currentVersion;

	public String getMdstore() {
		return mdstore;
	}

	public void setMdstore(final String mdstore) {
		this.mdstore = mdstore;
	}

	public String getCurrentVersion() {
		return currentVersion;
	}

	public void setCurrentVersion(final String currentVersion) {
		this.currentVersion = currentVersion;
	}

	public static MDStoreCurrentVersion newInstance(final String mdId, final String versionId) {
		final MDStoreCurrentVersion cv = new MDStoreCurrentVersion();
		cv.setMdstore(mdId);
		cv.setCurrentVersion(versionId);
		return cv;
	}

	public static MDStoreCurrentVersion newInstance(final MDStoreVersion v) {
		return newInstance(v.getMdstore(), v.getId());
	}

	@Override
	public String toString() {
		return String.format("MDStoreCurrentVersion [mdstore=%s, currentVersion=%s]", mdstore, currentVersion);
	}

	@Override
	public int hashCode() {
		return Objects.hash(currentVersion, mdstore);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof MDStoreCurrentVersion)) { return false; }
		final MDStoreCurrentVersion other = (MDStoreCurrentVersion) obj;
		return Objects.equals(currentVersion, other.currentVersion) && Objects.equals(mdstore, other.mdstore);
	}
}
