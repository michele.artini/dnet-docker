package eu.dnetlib.domain.mdstore.records;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * This class models a record in a Metadata store collection
 */
public class MetadataRecord implements Serializable {

	private static final long serialVersionUID = -226995288313973437L;

	public enum MetadataRecordEncoding {
		XML, JSON
	}

	/** The D-Net Identifier associated to the record */
	private String id;

	/** The original Identifier of the record */
	private String originalId;

	/** The encoding of the record, should be JSON or XML */
	private MetadataRecordEncoding encoding;

	/**
	 * The information about the provenance of the record see @{@link Provenance} for the model of this information
	 */
	private Provenance provenance;

	/** The content of the metadata */
	private String body;

	/** the date when the record has been collected */
	private LocalDateTime dateOfCollection;

	/** the date when the record has been transformed */
	private LocalDateTime dateOfTransformation;

	public MetadataRecord() {}

	public MetadataRecord(
			final String originalId,
			final MetadataRecordEncoding encoding,
			final Provenance provenance,
			final String body,
			final LocalDateTime dateOfCollection) {

		this.originalId = originalId;
		this.encoding = encoding;
		this.provenance = provenance;
		this.body = body;
		this.dateOfCollection = dateOfCollection;
		id = MetadataRecord.generateIdentifier(originalId, this.provenance.getNsPrefix());
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getOriginalId() {
		return originalId;
	}

	public void setOriginalId(final String originalId) {
		this.originalId = originalId;
	}

	public MetadataRecordEncoding getEncoding() {
		return encoding;
	}

	public void setEncoding(final MetadataRecordEncoding encoding) {
		this.encoding = encoding;
	}

	public Provenance getProvenance() {
		return provenance;
	}

	public void setProvenance(final Provenance provenance) {
		this.provenance = provenance;
	}

	public String getBody() {
		return body;
	}

	public void setBody(final String body) {
		this.body = body;
	}

	public LocalDateTime getDateOfCollection() {
		return dateOfCollection;
	}

	public void setDateOfCollection(final LocalDateTime dateOfCollection) {
		this.dateOfCollection = dateOfCollection;
	}

	public LocalDateTime getDateOfTransformation() {
		return dateOfTransformation;
	}

	public void setDateOfTransformation(final LocalDateTime dateOfTransformation) {
		this.dateOfTransformation = dateOfTransformation;
	}

	@Override
	public boolean equals(final Object o) {
		if (!(o instanceof MetadataRecord)) { return false; }
		return ((MetadataRecord) o).getId().equalsIgnoreCase(id);
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	public static String generateIdentifier(final String originalId, final String nsPrefix) {
		return String.format("%s::%s", nsPrefix, DigestUtils.md5Hex(originalId));
	}

}
