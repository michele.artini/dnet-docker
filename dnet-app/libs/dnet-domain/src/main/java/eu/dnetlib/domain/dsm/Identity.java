package eu.dnetlib.domain.dsm;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

/**
 * Created by claudio on 13/04/2017.
 */

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "dsm_identities")
public class Identity implements Serializable {

	private static final long serialVersionUID = 8403208596597224509L;

	@Id
	@Column(name = "pid")
	private String pid;

	@Column(name = "issuertype")
	private String issuertype;

	public String getPid() {
		return pid;
	}

	public void setPid(final String pid) {
		this.pid = pid;
	}

	public String getIssuertype() {
		return issuertype;
	}

	public void setIssuertype(final String issuertype) {
		this.issuertype = issuertype;
	}

}
