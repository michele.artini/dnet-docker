package eu.dnetlib.domain.wfs.subscriptions;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;

@Entity
@Table(name = "wf_subscriptions")
@IdClass(WfSubscriptionPK.class)
public class WfSubscription implements Serializable {

	private static final long serialVersionUID = -3662770213782581404L;

	@Id
	@Column(name = "wf_conf_id")
	private String wfConfigurationId;

	@Id
	@Column(name = "condition")
	@Enumerated(EnumType.STRING)
	private NotificationCondition condition;

	@Id
	@Column(name = "email")
	private String email;

	@Column(name = "message_id")
	private String messageId;

	public String getWfConfigurationId() {
		return wfConfigurationId;
	}

	public void setWfConfigurationId(final String wfConfigurationId) {
		this.wfConfigurationId = wfConfigurationId;
	}

	public NotificationCondition getCondition() {
		return condition;
	}

	public void setCondition(final NotificationCondition condition) {
		this.condition = condition;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(final String messageId) {
		this.messageId = messageId;
	}

}
