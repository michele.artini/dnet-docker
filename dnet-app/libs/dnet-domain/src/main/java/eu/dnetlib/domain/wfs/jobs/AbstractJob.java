package eu.dnetlib.domain.wfs.jobs;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.annotations.Type;

import io.hypersistence.utils.hibernate.type.json.JsonType;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractJob implements Serializable {

	private static final long serialVersionUID = -5402950017251447505L;

	@Id
	@Column(name = "process_id")
	private String processId;

	@Column(name = "name")
	private String name;

	@Column(name = "family")
	private String family;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private JobStatus status;

	@Column(name = "start_date")
	private LocalDateTime startDate;

	@Column(name = "end_date")
	private LocalDateTime endDate;

	@Column(name = "wf_conf_id")
	private String wfConfId;

	@Column(name = "ds_id")
	private String dsId;

	@Column(name = "ds_name")
	private String dsName;

	@Column(name = "ds_api")
	private String apiId;

	@Type(JsonType.class)
	@Column(name = "input")
	private Map<String, Object> inputParams = new HashMap<>();

	@Type(JsonType.class)
	@Column(name = "output")
	private Map<String, Object> outputParams = new HashMap<>();

	public String getProcessId() {
		return this.processId;
	}

	public void setProcessId(final String processId) {
		this.processId = processId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getFamily() {
		return this.family;
	}

	public void setFamily(final String family) {
		this.family = family;
	}

	public JobStatus getStatus() {
		return this.status;
	}

	public void setStatus(final JobStatus status) {
		this.status = status;
	}

	public LocalDateTime getStartDate() {
		return this.startDate;
	}

	public void setStartDate(final LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return this.endDate;
	}

	public void setEndDate(final LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public String getWfConfId() {
		return this.wfConfId;
	}

	public void setWfConfId(final String wfConfId) {
		this.wfConfId = wfConfId;
	}

	public String getDsId() {
		return this.dsId;
	}

	public void setDsId(final String dsId) {
		this.dsId = dsId;
	}

	public String getDsName() {
		return this.dsName;
	}

	public void setDsName(final String dsName) {
		this.dsName = dsName;
	}

	public String getApiId() {
		return this.apiId;
	}

	public void setApiId(final String apiId) {
		this.apiId = apiId;
	}

	public Map<String, Object> getInputParams() {
		return this.inputParams;
	}

	public void setInputParams(final Map<String, Object> inputParams) {
		this.inputParams = inputParams;
	}

	public Map<String, Object> getOutputParams() {
		return this.outputParams;
	}

	public void setOutputParams(final Map<String, Object> outputParams) {
		this.outputParams = outputParams;
	}

}
