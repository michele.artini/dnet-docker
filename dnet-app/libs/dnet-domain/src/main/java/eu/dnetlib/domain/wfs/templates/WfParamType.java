package eu.dnetlib.domain.wfs.templates;

public enum WfParamType {
	STRING,
	NUMBER,
	DATE,
	BOOLEAN
}
