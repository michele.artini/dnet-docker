package eu.dnetlib.domain.index;

import java.io.Serializable;
import java.util.Objects;

public class IndexFieldPK implements Serializable {

	private static final long serialVersionUID = -2193243559770663481L;

	private String indexId;

	private String name;

	public IndexFieldPK() {}

	public IndexFieldPK(final String indexId, final String name) {
		this.indexId = indexId;
		this.name = name;
	}

	public String getIndexId() {
		return indexId;
	}

	public void setIndexId(final String indexId) {
		this.indexId = indexId;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, indexId);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if ((obj == null) || (getClass() != obj.getClass())) { return false; }
		final IndexFieldPK other = (IndexFieldPK) obj;
		return Objects.equals(name, other.name) && Objects.equals(indexId, other.indexId);
	}

	@Override
	public String toString() {
		return "IndexFieldPK [indexId=" + indexId + ", name=" + name + "]";
	}
}
