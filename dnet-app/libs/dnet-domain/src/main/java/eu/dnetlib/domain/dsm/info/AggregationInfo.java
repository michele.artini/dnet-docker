package eu.dnetlib.domain.dsm.info;

public abstract class AggregationInfo {

	private long numberOfRecords;

	private String date;

	private AggregationStage aggregationStage;

	private boolean indexedVersion = false;

	private boolean completedSuccessfully = true;

	public AggregationInfo() {}

	public long getNumberOfRecords() {
		return numberOfRecords;
	}

	public void setNumberOfRecords(final long numberOfRecords) {
		this.numberOfRecords = numberOfRecords;
	}

	public String getDate() {
		return date;
	}

	public void setDate(final String date) {
		this.date = date;
	}

	public AggregationStage getAggregationStage() {
		return aggregationStage;
	}

	public void setAggregationStage(final AggregationStage aggregationStage) {
		this.aggregationStage = aggregationStage;
	}

	public boolean isIndexedVersion() {
		return indexedVersion;
	}

	public void setIndexedVersion(final boolean indexedVersion) {
		this.indexedVersion = indexedVersion;
	}

	public boolean isCompletedSuccessfully() {
		return completedSuccessfully;
	}

	public void setCompletedSuccessfully(final boolean completedSuccessfully) {
		this.completedSuccessfully = completedSuccessfully;
	}
}
