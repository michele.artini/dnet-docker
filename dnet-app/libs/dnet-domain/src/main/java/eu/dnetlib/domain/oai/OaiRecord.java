package eu.dnetlib.domain.oai;

public interface OaiRecord<T, K> extends OaiIdentifier<T> {

	K getBody();

}
