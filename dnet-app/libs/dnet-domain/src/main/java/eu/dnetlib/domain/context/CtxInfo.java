package eu.dnetlib.domain.context;

import java.io.Serializable;

import org.hibernate.annotations.Type;

import io.hypersistence.utils.hibernate.type.json.JsonType;
import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;

@MappedSuperclass
public abstract class CtxInfo implements Serializable {

	private static final long serialVersionUID = 4912082158208138795L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "label")
	private String label;

	@Type(JsonType.class)
	@Column(name = "params")
	private Parameter[] parameters;

	@Transient
	private long nChilds = 0;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public Parameter[] getParameters() {
		return parameters;
	}

	public void setParameters(final Parameter[] parameters) {
		this.parameters = parameters;
	}

	public long getnChilds() {
		return nChilds;
	}

	public void setnChilds(final long nChilds) {
		this.nChilds = nChilds;
	}

}
