package eu.dnetlib.domain.wfs;

public class WorkflowsConstants {

	// TODO (LOW PRIORITY): remove unused constants

	public static final String SUCCESS_NODE = "success";

	public static final String DATASOURCE_PREFIX = "datasource:";

	public static final String LOG_WF_NAME = "system:wfName";

	public static final String LOG_WF_FAMILY = "system:family";
	public static final String LOG_WF_PROCESS_ID = "system:processId";
	public static final String LOG_WF_PROCESS_STATUS = "system:processStatus";
	public static final String LOG_WF_PROCESS_START_DATE = "system:startDate";
	public static final String LOG_WF_PROCESS_END_DATE = "system:endDate";
	public static final String LOG_WF_PARENT = "system:parentProfileId";

	public static final String LOG_SYSTEM_ERROR = "system:error";
	public static final String LOG_SYSTEM_ERROR_STACKTRACE = "system:error:stacktrace";

	public static final String LOG_DATASOURCE_NAME = WorkflowsConstants.DATASOURCE_PREFIX + "name";
	public static final String LOG_DATASOURCE_ID = WorkflowsConstants.DATASOURCE_PREFIX + "id";
	public static final String LOG_DATASOURCE_INTERFACE = WorkflowsConstants.DATASOURCE_PREFIX + "interface";

	public static final String BLACKBOARD_IS_BLACKBOARD = "blackboard:isBlackboard";
	public static final String BLACKBOARD_JOB = "blackboard:job";
	public static final String BLACKBOARD_SERVICE_ID = "blackboard:serviceId";
	public static final String BLACKBOARD_IS_GOING = "blackboard:isOngoing";
	public static final String BLACKBOARD_PARAM_PREFIX = "blackboard:param:";

	// public static final String DATASOURCE_ACRONYM = WorkflowsConstants.DATASOURCE_PREFIX + "acronym";
	// public static final String DATASOURCE_URL = WorkflowsConstants.DATASOURCE_PREFIX + "url";

	public static final int MIN_WF_PRIORITY = 0;
	public static final int MAX_WF_PRIORITY = 100;
	public static final int DEFAULT_WF_PRIORITY = 50;

	public static final String MAIN_LOG_PREFIX = "mainlog:";

	public static final String REPO_HI_FAMILY = "REPO-HI";
	public static final String REPO_BYE_FAMILY = "REPO-BYE";
	public static final String GENERIC_DESTROY_WF_FAMILY = "DESTROY-WF";
	public static final String AGGREGATION_FAMILY = "aggregation";
	public static final String UNKNOWN_FAMILY = "UNKNOWN";

	public static final String WF_CREATED_NOTIFICATION_CHANNEL = "CREATED_WF";

	// It must be NULL because aggreagation wfs are exposed by the DSM UI and not by the Workflows UI
	public static final String AGGREGATION_WF_CONFS_SECTION = null;

	public enum WorkflowStatus {

		EXECUTABLE("Executable", "icon-ok"),
		WAIT_USER_SETTINGS("Waiting user settings", "icon-edit"),
		WAIT_SYS_SETTINGS(
				"Waiting system settings",
				"icon-refresh");

		public String displayName;
		public String icon;

		WorkflowStatus(final String displayName, final String icon) {
			this.displayName = displayName;
			this.icon = icon;
		}
	}

}
