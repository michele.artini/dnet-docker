package eu.dnetlib.domain.context;

import java.io.Serializable;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

public class Parameter implements Serializable, Comparable<Parameter> {

	private static final long serialVersionUID = 925189891223642100L;

	private String name;
	private String value;

	public Parameter() {}

	public Parameter(final String name, final String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, value);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof Parameter)) { return false; }
		final Parameter other = (Parameter) obj;
		return Objects.equals(name, other.name) && Objects.equals(value, other.value);
	}

	@Override
	public String toString() {
		return String.format("Parameter [name=%s, value=%s]", name, value);
	}

	@Override
	public int compareTo(final Parameter p) {
		return StringUtils.compare(getName(), p.getName());
	}

}
