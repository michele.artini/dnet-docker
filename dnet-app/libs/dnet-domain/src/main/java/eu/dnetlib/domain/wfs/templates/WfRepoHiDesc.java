package eu.dnetlib.domain.wfs.templates;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class WfRepoHiDesc extends WfTemplateDesc implements Serializable {

	private static final long serialVersionUID = -5192964707849869720L;

	private Set<String> expectedEoscDsTypes = new HashSet<>();
	private Set<String> expectedCompliances = new HashSet<>();

	public Set<String> getExpectedEoscDsTypes() {
		return this.expectedEoscDsTypes;
	}

	public void setExpectedEoscDsTypes(final Set<String> expectedEoscDsTypes) {
		this.expectedEoscDsTypes = expectedEoscDsTypes;
	}

	public Set<String> getExpectedCompliances() {
		return this.expectedCompliances;
	}

	public void setExpectedCompliances(final Set<String> expectedCompliances) {
		this.expectedCompliances = expectedCompliances;
	}

}
