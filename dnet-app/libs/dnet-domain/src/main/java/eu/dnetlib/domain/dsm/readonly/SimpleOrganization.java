package eu.dnetlib.domain.dsm.readonly;

import java.io.Serializable;
import java.util.Objects;

public class SimpleOrganization implements Serializable {

	private static final long serialVersionUID = 3604149230859161834L;

	private String name;
	private String country;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof SimpleOrganization)) { return false; }
		final SimpleOrganization other = (SimpleOrganization) obj;
		return Objects.equals(name, other.name);
	}

}
