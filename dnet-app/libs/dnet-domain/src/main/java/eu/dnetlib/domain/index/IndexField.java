package eu.dnetlib.domain.index;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;

@Entity
@IdClass(IndexFieldPK.class)
@Table(name = "index_fields")
public class IndexField implements Serializable {

	private static final long serialVersionUID = -1414722965422426989L;

	@Id
	@Column(name = "idx_id")
	private String indexId;

	@Id
	@Column(name = "name")
	private String name;

	@Column(name = "constant")
	private String constant;

	@Column(name = "xpath")
	private String xpath;

	@Column(name = "result")
	private boolean result;

	public String getIndexId() {
		return indexId;
	}

	public void setIndexId(final String indexId) {
		this.indexId = indexId;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getXpath() {
		return xpath;
	}

	public void setXpath(final String xpath) {
		this.xpath = xpath;
	}

	public String getConstant() {
		return constant;
	}

	public void setConstant(final String constant) {
		this.constant = constant;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(final boolean result) {
		this.result = result;
	}

}
