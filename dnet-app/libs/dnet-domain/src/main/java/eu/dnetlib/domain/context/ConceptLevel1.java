package eu.dnetlib.domain.context;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "context_cat_concepts_lvl_1")
public class ConceptLevel1 extends CtxChildInfo {

	private static final long serialVersionUID = -759398689766245784L;

}
