package eu.dnetlib.domain.wfs.graph;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class NodeOutputParam implements Serializable {

	private static final long serialVersionUID = -7995475104154153199L;

	private String name;

	private String env;

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getEnv() {
		return this.env;
	}

	public void setEnv(final String env) {
		this.env = env;
	}

}
