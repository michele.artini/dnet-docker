package eu.dnetlib.domain.wfs.graph;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Arc implements Serializable {

	private static final long serialVersionUID = 7866138976929522262L;

	private String to;
	private String condition;

	public Arc() {}

	public Arc(final String to) {
		this.to = to;
	}

	public Arc(final String to, final String condition) {
		this.to = to;
		this.condition = condition;
	}

	public String getTo() {
		return this.to;
	}

	public void setTo(final String to) {
		this.to = to;
	}

	public String getCondition() {
		return this.condition;
	}

	public void setCondition(final String condition) {
		this.condition = condition;
	}
}
