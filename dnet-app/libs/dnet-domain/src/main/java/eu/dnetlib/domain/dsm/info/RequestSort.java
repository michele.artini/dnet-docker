package eu.dnetlib.domain.dsm.info;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public enum RequestSort {
	id, officialname, dateofvalidation, registrationdate
}
