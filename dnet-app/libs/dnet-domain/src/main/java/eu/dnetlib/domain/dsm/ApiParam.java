package eu.dnetlib.domain.dsm;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "dsm_apiparams")
@IdClass(ApiParamPK.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiParam implements Serializable {

	private static final long serialVersionUID = -8548855970690750005L;

	@Id
	@ManyToOne
	@JoinColumn(name = "api")
	private final Api api = null;

	@Id
	private String param;

	@Column(name = "value")
	private String value;

	public String getParam() {
		return param;
	}

	public void setParam(final String param) {
		this.param = param;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	@JsonIgnore
	public Api getApi() {
		return api;
	}

}
