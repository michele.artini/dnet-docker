package eu.dnetlib.domain.oai;

import java.io.Serializable;
import java.util.List;

public interface OaiIdentifier<T> extends Serializable {

	String getId();

	T getDate();

	List<String> getSets();

	boolean isDeleted();

}
