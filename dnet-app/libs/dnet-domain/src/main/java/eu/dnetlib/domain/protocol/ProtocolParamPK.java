package eu.dnetlib.domain.protocol;

import java.io.Serializable;
import java.util.Objects;

public class ProtocolParamPK implements Serializable {

	private static final long serialVersionUID = 3126524893218480136L;

	private String protocol;
	private String name;

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(final String protocol) {
		this.protocol = protocol;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, protocol);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof ProtocolParamPK)) { return false; }
		final ProtocolParamPK other = (ProtocolParamPK) obj;
		return Objects.equals(name, other.name) && Objects.equals(protocol, other.protocol);
	}

	@Override
	public String toString() {
		return String.format("ProtocolParamPK [protocol=%s, name=%s]", protocol, name);
	}

}
