package eu.dnetlib.domain.dsm.info;

public enum FilterType {
	exact,
	search,
	searchOrgs,
	multiSearch
}
