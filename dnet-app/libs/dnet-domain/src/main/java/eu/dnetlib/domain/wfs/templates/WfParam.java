package eu.dnetlib.domain.wfs.templates;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class WfParam implements Serializable {

	private static final long serialVersionUID = 5885589803738655166L;

	private String name;
	private String description;
	private WfParamType type = WfParamType.STRING;
	private String defaultValue;
	private List<String> validTerms = new ArrayList<>();
	private boolean required = true;
	private WfParamValidTermsSupplier validTermsSupplier;

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public WfParamType getType() {
		return this.type;
	}

	public void setType(final WfParamType type) {
		this.type = type;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(final String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public boolean isRequired() {
		return this.required;
	}

	public void setRequired(final boolean required) {
		this.required = required;
	}

	public List<String> getValidTerms() {
		return this.validTerms;
	}

	public void setValidTerms(final List<String> validTerms) {
		this.validTerms = validTerms;
	}

	public WfParamValidTermsSupplier getValidTermsSupplier() {
		return this.validTermsSupplier;
	}

	public void setValidTermsSupplier(final WfParamValidTermsSupplier validTermsSupplier) {
		this.validTermsSupplier = validTermsSupplier;
	}

}
