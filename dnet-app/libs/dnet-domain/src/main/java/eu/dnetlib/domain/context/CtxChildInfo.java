package eu.dnetlib.domain.context;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class CtxChildInfo extends CtxInfo {

	private static final long serialVersionUID = -9005145235908917048L;

	@Column(name = "claim")
	private boolean claim;

	@Column(name = "parent")
	private String parent;

	public boolean isClaim() {
		return claim;
	}

	public void setClaim(final boolean claim) {
		this.claim = claim;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(final String parent) {
		this.parent = parent;
	}

}
