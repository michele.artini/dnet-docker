package eu.dnetlib.domain.wfs.graph;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class NodeInputParam implements Serializable {

	private static final long serialVersionUID = 7815785723401725707L;

	private String name;

	/**
	 * a constant value
	 */
	private String value;

	/**
	 * a global param
	 */
	private String ref;

	/**
	 * a system property
	 */
	private String property;

	/**
	 * a property from the WorkFlow ENV
	 */
	private String env;

	public static NodeInputParam newRefParam(final String name, final String ref) {
		final NodeInputParam np = new NodeInputParam();
		np.setName(name);
		np.setRef(ref);
		return np;
	}

	public static NodeInputParam newSimpleParam(final String name, final String value) {
		final NodeInputParam np = new NodeInputParam();
		np.setName(name);
		np.setValue(value);
		return np;
	}

	public static NodeInputParam newPropertyParam(final String name, final String property) {
		final NodeInputParam np = new NodeInputParam();
		np.setName(name);
		np.setProperty(property);
		return np;
	}

	public static NodeInputParam newEnvParam(final String name, final String envProp) {
		final NodeInputParam np = new NodeInputParam();
		np.setName(name);
		np.setEnv(envProp);
		return np;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	public String getRef() {
		return this.ref;
	}

	public void setRef(final String ref) {
		this.ref = ref;
	}

	public String getProperty() {
		return this.property;
	}

	public void setProperty(final String property) {
		this.property = property;
	}

	public String getEnv() {
		return this.env;
	}

	public void setEnv(final String env) {
		this.env = env;
	}

}
