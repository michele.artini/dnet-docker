package eu.dnetlib.domain.dsm.info;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class DatasourceIgnoredProperties {

	@JsonIgnore
	protected String od_contenttypes;

	@JsonIgnore
	protected String provenanceaction;

	@JsonIgnore
	protected LocalDate releasestartdate;

	@JsonIgnore
	protected LocalDate releaseenddate;

	@JsonIgnore
	protected String missionstatementurl;

	@JsonIgnore
	protected Boolean dataprovider;

	@JsonIgnore
	protected Boolean serviceprovider;

	@JsonIgnore
	protected String databaseaccesstype;

	@JsonIgnore
	protected String datauploadtype;

	@JsonIgnore
	protected String databaseaccessrestriction;

	@JsonIgnore
	protected String datauploadrestriction;

	@JsonIgnore
	protected Boolean versioning;

	@JsonIgnore
	protected String citationguidelineurl;

	@JsonIgnore
	protected String qualitymanagementkind;

	@JsonIgnore
	protected String pidsystems;

	@JsonIgnore
	protected String certificates;

	@JsonIgnore
	protected String eoscType;

	@JsonIgnore
	protected Boolean dedupMainService;

	public String getOd_contenttypes() {
		return od_contenttypes;
	}

	public void setOd_contenttypes(final String od_contenttypes) {
		this.od_contenttypes = od_contenttypes;
	}

	public String getProvenanceaction() {
		return provenanceaction;
	}

	public void setProvenanceaction(final String provenanceaction) {
		this.provenanceaction = provenanceaction;
	}

	public LocalDate getReleasestartdate() {
		return releasestartdate;
	}

	public void setReleasestartdate(final LocalDate releasestartdate) {
		this.releasestartdate = releasestartdate;
	}

	public LocalDate getReleaseenddate() {
		return releaseenddate;
	}

	public void setReleaseenddate(final LocalDate releaseenddate) {
		this.releaseenddate = releaseenddate;
	}

	public String getMissionstatementurl() {
		return missionstatementurl;
	}

	public void setMissionstatementurl(final String missionstatementurl) {
		this.missionstatementurl = missionstatementurl;
	}

	public Boolean getDataprovider() {
		return dataprovider;
	}

	public void setDataprovider(final Boolean dataprovider) {
		this.dataprovider = dataprovider;
	}

	public Boolean getServiceprovider() {
		return serviceprovider;
	}

	public void setServiceprovider(final Boolean serviceprovider) {
		this.serviceprovider = serviceprovider;
	}

	public String getDatabaseaccesstype() {
		return databaseaccesstype;
	}

	public void setDatabaseaccesstype(final String databaseaccesstype) {
		this.databaseaccesstype = databaseaccesstype;
	}

	public String getDatauploadtype() {
		return datauploadtype;
	}

	public void setDatauploadtype(final String datauploadtype) {
		this.datauploadtype = datauploadtype;
	}

	public String getDatabaseaccessrestriction() {
		return databaseaccessrestriction;
	}

	public void setDatabaseaccessrestriction(final String databaseaccessrestriction) {
		this.databaseaccessrestriction = databaseaccessrestriction;
	}

	public String getDatauploadrestriction() {
		return datauploadrestriction;
	}

	public void setDatauploadrestriction(final String datauploadrestriction) {
		this.datauploadrestriction = datauploadrestriction;
	}

	public Boolean getVersioning() {
		return versioning;
	}

	public void setVersioning(final Boolean versioning) {
		this.versioning = versioning;
	}

	public String getCitationguidelineurl() {
		return citationguidelineurl;
	}

	public void setCitationguidelineurl(final String citationguidelineurl) {
		this.citationguidelineurl = citationguidelineurl;
	}

	public String getQualitymanagementkind() {
		return qualitymanagementkind;
	}

	public void setQualitymanagementkind(final String qualitymanagementkind) {
		this.qualitymanagementkind = qualitymanagementkind;
	}

	public String getPidsystems() {
		return pidsystems;
	}

	public void setPidsystems(final String pidsystems) {
		this.pidsystems = pidsystems;
	}

	public String getCertificates() {
		return certificates;
	}

	public void setCertificates(final String certificates) {
		this.certificates = certificates;
	}

	public String getEoscType() {
		return eoscType;
	}

	public void setEoscType(final String eoscType) {
		this.eoscType = eoscType;
	}

	public Boolean getDedupMainService() {
		return dedupMainService;
	}

	public void setDedupMainService(final Boolean dedupMainService) {
		this.dedupMainService = dedupMainService;
	}

}
