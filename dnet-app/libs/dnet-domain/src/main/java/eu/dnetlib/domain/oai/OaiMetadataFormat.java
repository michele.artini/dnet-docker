package eu.dnetlib.domain.oai;

import java.io.Serializable;

public interface OaiMetadataFormat extends Serializable {

	String getMetadataPrefix();

	String getMetadataSchema();

	String getMetadataNamespace();

}
