package eu.dnetlib.domain.dsm;

import java.util.HashMap;

import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ApiDesc {

	private String id;

	private String baseUrl;

	private String protocol;

	private String datasourceId;

	private String datasourceName;

	private String nsPrefix;

	private String metadataIdentifierPath;

	private final HashMap<String, String> params = new HashMap<>();

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(final String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(final String protocol) {
		this.protocol = protocol;
	}

	public String getDatasourceId() {
		return datasourceId;
	}

	public void setDatasourceId(final String datasourceId) {
		this.datasourceId = datasourceId;
	}

	public String getDatasourceName() {
		return datasourceName;
	}

	public void setDatasourceName(final String datasourceName) {
		this.datasourceName = datasourceName;
	}

	public String getNsPrefix() {
		return nsPrefix;
	}

	public void setNsPrefix(final String nsPrefix) {
		this.nsPrefix = nsPrefix;
	}

	public String getMetadataIdentifierPath() {
		return metadataIdentifierPath;
	}

	public void setMetadataIdentifierPath(final String metadataIdentifierPath) {
		this.metadataIdentifierPath = metadataIdentifierPath;
	}

	public HashMap<String, String> getParams() {
		return params;
	}

}
