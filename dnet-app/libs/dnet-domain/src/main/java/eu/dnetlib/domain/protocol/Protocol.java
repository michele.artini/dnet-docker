package eu.dnetlib.domain.protocol;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "protocols")
public class Protocol implements Serializable {

	private static final long serialVersionUID = -3448728311377943968L;

	@Id
	@Column(name = "id")
	private String id;

	public Protocol() {}

	public Protocol(final String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

}
