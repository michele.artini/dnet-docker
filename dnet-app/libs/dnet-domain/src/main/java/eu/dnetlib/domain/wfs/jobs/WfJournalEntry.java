package eu.dnetlib.domain.wfs.jobs;

import java.time.LocalDateTime;

import org.hibernate.annotations.Type;

import eu.dnetlib.domain.wfs.graph.runtime.RuntimeGraph;
import io.hypersistence.utils.hibernate.type.json.JsonType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "wf_journal_view")
public class WfJournalEntry extends AbstractJob {

	private static final long serialVersionUID = -326994850248506828L;

	@Column(name = "wf_executor")
	private String wfExecutor;

	@Type(JsonType.class)
	@Column(name = "graph")
	private RuntimeGraph graph;

	@Column(name = "last_update")
	private LocalDateTime lastUpdate;

	public LocalDateTime getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(final LocalDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getWfExecutor() {
		return this.wfExecutor;
	}

	public void setWfExecutor(final String wfExecutor) {
		this.wfExecutor = wfExecutor;
	}

	public RuntimeGraph getGraph() {
		return this.graph;
	}

	public void setGraph(final RuntimeGraph graph) {
		this.graph = graph;
	}

}
