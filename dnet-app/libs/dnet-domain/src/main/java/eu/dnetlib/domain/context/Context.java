package eu.dnetlib.domain.context;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "contexts")
public class Context extends CtxInfo {

	private static final long serialVersionUID = -8981861647499495787L;

	@Column(name = "type")
	private String type;

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

}
