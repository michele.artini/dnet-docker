package eu.dnetlib.domain.dsm.info;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class AggregationHistoryResponse extends Response {

	private List<AggregationInfo> aggregationInfo;

	public AggregationHistoryResponse(final List<AggregationInfo> aggregationInfo) {
		super();
		this.aggregationInfo = aggregationInfo;
	}

	public List<AggregationInfo> getAggregationInfo() {
		return aggregationInfo;
	}

	public void setAggregationInfo(final List<AggregationInfo> aggregationInfo) {
		this.aggregationInfo = aggregationInfo;
	}
}
