package eu.dnetlib.domain.wfs.graph.runtime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class RuntimeGraph implements Serializable {

	private static final long serialVersionUID = 6472378218385082376L;

	private Map<String, RuntimeNode> nodes = new HashMap<>();
	private List<RuntimeArc> arcs = new ArrayList<>();

	public Map<String, RuntimeNode> getNodes() {
		return this.nodes;
	}

	public void setNodes(final Map<String, RuntimeNode> nodes) {
		this.nodes = nodes;
	}

	public List<RuntimeArc> getArcs() {
		return this.arcs;
	}

	public void setArcs(final List<RuntimeArc> arcs) {
		this.arcs = arcs;
	}

	@Override
	public String toString() {
		return "\n************************\nNodes: " + this.nodes + "\nArcs:  " + this.arcs + "\n************************\n";
	}

}
