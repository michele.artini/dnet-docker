package eu.dnetlib.domain.oai;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "oai_conf")
public class OaiConfiguration implements Serializable {

	private static final long serialVersionUID = 4322331956673359927L;

	@Id
	@Column(name = "id")
	private Integer id;

	@Column(name = "repository_name")
	private String repositoryName;

	@Column(name = "public_baseurl")
	private String publicBaseUrl;

	@Column(name = "admin_email")
	private String adminEmail;

	@Column(name = "native_format")
	private String nativeFormat;

	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public String getRepositoryName() {
		return repositoryName;
	}

	public void setRepositoryName(final String repositoryName) {
		this.repositoryName = repositoryName;
	}

	public String getPublicBaseUrl() {
		return publicBaseUrl;
	}

	public void setPublicBaseUrl(final String publicBaseUrl) {
		this.publicBaseUrl = publicBaseUrl;
	}

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(final String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public String getNativeFormat() {
		return nativeFormat;
	}

	public void setNativeFormat(final String nativeFormat) {
		this.nativeFormat = nativeFormat;
	}

}
