package eu.dnetlib.domain.dsm.info;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class SimpleResponse<T> extends Response {

	private List<T> response;

	public List<T> getResponse() {
		return response;
	}

	public SimpleResponse<T> setResponse(final List<T> response) {
		this.response = response;
		return this;
	}
}
