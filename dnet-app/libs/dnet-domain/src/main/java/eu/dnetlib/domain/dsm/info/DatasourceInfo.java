package eu.dnetlib.domain.dsm.info;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
@Schema(name = "Datasource info model", description = "provides information about the datasource and its aggregation status")
public class DatasourceInfo {

	private long indexRecords;

	private long fundedContent;

	private long fulltexts;

	private String lastIndexingDate;

	private String firstHarvestDate;

	private DatasourceDetails datasource;

	private AggregationInfo lastCollection;

	private AggregationInfo lastTransformation;

	private List<AggregationInfo> aggregationHistory;

	public DatasourceInfo() {
		super();
	}

	public DatasourceInfo setIndexRecords(final long indexRecords) {
		this.indexRecords = indexRecords;
		return this;
	}

	public DatasourceInfo setFundedContent(final long fundedContent) {
		this.fundedContent = fundedContent;
		return this;
	}

	public DatasourceInfo setFulltexts(final long fulltexts) {
		this.fulltexts = fulltexts;
		return this;
	}

	public DatasourceInfo setLastIndexingDate(final String lastIndexingDate) {
		this.lastIndexingDate = lastIndexingDate;
		return this;
	}

	public DatasourceInfo setAggregationHistory(final List<AggregationInfo> aggregationHistory) {
		this.aggregationHistory = aggregationHistory;
		return this;
	}

	public DatasourceInfo setLastCollection(final AggregationInfo lastCollection) {
		this.lastCollection = lastCollection;
		return this;
	}

	public DatasourceInfo setLastTransformation(final AggregationInfo lastTransformation) {
		this.lastTransformation = lastTransformation;
		return this;
	}

	public long getIndexRecords() {
		return indexRecords;
	}

	public long getFundedContent() {
		return fundedContent;
	}

	public long getFulltexts() {
		return fulltexts;
	}

	public String getLastIndexingDate() {
		return lastIndexingDate;
	}

	public List<AggregationInfo> getAggregationHistory() {
		return aggregationHistory;
	}

	public AggregationInfo getLastCollection() {
		return lastCollection;
	}

	public AggregationInfo getLastTransformation() {
		return lastTransformation;
	}

	public DatasourceDetails getDatasource() {
		return datasource;
	}

	public DatasourceInfo setDatasource(final DatasourceDetails datasource) {
		this.datasource = datasource;
		return this;
	}

	public String getFirstHarvestDate() {
		return firstHarvestDate;
	}

	public void setFirstHarvestDate(final String firstHarvestDate) {
		this.firstHarvestDate = firstHarvestDate;
	}
}
