package eu.dnetlib.domain.dsm.info;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonAutoDetect
@Schema(name = "Filter name", description = "List of the field names used to filter datasources")
public enum FilterName {

	id,
	managed,
	collectedfrom, // exact match
	officialname,
	englishname,
	websiteurl,
	contactemail,
	registeredby,
	typology,
	eoscDatasourceType,
	platform, // like match
	country; // exact match on related organization

	public static FilterType type(final FilterName filterName) {
		switch (filterName) {
		case id:
		case managed:
		case collectedfrom:
			return FilterType.exact;
		case officialname:
		case englishname:
		case websiteurl:
		case contactemail:
		case registeredby:
		case typology:
		case platform:
			return FilterType.search;
		case eoscDatasourceType:
			return FilterType.multiSearch;
		case country:
			return FilterType.searchOrgs;
		default:
			throw new IllegalStateException("unmapped filter type for: " + filterName);
		}
	}

}
