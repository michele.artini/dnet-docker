package eu.dnetlib.domain.oai;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "oai_sets")
public class ExportedOaiSet implements OaiSet {

	private static final long serialVersionUID = 7405955731269852254L;

	@Id
	@Column(name = "set_spec")
	private String setSpec;

	@Column(name = "set_name")
	private String setName;

	@Column(name = "description")
	private String description;

	@Column(name = "ds_id")
	private String dsId;

	@Override
	public String getSetSpec() {
		return this.setSpec;
	}

	public void setSetSpec(final String setSpec) {
		this.setSpec = setSpec;
	}

	@Override
	public String getSetName() {
		return this.setName;
	}

	public void setSetName(final String setName) {
		this.setName = setName;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getDsId() {
		return this.dsId;
	}

	public void setDsId(final String dsId) {
		this.dsId = dsId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
