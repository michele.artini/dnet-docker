package eu.dnetlib.domain.dsm.info;

import java.time.LocalDate;
import java.util.Set;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

/**
 * Created by claudio on 12/09/16.
 */
@JsonAutoDetect
@Schema(name = "Datasource model", description = "provides information about the datasource")
public class DatasourceDetails extends DatasourceIgnoredProperties {

	@NotBlank
	private String id;

	@Transient
	private String openaireId;

	@NotBlank
	private String officialname;

	@NotBlank
	private String englishname;

	private String websiteurl;

	private String logourl;

	@Email
	private String contactemail;

	private Double latitude;

	private Double longitude;

	private String timezone;

	@NotBlank
	private String namespaceprefix;

	private String languages;

	private LocalDate dateofvalidation;

	@NotBlank
	private String eoscDatasourceType;

	private LocalDate dateofcollection;

	private String platform;

	private String activationId;

	private String description;

	private String issn;

	private String eissn;

	private String lissn;

	@Email
	private String registeredby;

	private String subjects;

	protected String aggregator = "OPENAIRE";

	protected String collectedfrom;

	private Boolean managed;

	private Boolean consentTermsOfUse;

	private Boolean fullTextDownload;

	private LocalDate consentTermsOfUseDate;

	private LocalDate lastConsentTermsOfUseDate;

	private Set<OrganizationDetails> organizations;

	private Set<IdentitiesDetails> identities;

	private String status;

	@Deprecated
	private String typology;

	private LocalDate registrationdate;

	public String getId() {
		return id;
	}

	public String getOpenaireId() {
		return openaireId;
	}

	public String getOfficialname() {
		return officialname;
	}

	public String getEnglishname() {
		return englishname;
	}

	public String getWebsiteurl() {
		return websiteurl;
	}

	public String getLogourl() {
		return logourl;
	}

	public String getContactemail() {
		return contactemail;
	}

	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public String getTimezone() {
		return timezone;
	}

	public String getLanguages() {
		return languages;
	}

	public String getNamespaceprefix() {
		return namespaceprefix;
	}

	public LocalDate getDateofvalidation() {
		return dateofvalidation;
	}

	public String getEoscDatasourceType() {
		return eoscDatasourceType;
	}

	public LocalDate getDateofcollection() {
		return dateofcollection;
	}

	public String getPlatform() {
		return platform;
	}

	public String getActivationId() {
		return activationId;
	}

	public String getDescription() {
		return description;
	}

	public String getIssn() {
		return issn;
	}

	public String getEissn() {
		return eissn;
	}

	public String getLissn() {
		return lissn;
	}

	public String getRegisteredby() {
		return registeredby;
	}

	public String getSubjects() {
		return subjects;
	}

	public String getAggregator() {
		return aggregator;
	}

	public String getCollectedfrom() {
		return collectedfrom;
	}

	public Boolean getManaged() {
		return managed;
	}

	public Boolean getConsentTermsOfUse() {
		return consentTermsOfUse;
	}

	public Boolean getFullTextDownload() {
		return fullTextDownload;
	}

	public Set<OrganizationDetails> getOrganizations() {
		return organizations;
	}

	public Set<IdentitiesDetails> getIdentities() {
		return identities;
	}

	public DatasourceDetails setId(final String id) {
		this.id = id;
		return this;
	}

	public DatasourceDetails setOpenaireId(final String openaireId) {
		this.openaireId = openaireId;
		return this;
	}

	public DatasourceDetails setOfficialname(final String officialname) {
		this.officialname = officialname;
		return this;
	}

	public DatasourceDetails setEnglishname(final String englishname) {
		this.englishname = englishname;
		return this;
	}

	public DatasourceDetails setWebsiteurl(final String websiteurl) {
		this.websiteurl = websiteurl;
		return this;
	}

	public DatasourceDetails setLogourl(final String logourl) {
		this.logourl = logourl;
		return this;
	}

	public DatasourceDetails setContactemail(final String contactemail) {
		this.contactemail = contactemail;
		return this;
	}

	public DatasourceDetails setLatitude(final Double latitude) {
		this.latitude = latitude;
		return this;
	}

	public DatasourceDetails setLongitude(final Double longitude) {
		this.longitude = longitude;
		return this;
	}

	public DatasourceDetails setTimezone(final String timezone) {
		this.timezone = timezone;
		return this;
	}

	public DatasourceDetails setLanguages(final String languages) {
		this.languages = languages;
		return this;
	}

	public DatasourceDetails setNamespaceprefix(final String namespaceprefix) {
		this.namespaceprefix = namespaceprefix;
		return this;
	}

	public DatasourceDetails setDateofvalidation(final LocalDate dateofvalidation) {
		this.dateofvalidation = dateofvalidation;
		return this;
	}

	public DatasourceDetails setEoscDatasourceType(final String eoscDatasourceType) {
		this.eoscDatasourceType = eoscDatasourceType;
		return this;
	}

	public DatasourceDetails setDateofcollection(final LocalDate dateofcollection) {
		this.dateofcollection = dateofcollection;
		return this;
	}

	public DatasourceDetails setPlatform(final String platform) {
		this.platform = platform;
		return this;
	}

	public DatasourceDetails setActivationId(final String activationId) {
		this.activationId = activationId;
		return this;
	}

	public DatasourceDetails setDescription(final String description) {
		this.description = description;
		return this;
	}

	public DatasourceDetails setIssn(final String issn) {
		this.issn = issn;
		return this;
	}

	public DatasourceDetails setEissn(final String eissn) {
		this.eissn = eissn;
		return this;
	}

	public DatasourceDetails setLissn(final String lissn) {
		this.lissn = lissn;
		return this;
	}

	public DatasourceDetails setRegisteredby(final String registeredby) {
		this.registeredby = registeredby;
		return this;
	}

	public DatasourceDetails setSubjects(final String subjects) {
		this.subjects = subjects;
		return this;
	}

	public DatasourceDetails setAggregator(final String aggregator) {
		this.aggregator = aggregator;
		return this;
	}

	public DatasourceDetails setCollectedfrom(final String collectedfrom) {
		this.collectedfrom = collectedfrom;
		return this;
	}

	public DatasourceDetails setManaged(final Boolean managed) {
		this.managed = managed;
		return this;
	}

	public DatasourceDetails setOrganizations(final Set<OrganizationDetails> organizations) {
		this.organizations = organizations;
		return this;
	}

	public DatasourceDetails setIdentities(final Set<IdentitiesDetails> identities) {
		this.identities = identities;
		return this;
	}

	public DatasourceDetails setConsentTermsOfUse(final Boolean consentTermsOfUse) {
		this.consentTermsOfUse = consentTermsOfUse;
		return this;
	}

	public DatasourceDetails setFullTextDownload(final Boolean fullTextDownload) {
		this.fullTextDownload = fullTextDownload;
		return this;
	}

	public LocalDate getConsentTermsOfUseDate() {
		return consentTermsOfUseDate;
	}

	public DatasourceDetails setConsentTermsOfUseDate(final LocalDate consentTermsOfUseDate) {
		this.consentTermsOfUseDate = consentTermsOfUseDate;
		return this;
	}

	public String getStatus() {
		return status;
	}

	public DatasourceDetails setStatus(final String status) {
		this.status = status;
		return this;
	}

	@Deprecated
	public String getTypology() {
		return typology;
	}

	@Deprecated
	public DatasourceDetails setTypology(final String typology) {
		this.typology = typology;
		return this;
	}

	public LocalDate getLastConsentTermsOfUseDate() {
		return lastConsentTermsOfUseDate;
	}

	public DatasourceDetails setLastConsentTermsOfUseDate(final LocalDate lastConsentTermsOfUseDate) {
		this.lastConsentTermsOfUseDate = lastConsentTermsOfUseDate;
		return this;
	}

	public LocalDate getRegistrationdate() {
		return registrationdate;
	}

	public DatasourceDetails setRegistrationdate(final LocalDate registrationdate) {
		this.registrationdate = registrationdate;
		return this;
	}

}
