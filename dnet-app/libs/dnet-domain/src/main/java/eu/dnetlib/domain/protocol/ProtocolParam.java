package eu.dnetlib.domain.protocol;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "protocol_params")
@IdClass(ProtocolParamPK.class)
public class ProtocolParam implements Serializable {

	private static final long serialVersionUID = -8190220047985766140L;

	@Id
	@Column(name = "protocol")
	@JsonIgnore
	private String protocol;

	@Id
	@Column(name = "param_name")
	private String name;

	@Column(name = "param_label")
	private String label;

	@Enumerated(EnumType.STRING)
	@Column(name = "param_type")
	private ProtocolParamType type;

	@Column(name = "optional")
	private boolean optional;

	@Column(name = "has_sel_function")
	private boolean hasSelFunction;

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(final String protocol) {
		this.protocol = protocol;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public ProtocolParamType getType() {
		return type;
	}

	public void setType(final ProtocolParamType type) {
		this.type = type;
	}

	public boolean isOptional() {
		return optional;
	}

	public void setOptional(final boolean optional) {
		this.optional = optional;
	}

	public boolean isHasSelFunction() {
		return hasSelFunction;
	}

	public void setHasSelFunction(final boolean hasSelFunction) {
		this.hasSelFunction = hasSelFunction;
	}

}
