package eu.dnetlib.domain.dsm;

import java.io.Serializable;
import java.util.Objects;

public class PidSystemPK implements Serializable {

	private static final long serialVersionUID = 8579974236296952193L;

	private String service;
	private String type;
	private String scheme;

	public String getService() {
		return service;
	}

	public void setService(final String service) {
		this.service = service;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getScheme() {
		return scheme;
	}

	public void setScheme(final String scheme) {
		this.scheme = scheme;
	}

	@Override
	public int hashCode() {
		return Objects.hash(scheme, service, type);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof PidSystemPK)) { return false; }
		final PidSystemPK other = (PidSystemPK) obj;
		return Objects.equals(scheme, other.scheme) && Objects.equals(service, other.service) && Objects.equals(type, other.type);
	}

}
