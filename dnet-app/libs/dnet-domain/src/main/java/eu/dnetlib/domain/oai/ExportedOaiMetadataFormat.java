package eu.dnetlib.domain.oai;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "oai_md_formats")
public class ExportedOaiMetadataFormat implements OaiMetadataFormat {

	private static final long serialVersionUID = -4526570547812092275L;

	@Id
	@Column(name = "prefix")
	private String metadataPrefix;

	@Column(name = "schema")
	private String metadataSchema;

	@Column(name = "namespace")
	private String metadataNamespace;

	@Column(name = "xslt")
	private String xslt;

	@Override
	public String getMetadataPrefix() {
		return this.metadataPrefix;
	}

	public void setMetadataPrefix(final String metadataPrefix) {
		this.metadataPrefix = metadataPrefix;
	}

	@Override
	public String getMetadataSchema() {
		return this.metadataSchema;
	}

	public void setMetadataSchema(final String metadataSchema) {
		this.metadataSchema = metadataSchema;
	}

	@Override
	public String getMetadataNamespace() {
		return this.metadataNamespace;
	}

	public void setMetadataNamespace(final String metadataNamespace) {
		this.metadataNamespace = metadataNamespace;
	}

	public String getXslt() {
		return this.xslt;
	}

	public void setXslt(final String xslt) {
		this.xslt = xslt;
	}

}
