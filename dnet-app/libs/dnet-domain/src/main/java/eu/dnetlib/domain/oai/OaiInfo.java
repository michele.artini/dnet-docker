package eu.dnetlib.domain.oai;

import java.io.Serializable;
import java.util.List;

public interface OaiInfo<T> extends Serializable {

	String getRepositoryName();

	String getBaseURL();

	String getProtocolVersion();

	T getEarliestDatestamp();

	String getDeletedRecord();

	String getGranularity();

	List<String> getAdminEmails();

	List<String> getCompressions();

	List<String> getDescriptions();

}
