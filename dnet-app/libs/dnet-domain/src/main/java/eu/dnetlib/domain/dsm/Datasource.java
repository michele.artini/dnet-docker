package eu.dnetlib.domain.dsm;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "dsm_services")
public class Datasource implements Serializable {

	private static final long serialVersionUID = -8541999675119205637L;

	public static final String DEFAULT_EOSC_TYPE = "Data Source";

	@Id
	private String id;

	@Transient
	private String openaireId;

	@Column(nullable = false)
	private String officialname;
	private String englishname;
	private String websiteurl;
	private String logourl;
	private String contactemail;
	private Double latitude;
	private Double longitude;
	private String timezone;
	private String status;

	@Column(name = "namespaceprefix", columnDefinition = "bpchar(12)", nullable = false, updatable = false)
	private String namespaceprefix;

	private String languages;

	private String collectedfrom;
	private Date dateofvalidation;

	@Column(name = "eosc_datasource_type")
	private String eoscDatasourceType;

	private String provenanceaction;
	private Date dateofcollection;
	private String platform;

	@Column(name = "activationid")
	private String activationId;
	private String description;

	private Date releasestartdate;
	private Date releaseenddate;
	private String missionstatementurl;

	private String databaseaccesstype;
	private String datauploadtype;
	private String databaseaccessrestriction;
	private String datauploadrestriction;

	private String citationguidelineurl;

	@OneToMany(mappedBy = "service", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<PidSystem> pidsystems = new HashSet<>();

	private String certificates;
	private String aggregator;

	private String issn;
	private String eissn;
	private String lissn;

	private String registeredby;

	private LocalDateTime registrationdate;

	private String subjects;

	private Boolean managed;

	@Column(name = "consenttermsofuse")
	private Boolean consentTermsOfUse;

	@Column(name = "consenttermsofusedate")
	private LocalDateTime consentTermsOfUseDate;

	@Column(name = "lastconsenttermsofusedate")
	private LocalDateTime lastConsentTermsOfUseDate;

	@Column(name = "fulltextdownload")
	private Boolean fullTextDownload;

	@Deprecated
	@Column(name = "_typology_to_remove_")
	private String typology;

	@Column(name = "eosc_type")
	private String eoscType = DEFAULT_EOSC_TYPE;

	@Column(name = "dedup_main_service")
	private Boolean dedupMainService = true;

	@ManyToMany(cascade = {
		CascadeType.PERSIST, CascadeType.MERGE
	}, fetch = FetchType.EAGER)
	@JoinTable(name = "dsm_service_organization", joinColumns = @JoinColumn(name = "service"), inverseJoinColumns = @JoinColumn(name = "organization"))
	private Set<Organization> organizations = new HashSet<>();;

	@ManyToMany(cascade = {
		CascadeType.PERSIST, CascadeType.MERGE
	}, fetch = FetchType.EAGER)
	@JoinTable(name = "dsm_servicepids", joinColumns = @JoinColumn(name = "service"), inverseJoinColumns = @JoinColumn(name = "pid"))
	private Set<Identity> identities = new HashSet<>();;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getOfficialname() {
		return officialname;
	}

	public void setOfficialname(final String officialname) {
		this.officialname = officialname;
	}

	public String getEnglishname() {
		return englishname;
	}

	public void setEnglishname(final String englishname) {
		this.englishname = englishname;
	}

	public String getWebsiteurl() {
		return websiteurl;
	}

	public void setWebsiteurl(final String websiteurl) {
		this.websiteurl = websiteurl;
	}

	public String getLogourl() {
		return logourl;
	}

	public void setLogourl(final String logourl) {
		this.logourl = logourl;
	}

	public String getContactemail() {
		return contactemail;
	}

	public void setContactemail(final String contactemail) {
		this.contactemail = contactemail;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(final Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(final Double longitude) {
		this.longitude = longitude;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(final String timezone) {
		this.timezone = timezone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String getNamespaceprefix() {
		return namespaceprefix;
	}

	public void setNamespaceprefix(final String namespaceprefix) {
		this.namespaceprefix = namespaceprefix;
	}

	public String getLanguages() {
		return languages;
	}

	public void setLanguages(final String languages) {
		this.languages = languages;
	}

	public String getCollectedfrom() {
		return collectedfrom;
	}

	public void setCollectedfrom(final String collectedfrom) {
		this.collectedfrom = collectedfrom;
	}

	public Date getDateofvalidation() {
		return dateofvalidation;
	}

	public void setDateofvalidation(final Date dateofvalidation) {
		this.dateofvalidation = dateofvalidation;
	}

	public String getEoscDatasourceType() {
		return eoscDatasourceType;
	}

	public void setEoscDatasourceType(final String eoscDatasourceType) {
		this.eoscDatasourceType = eoscDatasourceType;
	}

	public String getProvenanceaction() {
		return provenanceaction;
	}

	public void setProvenanceaction(final String provenanceaction) {
		this.provenanceaction = provenanceaction;
	}

	public Date getDateofcollection() {
		return dateofcollection;
	}

	public void setDateofcollection(final Date dateofcollection) {
		this.dateofcollection = dateofcollection;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(final String platform) {
		this.platform = platform;
	}

	public String getActivationId() {
		return activationId;
	}

	public void setActivationId(final String activationId) {
		this.activationId = activationId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public Date getReleasestartdate() {
		return releasestartdate;
	}

	public void setReleasestartdate(final Date releasestartdate) {
		this.releasestartdate = releasestartdate;
	}

	public Date getReleaseenddate() {
		return releaseenddate;
	}

	public void setReleaseenddate(final Date releaseenddate) {
		this.releaseenddate = releaseenddate;
	}

	public String getMissionstatementurl() {
		return missionstatementurl;
	}

	public void setMissionstatementurl(final String missionstatementurl) {
		this.missionstatementurl = missionstatementurl;
	}

	public String getDatabaseaccesstype() {
		return databaseaccesstype;
	}

	public void setDatabaseaccesstype(final String databaseaccesstype) {
		this.databaseaccesstype = databaseaccesstype;
	}

	public String getDatauploadtype() {
		return datauploadtype;
	}

	public void setDatauploadtype(final String datauploadtype) {
		this.datauploadtype = datauploadtype;
	}

	public String getDatabaseaccessrestriction() {
		return databaseaccessrestriction;
	}

	public void setDatabaseaccessrestriction(final String databaseaccessrestriction) {
		this.databaseaccessrestriction = databaseaccessrestriction;
	}

	public String getDatauploadrestriction() {
		return datauploadrestriction;
	}

	public void setDatauploadrestriction(final String datauploadrestriction) {
		this.datauploadrestriction = datauploadrestriction;
	}

	public String getCitationguidelineurl() {
		return citationguidelineurl;
	}

	public void setCitationguidelineurl(final String citationguidelineurl) {
		this.citationguidelineurl = citationguidelineurl;
	}

	public String getCertificates() {
		return certificates;
	}

	public void setCertificates(final String certificates) {
		this.certificates = certificates;
	}

	public String getAggregator() {
		return aggregator;
	}

	public void setAggregator(final String aggregator) {
		this.aggregator = aggregator;
	}

	public String getIssn() {
		return issn;
	}

	public void setIssn(final String issn) {
		this.issn = issn;
	}

	public String getEissn() {
		return eissn;
	}

	public void setEissn(final String eissn) {
		this.eissn = eissn;
	}

	public String getLissn() {
		return lissn;
	}

	public void setLissn(final String lissn) {
		this.lissn = lissn;
	}

	public String getRegisteredby() {
		return registeredby;
	}

	public void setRegisteredby(final String registeredby) {
		this.registeredby = registeredby;
	}

	public LocalDateTime getRegistrationdate() {
		return registrationdate;
	}

	public void setRegistrationdate(final LocalDateTime registrationdate) {
		this.registrationdate = registrationdate;
	}

	public String getSubjects() {
		return subjects;
	}

	public void setSubjects(final String subjects) {
		this.subjects = subjects;
	}

	public Boolean getManaged() {
		return managed;
	}

	public void setManaged(final Boolean managed) {
		this.managed = managed;
	}

	public Boolean getConsentTermsOfUse() {
		return consentTermsOfUse;
	}

	public void setConsentTermsOfUse(final Boolean consentTermsOfUse) {
		this.consentTermsOfUse = consentTermsOfUse;
	}

	public LocalDateTime getConsentTermsOfUseDate() {
		return consentTermsOfUseDate;
	}

	public void setConsentTermsOfUseDate(final LocalDateTime consentTermsOfUseDate) {
		this.consentTermsOfUseDate = consentTermsOfUseDate;
	}

	public LocalDateTime getLastConsentTermsOfUseDate() {
		return lastConsentTermsOfUseDate;
	}

	public void setLastConsentTermsOfUseDate(final LocalDateTime lastConsentTermsOfUseDate) {
		this.lastConsentTermsOfUseDate = lastConsentTermsOfUseDate;
	}

	public Boolean getFullTextDownload() {
		return fullTextDownload;
	}

	public void setFullTextDownload(final Boolean fullTextDownload) {
		this.fullTextDownload = fullTextDownload;
	}

	public Set<Organization> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(final Set<Organization> organizations) {
		this.organizations = organizations;
	}

	public Set<Identity> getIdentities() {
		return identities;
	}

	public void setIdentities(final Set<Identity> identities) {
		this.identities = identities;
	}

	public Set<PidSystem> getPidsystems() {
		return pidsystems;
	}

	public void setPidsystems(final Set<PidSystem> pidsystems) {
		this.pidsystems = pidsystems;
	}

	public String getOpenaireId() {
		return openaireId;
	}

	public void setOpenaireId(final String openaireId) {
		this.openaireId = openaireId;
	}

	@Deprecated
	public String getTypology() {
		return typology;
	}

	@Deprecated
	public void setTypology(final String typology) {
		this.typology = typology;
	}

	public String getEoscType() {
		return eoscType;
	}

	public void setEoscType(final String eoscType) {
		this.eoscType = eoscType;
	}

	public Boolean getDedupMainService() {
		return dedupMainService;
	}

	public void setDedupMainService(final Boolean dedupMainService) {
		this.dedupMainService = dedupMainService;
	}
}
