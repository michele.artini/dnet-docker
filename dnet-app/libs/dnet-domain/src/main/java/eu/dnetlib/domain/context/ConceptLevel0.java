package eu.dnetlib.domain.context;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "context_cat_concepts_lvl_0")
public class ConceptLevel0 extends CtxChildInfo {

	private static final long serialVersionUID = -4775331902088912839L;

}
