package eu.dnetlib.domain.mdstore;

public enum MDStoreType {
	HDFS,
	MOCK,
	SQL_DB
}
