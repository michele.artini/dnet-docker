package eu.dnetlib.common.mapping;

public interface RecordTransformer<T, K> {

	public K transform(T source);
}
