package eu.dnetlib.common.mapping.cleaner;

import java.io.StringReader;

import org.apache.commons.lang3.BooleanUtils;
import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.SimpleResourceClient;
import eu.dnetlib.common.clients.VocabularyClient;
import eu.dnetlib.errors.CleanerException;

public class CleanerFactory {

	private final DnetServiceClientFactory clientFactory;

	public CleanerFactory(final DnetServiceClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	public Cleaner newCleaner(final String ruleId) throws CleanerException {

		final SimpleResourceClient simpleResourceClient = clientFactory.getClient(SimpleResourceClient.class);
		final VocabularyClient vocabularyClient = clientFactory.getClient(VocabularyClient.class);

		final String prof = simpleResourceClient.findResourceContent(ruleId, String.class);

		try {
			final SAXReader reader = new SAXReader();
			final Document doc = reader.read(new StringReader(prof));

			final Cleaner cleaner = new Cleaner();

			for (final Node node : doc.selectNodes("//RULE")) {
				final String xpath = node.valueOf("@xpath");
				final String voc = node.valueOf("@vocabulary");
				final boolean strict = BooleanUtils.toBoolean(node.valueOf("@strict"));

				final VocabularyRule rule = new VocabularyRule(xpath, strict, voc, vocabularyClient.listTerms(voc));

				cleaner.getRules().add(rule);
			}
			return cleaner;
		} catch (final Exception e) {
			throw new CleanerException("Error obtaing cleaner rule " + ruleId, e);
		}
	}

}
