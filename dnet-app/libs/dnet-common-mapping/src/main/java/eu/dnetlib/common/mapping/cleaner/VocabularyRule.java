package eu.dnetlib.common.mapping.cleaner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.Node;

import eu.dnetlib.domain.vocabulary.Synonym;
import eu.dnetlib.domain.vocabulary.VocabularyTerm;
import eu.dnetlib.errors.CleanerException;

/**
 * @author michele
 *
 *         Vocabulary rules must be declared in a CleanerDS profile, for each vocabulary must be present the relative VocabularyDS profile:
 *
 *         <RULE xpath="//country" vocabulary="dnet:countries" />
 */

public class VocabularyRule {

	private static final Log log = LogFactory.getLog(VocabularyRule.class);

	private final String vocabulary;
	private final String xpath;
	private final boolean strict;

	private final Map<String, String> synonyms = new HashMap<>();
	private final Set<String> validTerms = new HashSet<>();

	public VocabularyRule(final String xpath, final boolean strict, final String vocabulary, final Iterable<VocabularyTerm> terms) throws CleanerException {
		this.xpath = xpath;
		this.strict = strict;
		this.vocabulary = vocabulary;
		loadSynonymsAndTerms(terms);
	}

	public List<Map<String, String>> applyXpathRule(final Document doc) throws CleanerException {
		final List<Map<String, String>> errors = new ArrayList<>();

		final String id = doc.valueOf("//*[local-name()='objIdentifier']");

		for (final Node node : doc.selectNodes(xpath)) {

			final String oldValue = node.getText().trim();

			final String newValue = calculateNewValue(oldValue);
			if (strict) {
				final Map<String, String> err = verifyValue(newValue);
				if (err != null) {
					errors.add(err);

					if (log.isDebugEnabled()) {
						log.debug("[" + newValue + "] is INVALID, " + "RULE: " + toString() + ", " + "RECORD: " + id + ", " + "XPATH: "
								+ xpath);
					}
				}
			}

			if (log.isDebugEnabled() && !newValue.equals(oldValue)) {
				log.debug("[" + oldValue + "] => [" + newValue + "], " + toString() + ", " + "RECORD: " + id + ", " + "XPATH: " + xpath);
			}

			node.setText(newValue);
		}

		return errors;
	}

	private String calculateNewValue(final String oldValue) throws CleanerException {
		log.debug("calculating new value for: " + oldValue);

		if (synonyms.isEmpty()) {
			log.debug("Vocabulary terms is void, vocabularies: " + vocabulary);
		}

		String newValue = null;

		if (synonyms.containsKey(oldValue.toLowerCase())) {
			newValue = synonyms.get(oldValue.toLowerCase());
		}

		if (newValue == null) {
			log.debug("Synonym " + oldValue + " not found in vocabulary");
			return oldValue;
		}

		return newValue;
	}

	private void loadSynonymsAndTerms(final Iterable<VocabularyTerm> terms) {
		for (final VocabularyTerm term : terms) {
			validTerms.add(term.getCode().toLowerCase());
			synonyms.put(term.getCode().toLowerCase(), term.getCode());
			synonyms.put(term.getName().toLowerCase(), term.getCode());
			for (final Synonym s : term.getSynonyms()) {
				synonyms.put(s.getTerm().toLowerCase(), term.getCode());
			}
		}
		log.debug("VOCABULARY " + vocabulary.trim() + " - terms size " + synonyms.size());
	}

	protected Map<String, String> verifyValue(final String value) throws CleanerException {
		if (synonyms.isEmpty()) {
			log.debug("Vocabulary terms is void, vocabulary: " + vocabulary);
		}

		if (validTerms.contains(value.toLowerCase())) { return null; }

		final Map<String, String> error = new HashMap<>();
		error.put("term", value);
		error.put("vocabulary", vocabulary);
		error.put("xpath", xpath);

		return error;
	}

	public Map<String, String> getVocabularyTerms() {
		return synonyms;
	}

	@Override
	public String toString() {
		return String.format("VocabularyRule [xpath=%s, strict=%s, vocabulary=%s]", xpath, strict, vocabulary);
	}

}
