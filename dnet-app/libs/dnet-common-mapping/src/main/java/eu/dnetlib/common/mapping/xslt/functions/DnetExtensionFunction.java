package eu.dnetlib.common.mapping.xslt.functions;

import net.sf.saxon.s9api.ExtensionFunction;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SequenceType;
import net.sf.saxon.s9api.XdmValue;

public abstract class DnetExtensionFunction implements ExtensionFunction {

	private final QName name;

	public DnetExtensionFunction(final String uri, final String lexical) {
		this.name = new QName(uri, lexical);
	}

	@Override
	public final QName getName() {
		return this.name;
	}

	@Override
	public abstract SequenceType getResultType();

	@Override
	public abstract SequenceType[] getArgumentTypes();

	@Override
	public abstract XdmValue call(final XdmValue[] xdmValues);

}
