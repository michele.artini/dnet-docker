package eu.dnetlib.common.mapping.xslt.functions;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.github.sisyphsu.dateparser.DateParserUtils;

import net.sf.saxon.s9api.ItemType;
import net.sf.saxon.s9api.OccurrenceIndicator;
import net.sf.saxon.s9api.SequenceType;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XdmValue;

@Component
public class XsltDateCleaner extends DnetExtensionFunction {

	public static final String DATE_FORMAT = "yyyy-MM-dd";

	public XsltDateCleaner(final String uri, final String lexical) {
		super(uri, lexical);
	}

	@Override
	public SequenceType getResultType() {
		return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ZERO_OR_ONE);
	}

	@Override
	public SequenceType[] getArgumentTypes() {
		return new SequenceType[] {
				SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ZERO_OR_ONE)
		};
	}

	@Override
	public XdmValue call(final XdmValue[] xdmValues) {
		final XdmValue r = xdmValues[0];
		if (r.size() == 0) { return new XdmAtomicValue(""); }
		final String currentValue = xdmValues[0].itemAt(0).getStringValue();
		return new XdmAtomicValue(clean(currentValue));
	}

	public String clean(final String inputDate) {
		if (StringUtils.isBlank(inputDate)) { return null; }

		try {
			final LocalDate date = DateParserUtils
					.parseDate(inputDate.trim())
					.toInstant()
					.atZone(ZoneId.systemDefault())
					.toLocalDate();
			return DateTimeFormatter.ofPattern(DATE_FORMAT).format(date);
		} catch (final DateTimeParseException e) {
			return null;
		}
	}

}
