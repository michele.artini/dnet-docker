package eu.dnetlib.common.mapping.xslt.functions;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import net.sf.saxon.s9api.ItemType;
import net.sf.saxon.s9api.OccurrenceIndicator;
import net.sf.saxon.s9api.SequenceType;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XdmValue;

public class CalculateMd5 extends DnetExtensionFunction {

	public CalculateMd5(final String uri, final String lexical) {
		super(uri, lexical);
	}

	@Override
	public SequenceType getResultType() {
		return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ZERO_OR_ONE);
	}

	@Override
	public SequenceType[] getArgumentTypes() {
		return new SequenceType[] { SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ZERO_OR_ONE) };
	}

	@Override
	public XdmValue call(final XdmValue[] xdmValues) {
		final XdmValue r = xdmValues[0];
		if (r.size() == 0) { return new XdmAtomicValue(""); }
		final String currentValue = xdmValues[0].itemAt(0).getStringValue();
		return new XdmAtomicValue(StringUtils.isNotBlank(currentValue) ? DigestUtils.md5Hex(currentValue) : null);
	}
}
