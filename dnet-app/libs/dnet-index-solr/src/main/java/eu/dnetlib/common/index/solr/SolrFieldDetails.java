package eu.dnetlib.common.index.solr;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.dom4j.Document;
import org.dom4j.Node;

import eu.dnetlib.domain.index.IndexField;
import eu.dnetlib.utils.DateUtils;

public class SolrFieldDetails implements Serializable {

	private static final long serialVersionUID = -8910762200990817492L;

	private String constant = "";
	private boolean result = true;
	private String xpath = "";
	private String type = "solr.TextField";
	private boolean multiValued = false;

	public SolrFieldDetails() {}

	public SolrFieldDetails(final IndexField f) {
		constant = f.getConstant();
		result = f.isResult();
		xpath = f.getXpath();
	}

	public String getConstant() {
		return constant;
	}

	public void setConstant(final String constant) {
		this.constant = constant;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(final boolean result) {
		this.result = result;
	}

	public String getXpath() {
		return xpath;
	}

	public void setXpath(final String xpath) {
		this.xpath = xpath;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public boolean isMultiValued() {
		return multiValued;
	}

	public void setMultiValued(final boolean multiValued) {
		this.multiValued = multiValued;
	}

	public Object calculateValue(final Document xmlDoc) {
		if (StringUtils.isNotBlank(xpath) && multiValued) {
			return xmlDoc.selectNodes(xpath)
					.stream()
					.map(Node::getText)
					.map(String::trim)
					.distinct()
					.map(s -> convertToType(s, type))
					.toArray();
		}
		if (StringUtils.isNotBlank(xpath) && !multiValued) { return convertToType(xmlDoc.valueOf(xpath), type); }
		if (StringUtils.isNotBlank(constant) && multiValued) { return new Object[] { convertToType(constant, type) }; }
		if (StringUtils.isNotBlank(constant) && !multiValued) { return convertToType(constant, type); }

		return null;
	}

	private Object convertToType(final String s, final String solrType) {
		// TODO (LOW PRIORITY): probably it is necessary to extend the mapping
		if (solrType.contains("LongField") || solrType.contains("IntField") || solrType.contains("short")) { return NumberUtils.toLong(s); }
		if (solrType.contains("float") || solrType.contains("double")) { return NumberUtils.toDouble(s); }
		if (solrType.contains("date")) { return DateUtils.parseDateTime(s); }
		return s;
	}

}
