package eu.dnetlib.common.index.solr;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.request.CollectionAdminRequest;
import org.apache.solr.client.solrj.request.schema.FieldTypeDefinition;
import org.apache.solr.client.solrj.request.schema.SchemaRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.client.solrj.response.schema.SchemaRepresentation;
import org.apache.solr.client.solrj.response.schema.SchemaResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.cloud.SolrZkClient;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;

import eu.dnetlib.domain.index.IndexConfiguration;
import eu.dnetlib.domain.index.IndexField;
import eu.dnetlib.errors.DnetException;
import eu.dnetlib.errors.DnetRuntimeException;

public class SolrService {

	private static final Log log = LogFactory.getLog(SolrService.class);

	private static final String INDEX_DATE_FIELD = "indexDate";

	// https://solr.apache.org/guide/solr/latest/deployment-guide/solrj.html

	private final String[] solrUrls;

	private final String zooServer;

	public SolrService(final String zooServer, final String... solrUrls) {
		this.zooServer = zooServer;
		this.solrUrls = solrUrls;
	}

	public List<Map<String, Object>> query(final IndexConfiguration conf, final String q, final int from, final int limit, final String sortField)
			throws DnetException {
		try (final SolrClient solr = newSolrClient()) {

			final SolrQuery query = new SolrQuery(q);
			query.setStart(from);
			query.setRows(limit);

			conf.getFields()
					.stream()
					.filter(IndexField::isResult)
					.map(IndexField::getName)
					.forEach(query::addField);

			if (StringUtils.isNotBlank(sortField)) {
				query.setSort("id", ORDER.asc);
			}

			final QueryResponse response = solr.query(conf.getId(), query);
			final SolrDocumentList documents = response.getResults();

			log.debug("Found " + documents.getNumFound() + " documents");

			return documents.stream()
					.map(SolrDocument::getFieldValueMap)
					.collect(Collectors.toList());
		} catch (final Throwable e) {
			log.error("error executing query", e);
			throw new DnetException("error executing query", e);
		}
	}

	public int indexRecord(final IndexConfiguration conf, final String xml, final boolean commit) throws DnetException {

		try (final SolrClient solr = newSolrClient()) {
			final Map<String, SolrFieldDetails> solrFields = findSolrFields(conf);
			final UpdateResponse updateResponse = solr.add(conf.getId(), asSolrDocument(xml, solrFields));
			if (commit) {
				forceCommit(solr, conf.getId());
			}
			return updateResponse.getResponse().size();
		} catch (final Throwable e) {
			log.error("error indexing a record", e);
			log.debug(xml);
			throw new DnetException("error indexing a record", e);
		}
	}

	public int indexRecords(final IndexConfiguration conf, final Stream<String> inputStream) throws DnetException {
		try (final SolrClient solr = newSolrClient()) {
			final Map<String, SolrFieldDetails> solrFields = findSolrFields(conf);
			final Iterator<SolrInputDocument> iterator = inputStream.map(xml -> asSolrDocument(xml, solrFields)).iterator();
			final UpdateResponse updateResponse = solr.add(conf.getId(), iterator);
			forceCommit(solr, conf.getId());
			return updateResponse.getResponse().size();
		} catch (final Throwable e) {
			log.error("error indexing a record from the stream", e);
			throw new DnetException("error indexing a record from the stream", e);
		}
	}

	public void commit(final IndexConfiguration conf) throws DnetException {
		try (final SolrClient solr = newSolrClient()) {
			forceCommit(solr, conf.getId());
		} catch (final Throwable e) {
			log.error("error executing commit", e);
			throw new DnetException("error executing commit", e);
		}
	}

	private CloudSolrClient newSolrClient() {
		return new CloudSolrClient.Builder(Arrays.asList(solrUrls)).build();
	}

	private void forceCommit(final SolrClient client, final String solrCollection) throws SolrServerException, IOException {
		client.commit(solrCollection);
	}

	private SolrInputDocument asSolrDocument(final String xml, final Map<String, SolrFieldDetails> solrFields) {
		try {
			final Document xmlDoc = DocumentHelper.parseText(xml);

			final SolrInputDocument doc = new SolrInputDocument();
			solrFields.entrySet().forEach(e -> {
				final String name = e.getKey();
				final SolrFieldDetails f = e.getValue();
				final Object value = f.calculateValue(xmlDoc);
				if (value != null) {
					doc.addField(name, value);
				}
			});

			doc.addField(INDEX_DATE_FIELD, System.currentTimeMillis());

			return doc;
		} catch (final Throwable e) {
			log.error("error preparing solr document", e);
			log.debug(xml);
			throw new DnetRuntimeException("error preparing solr document", e);
		}
	}

	public boolean existsIndex(final String indexId) throws DnetException {
		try (final CloudSolrClient solr = newSolrClient()) {
			return CollectionAdminRequest.listCollections(solr).contains(indexId);
		} catch (final Throwable e) {
			log.error("error verifying index: " + indexId, e);
			throw new DnetException("error executing commit: " + indexId, e);
		}
	}

	public void createIndex(final IndexConfiguration conf) throws DnetException {
		try (final CloudSolrClient solr = newSolrClient()) {
			CollectionAdminRequest.createCollection(conf.getId(), conf.getSolrConfig(), conf.getNumShards(), conf.getNumReplicas()).process(solr);
		} catch (final Throwable e) {
			log.error("error creating index: " + conf.getId(), e);
			throw new DnetException("error creating index: " + conf.getId(), e);
		}
	}

	protected Map<String, SolrFieldDetails> findSolrFields(final IndexConfiguration conf) throws DnetException {
		try (final CloudSolrClient solr = newSolrClient()) {
			final SchemaRequest request = new SchemaRequest();
			final SchemaResponse response = request.process(solr, conf.getId());

			final Map<String, SolrFieldDetails> res = new HashMap<>();

			for (final IndexField f : conf.getFields()) {
				res.put(f.getName(), new SolrFieldDetails(f));
			}

			final Map<String, String> fieldTypes = new HashMap<>();

			for (final FieldTypeDefinition fdt : response.getSchemaRepresentation().getFieldTypes()) {
				final String name = fdt.getAttributes().getOrDefault("name", "").toString();
				final String type = fdt.getAttributes().getOrDefault("class", "").toString();
				if (StringUtils.isNoneBlank(name, type)) {
					fieldTypes.put(name, type);
				}
			}

			for (final Map<String, Object> map : response.getSchemaRepresentation().getFields()) {
				final String name = map.getOrDefault("name", "").toString();
				if (StringUtils.isNotBlank(name)) {
					if (!res.containsKey(name)) {
						res.put(name, new SolrFieldDetails());
					}
					final Object type = map.get("type");
					final Object multiValued = map.get("multiValued");
					if ((type != null) && fieldTypes.containsKey(type.toString())) {
						res.get(name).setType(fieldTypes.get(type.toString()));
					}
					if (multiValued != null) {
						res.get(name).setMultiValued(BooleanUtils.toBoolean(multiValued.toString()));
					}
				}
			}

			return res;
		} catch (final Throwable e) {
			log.error("error preparing fields: " + conf.getId(), e);
			throw new DnetException("error preparing fields: " + conf.getId(), e);
		}
	}

	public void deleteIndex(final String indexId) throws DnetException {
		try (final CloudSolrClient solr = newSolrClient()) {
			CollectionAdminRequest.deleteCollection(indexId).process(solr);
		} catch (final Throwable e) {
			log.error("error deleting index: " + indexId, e);
			throw new DnetException("error deleting commit: " + indexId, e);
		}
	}

	public void deleteByQuery(final String indexConfigurationId, final String q) throws DnetException {
		try (final CloudSolrClient solr = newSolrClient()) {
			solr.deleteByQuery(indexConfigurationId, q);
			forceCommit(solr, indexConfigurationId);
		} catch (final Throwable e) {
			log.error("error deleting records, q=" + q, e);
			throw new DnetException("error deleting old records, q=" + q, e);
		}
	}

	public void deleteByQuery(final IndexConfiguration conf, final String q) throws DnetException {
		deleteByQuery(conf.getId(), q);
	}

	public void deleteOldRecords(final IndexConfiguration conf, final long date) throws DnetException {
		deleteByQuery(conf, String.format("%s:[* TO %s]", INDEX_DATE_FIELD, date - 1));
	}

	public List<String> listAvailableSchemas() throws DnetException {
		try (final SolrZkClient zkClient = new SolrZkClient.Builder()
				.withUrl(zooServer)
				.build()) {

			return Arrays.asList(StringUtils.split(zkClient.listZnode("/configs", false), "\n"));
		} catch (final Throwable e) {
			log.error("error finding schemas", e);
			throw new DnetException("error finding schemas", e);
		}
	}

	public SchemaRepresentation findSolrSchema(final String indexId) throws DnetException {
		try (final CloudSolrClient solr = newSolrClient()) {
			final SchemaRequest request = new SchemaRequest();
			final SchemaResponse response = request.process(solr, indexId);
			return response.getSchemaRepresentation();
		} catch (final Throwable e) {
			log.error("error finding schema: " + indexId, e);
			throw new DnetException("error finding schema: " + indexId, e);
		}
	}

}
