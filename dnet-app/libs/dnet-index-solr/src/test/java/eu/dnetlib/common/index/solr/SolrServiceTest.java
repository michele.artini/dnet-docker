package eu.dnetlib.common.index.solr;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.domain.index.IndexConfiguration;
import eu.dnetlib.errors.DnetException;

@Disabled
public class SolrServiceTest {

	// TODO (MEDIUM PRIORITY) Complete the tests

	private SolrService solr;
	private IndexConfiguration conf;

	private static final String TEST_INDEX = "prova";

	@BeforeEach
	void setUp() throws Exception {
		solr = new SolrService("localhost", "http://localhost:8983/solr");
		conf = new IndexConfiguration();
		conf.setId(TEST_INDEX);
		conf.setFields(new ArrayList<>());
	}

	@Test
	void testQuery() {
		fail("Not yet implemented");
	}

	@Test
	void testIndexRecord() {
		fail("Not yet implemented");
	}

	@Test
	void testIndexRecords() {
		fail("Not yet implemented");
	}

	@Test
	void testCommit() {
		fail("Not yet implemented");
	}

	@Test
	void testCreateIndex() throws DnetException {
		final IndexConfiguration conf2 = new IndexConfiguration();
		conf2.setId("test_index_" + UUID.randomUUID());
		conf2.setNumReplicas(1);
		conf2.setNumShards(1);
		conf2.setSolrConfig("_default");
		conf2.setFields(new ArrayList<>());

		assertFalse(solr.existsIndex(conf2.getId()));
		solr.createIndex(conf2);
		assertTrue(solr.existsIndex(conf2.getId()));
		solr.deleteIndex(conf2.getId());
		assertFalse(solr.existsIndex(conf2.getId()));
	}

	@Test
	void testFindSolrSchema() throws DnetException, JsonProcessingException {
		final Map<String, SolrFieldDetails> solrFields = solr.findSolrFields(conf);
		System.out.println(new ObjectMapper().writeValueAsString(solrFields));
	}

	@Test
	void testFindSchemas() throws DnetException, JsonProcessingException {
		final List<String> list = solr.listAvailableSchemas();
		System.out.println(new ObjectMapper().writeValueAsString(list));
	}

	@Test
	void testDeleteByQuery() {
		fail("Not yet implemented");
	}

	@Test
	void testDeleteOldRecords() {
		fail("Not yet implemented");
	}

}
