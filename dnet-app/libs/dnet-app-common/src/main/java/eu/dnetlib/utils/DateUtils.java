package eu.dnetlib.utils;

import java.io.StringWriter;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {

	private static final long SECOND = 1000;
	private static final long MINUTE = SECOND * 60;
	private static final long HOUR = MINUTE * 60;
	private static final long DAY = HOUR * 24;
	private static final long YEAR = DAY * 365;

	private static final DateTimeFormatter DATEFORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.getDefault());

	private static final DateTimeFormatter ISO8601FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());

	public static String elapsedTime(long t) {
		final StringWriter a = new StringWriter();

		boolean prependBlank = false;
		if (t >= YEAR) {
			a.append(floor(t, YEAR));
			a.append(' ');
			a.append('y');
			prependBlank = true;
		}
		t %= YEAR;
		if (t >= DAY) {
			if (prependBlank) {
				a.append(' ');
			}
			a.append(floor(t, DAY));
			a.append(' ');
			a.append('d');
			prependBlank = true;
		}
		t %= DAY;
		if (t >= HOUR) {
			if (prependBlank) {
				a.append(' ');
			}
			a.append(floor(t, HOUR));
			a.append(' ');
			a.append('h');
			prependBlank = true;
		}
		t %= HOUR;
		if (t >= MINUTE) {
			if (prependBlank) {
				a.append(' ');
			}
			a.append(floor(t, MINUTE));
			a.append(' ');
			a.append('m');
			prependBlank = true;
		}
		t %= MINUTE;
		if (t >= SECOND) {
			if (prependBlank) {
				a.append(' ');
			}
			a.append(floor(t, SECOND));
			a.append(' ');
			a.append('s');
			prependBlank = true;
		}
		t %= SECOND;
		if (t > 0) {
			if (prependBlank) {
				a.append(' ');
			}
			a.append(Integer.toString((int) t));
			a.append(' ');
			a.append('m');
			a.append('s');
		}
		return a.toString();
	}

	public static String calculate_ISO8601(final long l) {
		return calculate_ISO8601(LocalDateTime.ofInstant(Instant.ofEpochMilli(l), TimeZone
				.getDefault()
				.toZoneId()));
	}

	public static String calculate_ISO8601(final LocalDateTime time) {
		final String result = time.format(ISO8601FORMAT);
		return result.substring(0, result.length() - 2) + ":" + result.substring(result.length() - 2);
	}

	private static String floor(final long d, final long n) {
		return Long.toString(Math.floorDiv(d, n));
	}

	public static long now() {
		return Instant.now().toEpochMilli();
	}

	public static LocalDate parseDate(final String s) {
		return LocalDate.parse(s, DATEFORMAT);
	}

	public static LocalDateTime parseDateTime(final String s) {
		return LocalDateTime.parse(s, ISO8601FORMAT);
	}

	public static String now_ISO8601() {
		return calculate_ISO8601(now());
	}

}
