package eu.dnetlib.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class MDStoreManagerException extends DnetException {

	/**
	 *
	 */
	private static final long serialVersionUID = -7503316126409002675L;

	public MDStoreManagerException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public MDStoreManagerException(final String message) {
		super(message);
	}

}
