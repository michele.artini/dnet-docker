package eu.dnetlib.utils;

public interface DnetCallback<T> {

	void onSuccess(T t);

	void onFail(T t, Throwable e);
}
