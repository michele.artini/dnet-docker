package eu.dnetlib.errors;

public class DnetRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 5614504079402744578L;

	public DnetRuntimeException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public DnetRuntimeException(final String message) {
		super(message);
	}

}
