package eu.dnetlib.common.clients;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;

import eu.dnetlib.domain.vocabulary.Vocabulary;
import eu.dnetlib.domain.vocabulary.VocabularyTerm;

public class VocabularyClient extends DnetServiceClient {

	@Cacheable("vocabulary_list")
	public List<Vocabulary> listVocs() {
		return Arrays.asList(httpGet("/api/vocs/", Vocabulary[].class, Map.of()));
	}

	@Cacheable("vocabulary_terms")
	public List<VocabularyTerm> listTerms(final String vocId) {
		return Arrays.asList(httpGet("/api/vocs/{vocId}/terms", VocabularyTerm[].class, Map.of("vocId", vocId)));
	}

	public boolean isValidTerm(final String vocId, final String termCode) {
		for (final VocabularyTerm t : listTerms(vocId)) {
			if (t.getCode().equals(termCode)) { return true; }
		}
		return false;
	}

}
