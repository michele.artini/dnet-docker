package eu.dnetlib.common.clients;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import eu.dnetlib.domain.oai.OaiConfiguration;
import eu.dnetlib.domain.oai.ExportedOaiSet;

public class OaiManagerClient extends DnetServiceClient {

	public OaiConfiguration getOaiConfiguration() {
		return httpGet("/api/oai/conf", OaiConfiguration.class, Map.of());
	}

	public List<ExportedOaiSet> listSets() {
		return Arrays.asList(httpGet("/api/oai/sets", ExportedOaiSet[].class, Map.of()));
	}

	public void createOaiSet(final ExportedOaiSet oaiSet) {
		httpPostObject("/api/oai/sets", oaiSet, ExportedOaiSet[].class, Map.of());
	}

}
