package eu.dnetlib.common.app;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.IsManagerClient;
import eu.dnetlib.domain.service.ServiceStatus;
import eu.dnetlib.domain.service.ServiceType;
import io.micrometer.core.instrument.ImmutableTag;
import io.micrometer.core.instrument.Metrics;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.tags.Tag;

public abstract class AbstractDnetApp {

	private static final String DEFAULT_VERSION = "1.1";

	protected static final License APACHE_2_LICENSE = new License().name("Apache 2.0").url("http://www.apache.org/licenses/LICENSE-2.0");

	protected static final License AGPL_3_LICENSE =
			new License().name("GNU Affero General Public License v3.0 or later").url("https://www.gnu.org/licenses/agpl-3.0.txt");

	@Value("${maven.pom.path}")
	private ClassPathResource pom;

	@Value("${server.port:8080}")
	private int serverPort;

	@Value("${server.title}")
	private String serverTitle;

	@Value("${server.description}")
	private String serverDesc;

	@Value("${dnet.is.url}")
	private String informationServiceUrl;

	private static final Log log = LogFactory.getLog(AbstractDnetApp.class);

	private static final ScheduledThreadPoolExecutor registrationThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);

	public static final String SWAGGER_API_GROUP = "dnet-apis";

	@PostConstruct
	public void init() {
		final MavenXpp3Reader reader = new MavenXpp3Reader();
		try {
			final Model model = reader.read(new InputStreamReader(pom.getInputStream()));

			log.info(String.format("registering metric for %s", model.getArtifactId()));

			final ImmutableTag tag1 = new ImmutableTag("component", model.getGroupId() + ":" + model.getArtifactId());
			final ImmutableTag tag2 = new ImmutableTag("version", model.getVersion());
			final ImmutableTag tag3 = new ImmutableTag("scmtag", model.getScm().getTag());

			Metrics.gauge("micrometer_info", Arrays.asList(tag1, tag2, tag3), 1);
		} catch (IOException | XmlPullParserException e) {
			log.error("Error registering metric", e);
		}

		registrationThreadPoolExecutor.scheduleAtFixedRate(() -> {
			final IsManagerClient client = new IsManagerClient();
			client.setBaseUrl(informationServiceUrl);

			try {
				final ServiceStatus status =
						client.registerService(serviceType(), "http://" + InetAddress.getLocalHost().getHostAddress() + ":" + serverPort);

				log.info("Service registered with name: " + status.getName());

				ServiceStatusRegistry.setStatus(status);
			} catch (final Throwable e) {
				log.warn("Error registering service", e);
			}
		}, 10, 300, TimeUnit.SECONDS);
	}

	protected abstract ServiceType serviceType();

	@Bean
	public OpenAPI newSwaggerDocket() {
		return new OpenAPI()
				.info(getSwaggerInfo())
				.tags(getSwaggerTags());
	}

	@Bean
	public GroupedOpenApi publicApi() {
		return GroupedOpenApi.builder()
				.group(SWAGGER_API_GROUP)
				.displayName(serverTitle)
				.pathsToMatch("/api/**")
				.build();
	}

	@Bean
	public DnetServiceClientFactory dnetServiceClientFactory() {
		return new DnetServiceClientFactory(informationServiceUrl);
	}

	protected Info getSwaggerInfo() {
		return new Info()
				.title(serverTitle)
				.description(serverDesc)
				.version(swaggerVersion())
				.license(swaggerLicense());
	}

	protected List<Tag> getSwaggerTags() {
		return Arrays.asList(new Tag().name(serverTitle).description(serverDesc));
	}

	protected String swaggerVersion() {
		return DEFAULT_VERSION;
	}

	protected License swaggerLicense() {
		return AGPL_3_LICENSE;
	}

}
