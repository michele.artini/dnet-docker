package eu.dnetlib.common.clients;

import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

public class DnetServiceClient {

	private String baseUrl;
	private DnetServiceClientFactory factory;

	protected final <T> T httpGet(final String path, final Class<T> clazz, final Map<String, ?> uriVariables) {
		return new RestTemplate().getForObject(baseUrl + path, clazz, uriVariables);
	}

	protected final void httpDelete(final String path, final Map<String, ?> uriVariables) {
		new RestTemplate().delete(baseUrl + path, uriVariables);
	}

	protected <T> T httpPostObject(final String path, final Object obj, final Class<T> responseClazz, final Map<String, ?> uriVariables) {
		return new RestTemplate().postForObject(baseUrl + path, obj, responseClazz, uriVariables);
	}

	protected <T> T httpPostParams(final String path, final Map<String, Object> params, final Class<T> responseClazz, final Map<String, ?> uriVariables) {
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		final LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		params.forEach((k, v) -> map.add(k, v));

		final ResponseEntity<T> response = new RestTemplate().postForEntity(baseUrl + path, new HttpEntity<>(map, headers), responseClazz, uriVariables);

		return response.getBody();
	}

	public final String getBaseUrl() {
		return baseUrl;
	}

	public final void setBaseUrl(final String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public final DnetServiceClientFactory getFactory() {
		return factory;
	}

	public final void setFactory(final DnetServiceClientFactory factory) {
		this.factory = factory;
	}

}
