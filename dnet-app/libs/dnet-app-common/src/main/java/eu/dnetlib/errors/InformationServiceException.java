package eu.dnetlib.errors;

public class InformationServiceException extends DnetException {

	private static final long serialVersionUID = 3566871386890315850L;

	public InformationServiceException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public InformationServiceException(final String message) {
		super(message);
	}

}
