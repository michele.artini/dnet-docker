package eu.dnetlib.common.clients;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import eu.dnetlib.domain.resource.SimpleResource;

public class SimpleResourceClient extends DnetServiceClient {

	public static enum ResourceType {
		wf_template,
		transformation_rule_xslt
	}

	public List<String> listResources(final ResourceType type) {
		return Arrays.asList(httpGet("/api/resources/byType/{type}", String[].class, Map.of("type", type)));
	}

	public String getResourceContent(final String id) {
		return httpGet("/api/resources/{id}/content", String.class, Map.of("id", id));
	}

	public SimpleResource findResource(final String id) {
		return httpGet("/api/resources/{id}/metadata", SimpleResource.class, Map.of("id", id));
	}

	public SimpleResource findResource(final ResourceType type, final String name) {
		return httpGet("/api/resources/byType/{type}/{name}/metadata", SimpleResource.class, Map.of("type", type, "name", name));
	}

	public <T> T findResourceContent(final String id, final Class<T> clazz) {
		return httpGet("/api/resources/{id}/content", clazz, Map.of("id", id));
	}

	public <T> T findResourceContent(final ResourceType type, final String name, final Class<T> clazz) {
		return httpGet("/api/resources/byType/{type}/{name}/content", clazz, Map.of("type", type, "name", name));
	}

	public void deleteResource(final String id) {
		httpDelete("/api/resources/{id}", Map.of("id", id));
	}

	public void updateResourceContent(final String id, final Object content) {
		httpPostObject("/api/resources/{id}/content", content, Void.class, Map.of("id", id));
	}

	public void updateResourceMetadata(final String id, final Object content) {
		httpPostObject("/api/resources/{id}/metadata", content, Void.class, Map.of("id", id));
	}

	public SimpleResource prepareNewResource(final String name,
			final String desc,
			final ResourceType type,
			final String subtype,
			final String description,
			final Object content) {
		final Map<String, Object> params = new HashMap<>();
		params.put("name", name);
		params.put("type", type);
		params.put("subtype", subtype);
		params.put("description", description);
		params.put("content", content);
		return httpPostParams("/api/resources/", params, SimpleResource.class, Map.of());
	}

	public List<SimpleResource> findResources(final ResourceType type, final String subType) {

		final String path = "/api/resources/byType/{type}";

		final SimpleResource[] arr = StringUtils.isBlank(subType)
				? httpGet(path, SimpleResource[].class, Map.of("type", type))
				: httpGet(path + "?subtype={subtype}", SimpleResource[].class, Map.of("type", type, "subtype", subType));

		return Arrays.asList(arr);
	}

}
