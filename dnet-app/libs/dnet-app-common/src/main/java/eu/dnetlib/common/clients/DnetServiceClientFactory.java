package eu.dnetlib.common.clients;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import eu.dnetlib.domain.service.ServiceType;
import eu.dnetlib.errors.DnetRuntimeException;

public class DnetServiceClientFactory {

	private final String isManagerUrl;

	private final ScheduledThreadPoolExecutor urlsThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);

	private final Map<ServiceType, Set<String>> urls = new ConcurrentHashMap<>();

	public DnetServiceClientFactory(final String isManagerUrl) {
		this.isManagerUrl = isManagerUrl;

		urlsThreadPoolExecutor.scheduleAtFixedRate(this::updateUrlsMap, 10, 300, TimeUnit.SECONDS);
	}

	public <T extends DnetServiceClient> T getClient(final String baseUrl, final Class<T> clazz) {
		try {
			final T client = clazz.getConstructor().newInstance();
			client.setBaseUrl(baseUrl);
			client.setFactory(this);
			return client;
		} catch (final Exception e) {
			throw new DnetRuntimeException("Client not available: " + clazz, e);
		}
	}

	public <T extends DnetServiceClient> T getClient(final Class<T> clazz) {
		return getClient(findBaseUrl(serviceType(clazz)), clazz);
	}

	private <T extends DnetServiceClient> String findBaseUrl(final ServiceType type) {
		if (type == ServiceType.is_manager) { return isManagerUrl; }

		for (int i = 0; i < 2; i++) {
			synchronized (urls) {
				if (urls.containsKey(type) && !urls.get(type).isEmpty()) {
					final List<String> list = new ArrayList<>(urls.get(type));
					Collections.shuffle(list);
					return list.get(0);
				}
			}
			updateUrlsMap();
		}

		throw new DnetRuntimeException("Missing BaseUrl for " + type);
	}

	private void updateUrlsMap() {
		synchronized (urls) {
			final IsManagerClient client = new IsManagerClient();
			client.setBaseUrl(isManagerUrl);

			urls.clear();
			client.availableServices().forEach(s -> {
				final ServiceType type = s.getType();
				if (!urls.containsKey(type)) {
					urls.put(type, Collections.synchronizedSet(new HashSet<>()));
				}
				urls.get(type).add(s.getBaseUrl());
			});
		}
	}

	private <T extends DnetServiceClient> ServiceType serviceType(final Class<T> clazz) {
		if (clazz == IsManagerClient.class) { return ServiceType.is_manager; }
		if (clazz == MDStoreManagerClient.class) { return ServiceType.mdstore_manager; }
		if (clazz == DsmClient.class) { return ServiceType.datasource_manager; }
		if (clazz == EmailClient.class) { return ServiceType.mail_sender; }
		if (clazz == SimpleResourceClient.class) { return ServiceType.resource_manager; }
		if (clazz == VocabularyClient.class) { return ServiceType.vocabulary_manager; }
		if (clazz == WfExecutorClient.class) { return ServiceType.wf_executor; }
		if (clazz == WfManagerClient.class) { return ServiceType.wf_manager; }
		if (clazz == OaiManagerClient.class) { return ServiceType.oai_manager; }
		if (clazz == IndexManagerClient.class) { return ServiceType.index_manager; }
		throw new DnetRuntimeException("Missing Service Type for " + clazz);
	}

}
