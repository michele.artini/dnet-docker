package eu.dnetlib.errors;

public class DnetException extends Exception {

	private static final long serialVersionUID = 9072565469400759294L;

	public DnetException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public DnetException(final String message) {
		super(message);
	}

}
