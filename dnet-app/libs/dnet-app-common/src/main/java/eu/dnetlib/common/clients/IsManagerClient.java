package eu.dnetlib.common.clients;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.dnetlib.domain.service.ServiceStatus;
import eu.dnetlib.domain.service.ServiceType;

public class IsManagerClient extends DnetServiceClient {

	public ServiceStatus registerService(final ServiceType serviceType, final String serviceBaseUrl) {
		final Map<String, Object> params = new HashMap<>();
		params.put("baseUrl", serviceBaseUrl);
		return httpPostParams("/api/register/{serviceType}", params, ServiceStatus.class, Map.of("serviceType", serviceType));
	}

	public List<ServiceStatus> availableServices() {
		return Arrays.asList(httpGet("/api/services", ServiceStatus[].class, Map.of()));
	}

}
