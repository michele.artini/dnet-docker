package eu.dnetlib.errors;

public class DsmException extends DnetException {

	private static final long serialVersionUID = 8980196353591772127L;

	public DsmException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public DsmException(final String message) {
		super(message);
	}
}
