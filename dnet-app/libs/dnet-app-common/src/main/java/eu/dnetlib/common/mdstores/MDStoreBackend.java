package eu.dnetlib.common.mdstores;

import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import eu.dnetlib.domain.mdstore.MDStore;
import eu.dnetlib.domain.mdstore.MDStoreVersion;
import eu.dnetlib.domain.mdstore.records.MetadataRecord;
import eu.dnetlib.errors.MDStoreManagerException;

public interface MDStoreBackend {

	void completeNewMDStore(MDStore mdstore);

	void completeNewMDStoreVersion(MDStoreVersion version);

	void delete(MDStore mdstore) throws MDStoreManagerException;

	void delete(MDStoreVersion version) throws MDStoreManagerException;

	List<MetadataRecord> listEntries(MDStoreVersion version, long limit) throws MDStoreManagerException;

	void saveRecords(MDStoreVersion version, Stream<MetadataRecord> inputStream);

	Stream<MetadataRecord> streamEntries(MDStoreVersion version) throws MDStoreManagerException;

	Set<String> fixInconsistencies(boolean delete) throws MDStoreManagerException;

	long countRecords(String versionId);

}
