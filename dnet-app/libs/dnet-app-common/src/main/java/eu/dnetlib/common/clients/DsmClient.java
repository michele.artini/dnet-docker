package eu.dnetlib.common.clients;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.domain.dsm.Datasource;
import eu.dnetlib.domain.protocol.ProtocolDesc;

public class DsmClient extends DnetServiceClient {

	public List<ProtocolDesc> listProtocols() {
		return Arrays.asList(httpGet("/api/protocols/", ProtocolDesc[].class, Map.of()));
	}

	public Api updateApiCollectionInfo(final String apiId, final String mdId, final long size) {
		final Map<String, Object> params = new HashMap<>();
		params.put("mdId", mdId);
		params.put("size", size);

		httpPostParams("/api/ds-api/{apiId}/last-collection", params, Void.class, Map.of("apiId", apiId));

		return findApi(apiId);
	}

	public Api updateApiAggregationInfo(final String apiId, final String mdId, final long size) {
		final Map<String, Object> params = new HashMap<>();
		params.put("mdId", mdId);
		params.put("size", size);

		httpPostParams("/api/ds-api/{apiId}/last-aggregation", params, Void.class, Map.of("apiId", apiId));

		return findApi(apiId);
	}

	public Api updateApiDownloadInfo(final String apiId, final String objId, final long size) {
		final Map<String, Object> params = new HashMap<>();
		params.put("objId", objId);
		params.put("size", size);

		httpPostParams("/api/ds-api/{apiId}/last-download", params, Void.class, Map.of("apiId", apiId));

		return findApi(apiId);
	}

	public Datasource findDs(final String dsId) {
		return httpGet("/api/ds/{dsId}", Datasource.class, Map.of("dsId", dsId));
	}

	public Api findApi(final String apiId) {
		return httpGet("/api/ds-api/{apiId}", Api.class, Map.of("apiId", apiId));
	}

	public void setManaged(final Datasource ds, final boolean b) {
		final Map<String, Object> params = new HashMap<>();
		params.put("managed", b);

		httpPostParams("/api/ds/{dsId}/manage", params, Void.class, Map.of("dsId", ds.getId()));

		ds.setManaged(b);
	}

	public void setApiActive(final Api api, final boolean b) {
		final Map<String, Object> params = new HashMap<>();
		params.put("managed", b);

		httpPostParams("/api/ds-api/{apiId}/active", params, Void.class, Map.of("apiId", api.getId()));

		api.setActive(b);
	}

}
