package eu.dnetlib.common.controller.parts;

import java.util.ArrayList;
import java.util.List;

public class InfoSection<T> {

	private final String name;
	private final List<T> data;

	public InfoSection(final String name) {
		this.name = name;
		this.data = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public List<T> getData() {
		return data;
	}

}
