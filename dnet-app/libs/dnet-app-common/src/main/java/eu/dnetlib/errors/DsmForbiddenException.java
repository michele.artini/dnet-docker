package eu.dnetlib.errors;

public class DsmForbiddenException extends DsmException {

	private static final long serialVersionUID = 1283353531580924944L;

	public DsmForbiddenException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public DsmForbiddenException(final String message) {
		super(message);
	}

}
