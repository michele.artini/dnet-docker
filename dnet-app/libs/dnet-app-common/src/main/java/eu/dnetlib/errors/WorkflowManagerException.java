package eu.dnetlib.errors;

public class WorkflowManagerException extends DnetException {

	private static final long serialVersionUID = -9067581185191425823L;

	public WorkflowManagerException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public WorkflowManagerException(final String message) {
		super(message);
	}
}
