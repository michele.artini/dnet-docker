package eu.dnetlib.utils;

public interface CountedValue {

	public String getValue();

	public void setValue();

	public long getCount();

	public void setCount(final long count);

}
