package eu.dnetlib.common.clients;

import java.util.Map;

import eu.dnetlib.domain.index.IndexConfiguration;

public class IndexManagerClient extends DnetServiceClient {

	public IndexConfiguration getIndexConfiguration(final String indexConfId) {
		return httpGet("/api/index/configurations/{id}", IndexConfiguration.class, Map.of("id", indexConfId));
	}

}
