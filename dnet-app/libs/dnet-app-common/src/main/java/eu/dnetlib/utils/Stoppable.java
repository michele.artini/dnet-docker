package eu.dnetlib.utils;

public interface Stoppable {

	void stop();

	void resume();

	public StoppableDetails getStopDetails();
}
