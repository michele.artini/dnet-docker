package eu.dnetlib.errors;

public class TransformationException extends DnetException {

	private static final long serialVersionUID = 1808159249448633810L;

	public TransformationException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public TransformationException(final String message) {
		super(message);
	}
}
