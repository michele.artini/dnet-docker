package eu.dnetlib.common.app;

import eu.dnetlib.domain.service.ServiceStatus;

public class ServiceStatusRegistry {

	private static ServiceStatus status;

	public static ServiceStatus getStatus() {
		return status;
	}

	public static void setStatus(final ServiceStatus status) {
		ServiceStatusRegistry.status = status;
	}

}
