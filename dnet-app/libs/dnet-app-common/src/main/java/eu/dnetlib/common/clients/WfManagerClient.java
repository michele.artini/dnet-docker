package eu.dnetlib.common.clients;

import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import eu.dnetlib.domain.dsm.info.AggregationInfo;
import eu.dnetlib.domain.dsm.info.AggregationStage;
import eu.dnetlib.domain.dsm.info.CollectionInfo;
import eu.dnetlib.domain.dsm.info.CollectionMode;
import eu.dnetlib.domain.dsm.info.TransformationInfo;
import eu.dnetlib.domain.wfs.jobs.JobStatus;
import eu.dnetlib.domain.wfs.jobs.WfJournalEntry;
import eu.dnetlib.errors.DsmException;

public class WfManagerClient extends DnetServiceClient {

	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	public List<AggregationInfo> getAggregationHistory(final String dsId) throws DsmException {
		final WfJournalEntry[] history = httpGet("/api/history/byDsId/{dsId}", WfJournalEntry[].class, Map.of("dsId", dsId));

		return Arrays.stream(history)
				.filter(job -> (job.getStatus() == JobStatus.success) || (job.getStatus() == JobStatus.failure))
				.map(job -> {
					final Map<String, Object> details = job.getOutputParams();

					final AggregationStage stage = AggregationStage.parse((String) details.get("system:wfName"));
					final boolean success = job.getStatus() == JobStatus.success;
					final String date = job.getEndDate().format(dateTimeFormatter);
					final long total = success ? getNumberOfRecords(details) : 0;
					return switch (stage) {
					case COLLECT -> {
						final CollectionInfo cInfo = new CollectionInfo();
						cInfo.setAggregationStage(stage);
						cInfo.setCollectionMode(getCollectionMode(details));
						cInfo.setNumberOfRecords(total);
						cInfo.setDate(date);
						cInfo.setCompletedSuccessfully(success);
						yield cInfo;
					}
					case TRANSFORM -> {
						final TransformationInfo tInfo = new TransformationInfo();
						tInfo.setAggregationStage(stage);
						tInfo.setNumberOfRecords(total);
						tInfo.setDate(date);
						tInfo.setCompletedSuccessfully(success);
						yield tInfo;
					}
					};
				})
				.collect(Collectors.toList());
	}

	private CollectionMode getCollectionMode(final Map<String, Object> details) {
		// TODO (LOW PRIORITY) : verify the terms in the history
		return Optional.ofNullable((String) details.get("system:node:SELECT_MODE:selection"))
				.map(CollectionMode::valueOf)
				.orElseGet(() -> Optional.ofNullable((String) details.get("collectionMode"))
						.map(CollectionMode::valueOf)
						.orElse(null));
	}

	private Integer getNumberOfRecords(final Map<String, Object> details) {
		// TODO (LOW PRIORITY) : verify the terms in the history
		final String sinkSize = (String) details.get("mainlog:sinkSize");
		final String total = (String) details.get("mainlog:total");
		if (StringUtils.isNotBlank(sinkSize)) { return NumberUtils.toInt(sinkSize); }
		if (StringUtils.isNotBlank(total)) { return NumberUtils.toInt(total); }
		return -1;
	}

}
