package eu.dnetlib.errors;

public class CollectorException extends DnetException {

	private static final long serialVersionUID = 5908441770011815938L;

	public CollectorException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public CollectorException(final String message) {
		super(message);
	}
}
