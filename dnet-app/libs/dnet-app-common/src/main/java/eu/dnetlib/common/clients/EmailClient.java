package eu.dnetlib.common.clients;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.StringTemplateResolver;

import eu.dnetlib.domain.email.EmailMessage;
import eu.dnetlib.domain.email.EmailTemplate;

public class EmailClient extends DnetServiceClient {

	private static final Log log = LogFactory.getLog(EmailClient.class);

	private final TemplateEngine templateEngine;

	public EmailClient() {
		super();

		final StringTemplateResolver templateResolver = new StringTemplateResolver();
		templateResolver.setTemplateMode(TemplateMode.HTML);

		templateEngine = new TemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);
	}

	public void sendStoredMail(final String toMail, final String fromMail, final String fromName, final String messageId, final Map<String, Object> params) {
		final EmailTemplate template = getFactory().getClient(SimpleResourceClient.class).findResourceContent(messageId, EmailTemplate.class);

		final EmailMessage message = new EmailMessage();
		message.setTo(toMail);
		message.setSubject(applyTemplate(template.getSubject(), params));
		message.setContent(applyTemplate(template.getMessage(), params));
		message.setFromMail(fromMail);
		message.setFromName(fromName);
		message.setCcs(null);

		final String res = httpPostObject("/api/emails/send", message, String.class, Map.of());

		log.info(res);
	}

	private String applyTemplate(final String template, final Map<String, Object> params) {
		final Context context = new Context();
		context.setVariables(params);
		return templateEngine.process(template, context);
	}
}
