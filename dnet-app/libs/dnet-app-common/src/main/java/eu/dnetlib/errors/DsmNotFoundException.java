package eu.dnetlib.errors;

public class DsmNotFoundException extends DsmException {

	private static final long serialVersionUID = -1318946320754167152L;

	public DsmNotFoundException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public DsmNotFoundException(final String message) {
		super(message);
	}

}
