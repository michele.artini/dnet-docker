package eu.dnetlib.common.controller.parts;

import java.io.Serializable;

import org.apache.commons.lang3.exception.ExceptionUtils;

public class ErrorMessage implements Serializable {

	private static final long serialVersionUID = 9145071505635293147L;

	private String message;
	private String stacktrace;

	public ErrorMessage() {}

	public ErrorMessage(final Exception e) {
		this(e.getMessage(), ExceptionUtils.getStackTrace(e));
	}

	public ErrorMessage(final String message, final String stacktrace) {
		this.message = message;
		this.stacktrace = stacktrace;
	}

	public String getMessage() {
		return message;
	}

	public String getStacktrace() {
		return stacktrace;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public void setStacktrace(final String stacktrace) {
		this.stacktrace = stacktrace;
	}

}
