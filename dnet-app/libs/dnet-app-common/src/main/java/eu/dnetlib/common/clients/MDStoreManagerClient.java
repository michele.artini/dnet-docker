package eu.dnetlib.common.clients;

import java.util.HashMap;
import java.util.Map;

import eu.dnetlib.domain.mdstore.MDStoreType;
import eu.dnetlib.domain.mdstore.MDStoreVersion;
import eu.dnetlib.domain.mdstore.MDStoreWithInfo;

public class MDStoreManagerClient extends DnetServiceClient {

	public MDStoreWithInfo createMDStore(final String format,
			final String layout,
			final String interpretation,
			final MDStoreType type,
			final String dsName,
			final String dsId,
			final String apiId) {

		final Map<String, Object> params = new HashMap<>();
		params.put("dsName", dsName);
		params.put("dsId", dsId);
		params.put("apiId", apiId);

		return httpPostParams("/api/mdstores/new/{format}/{layout}/{interpretation}/{type}", params, MDStoreWithInfo.class, Map
				.of("format", format, "layout", layout, "interpretation", interpretation, "type", type.name()));
	}

	public void deleteMdStore(final String mdId) {
		httpDelete("/api/mdstores/mdstore/{mdId}", Map.of("mdId", mdId));
	}

	public MDStoreWithInfo findMDStore(final String mdId) {
		return httpGet("/api/mdstores/mdstore/{mdId}", MDStoreWithInfo.class, Map.of("mdId", mdId));
	}

	public MDStoreVersion newVersion(final String mdId) {
		return httpGet("/api/mdstores/mdstore/{mdId}/newVersion", MDStoreVersion.class, Map.of("mdId", mdId));
	}

	public void commitVersion(final String versionId, final long size) {
		httpGet("/api/mdstores/version/{versionId}/commit/{size}", Void.class, Map.of("versionId", versionId, "size", size));
	}

	public void abortVersion(final MDStoreVersion version) {
		httpGet("/api/mdstores/version/{versionId}/abort", Void.class, Map.of("versionId", version.getId()));
	}

	public MDStoreVersion findVersion(final String versionId) {
		return httpGet("/api/mdstores/version/{versionId}", MDStoreVersion.class, Map.of("versionId", versionId));
	}

	public MDStoreVersion startReading(final String mdId) {
		return httpGet("/api/mdstores/mdstore/{mdId}/startReading", MDStoreVersion.class, Map.of("mdId", mdId));
	}

	public MDStoreVersion endReading(final MDStoreVersion version) {
		return httpGet("/api/mdstores/version/{versionId}/endReading", MDStoreVersion.class, Map.of("versionId", version.getId()));
	}

	public boolean existsMdSstore(final String mdId) {
		try {
			return findMDStore(mdId) != null;
		} catch (final Exception e) {
			return false;
		}
	}
}
