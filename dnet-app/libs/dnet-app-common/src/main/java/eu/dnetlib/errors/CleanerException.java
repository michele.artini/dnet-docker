package eu.dnetlib.errors;

public class CleanerException extends DnetException {

	private static final long serialVersionUID = 2801195136722513362L;

	public CleanerException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public CleanerException(final String message) {
		super(message);
	}
}
