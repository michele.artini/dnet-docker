package eu.dnetlib.utils;

public class StoppableDetails {

	public enum StopStatus {
		STOPPED,
		STOPPING,
		RUNNING
	};

	private final String title;
	private final String description;
	private final StopStatus status;

	public StoppableDetails(final String title, final String description, final StopStatus status) {
		this.title = title;
		this.description = description;
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public StopStatus getStatus() {
		return status;
	}

}
