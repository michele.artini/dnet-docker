package eu.dnetlib.common.controller.parts;

import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

public class JavaModule implements Comparable<JavaModule> {

	private final String group;
	private final String name;
	private final Set<String> versions = new LinkedHashSet<>();
	private final Set<String> files = new LinkedHashSet<>();

	public JavaModule(final String group, final String name) {
		this.group = group;
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public String getName() {
		return name;
	}

	public Set<String> getVersions() {
		return versions;
	}

	public Set<String> getFiles() {
		return files;
	}

	public void addFileAndVersion(final String file, final String version) {
		files.add(file);
		versions.add(version);
	}

	@Override
	public int compareTo(final JavaModule o) {
		if (getGroup().equals(o.getGroup())) {
			return StringUtils.compare(getName(), o.getName());
		} else {
			return StringUtils.compare(getGroup(), o.getGroup());
		}
	}

}
