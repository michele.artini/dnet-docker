package eu.dnetlib.common.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import eu.dnetlib.common.controller.parts.ErrorMessage;

public class DnetRestController {

	private static final Log log = LogFactory.getLog(DnetRestController.class);

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ErrorMessage handleException(final Exception e) {
		log.error("Error processing http method", e);
		return new ErrorMessage(e);
	}

}
