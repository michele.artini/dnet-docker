package eu.dnetlib.wfs.procs;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by michele on 19/11/15.
 */
public class RuntimeEnv {

	private final Map<String, Object> attributes = new HashMap<>();

	public Map<String, Object> getAttributes() {
		return this.attributes;
	}

	public void clearEnv() {
		this.attributes.clear();
	}

	public void addAttributes(final Map<String, Object> map) {
		if (map != null) {
			this.attributes.putAll(map);
		}
	}

	public void setAttribute(final String name, final Object value) {
		this.attributes.put(name, value);
	}

	public Object getAttribute(final String name) {
		return this.attributes.get(name);
	}

	public <T> T getAttribute(final String name, final Class<T> clazz) {
		return clazz.cast(this.attributes.get(name));
	}

	public boolean hasAttribute(final String name) {
		return this.attributes.containsKey(name);
	}

	public Object removeAttribute(final String name) {
		return this.attributes.remove(name);
	}

}
