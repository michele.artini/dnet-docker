package eu.dnetlib.wfs.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.dnetlib.domain.wfs.WorkflowsConstants;
import eu.dnetlib.domain.wfs.conf.WfConfiguration;
import eu.dnetlib.domain.wfs.graph.Arc;
import eu.dnetlib.domain.wfs.graph.Node;
import eu.dnetlib.domain.wfs.templates.WfParam;
import eu.dnetlib.domain.wfs.templates.WfTemplate;

public class WfConfigurationUtils {

	private static final Log log = LogFactory.getLog(WfConfigurationUtils.class);

	public static boolean isConfigured(final List<ImmutablePair<String, WfTemplate>> wfTemplates, final WfConfiguration conf) {

		final Map<String, Object> confParams = allConfiguredParameters(wfTemplates, conf);

		final List<String> invalids = wfTemplates.stream()
				.map(ImmutablePair::getValue)
				.map(WfTemplate::getParameters)
				.flatMap(List::stream)
				.filter(WfParam::isRequired)
				.filter(p -> StringUtils.isBlank(p.getDefaultValue()))
				.map(WfParam::getName)
				.filter(p -> (confParams.get(p) == null) || StringUtils.isBlank(confParams.get(p).toString()))
				.toList();

		if (log.isDebugEnabled()) {
			log.debug(String.format("WfConf %s not configured: there are %s required parameters without value:", conf.getId(), invalids.size()));
			invalids.forEach(p -> log.debug(" - " + p));
		}

		return invalids.isEmpty();
	}

	public static Map<String, Object> allConfiguredParameters(final List<ImmutablePair<String, WfTemplate>> wfTemplates, final WfConfiguration conf) {
		final Map<String, Object> all = new HashMap<>();

		for (final ImmutablePair<String, WfTemplate> pair : wfTemplates) {
			final WfTemplate tmpl = pair.getValue();

			if (tmpl.getParameters() != null) {
				tmpl.getParameters()
						.stream()
						.filter(p -> StringUtils.isNotBlank(p.getDefaultValue()))
						.forEach(p -> all.put(p.getName(), p.getDefaultValue()));
			}

			if (conf != null) {
				if (conf.getUserParams() != null) {
					all.putAll(conf.getSystemParams());
				}
				if (conf.getSystemParams() != null) {
					all.putAll(conf.getUserParams());
				}
			}

			if (log.isDebugEnabled()) {
				log.debug("** CONFIGURED INPUT WF PARAMETERS **");
				all.forEach((k, v) -> log.debug("* " + k + ": " + v));
				log.debug("************************************");

			}
		}

		return all;
	}

	public static List<Node> compositeGraph(final List<ImmutablePair<String, WfTemplate>> wfTemplates) {
		if (wfTemplates.size() == 0) { return new ArrayList<>(); }
		if (wfTemplates.size() == 1) { return wfTemplates.get(0).getValue().getGraph(); }

		final List<Node> res = new ArrayList<>();
		for (int i = 0; i < wfTemplates.size(); i++) {

			final String wfName = wfTemplates.get(i).getKey();
			final List<Node> nodes = wfTemplates.get(i).getValue().getGraph();

			final Node beginNode = new Node(wfName + ".BEGIN");
			beginNode.setStart(i == 0);

			final Node endNode = new Node(wfName + ".END");
			endNode.setJoin(true);

			if (i < (wfTemplates.size() - 1)) {
				endNode.getArcs().add(new Arc(wfTemplates.get(i + 1).getKey() + ".BEGIN"));
			} else {
				endNode.getArcs().add(new Arc(WorkflowsConstants.SUCCESS_NODE));
			}

			for (final Node n : nodes) {
				final String newName = wfName + "." + n.getName();
				n.setName(newName);

				if (n.isStart()) {
					n.setStart(false);
					beginNode.getArcs().add(new Arc(newName));
				}

				if (n.getArcs().size() > 0) {
					for (final Arc arc : n.getArcs()) {
						if (WorkflowsConstants.SUCCESS_NODE.equals(arc.getTo())) {
							arc.setTo(endNode.getName());
						} else {
							arc.setTo(wfName + "." + arc.getTo());
						}
					}
				} else {
					n.getArcs().add(new Arc(endNode.getName()));
				}

				res.add(n);
			}

			res.add(beginNode);
			res.add(endNode);
		}
		return res;
	}

	public static String calculateFamily(final WfConfiguration conf, final boolean destroy) {
		if (destroy) {
			if (StringUtils.isNotBlank(conf.getDsId())) { return WorkflowsConstants.REPO_BYE_FAMILY; }

			return WorkflowsConstants.GENERIC_DESTROY_WF_FAMILY;
		}

		if (!StringUtils.isBlank(conf.getSection())) { return conf.getSection(); }

		if (StringUtils.isNotBlank(conf.getDsId())) { return WorkflowsConstants.AGGREGATION_FAMILY; }

		return WorkflowsConstants.UNKNOWN_FAMILY;
	}

}
