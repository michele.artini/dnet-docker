package eu.dnetlib.wfs.repository;

import eu.dnetlib.domain.wfs.conf.WfSection;
import eu.dnetlib.utils.ReadOnlyRepository;

public interface WfSectionRepository extends ReadOnlyRepository<WfSection, String> {

}
