package eu.dnetlib.wfs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.wfs.subscriptions.WfSubscription;
import eu.dnetlib.domain.wfs.subscriptions.WfSubscriptionPK;

public interface WfSubscriptionRepository extends JpaRepository<WfSubscription, WfSubscriptionPK> {

	List<WfSubscription> findByWfConfigurationId(String wfConfigurationId);

	void deleteByWfConfigurationId(String wfConfigurationId);
}
