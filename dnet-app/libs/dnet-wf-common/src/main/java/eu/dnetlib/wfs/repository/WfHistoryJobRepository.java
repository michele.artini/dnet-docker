package eu.dnetlib.wfs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.wfs.jobs.WfHistoryJob;

public interface WfHistoryJobRepository extends JpaRepository<WfHistoryJob, String> {

}
