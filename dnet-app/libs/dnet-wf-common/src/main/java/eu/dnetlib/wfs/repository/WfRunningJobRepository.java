package eu.dnetlib.wfs.repository;

import java.time.LocalDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import eu.dnetlib.domain.wfs.jobs.JobStatus;
import eu.dnetlib.domain.wfs.jobs.WfRunningJob;

public interface WfRunningJobRepository extends JpaRepository<WfRunningJob, String> {

	@Modifying
	@Transactional
	@Query(value = "update WfRunningJob set wfExecutor = ?2, status = ?3, startDate = ?4, lastUpdate = ?4 where processId = ?1 and wfExecutor is NULL")
	void tryAssegnment(String id, String workerId, JobStatus status, LocalDateTime startDate);

	@Modifying
	@Transactional
	@Query(value = "delete from WfRunningJob where endDate is NOT NULL and endDate < ?1 and status IN ('success','failure','killed')")
	void deleteOldJobs(LocalDateTime date);

}
