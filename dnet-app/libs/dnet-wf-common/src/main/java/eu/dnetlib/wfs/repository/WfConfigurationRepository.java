package eu.dnetlib.wfs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.wfs.conf.WfConfiguration;

public interface WfConfigurationRepository extends JpaRepository<WfConfiguration, String> {

	List<WfConfiguration> findBySection(String section);

	List<WfConfiguration> findByApiId(String apiId);

}
