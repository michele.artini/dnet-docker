package eu.dnetlib.wfs.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import eu.dnetlib.domain.wfs.jobs.JobStatus;
import eu.dnetlib.domain.wfs.jobs.WfJournalEntry;
import eu.dnetlib.utils.ReadOnlyRepository;

public interface WfJournalEntryRepository extends ReadOnlyRepository<WfJournalEntry, String> {

	// IMPORTANT: Postgres, by default, should return nulls first when you specify 'ORDER BY DESC'.
	// Otherwise it will be necessary to implement the native queries specifying 'ORDER BY end_date DESC NULLS FIRST';

	Page<WfJournalEntry> findByLastUpdateBetweenOrderByLastUpdateDesc(LocalDateTime start, LocalDateTime end, Pageable pageable);

	Page<WfJournalEntry> findByWfConfIdOrderByLastUpdateDesc(String id, Pageable pageable);

	Page<WfJournalEntry> findAllByOrderByLastUpdateDesc(Pageable pageable);

	Page<WfJournalEntry> findByDsIdOrderByLastUpdateDesc(String dsId, Pageable pageable);

	Page<WfJournalEntry> findByApiIdOrderByLastUpdateDesc(String apiId, Pageable pageable);

	List<WfJournalEntry> findByStatus(JobStatus status);

	Optional<WfJournalEntry> findFirstByWfConfIdAndEndDateNotNullOrderByEndDateDesc(String id);

}
