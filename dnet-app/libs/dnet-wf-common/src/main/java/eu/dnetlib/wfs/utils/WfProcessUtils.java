package eu.dnetlib.wfs.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class WfProcessUtils {

	private static String oldGeneratedId = "";

	private static final DateTimeFormatter processIdFormatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss_S");

	private static final Log log = LogFactory.getLog(WfProcessUtils.class);

	public static synchronized String generateProcessId() {
		String id = "";
		do {
			id = "wf_" + LocalDateTime.now().format(processIdFormatter);
			log.info("Generated processID " + id);
		} while (oldGeneratedId.equals(id));

		oldGeneratedId = id;

		return id;
	}
}
