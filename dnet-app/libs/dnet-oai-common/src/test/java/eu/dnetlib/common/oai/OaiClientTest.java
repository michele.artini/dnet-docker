package eu.dnetlib.common.oai;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.domain.oai.OaiIdentifier;
import eu.dnetlib.domain.oai.OaiInfo;
import eu.dnetlib.domain.oai.OaiMetadataFormat;
import eu.dnetlib.domain.oai.OaiRecord;
import eu.dnetlib.domain.oai.OaiSet;

@Disabled
public class OaiClientTest {

	private OaiClient oai;

	private final ObjectMapper mapper = new ObjectMapper();

	private final String BASE_URL = "https://openportal.isti.cnr.it/oai";

	@BeforeEach
	void setUp() throws Exception {
		this.oai = new OaiClient();
	}

	@Test
	void testListSets() throws Exception {
		final OaiResponse<List<OaiSet>> res = this.oai.listSets(this.BASE_URL, null);

		System.err.println(this.mapper.writeValueAsString(res));
	}

	@Test
	void testListMetadataFormat() throws Exception {
		final OaiResponse<List<OaiMetadataFormat>> res = this.oai.listMetadataFormats(this.BASE_URL);

		System.err.println(this.mapper.writeValueAsString(res));
	}

	@Test
	void testListRecords() throws Exception {
		final OaiResponse<List<OaiRecord<String, String>>> res = this.oai.listRecords(this.BASE_URL, "oai_dc", "ISTI", null);

		System.err.println(this.mapper.writeValueAsString(res));
	}

	@Test
	void testListIdentifiers() throws Exception {
		final OaiResponse<List<OaiIdentifier<String>>> res = this.oai.listIdentifiers(this.BASE_URL, "oai_dc", "ISTI", null);

		System.err.println(this.mapper.writeValueAsString(res));
	}

	@Test
	void testGetRecord() throws Exception {
		final OaiResponse<OaiRecord<String, String>> res =
				this.oai.getRecord(this.BASE_URL, "oai_dc", "oai:dnet:people______::1ba6ca3a2416b67b4c2206f2d6b865da");

		System.err.println(this.mapper.writeValueAsString(res));

	}

	@Test
	void testInfo() throws Exception {
		final OaiResponse<OaiInfo<String>> res = this.oai.info(this.BASE_URL);

		System.err.println(this.mapper.writeValueAsString(res));
	}

}
