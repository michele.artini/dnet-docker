package eu.dnetlib.common.oai;

import org.dom4j.Element;

import eu.dnetlib.domain.oai.OaiMetadataFormat;

public class OaiMetadataFormatImpl implements OaiMetadataFormat {

	private static final long serialVersionUID = 1695382415748936665L;

	private String metadataPrefix;

	private String metadataSchema;

	private String metadataNamespace;

	public OaiMetadataFormatImpl(final Element node) {
		this.metadataPrefix = node.valueOf("./*[local-name()='metadataPrefix']");
		this.metadataSchema = node.valueOf("./*[local-name()='schema']");
		this.metadataNamespace = node.valueOf("./*[local-name()='metadataNamespace']");
	}

	@Override
	public String getMetadataPrefix() {
		return this.metadataPrefix;
	}

	public void setMetadataPrefix(final String metadataPrefix) {
		this.metadataPrefix = metadataPrefix;
	}

	@Override
	public String getMetadataSchema() {
		return this.metadataSchema;
	}

	public void setMetadataSchema(final String metadataSchema) {
		this.metadataSchema = metadataSchema;
	}

	@Override
	public String getMetadataNamespace() {
		return this.metadataNamespace;
	}

	public void setMetadataNamespace(final String metadataNamespace) {
		this.metadataNamespace = metadataNamespace;
	}

}
