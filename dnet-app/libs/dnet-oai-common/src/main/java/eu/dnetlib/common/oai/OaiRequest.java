package eu.dnetlib.common.oai;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class OaiRequest {

	private final String baseUrl;
	private final String verb;
	private final String mdf;
	private final String set;
	private final String id;
	private final String token;

	public static OaiRequest newIdentify(final String baseUrl) {
		return new OaiRequest(baseUrl, OaiVerb.IDENTIFY.getVerb(), null, null, null, null);
	}

	public static OaiRequest newListMetadataFormats(final String baseUrl) {
		return new OaiRequest(baseUrl, OaiVerb.LIST_METADATA_FORMATS.getVerb(), null, null, null, null);
	}

	public static OaiRequest newListSets(final String baseUrl) {
		return new OaiRequest(baseUrl, OaiVerb.LIST_SETS.getVerb(), null, null, null, null);
	}

	public static OaiRequest newListSetsWithToken(final String baseUrl, final String reusumptionToken) {
		return new OaiRequest(baseUrl, OaiVerb.LIST_SETS.getVerb(), null, null, null, reusumptionToken);
	}

	public static OaiRequest newListRecords(final String baseUrl, final String mdPrefix, final String set) {
		return new OaiRequest(baseUrl, OaiVerb.LIST_RECORDS.getVerb(), mdPrefix, set, null, null);
	}

	public static OaiRequest newListRecordsWithToken(final String baseUrl, final String reusumptionToken) {
		return new OaiRequest(baseUrl, OaiVerb.LIST_RECORDS.getVerb(), null, null, null, reusumptionToken);
	}

	public static OaiRequest newListIdentifiers(final String baseUrl, final String mdPrefix, final String set) {
		return new OaiRequest(baseUrl, OaiVerb.LIST_IDENTIFIERS.getVerb(), mdPrefix, set, null, null);
	}

	public static OaiRequest newListIdentifiersWithToken(final String baseUrl, final String reusumptionToken) {
		return new OaiRequest(baseUrl, OaiVerb.LIST_IDENTIFIERS.getVerb(), null, null, null, reusumptionToken);
	}

	public static OaiRequest newGetRecord(final String baseUrl, final String mdPrefix, final String id) {
		return new OaiRequest(baseUrl, OaiVerb.GET_RECORD.getVerb(), mdPrefix, null, id, null);
	}

	private OaiRequest(final String baseUrl, final String verb, final String mdf, final String set, final String id, final String token) {
		this.baseUrl = baseUrl;
		this.verb = verb;
		this.mdf = mdf;
		this.set = set;
		this.id = id;
		this.token = token;
	}

	public String getBaseUrl() {
		return this.baseUrl;
	}

	public String getVerb() {
		return this.verb;
	}

	public String getMdf() {
		return this.mdf;
	}

	public String getSet() {
		return this.set;
	}

	public String getId() {
		return this.id;
	}

	public String getToken() {
		return this.token;
	}

	public Map<String, Object> toQueryParams() {
		final Map<String, Object> params = new HashMap<>();

		if (StringUtils.isNotBlank(this.verb)) {
			params.put("verb", this.verb);
		}
		if (StringUtils.isNotBlank(this.mdf)) {
			params.put("metadataPrefix", this.mdf);
		}
		if (StringUtils.isNotBlank(this.set)) {
			params.put("set", this.set);
		}
		if (StringUtils.isNotBlank(this.id)) {
			params.put("identifier", this.id);
		}
		if (StringUtils.isNotBlank(this.token)) {
			params.put("resumptionToken", this.token);
		}

		return params;
	}

}
