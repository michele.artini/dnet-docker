package eu.dnetlib.common.oai;

import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

import eu.dnetlib.domain.oai.OaiIdentifier;

public class OaiIdentifierImpl implements OaiIdentifier<String> {

	private static final long serialVersionUID = -2097987180475959419L;

	private String id;

	private String date;

	private boolean deleted;

	private List<String> sets = new ArrayList<>();

	public OaiIdentifierImpl(final Element node) {
		this.id = node.valueOf("./*[local-name() = 'identifier']");
		this.date = node.valueOf("./*[local-name() = 'datestamp']");
		this.deleted = "deleted".equalsIgnoreCase(node.valueOf("@status"));

		node.selectNodes("./*[local-name() = 'setSpec']").forEach(s -> this.sets.add(s.getText()));
	}

	@Override
	public String getId() {
		return this.id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	@Override
	public String getDate() {
		return this.date;
	}

	public void setDate(final String date) {
		this.date = date;
	}

	@Override
	public List<String> getSets() {
		return this.sets;
	}

	public void setSets(final List<String> sets) {
		this.sets = sets;
	}

	@Override
	public boolean isDeleted() {
		return this.deleted;
	}

	public void setDeleted(final boolean deleted) {
		this.deleted = deleted;
	}
}
