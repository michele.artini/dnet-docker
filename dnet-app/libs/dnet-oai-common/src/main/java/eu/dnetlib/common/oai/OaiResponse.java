package eu.dnetlib.common.oai;

import java.io.Serializable;

public class OaiResponse<T> implements Serializable {

	private static final long serialVersionUID = -7841519266909952558L;
	private String url;
	private String verb;
	private long time;
	private int httpCode;
	private boolean valid;
	private String error;
	private int size;
	private int cursor;
	private int total;
	private T body;
	private String resumptionToken;

	public void setUrl(final String url) {
		this.url = url;
	}

	public String getUrl() {
		return this.url;
	}

	public String getVerb() {
		return this.verb;
	}

	public void setVerb(final String verb) {
		this.verb = verb;
	}

	public long getTime() {
		return this.time;
	}

	public void setTime(final long time) {
		this.time = time;
	}

	public int getHttpCode() {
		return this.httpCode;
	}

	public void setHttpCode(final int httpCode) {
		this.httpCode = httpCode;
	}

	public boolean isValid() {
		return this.valid;
	}

	public void setValid(final boolean valid) {
		this.valid = valid;
	}

	public String getError() {
		return this.error;
	}

	public void setError(final String error) {
		this.error = error;
	}

	public int getSize() {
		return this.size;
	}

	public void setSize(final int size) {
		this.size = size;
	}

	public int getCursor() {
		return this.cursor;
	}

	public void setCursor(final int cursor) {
		this.cursor = cursor;
	}

	public int getTotal() {
		return this.total;
	}

	public void setTotal(final int total) {
		this.total = total;
	}

	public T getBody() {
		return this.body;
	}

	public void setBody(final T body) {
		this.body = body;
	}

	public String getResumptionToken() {
		return this.resumptionToken;
	}

	public void setResumptionToken(final String resumptionToken) {
		this.resumptionToken = resumptionToken;
	}

}
