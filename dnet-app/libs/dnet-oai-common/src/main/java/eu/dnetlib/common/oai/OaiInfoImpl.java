package eu.dnetlib.common.oai;

import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

import eu.dnetlib.domain.oai.OaiInfo;

public class OaiInfoImpl implements OaiInfo<String> {

	private static final long serialVersionUID = -1062314589986610721L;

	private String repositoryName;

	private String baseURL;

	private String protocolVersion;

	private String earliestDatestamp;

	private String deletedRecord;

	private String granularity;

	private List<String> adminEmails = new ArrayList<>();

	private List<String> compressions = new ArrayList<>();

	private List<String> descriptions = new ArrayList<>();

	public OaiInfoImpl(final Element node) {
		this.repositoryName = node.valueOf("./*[local-name() = 'repositoryName']");
		this.baseURL = node.valueOf("./*[local-name() = 'baseURL']");
		this.protocolVersion = node.valueOf("./*[local-name() = 'protocolVersion']");
		this.earliestDatestamp = node.valueOf("./*[local-name() = 'earliestDatestamp']");
		this.deletedRecord = node.valueOf("./*[local-name() = 'deletedRecord']");
		this.granularity = node.valueOf("./*[local-name() = 'granularity']");
		node.selectNodes("./*[local-name() = 'adminEmail']").forEach(n -> this.adminEmails.add(n.getText()));
		node.selectNodes("./*[local-name() = 'compression']").forEach(n -> this.compressions.add(n.getText()));
		node.selectNodes("./*[local-name() = 'description']").forEach(n -> this.descriptions.add(n.getText()));
	}

	@Override
	public String getRepositoryName() {
		return this.repositoryName;
	}

	public void setRepositoryName(final String repositoryName) {
		this.repositoryName = repositoryName;
	}

	@Override
	public String getBaseURL() {
		return this.baseURL;
	}

	public void setBaseURL(final String baseURL) {
		this.baseURL = baseURL;
	}

	@Override
	public String getProtocolVersion() {
		return this.protocolVersion;
	}

	public void setProtocolVersion(final String protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	@Override
	public String getEarliestDatestamp() {
		return this.earliestDatestamp;
	}

	public void setEarliestDatestamp(final String earliestDatestamp) {
		this.earliestDatestamp = earliestDatestamp;
	}

	@Override
	public String getDeletedRecord() {
		return this.deletedRecord;
	}

	public void setDeletedRecord(final String deletedRecord) {
		this.deletedRecord = deletedRecord;
	}

	@Override
	public String getGranularity() {
		return this.granularity;
	}

	public void setGranularity(final String granularity) {
		this.granularity = granularity;
	}

	@Override
	public List<String> getAdminEmails() {
		return this.adminEmails;
	}

	public void setAdminEmails(final List<String> adminEmails) {
		this.adminEmails = adminEmails;
	}

	@Override
	public List<String> getCompressions() {
		return this.compressions;
	}

	public void setCompressions(final List<String> compressions) {
		this.compressions = compressions;
	}

	@Override
	public List<String> getDescriptions() {
		return this.descriptions;
	}

	public void setDescriptions(final List<String> descriptions) {
		this.descriptions = descriptions;
	}

}
