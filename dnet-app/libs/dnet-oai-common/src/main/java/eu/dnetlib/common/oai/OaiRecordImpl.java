package eu.dnetlib.common.oai;

import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;
import org.dom4j.Node;

import eu.dnetlib.domain.oai.OaiRecord;

public class OaiRecordImpl implements OaiRecord<String, String> {

	private static final long serialVersionUID = -2097987180475959419L;

	private String id;

	private String body;

	private String date;

	private List<String> sets = new ArrayList<>();

	private boolean deleted;

	public OaiRecordImpl(final Element node) {

		this.id = node.valueOf("./*[local-name() = 'header']/*[local-name() = 'identifier']");
		this.date = node.valueOf("./*[local-name() = 'header']/*[local-name() = 'datestamp']");
		this.deleted = "deleted".equalsIgnoreCase(node.valueOf("./*[local-name() = 'header']/@status"));

		node.selectNodes("./*[local-name() = 'header']/*[local-name() = 'setSpec']").forEach(s -> this.sets.add(s.getText()));

		final Node mdNode = node.selectSingleNode("./*[local-name() = 'metadata']/*");
		if (mdNode != null) {
			this.body = mdNode.asXML();
		}
	}

	@Override
	public String getId() {
		return this.id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	@Override
	public String getBody() {
		return this.body;
	}

	public void setBody(final String body) {
		this.body = body;
	}

	@Override
	public String getDate() {
		return this.date;
	}

	public void setDate(final String date) {
		this.date = date;
	}

	@Override
	public List<String> getSets() {
		return this.sets;
	}

	public void setSets(final List<String> sets) {
		this.sets = sets;
	}

	@Override
	public boolean isDeleted() {
		return this.deleted;
	}

	public void setDeleted(final boolean deleted) {
		this.deleted = deleted;
	}
}
