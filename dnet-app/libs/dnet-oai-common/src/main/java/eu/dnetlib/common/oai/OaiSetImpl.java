package eu.dnetlib.common.oai;

import org.dom4j.Element;

import eu.dnetlib.domain.oai.OaiSet;

public class OaiSetImpl implements OaiSet {

	private static final long serialVersionUID = -8488295530179957767L;

	private String setSpec;
	private String setName;
	private String description;

	public OaiSetImpl(final Element node) {
		this.setSpec = node.valueOf("./*[local-name()='setSpec']");
		this.setName = node.valueOf("./*[local-name()='setName']");
		this.description = node.valueOf("./*[local-name()='setDescription']");
	}

	@Override
	public String getSetSpec() {
		return this.setSpec;
	}

	public void setSetSpec(final String setSpec) {
		this.setSpec = setSpec;
	}

	@Override
	public String getSetName() {
		return this.setName;
	}

	public void setSetName(final String setName) {
		this.setName = setName;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

}
