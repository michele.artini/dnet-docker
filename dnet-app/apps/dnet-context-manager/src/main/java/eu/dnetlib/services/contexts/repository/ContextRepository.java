package eu.dnetlib.services.contexts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.context.Context;

public interface ContextRepository extends JpaRepository<Context, String> {

}
