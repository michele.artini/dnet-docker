package eu.dnetlib.services.contexts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.context.Category;

public interface CategoryRepository extends JpaRepository<Category, String> {

	long countByParent(String parent);

	Iterable<Category> findByParentOrderById(String parent);
}
