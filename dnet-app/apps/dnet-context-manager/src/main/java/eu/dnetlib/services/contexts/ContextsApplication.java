package eu.dnetlib.services.contexts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import eu.dnetlib.common.app.AbstractDnetApp;
import eu.dnetlib.domain.context.Category;
import eu.dnetlib.domain.context.ConceptLevel0;
import eu.dnetlib.domain.context.ConceptLevel1;
import eu.dnetlib.domain.context.ConceptLevel2;
import eu.dnetlib.domain.context.Context;
import eu.dnetlib.domain.service.ServiceType;

@SpringBootApplication
@EntityScan(basePackageClasses = { Context.class, Category.class, ConceptLevel0.class, ConceptLevel1.class, ConceptLevel2.class })
public class ContextsApplication extends AbstractDnetApp {
	// TODO (LOW PRIORITY): UPDATE THE MODEL AND THE APIs ACCORDING TO THE LAST VERSION IN OPENAIRE

	public static void main(final String[] args) {
		SpringApplication.run(ContextsApplication.class, args);
	}

	@Override
	protected ServiceType serviceType() {
		return ServiceType.context_manager;
	}

}
