package eu.dnetlib.services.contexts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.context.ConceptLevel0;

public interface ConceptLevel0Repository extends JpaRepository<ConceptLevel0, String> {

	long countByParent(String parent);

	Iterable<ConceptLevel0> findByParentOrderById(String parent);

}
