package eu.dnetlib.services.contexts.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.controller.DnetRestController;
import eu.dnetlib.services.contexts.importer.ContextImporter;
import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/import")
public class ImporterController extends DnetRestController {

	// EXAMPLE:
	// find ./VocabularyDSResourceType/ -name "*.xml" -exec curl -X POST "http://localhost:8280/api/import/vocabulary" -H "accept: */*" -H
	// "Content-Type: text/plain" --data-binary @{} \;
	// find ./DedupConfigurationDSResources/ -name "*.xml" -exec curl -X POST "http://localhost:8280/api/import/resource" -H "accept: */*"
	// -H "Content-Type: text/plain" --data-binary @{} \;

	@Autowired
	private ContextImporter contextImporter;

	@PostMapping(value = "/context", consumes = {
			MediaType.TEXT_PLAIN_VALUE, MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE
	})
	public List<String> importContext(final HttpServletRequest request) throws DocumentException, IOException {

		final String xml = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);

		contextImporter.loadFromOldProfile(xml);

		return Arrays.asList("Done.");

	}

}
