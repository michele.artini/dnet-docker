package eu.dnetlib.services.contexts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.context.ConceptLevel2;

public interface ConceptLevel2Repository extends JpaRepository<ConceptLevel2, String> {

	long countByParent(String parent);

	Iterable<ConceptLevel2> findByParentOrderById(String parent);

}
