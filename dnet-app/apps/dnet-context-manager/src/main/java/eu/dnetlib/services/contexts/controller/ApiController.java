package eu.dnetlib.services.contexts.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;

import eu.dnetlib.common.controller.DnetRestController;
import eu.dnetlib.domain.context.Category;
import eu.dnetlib.domain.context.Context;
import eu.dnetlib.domain.context.CtxChildInfo;
import eu.dnetlib.errors.InformationServiceException;
import eu.dnetlib.services.contexts.service.ContextService;

@RestController
@RequestMapping("/api/contexts")
public class ApiController extends DnetRestController {

	@Autowired
	private ContextService contextService;

	@GetMapping("/")
	public List<Context> listContexts() {
		return contextService.listContexts();
	}

	@GetMapping("/{ctxId}/full")
	public ObjectNode getContextFull(@PathVariable final String ctxId) throws InformationServiceException {
		return contextService.getContextFull(ctxId);
	}

	@GetMapping("/{ctxId}")
	public Context getContext(@PathVariable final String ctxId) throws InformationServiceException {
		return contextService.getContext(ctxId);
	}

	@GetMapping("/{parent}/categories")
	public Iterable<Category> listCategories(@PathVariable final String parent) {
		return contextService.listCategories(parent);
	}

	@GetMapping("/{level}/{parent}/concepts")
	public Iterable<? extends CtxChildInfo> listCategories(@PathVariable final int level, @PathVariable final String parent)
			throws InformationServiceException {
		return contextService.listConcepts(level, parent);
	}
}
