package eu.dnetlib.services.contexts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.context.ConceptLevel1;

public interface ConceptLevel1Repository extends JpaRepository<ConceptLevel1, String> {

	long countByParent(String parent);

	Iterable<ConceptLevel1> findByParentOrderById(String parent);

}
