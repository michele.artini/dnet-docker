package eu.dnetlib.services.dsm.controller;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.VocabularyClient;
import eu.dnetlib.common.oai.OaiClient;
import eu.dnetlib.common.oai.OaiResponse;
import eu.dnetlib.domain.common.KeyValue;
import eu.dnetlib.domain.vocabulary.VocabularyTerm;
import eu.dnetlib.services.dsm.service.ProtocolService;
import eu.dnetlib.services.dsm.utils.DsmBrowsableFields;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api")
@Tag(name = "OpenAIRE DSM API - other methods", description = "the OpenAIRE Datasource Manager API - other methods")
public class OtherController extends AbstractDsmController {

	@Autowired
	private DnetServiceClientFactory clientFactory;

	@Autowired
	private ProtocolService protocolService;

	@Value("${dsm.compatibilityLevels.vocabulary}")
	private String compatibilityLevelsVocabulary;

	@Value("${dsm.contentDescTypes.vocabulary}")
	private String contentDescTypesVocabulary;

	@GetMapping("/conf")
	public Map<String, Iterable<?>> configuration() {
		final Map<String, Iterable<?>> map = new LinkedHashMap<>();

		final VocabularyClient vocabularyClient = this.clientFactory.getClient(VocabularyClient.class);

		map.put("browsableFields", Arrays.stream(DsmBrowsableFields.values())
				.map(f -> new KeyValue<>(f.name(), f.desc))
				.collect(Collectors.toList()));
		map.put("protocols", this.protocolService.listProtocols());
		map.put("compatibilityLevels", vocabularyClient.listTerms(this.compatibilityLevelsVocabulary)
				.stream()
				.map(VocabularyTerm::getCode)
				.sorted()
				.collect(Collectors.toList()));
		map.put("contentDescTypes", vocabularyClient.listTerms(this.contentDescTypesVocabulary)
				.stream()
				.map(VocabularyTerm::getCode)
				.sorted()
				.collect(Collectors.toList()));

		return map;
	}

	@GetMapping("/oai-explorer/info")
	public OaiResponse<?> oaiExplorerInfo(@RequestParam final String baseUrl) throws Exception {
		return new OaiClient().info(baseUrl);
	}

	@GetMapping("/oai-explorer/sets")
	public OaiResponse<?> oaiExplorerSets(@RequestParam final String baseUrl, @RequestParam(required = false) final String token) throws Exception {
		return new OaiClient().listSets(baseUrl, token);
	}

	@GetMapping("/oai-explorer/mdformats")
	public OaiResponse<?> oaiExplorerMdFormats(@RequestParam final String baseUrl) throws Exception {
		return new OaiClient().listMetadataFormats(baseUrl);
	}

	@GetMapping("/oai-explorer/records")
	public OaiResponse<?> oaiExplorerRecords(@RequestParam final String baseUrl,
			@RequestParam(defaultValue = "oai_dc") final String format,
			@RequestParam(required = false) final String set,
			@RequestParam(required = false) final String token) throws Exception {
		return new OaiClient().listRecords(baseUrl, format, set, token);
	}

	@GetMapping("/oai-explorer/identifiers")
	public OaiResponse<?> oaiExplorerIdentifiers(@RequestParam final String baseUrl,
			@RequestParam(defaultValue = "oai_dc") final String format,
			@RequestParam(required = false) final String set,
			@RequestParam(required = false) final String token) throws Exception {
		return new OaiClient().listIdentifiers(baseUrl, format, set, token);
	}

	@GetMapping("/oai-explorer/record")
	public OaiResponse<?> oaiExplorerSingleRecord(@RequestParam final String baseUrl,
			@RequestParam(defaultValue = "oai_dc") final String format,
			@RequestParam final String id) throws Exception {
		return new OaiClient().getRecord(baseUrl, format, id);
	}

}
