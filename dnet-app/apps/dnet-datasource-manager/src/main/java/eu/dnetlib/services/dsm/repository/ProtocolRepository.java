package eu.dnetlib.services.dsm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.protocol.Protocol;

public interface ProtocolRepository extends JpaRepository<Protocol, String> {

}
