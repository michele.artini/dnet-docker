package eu.dnetlib.services.dsm.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import eu.dnetlib.domain.dsm.info.ApiDetails;
import eu.dnetlib.domain.dsm.info.ApiDetailsResponse;
import eu.dnetlib.domain.dsm.info.DatasourceDetailResponse;
import eu.dnetlib.domain.dsm.info.DatasourceDetails;
import eu.dnetlib.domain.dsm.info.DatasourceInfo;
import eu.dnetlib.domain.dsm.info.DatasourceSearchResponse;
import eu.dnetlib.domain.dsm.info.DatasourceSnippetExtended;
import eu.dnetlib.domain.dsm.info.DatasourceSnippetResponse;
import eu.dnetlib.domain.dsm.info.Header;
import eu.dnetlib.domain.dsm.info.SimpleResponse;

public class ResponseUtils {

	public static ApiDetailsResponse apiResponse(final List<ApiDetails> api, final long total) {
		final ApiDetailsResponse rsp = new ApiDetailsResponse().setApi(api);
		rsp.setHeader(header(total));
		return rsp;
	}

	public static DatasourceSnippetResponse snippetResponse(final List<DatasourceSnippetExtended> snippets, final long total) {
		final DatasourceSnippetResponse rsp = new DatasourceSnippetResponse(snippets);
		rsp.setHeader(header(total));
		return rsp;
	}

	public static DatasourceDetailResponse detailsResponse(final List<DatasourceDetails> details, final long total) {
		final DatasourceDetailResponse rsp = new DatasourceDetailResponse(details);
		rsp.setHeader(header(total));
		return rsp;
	}

	public static DatasourceSearchResponse searchResponse(final List<DatasourceInfo> infos, final long total) {
		final DatasourceSearchResponse rsp = new DatasourceSearchResponse(infos);
		rsp.setHeader(header(total));
		return rsp;
	}

	public static Header header(final Queue<Throwable> errors, final long total) {
		return Header.newInsance()
			.setExceptions(errors)
			.setTotal(total);
	}

	public static Header header(final long total) {
		return header(new LinkedList<>(), total);
	}

	public static <T> SimpleResponse<T> simpleResponse(final List<T> list) {
		final SimpleResponse<T> rsp = new SimpleResponse<T>().setResponse(list);;
		rsp.setHeader(header(new LinkedList<>(), list.size()));
		return rsp;
	}

}
