package eu.dnetlib.services.dsm.controller;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.WfManagerClient;
import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.domain.dsm.Datasource;
import eu.dnetlib.domain.dsm.Identity;
import eu.dnetlib.domain.dsm.info.AggregationHistoryResponse;
import eu.dnetlib.domain.dsm.info.AggregationInfo;
import eu.dnetlib.domain.dsm.info.ApiDetails;
import eu.dnetlib.domain.dsm.info.ApiDetailsResponse;
import eu.dnetlib.domain.dsm.info.DatasourceDetails;
import eu.dnetlib.domain.dsm.info.DatasourceDetailsUpdate;
import eu.dnetlib.domain.dsm.info.DatasourceDetailsWithApis;
import eu.dnetlib.errors.DsmException;
import eu.dnetlib.errors.DsmNotFoundException;
import eu.dnetlib.services.dsm.service.DsmService;
import eu.dnetlib.services.dsm.utils.DsmMappingUtils;
import eu.dnetlib.services.dsm.utils.ResponseUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/ds")
@Tag(name = "OpenAIRE DSM API - datasource methods", description = "the OpenAIRE Datasource Manager API - datasource methods")
public class DsController extends AbstractDsmController {

	private static final Log log = LogFactory.getLog(DsController.class);

	@Autowired
	private DnetServiceClientFactory clientFactory;

	@Autowired
	private DsmService dsmService;

	@PostMapping("/")
	public void saveDs(@Valid @RequestBody final DatasourceDetails datasource) throws DsmException {

		if (dsmService.existDs(datasource.getId())) {
			throw new DsmException(String.format("cannot register, datasource already defined '%s'", datasource.getId()));
		}
		dsmService.saveDs(DsmMappingUtils.asDbEntry(datasource));
		log.info("DS saved, " + datasource.getId());
	}

	@PostMapping("/{dsId}")
	public void updateDatasource(@PathVariable final String dsId, @RequestBody final DatasourceDetailsUpdate d) throws DsmException, DsmNotFoundException {

		// initialize with current values from DB
		final Datasource ds = dsmService.findDs(dsId);

		if (ds == null) { throw new DsmNotFoundException(String.format("ds '%s' does not exist", dsId)); }

		final Datasource update = DsmMappingUtils.asDbEntry(d);
		if (ds.getIdentities() != null) {
			final Set<Identity> identities = new HashSet<>(
					Stream.of(update.getIdentities(), ds.getIdentities())
							.flatMap(Collection::stream)
							.collect(Collectors.toMap(i -> i.getIssuertype() + i.getPid(), Function.identity(), (i1, i2) -> i1))
							.values());
			DsmMappingUtils.copyNonNullProperties(update, ds);
			ds.setIdentities(identities);
		} else {
			DsmMappingUtils.copyNonNullProperties(update, ds);
		}

		dsmService.saveDs(ds);
	}

	@PostMapping("/addWithApis")
	public void saveDsWithApis(@Valid @RequestBody final DatasourceDetailsWithApis d) throws DsmException {
		if (d.getDatasource() == null) { throw new DsmException("Datasource field is null"); }
		if (dsmService.existDs(d.getDatasource().getId())) {
			throw new DsmException(String.format("cannot register, datasource already defined '%s'", d.getDatasource().getId()));
		}
		dsmService.addDsAndApis(DsmMappingUtils.asDbEntry(d.getDatasource()), d.getApis());
	}

	@GetMapping("/{dsId}")
	public Datasource getDs(@PathVariable final String dsId) throws DsmException {
		return dsmService.findDs(dsId);
	}

	@GetMapping("/{dsId}/aggregationhistory")
	public AggregationHistoryResponse aggregationHistory(@PathVariable final String dsId) throws DsmException {
		final StopWatch stop = StopWatch.createStarted();
		final List<AggregationInfo> history = clientFactory.getClient(WfManagerClient.class).getAggregationHistory(dsId);
		final AggregationHistoryResponse rsp = new AggregationHistoryResponse(history);
		rsp.setHeader(ResponseUtils.header(history.size()));

		return prepareResponse(0, rsp.getAggregationInfo().size(), stop, rsp);
	}

	@GetMapping("/{dsId}/managed")
	public boolean isManaged(@PathVariable final String dsId) throws DsmException {
		return dsmService.isManaged(dsId);
	}

	@PostMapping("/{dsId}/manage")
	public void updateManaged(@PathVariable final String dsId, @RequestParam final boolean managed) throws DsmException {
		log.info(String.format("updated ds '%s' managed with '%s'", dsId, managed));
		dsmService.setManaged(dsId, managed);
	}

	@GetMapping("/{dsId}/apis")
	public ApiDetailsResponse listApis(
			@PathVariable final String dsId) throws DsmException {

		final StopWatch stop = StopWatch.createStarted();

		final Datasource ds = dsmService.findDs(dsId);
		final List<Api> apis = dsmService.findApis(dsId);
		final List<ApiDetails> api = apis.stream()
				.map(DsmMappingUtils::asDetails)
				.map(a -> a.setEoscDatasourceType(ds.getEoscDatasourceType()))
				.map(a -> a.setTypology(ds.getTypology()))
				.collect(Collectors.toList());
		final ApiDetailsResponse rsp = ResponseUtils.apiResponse(api, api.size());

		return prepareResponse(0, rsp.getApi().size(), stop, rsp);
	}
}
