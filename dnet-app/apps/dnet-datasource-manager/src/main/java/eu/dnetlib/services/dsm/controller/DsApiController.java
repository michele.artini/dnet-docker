package eu.dnetlib.services.dsm.controller;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.domain.dsm.info.ApiDetails;
import eu.dnetlib.errors.DsmException;
import eu.dnetlib.errors.DsmForbiddenException;
import eu.dnetlib.errors.DsmNotFoundException;
import eu.dnetlib.services.dsm.service.DsmService;
import eu.dnetlib.services.dsm.utils.DsmMappingUtils;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/ds-api")
@Tag(name = "OpenAIRE DSM API - datasource APIs methods", description = "the OpenAIRE Datasource Manager API - datasource APIs methods")
public class DsApiController extends AbstractDsmController {

	private static final Log log = LogFactory.getLog(DsApiController.class);

	@Autowired
	private DsmService dsmService;

	// APIs
	@PostMapping("/")
	public void addApi(@RequestBody final ApiDetails api) throws DsmException {
		if (StringUtils.isBlank(api.getDatasource())) { throw new DsmException("missing datasource id"); }
		if (StringUtils.isBlank(api.getId())) {
			api.setId(DsmMappingUtils.createId(api));
			log.info(String.format("missing api id, created '%s'", api.getId()));
		}
		dsmService.addApi(DsmMappingUtils.asDbEntry(api));
		log.info("API saved, id: " + api.getId());
	}

	@GetMapping("/{apiId}")
	public Api getApi(@PathVariable final String apiId) throws DsmNotFoundException {
		return dsmService.findApi(apiId);
	}

	@DeleteMapping("/{apiId}")
	public void deleteApi(@PathVariable final String apiId) throws DsmForbiddenException, DsmNotFoundException {
		dsmService.deleteApi(apiId);
	}

	@PostMapping("/{apiId}/baseurl")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK"),
			@ApiResponse(responseCode = "500", description = "unexpected error")
	})
	public void updateApiBaseUrl(
			@PathVariable final String apiId,
			@RequestParam final String baseUrl) throws DsmException {

		log.info(String.format("updated api '%s' baseurl with '%s'", apiId, baseUrl));

		dsmService.updateApiBaseUrl(apiId, baseUrl);
	}

	@PostMapping("/{apiId}/compliance")
	public void updateApiCompliance(
			@PathVariable final String apiId,
			@RequestParam final String compliance,
			@RequestParam(required = false, defaultValue = "false") final boolean override) throws DsmException {

		log.info(String.format("updated api '%s' compliance with '%s'", apiId, compliance));

		dsmService.updateApiCompliance(apiId, compliance, override);
	}

	@PostMapping("/{apiId}/oaiset")
	public void updateApiOaiSet(
			@RequestParam final String apiId,
			@RequestParam final String oaiSet) throws DsmException, DsmNotFoundException {

		dsmService.upsertApiOaiSet(apiId, oaiSet);
	}

	@PostMapping("/{apiId}/last-collection")
	public void updateApiCollectionInfo(@PathVariable final String apiId,
			@RequestParam final String mdId,
			@RequestParam final long size) throws DsmException {

		dsmService.updateApiCollectionInfo(apiId, mdId, size);
	}

	@PostMapping("/{apiId}/last-aggregation")
	public void updateApiAggregationInfo(@PathVariable final String apiId,
			@RequestParam final String mdId,
			@RequestParam final long size) throws DsmException {

		dsmService.updateApiAggregationInfo(apiId, mdId, size);
	}

	@PostMapping("/{apiId}/last-download")
	public void updateApiDownloadInfo(@PathVariable final String apiId,
			@RequestParam final String objId,
			@RequestParam final long size) throws DsmException {

		dsmService.updateApiDownloadInfo(apiId, objId, size);
	}

	@PostMapping("/{apiId}/active")
	public void updateApiDownloadInfo(@PathVariable final String apiId,
			@RequestParam final boolean active) throws DsmException {

		dsmService.setApiActive(apiId, active);
	}

	// other operations

}
