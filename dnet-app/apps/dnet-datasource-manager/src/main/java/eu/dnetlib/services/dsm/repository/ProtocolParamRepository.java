package eu.dnetlib.services.dsm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.protocol.ProtocolParam;
import eu.dnetlib.domain.protocol.ProtocolParamPK;

public interface ProtocolParamRepository extends JpaRepository<ProtocolParam, ProtocolParamPK> {

	Iterable<ProtocolParam> findByProtocol(String protocol);

	void deleteByProtocol(String protocol);

}
