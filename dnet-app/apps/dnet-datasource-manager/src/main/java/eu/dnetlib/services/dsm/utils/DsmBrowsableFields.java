package eu.dnetlib.services.dsm.utils;

public enum DsmBrowsableFields {

	type("Datasource typologies"),
	collectedfrom("Collected From"),
	country("Countries"),
	compliance("Compatibility levels"),
	protocol("API protocols"),
	active("API activation"),
	consenttermsofuse("Consent terms of use"),
	fulltextdownload("Fulltext download");

	public final String desc;

	private DsmBrowsableFields(final String desc) {
		this.desc = desc;
	}
}
