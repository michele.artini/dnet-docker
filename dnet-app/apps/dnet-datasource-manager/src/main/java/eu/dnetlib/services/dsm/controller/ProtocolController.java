package eu.dnetlib.services.dsm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.controller.DnetRestController;
import eu.dnetlib.domain.protocol.ProtocolDesc;
import eu.dnetlib.services.dsm.service.ProtocolService;

@RestController
@RequestMapping("/api/protocols")
public class ProtocolController extends DnetRestController {

	@Autowired
	private ProtocolService protocolService;

	@GetMapping("/")
	public List<ProtocolDesc> listProtocols() {
		return protocolService.listProtocols();
	}

	@DeleteMapping("/{protocol}")
	public List<ProtocolDesc> deleteProtocol(@PathVariable final String protocol) {
		protocolService.deleteProtocols(protocol);
		return protocolService.listProtocols();
	}

	@PostMapping("/{protocol}/terms")
	public List<ProtocolDesc> saveTerm(@PathVariable final ProtocolDesc protocol) {
		protocolService.saveProtocol(protocol);
		return protocolService.listProtocols();
	}

}
