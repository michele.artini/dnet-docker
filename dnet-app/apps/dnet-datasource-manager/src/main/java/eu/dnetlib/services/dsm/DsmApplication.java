package eu.dnetlib.services.dsm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import eu.dnetlib.common.app.AbstractDnetApp;
import eu.dnetlib.domain.service.ServiceType;

@SpringBootApplication
@EntityScan("eu.dnetlib.domain")
public class DsmApplication extends AbstractDnetApp {

	public static void main(final String[] args) {
		SpringApplication.run(DsmApplication.class, args);
	}

	@Override
	protected ServiceType serviceType() {
		return ServiceType.datasource_manager;
	}
}
