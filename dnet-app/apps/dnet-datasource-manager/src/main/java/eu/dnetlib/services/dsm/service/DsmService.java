package eu.dnetlib.services.dsm.service;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.VocabularyClient;
import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.domain.dsm.ApiParam;
import eu.dnetlib.domain.dsm.BrowseTerm;
import eu.dnetlib.domain.dsm.Datasource;
import eu.dnetlib.domain.dsm.info.ApiDetails;
import eu.dnetlib.domain.dsm.info.DatasourceDetailResponse;
import eu.dnetlib.domain.dsm.info.DatasourceSnippetResponse;
import eu.dnetlib.domain.dsm.info.RegisteredDatasourceInfo;
import eu.dnetlib.domain.dsm.info.RequestFilter;
import eu.dnetlib.domain.dsm.info.RequestSort;
import eu.dnetlib.domain.dsm.info.RequestSortOrder;
import eu.dnetlib.domain.dsm.info.SimpleDatasourceInfo;
import eu.dnetlib.domain.dsm.info.SimpleResponse;
import eu.dnetlib.domain.dsm.readonly.ApiWithAdditionalInfo;
import eu.dnetlib.domain.dsm.readonly.SimpleDsWithApis;
import eu.dnetlib.errors.DnetRuntimeException;
import eu.dnetlib.errors.DsmException;
import eu.dnetlib.errors.DsmForbiddenException;
import eu.dnetlib.errors.DsmNotFoundException;
import eu.dnetlib.services.dsm.repository.ApiRepository;
import eu.dnetlib.services.dsm.repository.ApiWithAdditionalInfoRepository;
import eu.dnetlib.services.dsm.repository.DatasourceRepository;
import eu.dnetlib.services.dsm.repository.SimpleDsWithApisRepository;
import eu.dnetlib.services.dsm.utils.DatasourceSpecs;
import eu.dnetlib.services.dsm.utils.DsmBrowsableFields;
import eu.dnetlib.services.dsm.utils.DsmMappingUtils;
import eu.dnetlib.services.dsm.utils.ResponseUtils;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;

@Service
public class DsmService {

	private static final Log log = LogFactory.getLog(DsmService.class);

	public static final String OAI = "oai";
	public static final String SET = "set";

	@Autowired
	private DatasourceRepository dsRepository;

	@Autowired
	private ApiRepository apiRepository;

	@Autowired
	private ApiWithAdditionalInfoRepository apiWithAdditionalInfoRepository;

	@Autowired
	private SimpleDsWithApisRepository simpleDsWithApisRepository;

	@Autowired
	private DnetServiceClientFactory clientFactory;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public Object setApiActive;

	@Transactional
	public DatasourceSnippetResponse searchSnippet(final RequestSort requestSortBy,
			final RequestSortOrder order,
			final RequestFilter requestFilter,
			final int page,
			final int size) throws DsmException {

		final Page<Datasource> dsPage = search(requestSortBy, order, requestFilter, page, size);
		return ResponseUtils.snippetResponse(dsPage.map(DsmMappingUtils::asSnippetExtended).getContent(), dsPage.getTotalElements());
	}

	public DatasourceDetailResponse searchDetails(final RequestSort requestSortBy,
			final RequestSortOrder order,
			final RequestFilter requestFilter,
			final int page,
			final int size) throws DsmException {

		final Page<Datasource> dsPage = search(requestSortBy, order, requestFilter, page, size);
		return ResponseUtils.detailsResponse(dsPage.map(DsmMappingUtils::asDetails).getContent(), dsPage.getTotalElements());
	}

	private Page<Datasource> search(final RequestSort requestSortBy,
			final RequestSortOrder order,
			final RequestFilter requestFilter,
			final int page,
			final int size)
			throws DsmException {

		final Specification<Datasource> spec = DatasourceSpecs.dsSpec(requestSortBy, order, requestFilter);
		return dsRepository.findAll(spec, PageRequest.of(page, size));
	}

	public Page<Datasource> searchRegistered(final RequestSort requestSortBy,
			final RequestSortOrder order,
			final RequestFilter requestFilter,
			final int page,
			final int size)
			throws DsmException {

		final Specification<Datasource> spec = DatasourceSpecs.dsSpec(requestSortBy, order, requestFilter).and(DatasourceSpecs.dsRegisteredbyNotNullSpec());
		return dsRepository.findAll(spec, PageRequest.of(page, size));
	}

	public Datasource findDs(final String dsId) throws DsmException {
		return dsRepository.findById(dsId).orElseThrow(() -> new DsmException("Datasource not found. ID: " + dsId));
	}

	public Datasource findDsByNsPrefix(final String prefix) throws DsmException {
		return dsRepository.findByNamespaceprefix(prefix).orElseThrow(() -> new DsmException("Datasource not found. NS Prefix: " + prefix));
	}

	public void setManaged(final Datasource ds, final boolean managed) {
		setManaged(ds.getId(), managed);
	}

	public void setManaged(final String dsId, final boolean managed) {
		log.info(String.format("setting managed = '%s' for ds '%s'", managed, dsId));
		dsRepository.setManaged(dsId, managed);
		apiRepository.setAllApiRemovableForDs(dsId, true);
	}

	public boolean isManaged(final String id) {
		return dsRepository.isManaged(id);
	}

	public void updateApiCompliance(final String apiId, final String compliance, final boolean override) {
		log.info(String.format("setting compatibility = '%s' for ds '%s'", compliance, apiId));
		apiRepository.updateCompatibility(apiId, compliance);
	}

	public List<Api> findApis(final String dsId) {
		return apiRepository.findByDatasource(dsId);
	}

	public void deleteApi(final String apiId) throws DsmForbiddenException, DsmNotFoundException {
		final Api api = apiRepository.findById(apiId).orElseThrow(() -> new DsmNotFoundException("Api not found. ID: " + apiId));
		try {
			if (!api.getRemovable()) { throw new DsmForbiddenException("api is not removable"); }

			apiRepository.deleteById(apiId);
			log.info(String.format("deleted api '%s'", apiId));
		} catch (final EntityNotFoundException e) {
			throw new DsmNotFoundException("api not found");
		}
	}

	public void addApi(final Api api) {
		apiRepository.save(api);
	}

	public boolean existDs(final String dsId) throws DsmException {
		return dsRepository.existsById(dsId);
	}

	public void saveDs(final Datasource d) {
		log.info(String.format("saving datasource '%s'", d.getId()));

		final Datasource datasource = dsRepository.save(d);
		log.info(String.format("saved datasource '%s'", datasource.getId()));
		ensureRegistrationDate(d.getId());
	}

	public void deleteDs(final String dsId) {
		dsRepository.deleteById(dsId);
		log.info(String.format("deleted datasource '%s'", dsId));
	}

	public void updateName(final String dsId, final String officialname, final String englishname) {
		if (StringUtils.isNoneBlank(officialname, englishname)) {
			dsRepository.setDatasourcename(dsId, officialname, englishname);
		} else if (StringUtils.isNotBlank(officialname)) {
			dsRepository.setDatasourcename(dsId, officialname, officialname);
		} else if (StringUtils.isNotBlank(englishname)) {
			dsRepository.setDatasourcename(dsId, englishname, englishname);
		} else {
			throw new DnetRuntimeException("Both datasource names are empty");
		}
	}

	public void updateLogoUrl(final String dsId, final String logourl) throws DsmException {
		dsRepository.setLogoUrl(dsId, logourl);
	}

	public void updateCoordinates(final String dsId, final Double latitude, final Double longitude) {
		dsRepository.setCoordinates(dsId, latitude, longitude);
	}

	public void updateApiBaseUrl(final String apiId, final String baseurl) {
		apiRepository.setBaseurl(apiId, baseurl);
	}

	@Transactional
	public boolean upsertApiOaiSet(final String apiId, final String oaiSet) throws DsmException, DsmNotFoundException {
		final Api api = apiRepository.findById(apiId).orElseThrow(() -> new DsmNotFoundException("Api not found. ID: " + apiId));
		if (!OAI.equalsIgnoreCase(api.getProtocol())) {
			throw new DsmException(String.format("won't add OAI set to a non OAI interface: '%s' has protocol '%s'", apiId, api.getProtocol()));
		}
		final Set<ApiParam> apiParams = api.getApiParams();

		if (!apiParams.stream().anyMatch(ap -> SET.equals(ap.getParam()))) {
			apiRepository.addApiParam(apiId, SET, oaiSet);
			log.info(String.format("added api '%s' oai set with '%s'", apiId, oaiSet));
			return true;
		}
		apiRepository.updateOaiSet(apiId, oaiSet);
		log.info(String.format("updated api '%s' oai set with '%s'", apiId, oaiSet));
		return false;
	}

	public List<String> findApiBaseURLs(final RequestFilter requestFilter, final int page, final int size) throws DsmException {
		final PageRequest pageable = PageRequest.of(page, size);
		final Specification<ApiWithAdditionalInfo> spec = DatasourceSpecs.apiSpec(requestFilter);
		final Set<String> set = apiWithAdditionalInfoRepository.findAll(spec, pageable)
				.getContent()
				.stream()
				.map(ApiWithAdditionalInfo::getBaseurl)
				.filter(StringUtils::isNotBlank)
				.collect(Collectors.toCollection(HashSet::new));
		return new ArrayList<>(set);
	}

	public void updateTimezone(final String dsId, final String timezone) {
		dsRepository.setTimezone(dsId, timezone);
	}

	public void updateEoscDatasourceType(final String dsId, final String type) throws DsmException {
		if (clientFactory.getClient(VocabularyClient.class).isValidTerm("eosc_datasource_types", type)) {
			throw new DsmException(String.format("invalid datasource type '%s', provide one according to vocabulary eosc_datasource_types"));
		}
		dsRepository.setEoscDatasourceType(dsId, type);
	}

	public void updateRegisteringUser(final String dsId, final String registeredBy) throws DsmException {

		ensureRegistrationDate(dsId);

		dsRepository.setRegisteringUser(dsId, registeredBy);

	}

	public void updatePlatform(final String dsId, final String platform) throws DsmException {
		dsRepository.setPlatform(dsId, platform);
	}

	public SimpleResponse<RegisteredDatasourceInfo> searchRecentRegistered(final int size) throws DsmException {
		try {
			final List<RegisteredDatasourceInfo> list =
					querySql("recent_registered_datasources.sql.st", BeanPropertyRowMapper.newInstance(RegisteredDatasourceInfo.class), size);
			return ResponseUtils.simpleResponse(list);
		} catch (final Throwable e) {
			log.error("error searching recent datasources", e);
			throw new DsmException("error searching recent datasources", e);
		}
	}

	public Long countRegisteredAfter(final String fromDate, final String typeFilter) throws DsmException {
		try {
			if (StringUtils.isNotBlank(typeFilter)) {
				final String sql =
						IOUtils.toString(getClass()
								.getResourceAsStream("/eu/dnetlib/openaire/sql/recent_registered_datasources_fromDate_typology.st.sql"), Charset
										.defaultCharset());

				return jdbcTemplate.queryForObject(sql, Long.class, fromDate, typeFilter + "%");
			}
			final String sql =
					IOUtils.toString(getClass().getResourceAsStream("/eu/dnetlib/openaire/sql/recent_registered_datasources_fromDate.st.sql"), Charset
							.defaultCharset());

			return jdbcTemplate.queryForObject(sql, Long.class, fromDate);

		} catch (final Throwable e) {
			log.error("error searching recent datasources", e);
			throw new DsmException("error searching recent datasources", e);
		}
	}

	public SimpleResponse<SimpleDatasourceInfo> searchRecentRegisteredV2(final int size) throws DsmException {
		try {
			final List<SimpleDatasourceInfo> list = querySql("recent_registered_datasources_v2.sql.st", rowMapperForSimpleDatasourceInfo(), size);
			return ResponseUtils.simpleResponse(list);
		} catch (final Throwable e) {
			log.error("error searching recent datasources", e);
			throw new DsmException("error searching recent datasources", e);
		}
	}

	public Long countFirstCollect(final String fromDate, final String typeFilter) throws DsmException {
		try {
			if (StringUtils.isNotBlank(typeFilter)) {
				return querySql("count_first_collected_datasources_fromDate_typology.st.sql", Long.class, typeFilter + "%", fromDate);
			}
			return querySql("count_first_collected_datasources_fromDate.st.sql", Long.class, fromDate);
		} catch (final Throwable e) {
			log.error("error searching datasources using the first collection date", e);
			throw new DsmException("error searching datasources using the first collection date", e);
		}
	}

	public List<SimpleDatasourceInfo> findFirstCollectedAfter(final String fromDate, final String typeFilter) throws DsmException {
		try {
			if (StringUtils.isNotBlank(typeFilter)) {
				return querySql("first_collected_datasources_fromDate_typology.st.sql", rowMapperForSimpleDatasourceInfo(), typeFilter + "%", fromDate);
			}
			return querySql("first_collected_datasources_fromDate.st.sql", rowMapperForSimpleDatasourceInfo(), fromDate);
		} catch (final Throwable e) {
			log.error("error searching datasources using the first collection date", e);
			throw new DsmException("error searching datasources using the first collection date", e);
		}
	}

	private <T> List<T> querySql(final String sqlFile, final RowMapper<T> rowMapper, final Object... params) throws IOException {
		final String sql = IOUtils.toString(getClass().getResourceAsStream("/sql/dsm/" + sqlFile), StandardCharsets.UTF_8);
		return jdbcTemplate.query(sql, rowMapper, params);
	}

	private <T> T querySql(final String sqlFile, final Class<T> clazz, final Object... params) throws IOException {
		final String sql = IOUtils.toString(getClass().getResourceAsStream("/sql/dsm/" + sqlFile), StandardCharsets.UTF_8);
		return jdbcTemplate.queryForObject(sql, clazz, params);
	}

	// HELPER
	private void ensureRegistrationDate(final String dsId) {
		if (!dsRepository.hasRegistrationdate(dsId)) {
			log.info("setting registration date for datasource: " + dsId);
			dsRepository.setRegistrationDate(dsId, LocalDate.now());
		}
	}

	private RowMapper<SimpleDatasourceInfo> rowMapperForSimpleDatasourceInfo() {

		return (rs, rowNum) -> {
			final SimpleDatasourceInfo info = new SimpleDatasourceInfo();

			info.setId(rs.getString("id"));
			info.setOfficialName(rs.getString("officialName"));
			info.setEnglishName(rs.getString("englishName"));
			info.setTypology(rs.getString("typology"));
			info.setEoscType(rs.getString("eoscType"));
			info.setEoscDatasourceType(rs.getString("eoscDatasourceType"));
			info.setRegisteredBy(rs.getString("registeredBy"));
			info.setRegistrationDate(rs.getString("registrationDate"));
			info.setFirstCollectionDate(rs.getString("firstCollectionDate"));
			info.setLastCollectionDate(rs.getString("lastCollectionDate"));
			info.setLastCollectionTotal(rs.getLong("lastCollectionTotal"));

			final Set<String> compatibilities = new HashSet<>();
			Collections.addAll(compatibilities, (String[]) rs.getArray("compatibilities").getArray());

			// The order of the condition is important
			if (compatibilities.contains("openaire-cris_1.1")) {
				info.setCompatibility("openaire-cris_1.1");
			} else if (compatibilities.contains("openaire4.0")) {
				info.setCompatibility("openaire4.0");
			} else if (compatibilities.contains("driver") && compatibilities.contains("openaire2.0")) {
				info.setCompatibility("driver-openaire2.0");
			} else if (compatibilities.contains("driver")) {
				info.setCompatibility("driver");
			} else if (compatibilities.contains("openaire2.0")) {
				info.setCompatibility("openaire2.0");
			} else if (compatibilities.contains("openaire3.0")) {
				info.setCompatibility("openaire3.0");
			} else if (compatibilities.contains("openaire2.0_data")) {
				info.setCompatibility("openaire2.0_data");
			} else if (compatibilities.contains("native")) {
				info.setCompatibility("native");
			} else if (compatibilities.contains("hostedBy")) {
				info.setCompatibility("hostedBy");
			} else if (compatibilities.contains("notCompatible")) {
				info.setCompatibility("notCompatible");
			} else {
				info.setCompatibility("UNKNOWN");
			}

			for (final String s : (String[]) rs.getArray("organizations").getArray()) {
				info.getOrganizations().put(StringUtils.substringBefore(s, "@@@").trim(), StringUtils.substringAfter(s, "@@@").trim());
			}

			return info;
		};
	}

	@Transactional
	public void addDsAndApis(final Datasource ds, final List<ApiDetails> apis) throws DsmException {
		try {
			saveDs(ds);
			if (apis != null) {
				for (final ApiDetails api : apis) {
					api.setDatasource(ds.getId());
					if (StringUtils.isBlank(api.getId())) {
						api.setId(DsmMappingUtils.createId(api));
						log.info(String.format("missing api id, created '%s'", api.getId()));
					}
					addApi(DsmMappingUtils.asDbEntry(api));
					log.info("API saved, id: " + api.getId());
				}
			}
		} catch (final Throwable e) {
			log.error("Error saving ds and/or api", e);
			throw new DsmException("Error saving ds and/or api", e);
		}
	}

	@Transactional
	@Cacheable("brosable_terms")
	public List<BrowseTerm> browseTerm(final DsmBrowsableFields f) {
		final List<BrowseTerm> list = switch (f) {
		case type -> simpleDsWithApisRepository.browseTermsForType();
		case collectedfrom -> simpleDsWithApisRepository.browseTermsForCollectedFrom();
		case compliance -> simpleDsWithApisRepository.browseTermsForCompliance();
		case country -> simpleDsWithApisRepository.browseTermsForCountry();
		case protocol -> simpleDsWithApisRepository.browseTermsForProtocol();
		case active -> simpleDsWithApisRepository.browseTermsForActive();
		case consenttermsofuse -> simpleDsWithApisRepository.browseTermsForConsenttermsofuse();
		case fulltextdownload -> simpleDsWithApisRepository.browseTermsForFulltextdownload();
		default -> throw new RuntimeException("not implemeted");
		};

		return list.stream()
				.filter(t -> StringUtils.isNotBlank(t.getName()))
				.sorted(Comparator.comparing(BrowseTerm::getTotal))
				.toList();
	}

	public Page<SimpleDsWithApis> searchByField(final DsmBrowsableFields f, final String value, final int page, final int size) {
		return switch (f) {
		case type -> simpleDsWithApisRepository.findByType(value, PageRequest.of(page, size));
		case collectedfrom -> simpleDsWithApisRepository.findByCollectedFrom(value, PageRequest.of(page, size));
		case compliance -> simpleDsWithApisRepository.findByCompliance(value, PageRequest.of(page, size));
		case country -> simpleDsWithApisRepository.findByCountry(value, PageRequest.of(page, size));
		case protocol -> simpleDsWithApisRepository.findByProtocol(value, PageRequest.of(page, size));
		case active -> simpleDsWithApisRepository.findByActive(Boolean.valueOf(value), PageRequest.of(page, size));
		case consenttermsofuse -> simpleDsWithApisRepository.findByConsenttermsofuse(Boolean.valueOf(value), PageRequest.of(page, size));
		case fulltextdownload -> simpleDsWithApisRepository.findByFulltextdownload(Boolean.valueOf(value), PageRequest.of(page, size));
		default -> throw new RuntimeException("not implemeted");
		};

	}

	public Page<SimpleDsWithApis> search(final String value, final int page, final int size) {
		return simpleDsWithApisRepository.search(value, PageRequest.of(page, size));
	}

	public Api findApi(final String id) throws DsmNotFoundException {
		return apiRepository.findById(id).orElseThrow(() -> new DsmNotFoundException("Api not found. ID: " + id));
	}

	public Api updateApiCollectionInfo(final String apiId, final String mdId, final long total) throws DsmException {
		if (StringUtils.isNotBlank(mdId)) {
			apiRepository.updateLastCollectionInfo(apiId, mdId, new Timestamp(System.currentTimeMillis()), total);
		} else {
			apiRepository.updateLastCollectionInfo(apiId, null, null, 0);
		}
		return findApi(apiId);
	}

	public Api updateApiAggregationInfo(final String apiId, final String mdId, final long total) throws DsmException {
		if (StringUtils.isNotBlank(mdId)) {
			apiRepository.updateLastAggregationInfo(apiId, mdId, new Timestamp(System.currentTimeMillis()), total);
		} else {
			apiRepository.updateLastAggregationInfo(apiId, null, null, 0);
		}
		return findApi(apiId);
	}

	public Api updateApiDownloadInfo(final String apiId, final String mdId, final long total) throws DsmException {
		if (StringUtils.isNotBlank(mdId)) {
			apiRepository.updateLastDownloadInfo(apiId, mdId, new Timestamp(System.currentTimeMillis()), total);
		} else {
			apiRepository.updateLastDownloadInfo(apiId, null, null, 0);
		}
		return findApi(apiId);
	}

	public void setApiActive(final String apiId, final boolean b) {
		apiRepository.setActive(apiId, b);
	}

}
