package eu.dnetlib.services.dsm.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.domain.dsm.BrowseTerm;
import eu.dnetlib.domain.dsm.Datasource;
import eu.dnetlib.domain.dsm.info.DatasourceDetailResponse;
import eu.dnetlib.domain.dsm.info.DatasourceSnippetResponse;
import eu.dnetlib.domain.dsm.info.RequestFilter;
import eu.dnetlib.domain.dsm.info.RequestSort;
import eu.dnetlib.domain.dsm.info.RequestSortOrder;
import eu.dnetlib.domain.dsm.info.SimpleDatasourceInfo;
import eu.dnetlib.domain.dsm.info.SimpleResponse;
import eu.dnetlib.domain.dsm.readonly.SimpleDsWithApis;
import eu.dnetlib.errors.DsmException;
import eu.dnetlib.services.dsm.service.DsmService;
import eu.dnetlib.services.dsm.utils.DsmBrowsableFields;
import eu.dnetlib.services.dsm.utils.DsmMappingUtils;
import eu.dnetlib.services.dsm.utils.ResponseUtils;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api")
@Tag(name = "OpenAIRE DSM API - search methods", description = "the OpenAIRE Datasource Manager API - search methods")
public class SearchController extends AbstractDsmController {

	@Autowired
	private DsmService dsmService;

	@GetMapping("/browse/{field}")
	public List<BrowseTerm> browse(@PathVariable final String field) {
		return dsmService.browseTerm(DsmBrowsableFields.valueOf(field));
	}

	@GetMapping("/search/{page}/{size}")
	public Page<SimpleDsWithApis> searchByField(
			@PathVariable final int page,
			@PathVariable final int size,
			@RequestParam(required = false) final String field,
			@RequestParam final String value) {

		if (StringUtils.isBlank(field)) { return dsmService.search(value, page, size); }

		return dsmService.searchByField(DsmBrowsableFields.valueOf(field), value, page, size);
	}

	@PostMapping("/search/{page}/{size}/baseUrl")
	public List<String> searchBaseUrls(
			@RequestBody final RequestFilter requestFilter,
			@PathVariable final int page,
			@PathVariable final int size) throws DsmException {

		return dsmService.findApiBaseURLs(requestFilter, page, size);
	}

	@PostMapping("/search/{page}/{size}/details")
	public DatasourceDetailResponse searchDsDetails(
			@RequestParam final RequestSort requestSortBy,
			@RequestParam final RequestSortOrder order,
			@RequestBody final RequestFilter requestFilter,
			@PathVariable final int page,
			@PathVariable final int size) throws DsmException {
		final StopWatch stop = StopWatch.createStarted();

		final DatasourceDetailResponse rsp = dsmService.searchDetails(requestSortBy, order, requestFilter, page, size);
		return prepareResponse(page, size, stop, rsp);
	}

	@PostMapping("/search/{page}/{size}/snippet")
	public DatasourceSnippetResponse searchSnippet(
			@RequestParam final RequestSort requestSortBy,
			@RequestParam final RequestSortOrder order,
			@RequestBody final RequestFilter requestFilter,
			@PathVariable final int page,
			@PathVariable final int size) throws DsmException {

		final StopWatch stop = StopWatch.createStarted();
		final DatasourceSnippetResponse rsp = dsmService.searchSnippet(requestSortBy, order, requestFilter, page, size);
		return prepareResponse(page, size, stop, rsp);
	}

	@PostMapping("/search/{page}/{size}/registered")
	public DatasourceSnippetResponse searchRegistered(
			@RequestParam final RequestSort requestSortBy,
			@RequestParam final RequestSortOrder order,
			@RequestBody final RequestFilter requestFilter,
			@PathVariable final int page,
			@PathVariable final int size) throws DsmException {
		final StopWatch stop = StopWatch.createStarted();

		final Page<Datasource> dsPage = dsmService.searchRegistered(requestSortBy, order, requestFilter, page, size);
		final DatasourceSnippetResponse rsp =
				ResponseUtils.snippetResponse(dsPage.map(DsmMappingUtils::asSnippetExtended).getContent(), dsPage.getTotalElements());
		return prepareResponse(page, size, stop, rsp);
	}

	@GetMapping("/search/firstCollected")
	public SimpleResponse<SimpleDatasourceInfo> firstCollectedAfter(@RequestParam final String fromDate,
			@RequestParam(required = false) final String typologyFilter) throws Throwable {
		final StopWatch stop = StopWatch.createStarted();
		final List<SimpleDatasourceInfo> list = dsmService.findFirstCollectedAfter(fromDate, typologyFilter);
		final SimpleResponse<SimpleDatasourceInfo> rsp = ResponseUtils.simpleResponse(list);

		return prepareResponse(1, list.size(), stop, rsp);
	}

	@GetMapping("/recentregistered/{size}")
	public SimpleResponse<?> recentRegistered(
			@PathVariable final int size,
			@RequestParam(required = false, defaultValue = "2") final int version) throws Throwable {
		final StopWatch stop = StopWatch.createStarted();

		final SimpleResponse<?> rsp = version == 2 ? dsmService.searchRecentRegisteredV2(size)
				: dsmService.searchRecentRegistered(size);

		return prepareResponse(1, size, stop, rsp);
	}

	@GetMapping("/count/registered")
	public Long countRegistered(@RequestParam final String fromDate,
			@RequestParam(required = false) final String typologyFilter) throws Throwable {
		return dsmService.countRegisteredAfter(fromDate, typologyFilter);
	}

	@GetMapping("/count/firstcollect")
	public Long countFirstCollectAfter(@RequestParam final String fromDate,
			@RequestParam(required = false) final String typologyFilter) throws Throwable {
		return dsmService.countFirstCollect(fromDate, typologyFilter);
	}

}
