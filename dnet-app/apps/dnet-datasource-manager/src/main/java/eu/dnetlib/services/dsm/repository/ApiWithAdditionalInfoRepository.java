package eu.dnetlib.services.dsm.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import eu.dnetlib.domain.dsm.readonly.ApiWithAdditionalInfo;
import eu.dnetlib.utils.ReadOnlyRepository;

@Repository
public interface ApiWithAdditionalInfoRepository extends ReadOnlyRepository<ApiWithAdditionalInfo, String>, JpaSpecificationExecutor<ApiWithAdditionalInfo> {

}
