package eu.dnetlib.services.dsm.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import eu.dnetlib.domain.dsm.Api;
import jakarta.transaction.Transactional;

@Repository
public interface ApiRepository extends JpaRepository<Api, String> {

	@Query("select a from #{#entityName} a where a.datasource = ?1")
	List<Api> findByDatasource(String dsId);

	@Modifying
	@Transactional
	@Query("update #{#entityName} a set a.baseurl = ?2 where a.id = ?1")
	void setBaseurl(String id, String baseurl);

	@Modifying
	@Transactional
	@Query("update #{#entityName} a set a.compatibility = ?2 where a.id = ?1")
	void updateCompatibility(String apiId, String compatibility);

	@Modifying
	@Transactional
	@Query("update #{#entityName} a set a.compatibilityOverride = ?2 where a.id = ?1")
	void updateCompatibilityOverride(String apiId, String compatibility);

	@Modifying
	@Transactional
	@Query(value = "update dsm_apiparams ap set value = ?2 where ap.param = 'set' and ap.api = ?1", nativeQuery = true)
	void updateOaiSet(String apiId, String oaiSet);

	@Modifying
	@Transactional
	@Query(value = "insert into dsm_apiparams(api, param, value, _dnet_resource_identifier_) values (?1, ?2, ?3, ?1||'@@'||?2)", nativeQuery = true)
	void addApiParam(String apiId, String param, String value);

	@Modifying
	@Transactional
	@Query("update #{#entityName} d set d.removable = ?2 where d.datasource = ?1")
	void setAllApiRemovableForDs(String id, boolean removable);

	@Modifying
	@Transactional
	@Query("update #{#entityName} set lastCollectionMdid = ?2, lastCollectionDate = ?3, lastCollectionTotal = ?4 where id = ?1")
	void updateLastCollectionInfo(String apiId, String mdId, Timestamp date, long total);

	@Modifying
	@Transactional
	@Query("update #{#entityName} set lastAggregationMdid = ?2, lastAggregationDate = ?3, lastAggregationTotal = ?4 where id = ?1")
	void updateLastAggregationInfo(String apiId, String mdId, Timestamp date, long total);

	@Modifying
	@Transactional
	@Query("update #{#entityName} set lastDownloadObjid = ?2, lastDownloadDate = ?3, lastDownloadTotal = ?4 where id = ?1")
	void updateLastDownloadInfo(String apiId, String mdId, Timestamp date, long total);

	@Modifying
	@Transactional
	@Query("update #{#entityName} set active = ?2 where id = ?1")
	void setActive(String id, boolean b);

}
