package eu.dnetlib.is.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.common.app.AbstractDnetApp;
import eu.dnetlib.common.controller.parts.ErrorMessage;
import eu.dnetlib.domain.service.ServiceStatus;
import eu.dnetlib.domain.service.ServiceType;
import eu.dnetlib.errors.DnetRuntimeException;
import eu.dnetlib.is.service.ServiceRegistry;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/proxy")
public class ServiceProxyController {

	@Autowired
	private ServiceRegistry registry;

	private static final Log log = LogFactory.getLog(ServiceProxyController.class);

	@RequestMapping(value = "/byId/{service}/api/{*path}", consumes = MediaType.ALL_VALUE)
	public void serviceCallById(@PathVariable final String service,
			@PathVariable final String path,
			final HttpServletRequest request,
			final HttpServletResponse response) throws URISyntaxException {

		final String baseUrl = registry.findBaseUrl(service);

		if (StringUtils.isBlank(baseUrl)) {
			error(HttpStatus.NOT_FOUND, "The required service is not registered: " + service, response);
		} else {
			serviceCall(baseUrl, path, request, response);
		}
	}

	@RequestMapping(value = "/byType/{serviceType}/api/{*path}", consumes = MediaType.ALL_VALUE)
	public void serviceCallByType(@PathVariable final ServiceType serviceType,
			@PathVariable final String path,
			final HttpServletRequest request,
			final HttpServletResponse response)
			throws URISyntaxException {

		for (final ServiceStatus s : registry.availableServices()) {
			if (s.getType() == serviceType) {
				serviceCall(s.getBaseUrl(), path, request, response);
				return;
			}
		}

		error(HttpStatus.NOT_FOUND, "No registered services of type " + serviceType, response);
	}

	private void serviceCall(final String baseUrl,
			final String path,
			final HttpServletRequest request,
			final HttpServletResponse response) {

		try (InputStream in = request.getInputStream()) {
			final String body = IOUtils.toString(in, StandardCharsets.UTF_8);
			final HttpMethod method = HttpMethod.valueOf(request.getMethod());
			final URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl)
					.path("/api" + path)
					.query(request.getQueryString())
					.build(true)
					.toUri();

			final HttpHeaders headers = new HttpHeaders();
			final Enumeration<String> headerNames = request.getHeaderNames();
			while (headerNames.hasMoreElements()) {
				final String headerName = headerNames.nextElement();
				headers.set(headerName, request.getHeader(headerName));
			}

			log.debug("Redirecting to uri: " + uri);
			log.debug(" - Method: " + method);
			log.debug(" - Body (REQUEST): " + body);

			final HttpEntity<?> httpEntity = new HttpEntity<>(body, headers);

			final ResponseEntity<String> entity = new RestTemplate().exchange(uri, method, httpEntity, String.class);

			log.debug(" - Body (RESPONSE): " + entity.getBody());

			try (OutputStream out = response.getOutputStream()) {
				if ((entity.getHeaders() != null) && (entity.getHeaders().getContentType() != null)) {
					final String type = entity.getHeaders().getContentType().getType();
					log.debug(" - Content type (RESPONSE): " + type);
					response.setContentType(type);
				}
				IOUtils.write(entity.getBody(), out, StandardCharsets.UTF_8);
			} catch (final IOException e) {
				throw new DnetRuntimeException(e.getMessage(), e);
			}
		} catch (final HttpStatusCodeException e) {
			error(e.getStatusCode().value(), e.getResponseBodyAs(ErrorMessage.class), response);
		} catch (final IOException e) {
			error(HttpStatus.INTERNAL_SERVER_ERROR, e, response);
		}
	}

	private void error(final HttpStatus code, final String message, final HttpServletResponse response) {
		error(code.value(), new ErrorMessage(message, null), response);
	}

	private void error(final HttpStatus code, final Exception e, final HttpServletResponse response) {
		error(code.value(), new ErrorMessage(e), response);
	}

	private void error(final int code, final ErrorMessage error, final HttpServletResponse response) {
		try (OutputStream out = response.getOutputStream()) {
			response.setStatus(code);
			response.setContentType(MediaType.APPLICATION_JSON_VALUE);

			log.debug("ERROR CODE: " + code);
			log.debug("ERROR MESSAGE: " + error.getMessage());
			log.debug("ERROR STACKTRACE: " + error.getStacktrace());

			IOUtils.write(new ObjectMapper().writeValueAsString(error), out, StandardCharsets.UTF_8);
		} catch (final IOException e) {
			log.debug("ERROR", e);
			throw new DnetRuntimeException(e.getMessage(), e);
		}
	}

	@RequestMapping("/byId/{service}/swagger-conf")
	public ResponseEntity<String> swaggerServiceHome(@PathVariable final String service) {

		try {
			final String baseUrl = registry.findBaseUrl(service);

			if (StringUtils.isBlank(baseUrl)) { return new ResponseEntity<>("The required service is not registered: " + service, HttpStatus.NOT_FOUND); }

			final HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			final String conf = new RestTemplate().getForObject(baseUrl + "/v3/api-docs/" + AbstractDnetApp.SWAGGER_API_GROUP, String.class);

			return new ResponseEntity<>(conf, headers, HttpStatus.OK);
		} catch (final HttpStatusCodeException e) {
			return new ResponseEntity<>(e.getResponseBodyAsString(), e.getResponseHeaders(), e.getStatusCode());
		}
	}

}
