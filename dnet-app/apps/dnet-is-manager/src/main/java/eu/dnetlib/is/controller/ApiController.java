package eu.dnetlib.is.controller;

import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import eu.dnetlib.common.controller.DnetRestController;
import eu.dnetlib.domain.service.ServiceStatus;
import eu.dnetlib.domain.service.ServiceType;
import eu.dnetlib.errors.DnetRuntimeException;
import eu.dnetlib.is.service.ServiceRegistry;

@RestController
@RequestMapping(value = { "/ajax-is", "/api" })
public class ApiController extends DnetRestController {

	private static final int SHORT_TIMEOUT = 5000; // 5 seconds

	private static final Log log = LogFactory.getLog(ApiController.class);

	@Autowired
	private ServiceRegistry registry;

	@RequestMapping(value = "/register/{type}", method = { RequestMethod.GET, RequestMethod.POST })
	public ServiceStatus registerService(@PathVariable final ServiceType type, @RequestParam final String baseUrl) throws Exception {
		return registry.registerService(type, baseUrl);
	}

	@GetMapping("/ping/{service}")
	public int pingService(@PathVariable final String service) {
		try {
			final String baseUrl = registry.findBaseUrl(service);
			if (StringUtils.isBlank(baseUrl)) {
				log.warn("Service not registered: " + service);
				return -1;
			}

			final SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
			factory.setConnectTimeout(SHORT_TIMEOUT);
			factory.setReadTimeout(SHORT_TIMEOUT);

			return new RestTemplate(factory).getForObject(baseUrl + "/ping", Integer.class);
		} catch (final Throwable e) {
			log.warn("The service does not respond: " + service, e);
			return -1;
		}
	}

	@GetMapping("/info/{service}")
	public List<?> infoService(@PathVariable final String service) {
		final String baseUrl = registry.findBaseUrl(service);
		if (StringUtils.isBlank(baseUrl)) { throw new DnetRuntimeException("Service not registered: " + service); }
		return new RestTemplate().getForObject(baseUrl + "/info", List.class);
	}

	@GetMapping("/services")
	public List<ServiceStatus> services() {
		return registry.availableServices();
	}

	@GetMapping("/service-types")
	public Set<String> serviceTypes() {
		return registry.availableServiceTypes();
	}

}
