package eu.dnetlib.is;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import eu.dnetlib.common.app.AbstractDnetApp;
import eu.dnetlib.domain.service.ServiceType;

@SpringBootApplication
public class MainApplication extends AbstractDnetApp {

	public static void main(final String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}

	@Override
	protected ServiceType serviceType() {
		return ServiceType.is_manager;
	}

}
