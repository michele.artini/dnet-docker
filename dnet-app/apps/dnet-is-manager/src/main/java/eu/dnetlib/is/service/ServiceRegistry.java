package eu.dnetlib.is.service;

import java.net.MalformedURLException;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import eu.dnetlib.domain.service.ServiceStatus;
import eu.dnetlib.domain.service.ServiceType;

@Service
public class ServiceRegistry {

	private final Map<String, ServiceStatus> map = new HashMap<>();

	public String findBaseUrl(final String service) {
		final ServiceStatus s = map.get(service);
		return s != null ? s.getBaseUrl() : null;
	}

	public synchronized ServiceStatus registerService(final ServiceType type, final String baseUrl) throws MalformedURLException {

		final ServiceStatus status = map.values()
				.stream()
				.filter(s -> s.getType() == type)
				.filter(s -> s.getBaseUrl().equals(baseUrl))
				.findFirst()
				.orElse(new ServiceStatus(type, baseUrl));

		status.setDate(LocalDateTime.now());

		map.put(status.getName(), status);

		return status;
	}

	public List<ServiceStatus> availableServices() {
		return map.values()
				.stream()
				.sorted(Comparator.comparing(ServiceStatus::getName))
				.toList();
	}

	public Set<String> availableServiceTypes() {
		return map.values()
				.stream()
				.map(ServiceStatus::getType)
				.map(ServiceType::toString)
				.collect(Collectors.toSet());
	}

}
