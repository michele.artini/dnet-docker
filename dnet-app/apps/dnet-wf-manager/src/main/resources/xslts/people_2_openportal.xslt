<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:transformExt="http://namespace.openaire.eu/java/org.apache.commons.codec.digest.DigestUtils" xmlns:datetime="http://exslt.org/dates-and-times" xmlns:exslt="http://exslt.org/common" xmlns:oai="http://www.openarchives.org/OAI/2.0/"
    xmlns:dnet="eu.dnetlib.data.utils.XsltFunctions"
    xmlns:dri="http://www.driver-repository.eu/namespace/dri" xmlns:TransformationFunction="eu.dnetlib.data.collective.transformation.core.xsl.ext.TransformationFunctionProxy" xmlns:dc="http://purl.org/dc/elements/1.1/" exclude-result-prefixes="xsl transformExt TransformationFunction dnet datetime exslt dc ">
    <xsl:param name="varOfficialName"/>
    
    <xsl:variable name="tf" select="TransformationFunction:getInstance()" />
    
    <xsl:template match="/">
        
	<xsl:variable name="tmpRights">
            <xsl:choose>
                <xsl:when test=".//dc:rights[.='info:eu-repo/semantics/closedAccess']">Closed Access</xsl:when>
                <xsl:when test=".//dc:rights[.='info:eu-repo/semantics/restrictedAccess']">Restricted</xsl:when>
                <xsl:when test=".//dc:rights[.='info:eu-repo/semantics/embargoedAccess']">Embargo</xsl:when>
                <xsl:when test=".//dc:rights[.='info:eu-repo/semantics/openAccess']">Open Access</xsl:when>
                <xsl:otherwise>Unknown</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="rights" select="normalize-space($tmpRights)"/>

        <oai:record>
            <xsl:copy-of select="//*[local-name() = 'header']"/>
            <oai:metadata>
                <record>
                    
                    <identifier><xsl:value-of select="normalize-space(//dri:recordIdentifier)"/></identifier>
                    
                    <title><xsl:value-of select="//dc:title[1]" /></title>

                    <creators>
	                    <xsl:for-each select="//dc:creator">
	                        <xsl:for-each select="tokenize(dnet:cleanNames(.), '#')">
	                            <xsl:if test="string-length(.) &amp;gt; 0">
	                                <creator><xsl:value-of select="."/></creator>
	                            </xsl:if>
	                        </xsl:for-each>
	                    </xsl:for-each>
                    </creators>
                    
                    <publisher><xsl:value-of select="//dc:publisher[1]"/></publisher>

					<subjects>
	                    <xsl:for-each select="//dc:subject">
	                        <subject><xsl:value-of select="."/></subject>
	                    </xsl:for-each>
	                </subjects>
                    
                    <contributors>
	                    <xsl:for-each select="//dc:contributor">
	                        <xsl:for-each select="tokenize(., ',')">
	                            <contributor><xsl:value-of select="normalize-space(.)"/></contributor>
	                        </xsl:for-each>
	                    </xsl:for-each>
	               </contributors>
                    
                   <date><xsl:value-of select="//dc:date[1]"/></date>
                                       
                   <language><xsl:value-of select="TransformationFunction:convertString($tf, //dc:language[1], 'Languages')"/></language>
                                       
                   <xsl:variable name="infoType" select="normalize-space(//dc:type[contains(.,'info:eu-repo/semantics')])"/>
                   <xsl:variable name="peopleType" select="normalize-space(//dc:type[not(contains(.,'info:eu-repo/semantics'))])"/>
                   <xsl:choose>
                       <xsl:when test="contains($infoType,'masterThesis')">
                           <type>Master thesis</type>
                       </xsl:when>
                       <xsl:when test="contains($infoType,'doctoralThesis')">
                           <type>Doctoral thesis</type>
                       </xsl:when>
                       <xsl:when test="contains($infoType,'bachelorThesis')">
                           <type>Bachelor thesis</type>
                       </xsl:when>
                       <xsl:otherwise>
                           <type><xsl:value-of select="TransformationFunction:convertString($tf, $peopleType, 'IopTypologies')" /></type>
                       </xsl:otherwise>
                   </xsl:choose>


<xsl:choose>
<xsl:when test="starts-with(normalize-space(//dri:repositoryId), '807f0d50-2dc9-447d-8914-b91c34cb5841')">
    <documentPage>
        <xsl:value-of select="concat('https://openportal.isti.cnr.it/doc?id=people______::', dnet:md5(normalize-space(//dri:recordIdentifier)))"/>
    </documentPage>
</xsl:when>
<xsl:when test="starts-with(normalize-space(//dri:repositoryId), 'a58f6fed-ac2d-4cfe-a175-d809858b0eeb')">
    <documentPage>
        <xsl:value-of select="concat('https://openportal.ispc.cnr.it/doc?id=people______::', dnet:md5(normalize-space(//dri:recordIdentifier)))"/>
    </documentPage>
</xsl:when>
<xsl:when test="starts-with(normalize-space(//dri:repositoryId), 'db0e729d-38d0-44e1-9750-80d3c048525d')">
    <documentPage>
        <xsl:value-of select="concat('https://openportal.ismar.cnr.it/doc?id=people______::', dnet:md5(normalize-space(//dri:recordIdentifier)))"/>
    </documentPage>
</xsl:when>
<xsl:when test="starts-with(normalize-space(//dri:repositoryId), '5bdb0333-29f0-40c9-ad33-1fc4de6b2efa')">
    <documentPage>
        <xsl:value-of select="concat('https://openportal.ino.cnr.it/doc?id=people______::', dnet:md5(normalize-space(//dri:recordIdentifier)))"/>
    </documentPage>
</xsl:when>
</xsl:choose>
                   <urls>
	                    <xsl:for-each select="//dc:identifier">
	                        <xsl:if test="starts-with(., 'http') and contains(., 'www.cnr.it')">
	                        <!-- <url rights="Unknown" hostedBy="CNR People"><xsl:value-of select="."/></url>-->
	                        </xsl:if>
	                        <xsl:if test="starts-with(., 'http') and contains(., 'publications.cnr.it')">
	                           <url rights="Unknown" hostedBy="CNR ExploRA"><xsl:value-of select="."/></url>
	                        </xsl:if>
	                        <xsl:if test="starts-with(., 'http') and not(contains(., 'doi.org/')) and not(contains(., '/www.cnr.it/')) and not(contains(., '/publications.cnr.it/'))">
	                            <url rights="{$rights}" hostedBy="{dnet:serverName(.)}"><xsl:value-of select="."/></url>
	                        </xsl:if>
	                        <xsl:if test="starts-with(., 'http') and contains(., 'doi.org/')">
                                  <xsl:variable name="doi" select="dnet:cleanDoi(substring-after(., 'doi.org/'))" />
                                  <url rights="{$rights}" hostedBy="DOI Resolver"><xsl:value-of select="concat('https://dx.doi.org/', $doi)"/></url>
	                        </xsl:if>
	                        <xsl:if test="starts-with(., 'info:doi:')">
	                            <xsl:variable name="doi" select="dnet:cleanDoi(substring-after(., 'info:doi:'))" />
	                            <url rights="{$rights}" hostedBy="DOI Resolver"><xsl:value-of select="concat('https://dx.doi.org/', $doi)"/></url>
	                        </xsl:if>
	                    </xsl:for-each>
                    </urls>

                    <dois>
	                    <xsl:for-each select="//dc:identifier">
	                        <xsl:if test="starts-with(., 'info:doi:')">
	                            <xsl:variable name="doi" select="dnet:cleanDoi(substring-after(., 'info:doi:'))" />
	                            <xsl:if test="$doi"><doi><xsl:value-of select="$doi"/></doi></xsl:if>
	                        </xsl:if>
	                        <xsl:if test="starts-with(., 'http') and contains(., 'doi.org/')">
	                            <xsl:variable name="doi" select="dnet:cleanDoi(substring-after(., 'doi.org/'))" />
	                            <xsl:if test="$doi"><doi><xsl:value-of select="$doi"/></doi></xsl:if>
	                        </xsl:if>
	                    </xsl:for-each>
                    </dois>
                                        
                    <xsl:choose>
                         <xsl:when test="//dc:identifier[starts-with(., 'http') and not(contains(., '/www.cnr.it/')) and not(contains(., '/publications.cnr.it/'))]">
                              <bestRights><xsl:value-of select="$rights"/></bestRights>
                         </xsl:when>
                         <xsl:when test="//dc:identifier[starts-with(., 'info:doi:')]">
                              <bestRights><xsl:value-of select="$rights"/></bestRights>
                         </xsl:when>
                         <xsl:otherwise>
                              <bestRights>Unknown</bestRights>
                         </xsl:otherwise>
                    </xsl:choose>

                    <abstract><xsl:value-of select="//dc:description[1]"/></abstract>

					<xsl:for-each select="//dc:source[(string-length(.) > 10) and not(starts-with(.,'info:cnr-pdr/source')) ]"> 
                    		<xsl:if test="position() = 1">
                    			<source><xsl:value-of select="." /></source>
                    		</xsl:if>
                    </xsl:for-each>
                    
                    <projects>
	                    <xsl:for-each select="//dc:relation[starts-with(., 'info:eu-repo/grantAgreement')]">
	                        <xsl:if test="dnet:isValidProject(.)">
	                            <project>
	                                <infoId><xsl:value-of select="." /></infoId>
	                                <openaireId><xsl:value-of select="position()" /></openaireId>
	                                <code><xsl:value-of select="position()" /></code>
	                                <name><xsl:value-of select="position()" /></name>
	                                <acronym><xsl:value-of select="position()" /></acronym>
	                                <funder><xsl:value-of select="position()" /></funder>
	                                <program><xsl:value-of select="position()" /></program>
	                                <jurisdiction><xsl:value-of select="position()" /></jurisdiction>
	                            </project>
	                        </xsl:if>
	                    </xsl:for-each>
                    </projects>
                    
                     <cnrPersons>
	                    <xsl:for-each select="//dc:relation[starts-with(., 'info:cnr-pdr/author')]">
	                        <cnrPerson>
	                            <infoId><xsl:value-of select="." /></infoId>
	                            <name><xsl:value-of select="dnet:calculatePersonName(.)" /></name>
	                        </cnrPerson>
	                    </xsl:for-each>
	                </cnrPersons>
                    
                    <citations />
                    
                    <collections>
                    		<xsl:for-each select="//oai:setSpec[normalize-space(.) != 'openaire']">
	                        <inCollection>
	                        		<code><xsl:value-of select="normalize-space(.)" /></code>
	                        		<name />
	                             <acronym />
	                        </inCollection>
	                    </xsl:for-each>
                    </collections>
                    
                </record>
                
            </oai:metadata>
        </oai:record>
    </xsl:template>
</xsl:stylesheet>