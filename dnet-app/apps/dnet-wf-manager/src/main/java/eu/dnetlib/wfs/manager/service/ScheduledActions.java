package eu.dnetlib.wfs.manager.service;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import eu.dnetlib.wfs.repository.WfRunningJobRepository;
import jakarta.annotation.PostConstruct;

@Service
public class ScheduledActions {

	private static final Log log = LogFactory.getLog(ScheduledActions.class);

	@Autowired
	private WfRunningJobRepository jobRepository;

	@PostConstruct
	public void firstCheck() {
		deleteOldWfJobs();
	}

	@Scheduled(initialDelay = 0, fixedRate = 12, timeUnit = TimeUnit.HOURS)
	public void cleanAndCheck() {
		deleteOldWfJobs();
	}

	private void deleteOldWfJobs() {
		log.info("Deleting old jobs");
		final LocalDateTime date = LocalDateTime.now().minusHours(24);
		this.jobRepository.deleteOldJobs(date);
	}

}
