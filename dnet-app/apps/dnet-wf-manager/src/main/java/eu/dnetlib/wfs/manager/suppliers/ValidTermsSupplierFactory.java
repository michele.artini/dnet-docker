package eu.dnetlib.wfs.manager.suppliers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.SimpleResourceClient;
import eu.dnetlib.domain.resource.SimpleResource;
import eu.dnetlib.domain.wfs.templates.WfParamValidTermsSupplier;

@Component
public class ValidTermsSupplierFactory {

	@Autowired
	private DnetServiceClientFactory clientFactory;

	public List<String> findTerms(final WfParamValidTermsSupplier ts) {
		return switch (ts) {
		case LIST_TRANSFORMATION_RULES -> listTransformationRules();
		default -> new ArrayList<>();
		};
	}

	private List<String> listTransformationRules() {
		return this.clientFactory.getClient(SimpleResourceClient.class)
				.findResources(SimpleResourceClient.ResourceType.transformation_rule_xslt, null)
				.stream()
				.map(SimpleResource::getName)
				.toList();
	}
}
