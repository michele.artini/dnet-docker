package eu.dnetlib.wfs.importer;

import java.io.File;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.domain.wfs.jobs.JobStatus;
import eu.dnetlib.domain.wfs.jobs.WfHistoryJob;
import eu.dnetlib.wfs.repository.WfHistoryJobRepository;

@Service
public class WfHistoryImporter {

	private static final Log log = LogFactory.getLog(WfHistoryImporter.class);

	@Autowired
	private WfHistoryJobRepository historyJobRepository;

	public void load(final String path) throws Exception {
		final ObjectMapper mapper = new ObjectMapper();

		final JsonNode rootNode = mapper.readTree(new File(path));

		rootNode.forEach(this::saveWf);
	}

	private void saveWf(final JsonNode node) {

		final WfHistoryJob wf = new WfHistoryJob();
		wf.setProcessId(node.get("system:processId").asText());
		wf.setName(node.get("system:wfName").asText());
		wf.setFamily(node.get("system:profileFamily").asText());

		if (node.has("dataprovider:id")) {
			wf.setDsId(node.get("dataprovider:id").asText());
		}
		if (node.has("dataprovider:name")) {
			wf.setDsName(node.get("dataprovider:name").asText());
		}
		if (node.has("dataprovider:interface")) {
			wf.setApiId(node.get("dataprovider:interface").asText());
		}

		wf.setStartDate(LocalDateTime
				.ofInstant(Instant.ofEpochMilli(NumberUtils.toLong(node.get("system:startDate").asText())), TimeZone.getDefault().toZoneId()));

		wf.setEndDate(LocalDateTime.ofInstant(Instant.ofEpochMilli(NumberUtils.toLong(node.get("system:endDate").asText())), TimeZone.getDefault().toZoneId()));

		if (BooleanUtils.toBoolean(node.get("system:isCompletedSuccessfully").asText())) {
			wf.setStatus(JobStatus.success);
		} else {
			wf.setStatus(JobStatus.failure);
		}

		final Map<String, Object> details = new LinkedHashMap<>();
		final Iterator<Entry<String, JsonNode>> fields = node.fields();
		while (fields.hasNext()) {
			final Entry<String, JsonNode> f = fields.next();
			if (f.getValue().isValueNode()) {
				details.put(f.getKey(), f.getValue().asText());
			}
		}

		wf.setOutputParams(details);

		this.historyJobRepository.save(wf);

		log.info("Wf saved with id: " + wf.getProcessId());

	}
}
