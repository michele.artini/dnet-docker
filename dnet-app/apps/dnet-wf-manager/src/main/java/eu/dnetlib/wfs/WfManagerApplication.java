package eu.dnetlib.wfs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;

import eu.dnetlib.common.app.AbstractDnetApp;
import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.mapping.cleaner.CleanerFactory;
import eu.dnetlib.common.mapping.xslt.XsltTransformerFactory;
import eu.dnetlib.domain.service.ServiceType;
import eu.dnetlib.domain.wfs.conf.WfConfiguration;
import eu.dnetlib.domain.wfs.conf.WfSection;
import eu.dnetlib.domain.wfs.jobs.WfJournalEntry;
import eu.dnetlib.domain.wfs.subscriptions.WfSubscription;

@SpringBootApplication
@EntityScan(basePackageClasses = {
		WfJournalEntry.class,
		WfConfiguration.class,
		WfSection.class,
		WfSubscription.class
})
public class WfManagerApplication extends AbstractDnetApp {

	public static void main(final String[] args) {
		SpringApplication.run(WfManagerApplication.class, args);
	}

	@Override
	protected ServiceType serviceType() {
		return ServiceType.wf_manager;
	}

	@Bean
	public XsltTransformerFactory xsltTransformFactory(final DnetServiceClientFactory clientFactory) {
		return new XsltTransformerFactory(clientFactory);
	}

	@Bean
	public CleanerFactory cleanerFactory(final DnetServiceClientFactory clientFactory) {
		return new CleanerFactory(clientFactory);
	}

}
