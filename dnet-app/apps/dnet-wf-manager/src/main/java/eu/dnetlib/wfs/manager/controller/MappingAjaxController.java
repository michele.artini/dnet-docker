package eu.dnetlib.wfs.manager.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.controller.DnetRestController;
import eu.dnetlib.common.mapping.RecordTransformer;
import eu.dnetlib.common.mapping.cleaner.CleanerFactory;
import eu.dnetlib.common.mapping.xslt.XsltTransformerFactory;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/mapping")
public class MappingAjaxController extends DnetRestController {

	@Autowired
	private CleanerFactory cleanerFactory;

	@Autowired
	private XsltTransformerFactory xsltFactory;

	@PostMapping(value = "/clean", consumes = {
			MediaType.TEXT_PLAIN_VALUE, MediaType.APPLICATION_XML_VALUE,
	}, produces = MediaType.APPLICATION_XML_VALUE)
	public void clean(@RequestParam final String rule, final HttpServletRequest req, final HttpServletResponse res) throws Exception {
		transform(req, res, this.cleanerFactory.newCleaner(rule));
	}

	@PostMapping(value = "/xsltTransform", consumes = { MediaType.TEXT_PLAIN_VALUE,
			MediaType.APPLICATION_XML_VALUE, }, produces = MediaType.APPLICATION_XML_VALUE)
	public void xsltTransform(@RequestParam final String rule, final HttpServletRequest req, final HttpServletResponse res) throws Exception {
		transform(req, res, this.xsltFactory.getTransformerById(rule, new HashMap<>()));
	}

	private void transform(final HttpServletRequest req, final HttpServletResponse res, final RecordTransformer<String, String> transformer)
			throws IOException {

		final String xmlIn = IOUtils.toString(req.getInputStream(), StandardCharsets.UTF_8);
		final String xmlOut = transformer.transform(xmlIn);

		res.setCharacterEncoding(StandardCharsets.UTF_8.name());

		res.setContentType(MediaType.APPLICATION_XML_VALUE);

		if (StringUtils.isNotBlank(xmlOut)) {
			IOUtils.write(xmlOut, res.getOutputStream(), StandardCharsets.UTF_8.name());
		} else {
			res.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Invalid record");
		}
	}

}
