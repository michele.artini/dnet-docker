package eu.dnetlib.wfs.manager.controller;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.SimpleResourceClient;
import eu.dnetlib.common.controller.DnetRestController;
import eu.dnetlib.domain.common.KeyValue;
import eu.dnetlib.domain.wfs.WorkflowsConstants;
import eu.dnetlib.domain.wfs.conf.WfConfiguration;
import eu.dnetlib.domain.wfs.conf.WfSection;
import eu.dnetlib.domain.wfs.graph.Node;
import eu.dnetlib.domain.wfs.jobs.WfJournalEntry;
import eu.dnetlib.domain.wfs.jobs.WfRunningJob;
import eu.dnetlib.domain.wfs.subscriptions.WfSubscription;
import eu.dnetlib.domain.wfs.templates.WfParam;
import eu.dnetlib.domain.wfs.templates.WfRepoHiDesc;
import eu.dnetlib.domain.wfs.templates.WfRepoHiParams;
import eu.dnetlib.domain.wfs.templates.WfTemplate;
import eu.dnetlib.domain.wfs.templates.WfTemplateDesc;
import eu.dnetlib.errors.WorkflowManagerException;
import eu.dnetlib.wfs.manager.service.WorkflowManagerService;
import eu.dnetlib.wfs.manager.suppliers.ValidTermsSupplierFactory;
import eu.dnetlib.wfs.utils.WfConfigurationUtils;

@RestController
@RequestMapping("/api")
public class ApiController extends DnetRestController {

	@Autowired
	private WorkflowManagerService wfManagerService;

	@Autowired
	private DnetServiceClientFactory clientFactory;

	@Autowired
	private ValidTermsSupplierFactory validTermsSupplierFactory;

	@GetMapping("/history")
	public List<WfJournalEntry> history(
			@RequestParam(required = true) final int total,
			@RequestParam(required = false) final LocalDate from,
			@RequestParam(required = false) final LocalDate to) {
		return this.wfManagerService.findHistory(total, from, to);
	}

	@GetMapping("/history/byDsId/{dsId}")
	public List<WfJournalEntry> historyByDsId(@PathVariable final String dsId) {
		return this.wfManagerService.recentHistoryForDsId(dsId);
	}

	@GetMapping("/history/byApiId/{apiId}")
	public List<WfJournalEntry> historyByApiId(@PathVariable final String apiId) {
		return this.wfManagerService.recentHistoryForApiId(apiId);
	}

	@GetMapping("/history/byConf/{wfConfId}")
	public List<WfJournalEntry> history(@PathVariable final String wfConfId) {
		return this.wfManagerService.recentHistoryForConfiguration(wfConfId);
	}

	@GetMapping("/proc/{processId}")
	public WfJournalEntry getProcessExecution(@PathVariable final String processId) {
		return this.wfManagerService.findProcessLog(processId);
	}

	@GetMapping("/sections")
	public Iterable<WfSection> listWfSections() throws Exception {
		return this.wfManagerService.listSections();
	}

	@GetMapping("/sections/{section}")
	public List<KeyValue<String>> listWfConfigurations(@PathVariable final String section) throws Exception {
		return this.wfManagerService.listWfConfigurationsBySection(section)
				.stream()
				.map(x -> new KeyValue<>(x.getId(), x.getName()))
				.collect(Collectors.toList());
	}

	@GetMapping("/conf/{id}")
	public WfConfiguration getWfConfiguration(@PathVariable final String id) throws Exception {
		return this.wfManagerService.findWorkflowConfiguration(id);
	}

	@PostMapping("/conf")
	public WfConfiguration saveWfConfiguration(@RequestBody final WfConfiguration conf) throws Exception {
		return this.wfManagerService.saveWfConfiguration(conf);
	}

	@DeleteMapping("/conf/{id}")
	public void deleteWfConfiguration(@PathVariable final String id) throws Exception {
		this.wfManagerService.deleteWfConfiguration(id);
	}

	@GetMapping("/conf/{id}/start")
	public List<WfJournalEntry> startWorkflowConfiguration(@PathVariable final String id,
			@RequestParam(name = "wf", required = false) final String[] overrideStartWfs)
			throws Exception {
		final WfConfiguration conf = this.wfManagerService.findWorkflowConfiguration(id);

		final String family = WfConfigurationUtils.calculateFamily(conf, false);

		final String[] wfTemplateIds = (overrideStartWfs != null) && (overrideStartWfs.length > 0)
				? overrideStartWfs
				: conf.getWorkflows().toArray(new String[conf.getWorkflows().size()]);

		this.wfManagerService.prepareNewJob(conf, family, wfTemplateIds);

		if (StringUtils.isNotBlank(conf.getApiId())) { return this.wfManagerService.recentHistoryForApiId(conf.getApiId()); }
		if (StringUtils.isNotBlank(conf.getDsId())) { return this.wfManagerService.recentHistoryForDsId(conf.getDsId()); }
		if (StringUtils.isNotBlank(conf.getId())) { return this.wfManagerService.recentHistoryForConfiguration(conf.getId()); }

		return this.wfManagerService.recentHistoryForConfiguration(id);
	}

	@GetMapping("/conf/{id}/destroy")
	public List<WfJournalEntry> destroyWorkflowConfiguration(@PathVariable final String id) throws Exception {
		final WfConfiguration conf = this.wfManagerService.findWorkflowConfiguration(id);

		final String family = WfConfigurationUtils.calculateFamily(conf, true);

		this.wfManagerService.prepareNewJob(conf, family, conf.getDestroyWf());

		if (StringUtils.isNotBlank(conf.getApiId())) { return this.wfManagerService.recentHistoryForApiId(conf.getApiId()); }
		if (StringUtils.isNotBlank(conf.getDsId())) { return this.wfManagerService.recentHistoryForDsId(conf.getDsId()); }
		if (StringUtils.isNotBlank(conf.getId())) { return this.wfManagerService.recentHistoryForConfiguration(conf.getId()); }

		return this.wfManagerService.recentHistory();
	}

	@GetMapping("/conf/{id}/subscriptions")
	public List<WfSubscription> listWorkflowSubscriptions(@PathVariable final String id) throws Exception {
		return this.wfManagerService.listSubscriptions(id);
	}

	@PostMapping("/conf/{id}/subscriptions")
	public void saveWorkflowSubscriptions(@PathVariable final String id, @RequestBody final List<WfSubscription> subscriptions) throws Exception {
		this.wfManagerService.saveSubscriptions(id, subscriptions);
	}

	@GetMapping("/repo-his")
	public List<WfRepoHiDesc> listRepoHIs() {
		return this.wfManagerService.listRepoHis();
	}

	@PostMapping("/repo-hi/{id}/start")
	public WfRunningJob startRepoHi(@PathVariable final String id, @RequestBody final WfRepoHiParams params) throws WorkflowManagerException {
		final WfTemplate wfTmpl = this.clientFactory
				.getClient(SimpleResourceClient.class)
				.findResourceContent(SimpleResourceClient.ResourceType.wf_template, id, WfTemplate.class);

		final List<Node> graph = wfTmpl.getGraph();

		// Prepare the parameters using their default values
		final Map<String, Object> wfParams = WfConfigurationUtils.allConfiguredParameters(Arrays.asList(new ImmutablePair<>(id, wfTmpl)), null);

		final String wfName = String.format("Aggregation of '%s' (api: %s)", params.getDsName(), params.getApiId());

		return this.wfManagerService
				.prepareNewJob(null, wfName, WorkflowsConstants.REPO_HI_FAMILY, params.getDsId(), params.getDsName(), params
						.getApiId(), graph, wfParams);
	}

	@GetMapping("/template/{id}")
	public WfTemplateDesc findWfTemplate(@PathVariable final String id) {
		return this.wfManagerService.findWfTemplateDesc(id);
	}

	@GetMapping("/parameters")
	public Collection<WfParam> findWfTemplateParameters(@RequestParam(name = "wf", required = true) final String[] workflows) throws Exception {

		final Map<String, WfParam> params = new LinkedHashMap<>();

		final SimpleResourceClient client = this.clientFactory.getClient(SimpleResourceClient.class);

		for (final String wf : workflows) {
			for (final WfParam p : client.findResourceContent(SimpleResourceClient.ResourceType.wf_template, wf, WfTemplate.class).getParameters()) {

				if (p.getValidTermsSupplier() != null) {
					p.setValidTerms(this.validTermsSupplierFactory.findTerms(p.getValidTermsSupplier()));
				}

				if (params.containsKey(p.getName())) {
					final WfParam main = params.get(p.getName());
					main.setDefaultValue(StringUtils.firstNonBlank(main.getDefaultValue(), p.getDefaultValue()));
					main.setRequired(main.isRequired() || p.isRequired());
					main.setDescription(StringUtils.firstNonBlank(main.getDescription(), p.getDescription()));

					if (!p.getValidTerms().isEmpty()) {
						if (main.getValidTerms().isEmpty()) {
							main.setValidTerms(p.getValidTerms());
						} else {
							main.setValidTerms(main.getValidTerms().stream().filter(t -> p.getValidTerms().contains(t)).toList());
						}
					}
				} else {
					params.put(p.getName(), p);
				}
			}
		}

		return params.values();

	}

	@GetMapping("/confs/byApi/{id}")
	public List<WfConfiguration> listWfConfigurationsByApi(@PathVariable final String id) {
		return this.wfManagerService.listWfConfigurationsByApiId(id);
	}

}
