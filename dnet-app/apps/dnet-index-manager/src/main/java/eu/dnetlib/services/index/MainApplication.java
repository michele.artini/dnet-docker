package eu.dnetlib.services.index;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;

import eu.dnetlib.common.app.AbstractDnetApp;
import eu.dnetlib.common.index.solr.SolrService;
import eu.dnetlib.domain.index.IndexConfiguration;
import eu.dnetlib.domain.index.IndexField;
import eu.dnetlib.domain.service.ServiceType;

@SpringBootApplication
@EntityScan(basePackageClasses = { IndexConfiguration.class, IndexField.class })
public class MainApplication extends AbstractDnetApp {

	@Value("${solr.urls}")
	private String[] solrUrls;

	@Value("${zookeeper.server}")
	private String zooServer;

	public static void main(final String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}

	@Bean
	public SolrService solrService() {
		return new SolrService(zooServer, solrUrls);
	}

	@Override
	protected ServiceType serviceType() {
		return ServiceType.index_manager;
	}
}
