package eu.dnetlib.services.index.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.index.IndexConfiguration;

public interface IndexConfigurationRepository extends JpaRepository<IndexConfiguration, String> {

}
