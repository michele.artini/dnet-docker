package eu.dnetlib.services.index.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.response.schema.SchemaRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.controller.DnetRestController;
import eu.dnetlib.common.index.solr.SolrService;
import eu.dnetlib.domain.index.IndexConfiguration;
import eu.dnetlib.errors.DnetException;
import eu.dnetlib.errors.InformationServiceException;
import eu.dnetlib.services.index.service.IndexConfigurationService;

@RestController
@RequestMapping("/api/index")
public class ApiController extends DnetRestController {

	@Autowired
	private IndexConfigurationService indexConfigurationService;

	@Autowired
	private SolrService solrService;

	@GetMapping("/schemas")
	public List<String> listAvailableSchemas() throws DnetException {
		return solrService.listAvailableSchemas();
	}

	@GetMapping("/configurations")
	public List<String> listConfigurations() {
		return indexConfigurationService.listConfigurations();
	}

	@PostMapping("/configurations")
	public IndexConfiguration saveConfiguration(@RequestBody final IndexConfiguration conf) throws DnetException {
		if (StringUtils.isBlank(conf.getId())) { throw new DnetException("Index Configuration ID is empty"); }

		return indexConfigurationService.saveConfiguration(conf);
	}

	@GetMapping("/configurations{id}")
	public IndexConfiguration saveConfiguration(@PathVariable final String id) throws InformationServiceException {
		return indexConfigurationService.getConfiguration(id);
	}

	@GetMapping("/configurations/{id}/solr-schema")
	public SchemaRepresentation findSolrConfiguration(@PathVariable final String name) throws DnetException {
		return solrService.findSolrSchema(name);
	}

	@PostMapping("/configurations{id}")
	public IndexConfiguration saveConfiguration(@PathVariable final String id, @RequestBody final IndexConfiguration conf) {
		conf.setId(id);
		return indexConfigurationService.saveConfiguration(conf);
	}

	@DeleteMapping("/configurations/{id}")
	public void deleteConfiguration(@PathVariable final String id) throws DnetException {
		solrService.deleteIndex(id);
		indexConfigurationService.deleteConfiguration(id);
	}

}
