package eu.dnetlib.services.index.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.dnetlib.domain.index.IndexConfiguration;
import eu.dnetlib.errors.InformationServiceException;
import eu.dnetlib.services.index.repository.IndexConfigurationRepository;
import eu.dnetlib.services.index.repository.IndexFieldRepository;
import jakarta.transaction.Transactional;

@Service
public class IndexConfigurationService {

	@Autowired
	private IndexConfigurationRepository indexConfigurationRepository;

	@Autowired
	private IndexFieldRepository indexFieldRepository;

	@Transactional
	public List<String> listConfigurations() {
		return indexConfigurationRepository.findAll().stream().map(IndexConfiguration::getId).collect(Collectors.toList());
	}

	@Transactional
	public IndexConfiguration getConfiguration(final String confId) throws InformationServiceException {
		final IndexConfiguration conf =
				indexConfigurationRepository.findById(confId).orElseThrow(() -> new InformationServiceException("Index configuration not found"));
		conf.setFields(indexFieldRepository.findByIndexId(confId));
		return conf;
	}

	@Transactional
	public IndexConfiguration saveConfiguration(final IndexConfiguration conf) {
		indexConfigurationRepository.save(conf);
		indexFieldRepository.deleteByIndexId(conf.getId());
		conf.getFields().forEach(f -> {
			f.setIndexId(conf.getId());
			indexFieldRepository.save(f);
		});

		return conf;
	}

	@Transactional
	public void deleteConfiguration(final String confId) {
		indexFieldRepository.deleteByIndexId(confId);
		indexConfigurationRepository.deleteById(confId);
	}

}
