package eu.dnetlib.services.index.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.index.IndexField;
import eu.dnetlib.domain.index.IndexFieldPK;

public interface IndexFieldRepository extends JpaRepository<IndexField, IndexFieldPK> {

	void deleteByIndexId(String confId);

	List<IndexField> findByIndexId(String confId);

}
