package eu.dnetlib.services.vocabularies.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.vocabulary.Vocabulary;

public interface VocabularyRepository extends JpaRepository<Vocabulary, String> {

}
