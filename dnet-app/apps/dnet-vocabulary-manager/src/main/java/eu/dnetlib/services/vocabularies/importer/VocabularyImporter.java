package eu.dnetlib.services.vocabularies.importer;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.dnetlib.domain.vocabulary.Synonym;
import eu.dnetlib.domain.vocabulary.Vocabulary;
import eu.dnetlib.domain.vocabulary.VocabularyTerm;
import eu.dnetlib.services.vocabularies.repository.VocabularyRepository;
import eu.dnetlib.services.vocabularies.repository.VocabularyTermRepository;
import jakarta.transaction.Transactional;

@Service
public class VocabularyImporter {

	@Autowired
	private VocabularyRepository vocabularyRepository;

	@Autowired
	private VocabularyTermRepository vocabularyTermRepository;

	@Transactional
	public Vocabulary importVocabulary(final String xml) throws Exception {
		final Document doc = DocumentHelper.parseText(xml);

		final Vocabulary voc = new Vocabulary();
		final String vocId = doc.valueOf("//VOCABULARY_NAME/@code");
		final String vocName = doc.valueOf("//VOCABULARY_NAME");
		final String vocDesc = doc.valueOf("//VOCABULARY_DESCRIPTION");

		voc.setId(vocId);
		voc.setName(vocName);
		voc.setDescription(vocDesc);

		vocabularyRepository.save(voc);

		for (final Node n : doc.selectNodes("//TERM")) {
			final VocabularyTerm term = new VocabularyTerm();
			term.setVocabulary(vocId);
			term.setCode(n.valueOf("@code"));
			term.setName(n.valueOf("@english_name"));
			term.setEncoding(n.valueOf("@encoding"));
			term.setSynonyms(n.selectNodes(".//SYNONYM")
				.stream()
				.map(ns -> new Synonym(ns.valueOf("@term"), ns.valueOf("@encoding")))
				.sorted()
				.distinct()
				.toArray(Synonym[]::new));

			vocabularyTermRepository.save(term);
		}

		return voc;
	}
}
