package eu.dnetlib.services.vocabularies.controller;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.controller.DnetRestController;
import eu.dnetlib.domain.vocabulary.Vocabulary;
import eu.dnetlib.services.vocabularies.importer.VocabularyImporter;
import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/import")
public class ImporterController extends DnetRestController {

	// EXAMPLE:
	// find ./VocabularyDSResourceType/ -name "*.xml" -exec curl -X POST "http://localhost:8280/api/import/vocabulary" -H "accept: */*" -H
	// "Content-Type: text/plain" --data-binary @{} \;
	// find ./DedupConfigurationDSResources/ -name "*.xml" -exec curl -X POST "http://localhost:8280/api/import/resource" -H "accept: */*"
	// -H "Content-Type: text/plain" --data-binary @{} \;

	@Autowired
	private VocabularyImporter vocabularyImporter;

	@PostMapping(value = "/vocabulary", consumes = MediaType.ALL_VALUE)
	public Vocabulary importVocabulary(final HttpServletRequest request) throws Exception, IOException {
		try (final InputStream in = request.getInputStream()) {
			return vocabularyImporter.importVocabulary(IOUtils.toString(in, StandardCharsets.UTF_8));
		}
	}

}
