package eu.dnetlib.services.vocabularies.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import eu.dnetlib.domain.vocabulary.VocabularyTerm;
import eu.dnetlib.domain.vocabulary.VocabularyTermPK;

public interface VocabularyTermRepository extends JpaRepository<VocabularyTerm, VocabularyTermPK> {

	Iterable<VocabularyTerm> findByVocabularyOrderByCode(String vocabulary);

	@Query(value = "SELECT vt.code FROM VocabularyTerm vt WHERE vt.vocabulary like ?1 ORDER BY vt.code")
	Iterable<String> findTermsByVocabulary(String vocabulary);
}
