package eu.dnetlib.services.vocabularies.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.controller.DnetRestController;
import eu.dnetlib.domain.vocabulary.Vocabulary;
import eu.dnetlib.domain.vocabulary.VocabularyTerm;
import eu.dnetlib.errors.InformationServiceException;
import eu.dnetlib.services.vocabularies.service.VocabularyService;

@RestController
@RequestMapping("/api/vocs")
public class ApiController extends DnetRestController {

	@Autowired
	private VocabularyService vocabularyService;

	@GetMapping("/")
	public List<Vocabulary> listVocs() {
		return vocabularyService.listVocs();
	}

	@GetMapping("/{vocabulary}/terms")
	public Iterable<VocabularyTerm> listTerms(@PathVariable final String vocabulary) {
		return vocabularyService.listTerms(vocabulary);
	}

	@GetMapping("/{vocabulary}")
	public Vocabulary getVoc(@PathVariable final String vocabulary) throws InformationServiceException {
		return vocabularyService.getVoc(vocabulary);
	}

	@DeleteMapping("/{vocabulary}")
	public List<Vocabulary> deleteVocs(@PathVariable final String vocabulary) {
		vocabularyService.deleteVocs(vocabulary);
		return vocabularyService.listVocs();
	}

	@PostMapping("/")
	public List<Vocabulary> saveVoc(@RequestBody final Vocabulary voc) {
		vocabularyService.saveVoc(voc);
		return vocabularyService.listVocs();
	}

	@PostMapping("/{vocabulary}/terms")
	public Iterable<VocabularyTerm> saveTerm(@PathVariable final String vocabulary, @RequestBody final VocabularyTerm term) {
		vocabularyService.saveTerms(vocabulary, term);
		return vocabularyService.listTerms(vocabulary);
	}

	@DeleteMapping("/{vocabulary}/terms/{term}")
	public Iterable<VocabularyTerm> deleteTerms(@PathVariable final String vocabulary, @PathVariable final String term) {
		vocabularyService.deleteTerms(vocabulary, term);
		return vocabularyService.listTerms(vocabulary);
	}

}
