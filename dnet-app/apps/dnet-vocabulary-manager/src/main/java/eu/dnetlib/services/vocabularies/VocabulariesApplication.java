package eu.dnetlib.services.vocabularies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import eu.dnetlib.common.app.AbstractDnetApp;
import eu.dnetlib.domain.service.ServiceType;
import eu.dnetlib.domain.vocabulary.Vocabulary;
import eu.dnetlib.domain.vocabulary.VocabularyTerm;

@SpringBootApplication
@EntityScan(basePackageClasses = { Vocabulary.class, VocabularyTerm.class })
public class VocabulariesApplication extends AbstractDnetApp {

	public static void main(final String[] args) {
		SpringApplication.run(VocabulariesApplication.class, args);
	}

	@Override
	protected ServiceType serviceType() {
		return ServiceType.vocabulary_manager;
	}
}
