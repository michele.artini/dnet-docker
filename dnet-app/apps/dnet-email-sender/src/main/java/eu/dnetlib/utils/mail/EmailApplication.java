package eu.dnetlib.utils.mail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import eu.dnetlib.common.app.AbstractDnetApp;
import eu.dnetlib.domain.service.ServiceType;

@SpringBootApplication
public class EmailApplication extends AbstractDnetApp {

	public static void main(final String[] args) {
		SpringApplication.run(EmailApplication.class, args);
	}

	@Override
	protected ServiceType serviceType() {
		return ServiceType.mail_sender;
	}
}
