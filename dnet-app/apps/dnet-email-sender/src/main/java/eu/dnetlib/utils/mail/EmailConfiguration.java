package eu.dnetlib.utils.mail;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "mail")
public class EmailConfiguration {

	private String smtpHost;
	private int smtpPort;
	private String smtpUser;
	private String smtpPassword;
	private String defaultFromEmail;
	private String defaultFromName;
	private String defaultCc;
	private String defaultBcc;

	public String getDefaultBcc() {
		return defaultBcc;
	}

	public void setDefaultBcc(final String defaultBcc) {
		this.defaultBcc = defaultBcc;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(final String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public int getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(final int smtpPort) {
		this.smtpPort = smtpPort;
	}

	public String getSmtpUser() {
		return smtpUser;
	}

	public void setSmtpUser(final String smtpUser) {
		this.smtpUser = smtpUser;
	}

	public String getSmtpPassword() {
		return smtpPassword;
	}

	public void setSmtpPassword(final String smtpPassword) {
		this.smtpPassword = smtpPassword;
	}

	public String getDefaultFromEmail() {
		return defaultFromEmail;
	}

	public void setDefaultFromEmail(final String defaultFromEmail) {
		this.defaultFromEmail = defaultFromEmail;
	}

	public String getDefaultFromName() {
		return defaultFromName;
	}

	public void setDefaultFromName(final String defaultFromName) {
		this.defaultFromName = defaultFromName;
	}

	public String getDefaultCc() {
		return defaultCc;
	}

	public void setDefaultCc(final String defaultCc) {
		this.defaultCc = defaultCc;
	}

}
