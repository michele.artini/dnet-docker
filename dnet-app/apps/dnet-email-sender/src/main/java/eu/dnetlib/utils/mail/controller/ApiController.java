package eu.dnetlib.utils.mail.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.controller.DnetRestController;
import eu.dnetlib.domain.email.EmailMessage;
import eu.dnetlib.errors.DnetException;
import eu.dnetlib.utils.mail.service.EmailService;

@RestController
@RequestMapping("/api/emails")
public class ApiController extends DnetRestController {

	@Autowired
	private EmailService emailService;

	@PostMapping("/send")
	public String sendMail(@RequestBody final EmailMessage email) throws DnetException {
		emailService.sendMail(email);

		return "Sending mail to " + email.getTo();
	}

}
