package eu.dnetlib.utils.mail.service;

import java.util.Date;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.annotation.PostConstruct;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.dnetlib.domain.email.EmailMessage;
import eu.dnetlib.errors.DnetException;
import eu.dnetlib.errors.DnetRuntimeException;
import eu.dnetlib.utils.mail.EmailConfiguration;

@Service
public class EmailService {

	private static final Log log = LogFactory.getLog(EmailService.class);

	private final BlockingQueue<Message> queue = new LinkedBlockingQueue<>();

	@Autowired
	private EmailConfiguration conf;

	@PostConstruct
	private void init() {
		new Thread(() -> {
			while (true) {
				try {
					final Message message = queue.take();
					if (message != null) {
						try {
							log.info("Sending mail...");
							Transport.send(message);
							log.info("...sent");
						} catch (final MessagingException e) {
							log.error("Error sending email", e);
							queue.add(message);
						}
					}
				} catch (final InterruptedException e1) {
					throw new RuntimeException(e1);
				}
			}
		}).start();
	}

	public void sendMail(final EmailMessage email) throws DnetException {
		try {
			final Session session = Session.getInstance(obtainProperties(), obtainAuthenticator());

			final MimeMessage mimeMessage = new MimeMessage(session);

			if (StringUtils.isNotBlank(email.getFromMail())) {
				mimeMessage.setFrom(new InternetAddress(email.getFromMail(),
						StringUtils.isBlank(email.getFromName()) ? email.getFromMail() : email.getFromName()));
			} else {
				mimeMessage.setFrom(new InternetAddress(conf.getDefaultFromEmail(), conf.getDefaultFromName()));
			}
			mimeMessage.setSubject(email.getSubject());
			mimeMessage.setContent(email.getContent(), "text/html; charset=utf-8");
			mimeMessage.setSentDate(new Date());

			addRecipients(mimeMessage, Message.RecipientType.TO, email.getTo());

			if (email.getCcs() != null) {
				addRecipients(mimeMessage, Message.RecipientType.CC, ArrayUtils.toStringArray(email.getCcs().toArray()));
			}
			if (StringUtils.isNotBlank(conf.getDefaultCc())) {
				addRecipients(mimeMessage, Message.RecipientType.CC, StringUtils.split(conf.getDefaultCc(), ","));
			}
			if (StringUtils.isNotBlank(conf.getDefaultBcc())) {
				addRecipients(mimeMessage, Message.RecipientType.BCC, StringUtils.split(conf.getDefaultBcc(), ","));
			}

			queue.add(mimeMessage);

			log.info("Mail to " + email.getTo() + " in queue");

		} catch (

		final Exception e) {
			log.error("Error sending mail", e);
			throw new DnetRuntimeException("Error sending mail", e);
		}
	}

	private void addRecipients(final MimeMessage mimeMessage, final RecipientType type, final String... ccs) throws MessagingException, AddressException {
		for (final String cc : ccs) {
			mimeMessage.addRecipient(type, new InternetAddress(cc.trim()));
		}
	}

	private Properties obtainProperties() {
		final Properties p = new Properties();
		p.put("mail.transport.protocol", "smtp");
		p.put("mail.smtp.host", conf.getSmtpHost());
		p.put("mail.smtp.port", conf.getSmtpPort());
		p.put("mail.smtp.auth", Boolean.toString(StringUtils.isNotBlank(conf.getSmtpUser())));
		return p;
	}

	private Authenticator obtainAuthenticator() {
		if (StringUtils.isBlank(conf.getSmtpUser())) { return null; }

		return new Authenticator() {

			private final PasswordAuthentication authentication =
					new PasswordAuthentication(conf.getSmtpUser(), conf.getSmtpPassword());

			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return authentication;
			}

		};
	}

}
