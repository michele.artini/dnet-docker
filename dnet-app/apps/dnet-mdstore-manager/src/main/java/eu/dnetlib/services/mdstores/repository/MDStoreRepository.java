package eu.dnetlib.services.mdstores.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.dnetlib.domain.mdstore.MDStore;

@Repository
public interface MDStoreRepository extends JpaRepository<MDStore, String> {

}
