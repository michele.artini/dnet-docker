package eu.dnetlib.services.mdstores.backends;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import eu.dnetlib.common.mdstores.MDStoreBackend;
import eu.dnetlib.domain.mdstore.MDStore;
import eu.dnetlib.domain.mdstore.MDStoreVersion;
import eu.dnetlib.domain.mdstore.records.MetadataRecord;
import eu.dnetlib.domain.mdstore.records.MetadataRecord.MetadataRecordEncoding;
import eu.dnetlib.domain.mdstore.records.Provenance;
import eu.dnetlib.errors.MDStoreManagerException;

@Service
public class MockBackend implements MDStoreBackend {

	private static final Provenance MOCK_PROVENANCE = new Provenance("mock________::MOCK_DS", "Mock Datasource", "mock________");

	@Override
	public void completeNewMDStore(final MDStore mdstore) {
		mdstore.getParams().put("mockParam1", "test");
		mdstore.getParams().put("mockParam2", "abc");
		mdstore.getParams().put("mockParam3", 1234);
	}

	@Override
	public void completeNewMDStoreVersion(final MDStoreVersion version) {
		version.getParams().put("mockParam1", "v_test");
		version.getParams().put("mockParam2", "v_abc");
		version.getParams().put("mockParam3", 1234);
	}

	@Override
	public void delete(final MDStore mdstore) throws MDStoreManagerException {}

	@Override
	public void delete(final MDStoreVersion version) throws MDStoreManagerException {}

	@Override
	public List<MetadataRecord> listEntries(final MDStoreVersion version, final long limit) throws MDStoreManagerException {
		final List<MetadataRecord> list = new ArrayList<>();

		for (int i = 0; i < limit; i++) {
			final MetadataRecord rec = new MetadataRecord();
			rec.setOriginalId("mck-" + i);
			rec.setId("mock________::mck-" + i);
			rec.setBody("<RECORD>" + i + "</RECORD>");
			rec.setDateOfCollection(LocalDateTime.now());
			rec.setDateOfTransformation(LocalDateTime.now());
			rec.setEncoding(MetadataRecordEncoding.XML);
			rec.setProvenance(MOCK_PROVENANCE);
			list.add(rec);
		}

		return list;
	}

	@Override
	public Stream<MetadataRecord> streamEntries(final MDStoreVersion version) throws MDStoreManagerException {
		return listEntries(version, 1000).stream();
	}

	@Override
	public Set<String> fixInconsistencies(final boolean delete) throws MDStoreManagerException {
		return new LinkedHashSet<>(Arrays.asList("1", "2", "3", "4"));
	}

	@Override
	public long countRecords(final String versionId) {
		throw new RuntimeException("NOT_IMPLEMENTED");
	}

	@Override
	public void saveRecords(final MDStoreVersion version, final Stream<MetadataRecord> inputStream) {
		throw new RuntimeException("NOT_IMPLEMENTED");
	}

}
