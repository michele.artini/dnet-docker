package eu.dnetlib.services.mdstores.backends;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import eu.dnetlib.common.mdstores.MDStoreBackend;
import eu.dnetlib.domain.mdstore.MDStore;
import eu.dnetlib.domain.mdstore.MDStoreVersion;
import eu.dnetlib.domain.mdstore.records.MetadataRecord;
import eu.dnetlib.errors.MDStoreManagerException;

@Service
public class DefaultBackend implements MDStoreBackend {

	@Override
	public void completeNewMDStore(final MDStore mdstore) {}

	@Override
	public void completeNewMDStoreVersion(final MDStoreVersion version) {}

	@Override
	public void delete(final MDStore mdstore) throws MDStoreManagerException {}

	@Override
	public void delete(final MDStoreVersion version) throws MDStoreManagerException {}

	@Override
	public List<MetadataRecord> listEntries(final MDStoreVersion version, final long limit) throws MDStoreManagerException {
		return new ArrayList<>();
	}

	@Override
	public Stream<MetadataRecord> streamEntries(final MDStoreVersion version) throws MDStoreManagerException {
		return Stream.empty();
	}

	@Override
	public Set<String> fixInconsistencies(final boolean delete) throws MDStoreManagerException {
		return new HashSet<>();
	}

	@Override
	public long countRecords(final String versionId) {
		return 0;
	}

	@Override
	public void saveRecords(final MDStoreVersion version, final Stream<MetadataRecord> inputStream) {}

}
