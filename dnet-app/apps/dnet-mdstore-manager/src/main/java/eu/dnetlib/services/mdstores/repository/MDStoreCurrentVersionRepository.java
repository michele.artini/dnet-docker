package eu.dnetlib.services.mdstores.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.dnetlib.domain.mdstore.MDStoreCurrentVersion;

@Repository
public interface MDStoreCurrentVersionRepository extends JpaRepository<MDStoreCurrentVersion, String> {

	long countByCurrentVersion(String versionId);
}
