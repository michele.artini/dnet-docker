package eu.dnetlib.services.mdstores.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.controller.DnetRestController;
import eu.dnetlib.domain.mdstore.MDStoreType;
import eu.dnetlib.domain.mdstore.MDStoreVersion;
import eu.dnetlib.domain.mdstore.MDStoreWithInfo;
import eu.dnetlib.domain.mdstore.records.MetadataRecord;
import eu.dnetlib.errors.MDStoreManagerException;
import eu.dnetlib.services.mdstores.service.MDStoreService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/mdstores")
@Tag(name = "Metadata Stores")
public class ApiController extends DnetRestController {

	@Autowired
	private MDStoreService service;

	@Value("${dhp.mdstore-manager.hadoop.cluster}")
	private String hadoopCluster;

	@Value("${dhp.mdstore-manager.hadoop.user}")
	private String hadoopUser;

	@Value("${dhp.mdstore-manager.hdfs.base-path}")
	private String hdfsBasePath;

	@Operation(summary = "Return all the mdstores")
	@GetMapping("/")
	public Iterable<MDStoreWithInfo> find() {
		return service.listMdStores();
	}

	@Operation(summary = "Return a mdstores by id")
	@GetMapping("/mdstore/{mdId}")
	public MDStoreWithInfo getMdStore(@PathVariable final String mdId) throws MDStoreManagerException {
		return service.findMdStore(mdId);
	}

	@Operation(summary = "Create a new mdstore")
	@PostMapping("/new/{format}/{layout}/{interpretation}/{type}")
	public MDStoreWithInfo createMDStore(
			@PathVariable final String format,
			@PathVariable final String layout,
			@PathVariable final String interpretation,
			@PathVariable final String type,
			@RequestParam(required = true) final String dsName,
			@RequestParam(required = true) final String dsId,
			@RequestParam(required = true) final String apiId) throws MDStoreManagerException {

		final String id = service.createMDStore(format, layout, interpretation, MDStoreType.valueOf(type), dsName, dsId, apiId);

		return service.findMdStore(id);
	}

	@Operation(summary = "Delete a mdstore by id")
	@DeleteMapping("/mdstore/{mdId}")
	public void delete(@PathVariable final String mdId)
			throws MDStoreManagerException {
		service.deleteMdStore(mdId);
	}

	@Operation(summary = "Return all the versions of a mdstore")
	@GetMapping("/mdstore/{mdId}/versions")
	public Iterable<MDStoreVersion> listVersions(@PathVariable final String mdId) throws MDStoreManagerException {
		return service.listVersions(mdId);
	}

	@Operation(summary = "Create a new preliminary version of a mdstore")
	@GetMapping("/mdstore/{mdId}/newVersion")
	public MDStoreVersion prepareNewVersion(
			@PathVariable final String mdId) throws MDStoreManagerException {
		return service.prepareMdStoreVersion(mdId);
	}

	@Operation(summary = "Promote a preliminary version to current")
	@GetMapping("/version/{versionId}/commit/{size}")
	public MDStoreVersion commitVersion(
			@PathVariable final String versionId,
			@PathVariable final long size) throws MDStoreManagerException {
		try {
			return service.commitMdStoreVersion(versionId, size);
		} finally {
			service.deleteExpiredVersions();
		}
	}

	@Operation(summary = "Abort a preliminary version")
	@GetMapping("/version/{versionId}/abort")
	public void commitVersion(@PathVariable final String versionId) throws MDStoreManagerException {
		service.deleteMdStoreVersion(versionId, true);
	}

	@Operation(summary = "Return an existing mdstore version")
	@GetMapping("/version/{versionId}")
	public MDStoreVersion getVersion(@PathVariable final String versionId)
			throws MDStoreManagerException {
		return service.findVersion(versionId);
	}

	@Operation(summary = "Delete a mdstore version")
	@DeleteMapping("/version/{versionId}")
	public void deleteVersion(@PathVariable final String versionId,
			@RequestParam(required = false, defaultValue = "false") final boolean force)
			throws MDStoreManagerException {

		service.deleteMdStoreVersion(versionId, force);
	}

	@Operation(summary = "Decrease the read count of a mdstore version")
	@GetMapping("/version/{versionId}/endReading")
	public MDStoreVersion endReading(@PathVariable final String versionId) throws MDStoreManagerException {
		return service.endReading(versionId);
	}

	@Operation(summary = "Reset the read count of a mdstore version")
	@GetMapping("/version/{versionId}/resetReading")
	public MDStoreVersion resetReading(@PathVariable final String versionId) throws MDStoreManagerException {
		return service.resetReading(versionId);
	}

	@Operation(summary = "Show informations")
	@GetMapping("/info")
	public Map<String, Object> info() {
		final Map<String, Object> info = new LinkedHashMap<>();
		info.put("number_of_mdstores", service.countMdStores());
		info.put("hadoop_user", hadoopUser);
		info.put("hadoop_cluster", hadoopCluster);
		info.put("hdfs_base_path", hdfsBasePath);
		info.put("expired_versions", service.listExpiredVersions());
		return info;
	}

	@Operation(summary = "read the records of a mdstore version")
	@GetMapping("/version/{versionId}/content/{limit}")
	public List<MetadataRecord> listVersionRecords(@PathVariable final String versionId, @PathVariable final long limit)
			throws MDStoreManagerException {
		return service.listVersionRecords(versionId, limit);
	}

	@Operation(summary = "read the records of a mdstore (current version)")
	@GetMapping("/mdstore/{mdId}/content/{limit}")
	public List<? extends MetadataRecord> listMdstoreRecords(@PathVariable final String mdId, @PathVariable final long limit) throws MDStoreManagerException {
		return service.listVersionRecords(service.findMdStore(mdId).getCurrentVersion(), limit);
	}

	@Operation(summary = "Return all the mdstore identifiers")
	@GetMapping("/ids")
	public List<String> findIdentifiers() {
		return service.listMdStoreIDs();
	}

	@Operation(summary = "Increase the read count of the current mdstore")
	@GetMapping("/mdstore/{mdId}/startReading")
	public MDStoreVersion startReading(@PathVariable final String mdId) throws MDStoreManagerException {
		return service.startReading(mdId);
	}

	@Operation(summary = "Fix the inconsistencies on HDFS")
	@GetMapping("/hdfs/inconsistencies")
	public Map<MDStoreType, Set<String>> fixHdfsInconsistencies(
			@RequestParam(required = false, defaultValue = "false") final boolean forceDelete)
			throws MDStoreManagerException {

		return service.fixInconsistencies(forceDelete);
	}

	@Operation(summary = "Delete expired versions")
	@DeleteMapping("/versions/expired")
	public void deleteExpiredVersions() {
		new Thread(service::deleteExpiredVersions).start();
	}

}
