package eu.dnetlib.services.mdstores.repository;

import org.springframework.stereotype.Repository;

import eu.dnetlib.domain.mdstore.MDStoreWithInfo;
import eu.dnetlib.utils.ReadOnlyRepository;

@Repository
public interface MDStoreWithInfoRepository extends ReadOnlyRepository<MDStoreWithInfo, String> {

}
