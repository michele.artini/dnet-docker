package eu.dnetlib.services.mdstores;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;

import eu.dnetlib.common.app.AbstractDnetApp;
import eu.dnetlib.common.mdstores.backends.sql.MDStoreSqlBackend;
import eu.dnetlib.domain.mdstore.MDStore;
import eu.dnetlib.domain.mdstore.MDStoreCurrentVersion;
import eu.dnetlib.domain.mdstore.MDStoreVersion;
import eu.dnetlib.domain.mdstore.MDStoreWithInfo;
import eu.dnetlib.domain.service.ServiceType;

@SpringBootApplication
@EntityScan(basePackageClasses = { MDStore.class, MDStoreCurrentVersion.class, MDStoreVersion.class, MDStoreWithInfo.class })
public class MdstoresApplication extends AbstractDnetApp {

	@Value("${mdstores.data.datasource.url}")
	private String databaseUrl;

	@Value("${mdstores.data.datasource.username}")
	private String databaseUsername;

	@Value("${mdstores.data.datasource.password}")
	private String databasePassword;

	public static void main(final String[] args) {
		SpringApplication.run(MdstoresApplication.class, args);
	}

	@Bean
	public MDStoreSqlBackend mdStoreSqlBackend() {
		return new MDStoreSqlBackend(databaseUrl, databaseUsername, databasePassword);
	}

	@Override
	protected ServiceType serviceType() {
		return ServiceType.mdstore_manager;
	}
}
