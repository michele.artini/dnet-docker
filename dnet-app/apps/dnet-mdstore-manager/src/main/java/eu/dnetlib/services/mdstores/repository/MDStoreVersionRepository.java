package eu.dnetlib.services.mdstores.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.dnetlib.domain.mdstore.MDStoreVersion;

@Repository
public interface MDStoreVersionRepository extends JpaRepository<MDStoreVersion, String> {

	void deleteByMdstore(String id);

	long countByMdstoreAndWriting(String id, boolean b);

	long countByMdstoreAndReadCountGreaterThan(String id, int count);

	Iterable<MDStoreVersion> findByMdstoreOrderById(String mdId);

}
