package eu.dnetlib.services.oai.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.oai.OaiConfiguration;

public interface OaiConfigurationRepository extends JpaRepository<OaiConfiguration, Integer> {

}
