package eu.dnetlib.services.oai.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.oai.ExportedOaiSet;

public interface OaiSetRepository extends JpaRepository<ExportedOaiSet, String> {

}
