package eu.dnetlib.services.oai.repository;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.oai.ExportedOaiRecord;

public interface OaiRecordRepository extends JpaRepository<ExportedOaiRecord, String> {

	Page<ExportedOaiRecord> findByOaiSetAndDateBetween(String oaiSet, LocalDate from, LocalDate until, Pageable page);

	Page<ExportedOaiRecord> findByDateBetween(LocalDate from, LocalDate until, Pageable page);

	void deleteByOaiSet(String oaiSet);

}
