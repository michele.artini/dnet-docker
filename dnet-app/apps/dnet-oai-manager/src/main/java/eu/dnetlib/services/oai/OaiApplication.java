package eu.dnetlib.services.oai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;

import eu.dnetlib.common.app.AbstractDnetApp;
import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.mapping.xslt.XsltTransformerFactory;
import eu.dnetlib.domain.oai.ExportedOaiRecord;
import eu.dnetlib.domain.oai.ExportedOaiSet;
import eu.dnetlib.domain.oai.OaiConfiguration;
import eu.dnetlib.domain.oai.OaiMetadataFormat;
import eu.dnetlib.domain.service.ServiceType;

@SpringBootApplication
@EntityScan(basePackageClasses = { OaiConfiguration.class, ExportedOaiSet.class, OaiMetadataFormat.class, ExportedOaiRecord.class })
public class OaiApplication extends AbstractDnetApp {

	public static void main(final String[] args) {
		SpringApplication.run(OaiApplication.class, args);
	}

	@Override
	protected ServiceType serviceType() {
		return ServiceType.oai_manager;
	}

	@Bean
	public XsltTransformerFactory xsltTransformFactory(final DnetServiceClientFactory clientFactory) {
		return new XsltTransformerFactory(clientFactory);
	}
}
