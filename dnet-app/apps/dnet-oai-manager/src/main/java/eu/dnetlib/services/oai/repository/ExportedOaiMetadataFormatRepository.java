package eu.dnetlib.services.oai.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.dnetlib.domain.oai.ExportedOaiMetadataFormat;

public interface ExportedOaiMetadataFormatRepository extends JpaRepository<ExportedOaiMetadataFormat, String> {

}
