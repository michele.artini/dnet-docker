package eu.dnetlib.services.oai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.controller.DnetRestController;
import eu.dnetlib.domain.oai.ExportedOaiMetadataFormat;
import eu.dnetlib.domain.oai.ExportedOaiSet;
import eu.dnetlib.domain.oai.OaiConfiguration;
import eu.dnetlib.errors.DnetException;
import eu.dnetlib.services.oai.service.OaiService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
@RequestMapping("/api/oai")
public class ApiController extends DnetRestController {

	@Autowired
	private OaiService oaiService;

	@GetMapping("/conf")
	public OaiConfiguration oaiConfiguration() throws DnetException {
		return this.oaiService.oaiConfiguration();
	}

	@PostMapping("/conf")
	public OaiConfiguration oaiConfiguration(@RequestBody final OaiConfiguration conf) throws DnetException {
		return this.oaiService.oaiConfiguration(conf);
	}

	@GetMapping("/md-formats")
	public List<ExportedOaiMetadataFormat> listMdFormats() throws DnetException {
		return this.oaiService.listMetadataFormats();
	}

	@PostMapping("/md-formats")
	public List<ExportedOaiMetadataFormat> addOrUpdateMdFormats(@RequestBody final ExportedOaiMetadataFormat mdFormat) throws DnetException {
		this.oaiService.addOrUpdateMetadataFormat(mdFormat);
		return this.oaiService.listMetadataFormats();
	}

	@DeleteMapping("/md-formats/{mdprefix}")
	public List<ExportedOaiMetadataFormat> deleteMdFormats(@PathVariable final String mdprefix) throws DnetException {
		this.oaiService.deleteMetadataFormat(mdprefix);
		return this.oaiService.listMetadataFormats();
	}

	@GetMapping("/sets")
	public List<ExportedOaiSet> listOaiSets() throws DnetException {
		return this.oaiService.listSets();
	}

	@PostMapping("/sets")
	public List<ExportedOaiSet> addOrUpdateOaiSet(@RequestBody final ExportedOaiSet oaiSet) throws DnetException {
		this.oaiService.addOrUpdateSet(oaiSet);
		return this.oaiService.listSets();
	}

	@DeleteMapping("/sets/{setSpec}")
	public List<ExportedOaiSet> deleteOaiSet(@PathVariable final String setSpec) throws DnetException {
		this.oaiService.deleteSet(setSpec);
		return this.oaiService.listSets();
	}
}
