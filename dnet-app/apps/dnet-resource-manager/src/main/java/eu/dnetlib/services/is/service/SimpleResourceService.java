package eu.dnetlib.services.is.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import eu.dnetlib.domain.resource.ResourceType;
import eu.dnetlib.domain.resource.SimpleResource;
import eu.dnetlib.errors.InformationServiceException;
import eu.dnetlib.services.is.repository.ResourceTypeRepository;
import eu.dnetlib.services.is.repository.SimpleResourceRepository;
import jakarta.transaction.Transactional;

@Service
public class SimpleResourceService {

	@Autowired
	private SimpleResourceRepository simpleResourceRepository;

	@Autowired
	private ResourceTypeRepository resourceTypeRepository;

	@Autowired
	private ResourceValidator resourceValidator;

	private static final Log log = LogFactory.getLog(SimpleResourceService.class);

	public SimpleResource getMetadata(final String id) throws InformationServiceException {
		return simpleResourceRepository.findById(id).orElseThrow(() -> new InformationServiceException("Id not found: " + id));
	}

	public String getContent(final String id) throws InformationServiceException {
		return simpleResourceRepository.findContentById(id).orElseThrow(() -> new InformationServiceException("Id not found: " + id));
	}

	public SimpleResource getMetadata(final String type, final String name) throws InformationServiceException {
		return simpleResourceRepository.findAllByTypeAndName(type, name)
				.orElseThrow(() -> new InformationServiceException("Resource not found, type: " + type + ", name: " + name));
	}

	public String getContentType(final String id) throws InformationServiceException {
		return simpleResourceRepository.findContentTypeById(id).orElseThrow(() -> new InformationServiceException("Id not found: " + id));
	}

	public List<SimpleResource> listResources(final String type, final String subType) {

		final List<SimpleResource> list = ((StringUtils.isNotBlank(subType)) ? simpleResourceRepository.findByTypeAndSubtype(type, subType)
				: simpleResourceRepository.findByType(type));

		return list.stream()
				.sorted((r1, r2) -> StringUtils.compareIgnoreCase(r1.getName(), r2.getName()))
				.collect(Collectors.toList());
	}

	public void deleteResource(@PathVariable final String id) {
		log.info("Deleting resource: " + id);
		simpleResourceRepository.deleteById(id);
	}

	@Transactional
	public SimpleResource saveNewResource(final String name,
			final String type,
			final String subtype,
			final String description,
			final String content) throws InformationServiceException {

		resourceValidator.validate(type, content);

		final LocalDateTime now = LocalDateTime.now();

		final SimpleResource res = new SimpleResource();
		res.setId(UUID.randomUUID().toString());
		res.setName(name);
		res.setType(type);
		res.setSubtype(subtype);
		res.setDescription(description);
		res.setCreationDate(now);
		res.setModificationDate(now);

		simpleResourceRepository.save(res);
		simpleResourceRepository.setContentById(res.getId(), content);

		return res;
	}

	@Transactional
	public void saveMetadata(final String id, final SimpleResource r) throws InformationServiceException {
		if (!simpleResourceRepository.existsById(id)) { throw new InformationServiceException("Resource not found"); }
		r.setId(id);
		simpleResourceRepository.save(r);
	}

	@Transactional
	public void saveContent(final String id, final String content) throws InformationServiceException {
		final SimpleResource res = simpleResourceRepository.findById(id).orElseThrow(() -> new InformationServiceException("Resource not found"));

		resourceValidator.validate(res.getType(), content);

		simpleResourceRepository.setContentById(id, content);

	}

	public Iterable<ResourceType> listResourceTypes() {
		return resourceTypeRepository.findAll();
	}

	public ResourceType findResourceType(@PathVariable final String id) {
		return resourceTypeRepository
				.findById(id)
				.orElse(new ResourceType("not_present", "???", MediaType.TEXT_PLAIN_VALUE, 0));
	}
}
