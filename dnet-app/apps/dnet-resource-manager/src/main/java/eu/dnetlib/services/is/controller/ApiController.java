package eu.dnetlib.services.is.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.controller.DnetRestController;
import eu.dnetlib.domain.resource.ResourceType;
import eu.dnetlib.domain.resource.SimpleResource;
import eu.dnetlib.errors.InformationServiceException;
import eu.dnetlib.services.is.service.SimpleResourceService;
import eu.dnetlib.utils.XmlIndenter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api")
public class ApiController extends DnetRestController {

	@Autowired
	private SimpleResourceService service;

	@GetMapping("/resources/byType/{type}")
	public List<SimpleResource> listResources(@PathVariable final String type, @RequestParam(required = false) final String subtype) {
		return service.listResources(type, subtype);
	}

	@GetMapping("/resources/{resId}/metadata")
	public SimpleResource getMetadata(@PathVariable final String resId) throws InformationServiceException {
		return service.getMetadata(resId);
	}

	@GetMapping("/resources/{resId}/content")
	public void getContent(@PathVariable final String resId, final HttpServletResponse res) throws InformationServiceException {
		final String ctype = service.getContentType(resId);

		res.setCharacterEncoding(StandardCharsets.UTF_8.name());
		res.setContentType(ctype);

		final String content =
				MediaType.APPLICATION_XML_VALUE.equals(ctype) ? XmlIndenter.indent(service.getContent(resId)) : service.getContent(resId);

		try {
			IOUtils.write(content, res.getOutputStream(), StandardCharsets.UTF_8.name());
		} catch (final IOException e) {
			throw new InformationServiceException("Error retrieving content", e);
		}
	}

	@GetMapping("/resources/byType/{type}/{name}/metadata")
	public SimpleResource getMetadata(@PathVariable final String type, @PathVariable final String name) throws InformationServiceException {
		return service.getMetadata(type, name);
	}

	@GetMapping("/resources/byType/{type}/{name}/content")
	public void getContent(@PathVariable final String type, @PathVariable final String name, final HttpServletResponse res) throws InformationServiceException {
		final SimpleResource sr = getMetadata(type, name);
		if (sr != null) {
			getContent(sr.getId(), res);
		}
	}

	@PostMapping("/resources/")
	public SimpleResource newResource(@RequestParam final String name,
			@RequestParam final String type,
			@RequestParam(required = false, defaultValue = "") final String subtype,
			@RequestParam(required = false, defaultValue = "") final String description,
			@RequestParam final String content)
			throws InformationServiceException {

		return service.saveNewResource(name, type, subtype, description, content);
	}

	@PostMapping("/resources/{resId}/metadata")
	public void saveMetadata(@PathVariable final String resId, @RequestBody final SimpleResource r) throws InformationServiceException {
		service.saveMetadata(resId, r);
	}

	@PostMapping("/resources/{resId}/content")
	public void saveContent(@PathVariable final String resId, @RequestParam final String content) throws InformationServiceException {
		service.saveContent(resId, content);
	}

	@DeleteMapping("/resources/{resId}")
	public void deleteResource(@PathVariable final String resId) {
		service.deleteResource(resId);
	}

	@GetMapping("/resourceTypes")
	public Iterable<ResourceType> resourceTypes() {
		return service.listResourceTypes();

	}

	@GetMapping("/resourceTypes/{id}")
	public ResourceType resourceType(@PathVariable final String id) {
		return service.findResourceType(id);
	}

	@PostMapping(value = "/resources/{resId}/file", consumes = {
			MediaType.TEXT_PLAIN_VALUE, MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE
	})
	public void uploadContent(@PathVariable final String resId, final HttpServletRequest request) throws InformationServiceException {
		try {
			final String content = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);
			service.saveContent(resId, content);
		} catch (final IOException e) {
			throw new InformationServiceException("Error processing input file", e);
		}
	}

}
