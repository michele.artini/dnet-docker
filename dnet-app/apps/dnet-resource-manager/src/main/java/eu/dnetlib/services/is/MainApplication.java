package eu.dnetlib.services.is;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import eu.dnetlib.common.app.AbstractDnetApp;
import eu.dnetlib.domain.resource.ResourceType;
import eu.dnetlib.domain.resource.SimpleResource;
import eu.dnetlib.domain.service.ServiceType;

@SpringBootApplication
@EntityScan(basePackageClasses = { ResourceType.class, SimpleResource.class })
public class MainApplication extends AbstractDnetApp {

	public static void main(final String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}

	@Override
	protected ServiceType serviceType() {
		return ServiceType.resource_manager;
	}
}
