package eu.dnetlib.services.is.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import eu.dnetlib.domain.resource.SimpleResource;

public interface SimpleResourceRepository extends JpaRepository<SimpleResource, String> {

	@Query(value = "select content from resources where id = ?1", nativeQuery = true)
	Optional<String> findContentById(String id);

	Optional<SimpleResource> findAllByTypeAndName(String type, String name);

	@Modifying
	@Query(value = "update resources set content = ?2 where id = ?1", nativeQuery = true)
	void setContentById(String id, String content);

	List<SimpleResource> findByType(String type);

	List<SimpleResource> findByTypeAndSubtype(String type, String subtype);

	@Query(value = "select t.content_type from resources r join resource_types t on (r.type = t.id) where r.id = ?1", nativeQuery = true)
	Optional<String> findContentTypeById(String id);

}
