package eu.dnetlib.services.is.controller;

import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.common.controller.DnetRestController;
import eu.dnetlib.services.is.importer.OldProfilesImporter;
import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/import")
public class ImporterController extends DnetRestController {

	// EXAMPLE:
	// find ./VocabularyDSResourceType/ -name "*.xml" -exec curl -X POST "http://localhost:8280/api/import/vocabulary" -H "accept: */*" -H
	// "Content-Type: text/plain" --data-binary @{} \;
	// find ./DedupConfigurationDSResources/ -name "*.xml" -exec curl -X POST "http://localhost:8280/api/import/resource" -H "accept: */*"
	// -H "Content-Type: text/plain" --data-binary @{} \;

	@Autowired
	private OldProfilesImporter oldProfilesImporter;

	@PostMapping(value = "/resource", consumes = {
			MediaType.TEXT_PLAIN_VALUE, MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE
	})
	public String importResource(final HttpServletRequest request) throws Exception {
		final String xml = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);

		return oldProfilesImporter.importSimpleResource(xml);
	}

}
