package eu.dnetlib.services.is.repository;

import eu.dnetlib.domain.resource.ResourceType;
import eu.dnetlib.utils.ReadOnlyRepository;

public interface ResourceTypeRepository extends ReadOnlyRepository<ResourceType, String> {

}
