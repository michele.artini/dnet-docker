package eu.dnetlib.services.is.importer;

import java.time.LocalDateTime;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.dnetlib.domain.resource.SimpleResource;
import eu.dnetlib.errors.InformationServiceException;
import eu.dnetlib.services.is.repository.SimpleResourceRepository;
import eu.dnetlib.services.is.service.ResourceValidator;
import eu.dnetlib.utils.XmlIndenter;
import jakarta.transaction.Transactional;

@Service
public class OldProfilesImporter {

	@Autowired
	private SimpleResourceRepository simpleResourceRepository;

	@Autowired
	private ResourceValidator resourceValidator;

	@Transactional
	public String importSimpleResource(final String xml) throws InformationServiceException {

		try {
			final Document doc = DocumentHelper.parseText(xml);

			final String id = StringUtils.substringBefore(doc.valueOf("//RESOURCE_IDENTIFIER/@value"), "_");
			final LocalDateTime now = LocalDateTime.now();

			final SimpleResource res = new SimpleResource();
			res.setId(id);
			res.setCreationDate(now);
			res.setModificationDate(now);

			String resContent;
			switch (doc.valueOf("//RESOURCE_TYPE/@value")) {
			case "CleanerDSResourceType":
				res.setType("cleaning_rule");
				res.setName(doc.valueOf("//CLEANER_NAME"));
				res.setDescription(doc.valueOf("//CLEANER_DESCRIPTION"));

				for (final Node n : doc.selectNodes("//RULE")) {
					final Element e = (Element) n;
					e.addAttribute("vocabulary", e.valueOf("@vocabularies"));
					for (final Attribute attr : e.attributes()) {
						if (attr.getName() == "vocabularies" || attr.getName() == "groovy") {
							attr.detach();
						}
					}
				}

				resContent = XmlIndenter.indent(doc.selectSingleNode("//CLEANER_RULES"));
				break;
			case "TransformationRuleDSResourceType":
				res.setName(doc.valueOf("//SCRIPT/TITLE"));
				res.setDescription("");
				if (doc.selectNodes("//*[local-name() = 'stylesheet']").size() > 0) {
					res.setType("transformation_rule_xslt");
					resContent = XmlIndenter.indent(doc.selectSingleNode("//*[local-name() = 'stylesheet']"));
				} else {
					final String code = doc.valueOf("//SCRIPT/CODE").trim();

					try {
						final Document xsltDoc = DocumentHelper.parseText(code);
						res.setType("transformation_rule_xslt");
						resContent = XmlIndenter.indent(xsltDoc);
					} catch (final DocumentException e) {
						res.setType("transformation_rule_legacy");
						resContent = code;
					}
				}
				break;
			case "HadoopJobConfigurationDSResourceType":
				res.setType("hadoop_job_configuration");
				res.setName(doc.valueOf("//HADOOP_JOB/@name"));
				res.setDescription(doc.valueOf("//HADOOP_JOB/DESCRIPTION"));
				resContent = XmlIndenter.indent(doc.selectSingleNode("//HADOOP_JOB"));
				break;
			case "DedupConfigurationDSResourceType":
				res.setType("dedup_configuration");
				res.setName(doc.valueOf("//DESCRIPTION"));
				res.setDescription(doc.valueOf("//DESCRIPTION"));
				resContent = doc.valueOf("//DEDUPLICATION");
				break;
			default:
				throw new InformationServiceException("Invalid resource type: " + doc.valueOf("//RESOURCE_TYPE/@value"));
			}

			resourceValidator.validate(res.getType(), resContent.trim());

			simpleResourceRepository.save(res);
			simpleResourceRepository.setContentById(id, resContent.trim());

			return res.getId();
		} catch (final Exception e) {
			throw new InformationServiceException("Error parsing file", e);
		}
	}

}
