package eu.dnetlib.wfs.nodes;

import java.util.stream.Stream;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.MDStoreManagerClient;
import eu.dnetlib.common.clients.OaiManagerClient;
import eu.dnetlib.common.mdstores.backends.sql.MDStoreSqlBackend;
import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.domain.dsm.Datasource;
import eu.dnetlib.domain.mdstore.MDStoreVersion;
import eu.dnetlib.domain.mdstore.MDStoreWithInfo;
import eu.dnetlib.domain.mdstore.records.MetadataRecord;
import eu.dnetlib.domain.oai.OaiConfiguration;
import eu.dnetlib.domain.oai.ExportedOaiSet;
import eu.dnetlib.errors.DnetException;
import eu.dnetlib.wfs.annotations.WfInputParam;
import eu.dnetlib.wfs.annotations.WfNode;
import eu.dnetlib.wfs.output.oai.OaiBackend;

@WfNode("md_oai_export")
public class MdExportOaiJobNode extends ProcessNode {

	@WfInputParam
	private Datasource ds;

	@WfInputParam
	private Api api;

	@WfInputParam
	private String inputMdId;

	@WfInputParam
	private String oaiSetSpec;

	@WfInputParam
	private boolean oaiRefresh;

	@Autowired
	private DnetServiceClientFactory clientFactory;

	@Autowired
	private MDStoreSqlBackend mdStoreSqlBackend;

	@Autowired
	private OaiBackend oaiBackend;

	@Override
	protected void execute() throws Exception {
		if (StringUtils.isBlank(this.oaiSetSpec)) {
			this.oaiSetSpec = DigestUtils.md5Hex(this.ds.getId()).substring(0, 8);
		}

		final OaiManagerClient oaiManager = this.clientFactory.getClient(OaiManagerClient.class);
		final MDStoreManagerClient mdstoreManager = this.clientFactory.getClient(MDStoreManagerClient.class);

		final OaiConfiguration conf = oaiManager.getOaiConfiguration();
		final MDStoreWithInfo mdstore = mdstoreManager.findMDStore(this.inputMdId);

		if (mdstore.getFormat().equals(conf.getNativeFormat())) {
			final boolean isSetRegistered = oaiManager.listSets()
					.stream()
					.map(ExportedOaiSet::getSetSpec)
					.filter(s -> s.equals(this.oaiSetSpec))
					.findFirst()
					.isPresent();

			if (!isSetRegistered) {
				final ExportedOaiSet oaiSet = new ExportedOaiSet();
				oaiSet.setSetSpec(this.oaiSetSpec);
				oaiSet.setSetName(this.ds.getOfficialname());
				oaiSet.setDescription("Publications of " + this.ds.getOfficialname());
				oaiSet.setDsId(this.ds.getId());
				oaiManager.createOaiSet(oaiSet);
			}

			final MDStoreVersion inputVersion = mdstoreManager.startReading(this.inputMdId);

			final Stream<MetadataRecord> records = this.mdStoreSqlBackend.streamEntries(inputVersion);
			try {
				this.oaiBackend.addRecords(records, this.oaiSetSpec, this.oaiRefresh);
			} finally {
				mdstoreManager.endReading(inputVersion);
			}
		}
		throw new DnetException("The native oai format (" + conf.getNativeFormat() + ") and the mdstore format (" + mdstore.getFormat() + ") are different");
	}

}
