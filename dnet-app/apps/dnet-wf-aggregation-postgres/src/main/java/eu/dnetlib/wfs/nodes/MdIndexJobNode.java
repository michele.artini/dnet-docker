package eu.dnetlib.wfs.nodes;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.IndexManagerClient;
import eu.dnetlib.common.clients.MDStoreManagerClient;
import eu.dnetlib.common.index.solr.SolrService;
import eu.dnetlib.common.mdstores.backends.sql.MDStoreSqlBackend;
import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.domain.dsm.Datasource;
import eu.dnetlib.domain.index.IndexConfiguration;
import eu.dnetlib.domain.mdstore.MDStoreVersion;
import eu.dnetlib.domain.mdstore.records.MetadataRecord;
import eu.dnetlib.wfs.annotations.WfInputParam;
import eu.dnetlib.wfs.annotations.WfNode;

@WfNode("md_index")
public class MdIndexJobNode extends ProcessNode {

	@WfInputParam
	private Datasource ds;

	@WfInputParam
	private Api api;

	@WfInputParam
	private String inputMdId;

	@WfInputParam
	private String indexConfId;

	@WfInputParam
	private boolean incremental;

	@Autowired
	private DnetServiceClientFactory clientFactory;

	@Autowired
	private MDStoreSqlBackend mdStoreSqlBackend;

	@Autowired
	private SolrService solrService;

	@Override
	protected void execute() throws Exception {
		final IndexManagerClient indexManager = this.clientFactory.getClient(IndexManagerClient.class);

		final IndexConfiguration conf = indexManager.getIndexConfiguration(this.indexConfId);

		if (!this.solrService.existsIndex(conf.getId())) {
			this.solrService.createIndex(conf);
		}

		final long start = System.currentTimeMillis();

		final MDStoreManagerClient mdstoreManager = this.clientFactory.getClient(MDStoreManagerClient.class);

		final MDStoreVersion inputVersion = mdstoreManager.startReading(this.inputMdId);

		try {
			final AtomicLong counter = new AtomicLong(0);
			final long total = inputVersion.getSize();

			this.solrService.indexRecords(conf, this.mdStoreSqlBackend.streamEntries(inputVersion)
					.map(MetadataRecord::getBody)
					.map(o -> updateProgressMessage(o, counter, total, PROGRESS_MESSAGE_INTERVAL)));

			if (!this.incremental) {
				this.solrService.deleteOldRecords(conf, start);
			}

		} finally {
			mdstoreManager.endReading(inputVersion);
		}

	}

}
