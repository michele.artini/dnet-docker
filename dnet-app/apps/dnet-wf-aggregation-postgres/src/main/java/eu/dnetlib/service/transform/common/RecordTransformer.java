package eu.dnetlib.service.transform.common;

public interface RecordTransformer<T, K> {

	public K transform(T source);
}
