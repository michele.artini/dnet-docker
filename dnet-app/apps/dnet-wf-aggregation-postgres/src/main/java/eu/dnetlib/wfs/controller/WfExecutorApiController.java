package eu.dnetlib.wfs.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/wfs")
public class WfExecutorApiController extends AbstractWfExecutorApiController {

}
