package eu.dnetlib.wfs.output.oai;

import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

import eu.dnetlib.domain.mdstore.records.MetadataRecord;
import jakarta.annotation.PostConstruct;

@Service
public class OaiBackend {

	@Value("${oai.database.url}")
	private String oaiDatabaseUrl;

	@Value("${oai.database.username}")
	private String oaiDatabaseUser;

	@Value("${oai.database.password}")
	private String oaiDatabasePassword;

	private JdbcTemplate jdbcTemplate;

	private TransactionTemplate transactionTemplate;

	@PostConstruct
	public void init() {
		final DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setUrl(oaiDatabaseUrl);
		ds.setUsername(oaiDatabaseUser);
		ds.setPassword(oaiDatabasePassword);

		final DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(ds);
		transactionTemplate = new TransactionTemplate(transactionManager);
		transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);

		jdbcTemplate = new JdbcTemplate(ds, true);
	}

	public void addRecords(final Stream<MetadataRecord> records, final String oaiSetSpec, final boolean refresh) {

		transactionTemplate.execute(status -> {
			if (refresh) {
				jdbcTemplate.update("delete from oai_records where oai_set = ?", oaiSetSpec);
			}
			records.forEach(r -> jdbcTemplate.update("INSERT INTO oai_records (id, body, set_spec) VALUES (?, ? ,?)", r.getId(), r.getBody(), oaiSetSpec));

			return 1;
		});

	}

}
