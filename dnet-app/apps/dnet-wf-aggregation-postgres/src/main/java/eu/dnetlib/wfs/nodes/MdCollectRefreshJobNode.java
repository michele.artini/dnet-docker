package eu.dnetlib.wfs.nodes;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.springframework.beans.factory.annotation.Autowired;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.DsmClient;
import eu.dnetlib.common.clients.MDStoreManagerClient;
import eu.dnetlib.common.mdstores.backends.sql.MDStoreSqlBackend;
import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.domain.dsm.Datasource;
import eu.dnetlib.domain.mdstore.MDStoreVersion;
import eu.dnetlib.domain.mdstore.records.MetadataRecord;
import eu.dnetlib.wfs.annotations.WfInputParam;
import eu.dnetlib.wfs.annotations.WfNode;
import eu.dnetlib.wfs.collector.CollectorService;
import eu.dnetlib.wfs.utils.MdBuilderFactory;
import eu.dnetlib.wfs.utils.XpathFilterFactory;

@WfNode("md_collect_refresh")
public class MdCollectRefreshJobNode extends ProcessNode {

	@WfInputParam
	private Datasource ds;

	@WfInputParam
	private Api api;

	@WfInputParam
	private String mdId;

	@WfInputParam(optional = true)
	private String filterXpath;

	@Autowired
	private DnetServiceClientFactory clientFactory;

	@Autowired
	private CollectorService collectorService;

	@Autowired
	private MDStoreSqlBackend mdStoreSqlBackend;

	@Override
	protected void execute() throws Exception {

		final Predicate<Document> filter = XpathFilterFactory.createFilter(this.filterXpath);
		final Function<Document, MetadataRecord> mdBuilder = MdBuilderFactory.createMdBuilder(this.ds, this.api);

		final MDStoreManagerClient mdstoreManager = this.clientFactory.getClient(MDStoreManagerClient.class);

		final MDStoreVersion outputVersion = mdstoreManager.newVersion(this.mdId);

		try {
			final AtomicLong counter = new AtomicLong(0);

			final Stream<MetadataRecord> stream = this.collectorService.collect(this.api)
					.map(xml -> {
						try {
							return DocumentHelper.parseText(xml);
						} catch (final DocumentException e) {
							throw new RuntimeException("Invalid record: " + xml);
						}
					})
					.filter(filter)
					.map(mdBuilder)
					.map(o -> updateProgressMessage(o, counter, null, PROGRESS_MESSAGE_INTERVAL));

			this.mdStoreSqlBackend.saveRecords(outputVersion, stream);

			final long size = this.mdStoreSqlBackend.countRecords(outputVersion.getId());

			mdstoreManager.commitVersion(outputVersion.getId(), size);

			updateProgressMessage(size + "/" + size);

			this.clientFactory.getClient(DsmClient.class).updateApiCollectionInfo(this.api.getId(), this.mdId, size);
		} catch (final Throwable e) {
			System.err.println("****************** ABORTED");
			mdstoreManager.abortVersion(outputVersion);
			throw e;
		}
	}

}
