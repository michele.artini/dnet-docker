package eu.dnetlib.wfs.nodes;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import eu.dnetlib.common.clients.DnetServiceClientFactory;
import eu.dnetlib.common.clients.DsmClient;
import eu.dnetlib.common.clients.MDStoreManagerClient;
import eu.dnetlib.common.mapping.RecordTransformer;
import eu.dnetlib.common.mapping.xslt.XsltTransformerFactory;
import eu.dnetlib.common.mdstores.backends.sql.MDStoreSqlBackend;
import eu.dnetlib.domain.dsm.Api;
import eu.dnetlib.domain.dsm.Datasource;
import eu.dnetlib.domain.mdstore.MDStoreVersion;
import eu.dnetlib.domain.mdstore.records.MetadataRecord;
import eu.dnetlib.wfs.annotations.WfInputParam;
import eu.dnetlib.wfs.annotations.WfNode;
import eu.dnetlib.wfs.utils.XpathFilterFactory;

@WfNode("md_transform_xslt")
public class MdTransformJobNode extends ProcessNode {

	@WfInputParam
	private Datasource ds;

	@WfInputParam
	private Api api;

	@WfInputParam
	private String inputMdId;

	@WfInputParam
	private String outputMdId;

	@WfInputParam(optional = true)
	private String filterXpath;

	@WfInputParam
	private String ruleId;

	@Autowired
	private XsltTransformerFactory xsltTransformFactory;

	@Autowired
	private DnetServiceClientFactory clientFactory;

	@Autowired
	private MDStoreSqlBackend mdStoreSqlBackend;

	@Override
	protected void execute() throws Exception {

		final Predicate<Document> filter = XpathFilterFactory.createFilter(this.filterXpath);

		final Map<String, Object> params = new HashMap<>();
		// TODO (LOW PRIORITY): which params ?
		final RecordTransformer<String, String> xslt = this.xsltTransformFactory.getTransformerById(this.ruleId, params);
		final MDStoreManagerClient mdstoreManager = this.clientFactory.getClient(MDStoreManagerClient.class);

		final MDStoreVersion inputVersion = mdstoreManager.startReading(this.inputMdId);
		final MDStoreVersion outputVersion = mdstoreManager.newVersion(this.outputMdId);

		try {
			final AtomicLong counter = new AtomicLong(0);
			final long total = inputVersion.getSize();

			final Stream<MetadataRecord> stream = this.mdStoreSqlBackend.streamEntries(inputVersion)
					.map(input -> {
						final MetadataRecord output = new MetadataRecord();
						BeanUtils.copyProperties(input, output);
						output.setBody(xslt.transform(input.getBody()));
						output.setDateOfTransformation(LocalDateTime.now());
						return output;
					})
					.filter(record -> {
						try {
							final Document doc = DocumentHelper.parseText(record.getBody());
							return filter.test(doc);
						} catch (final DocumentException e) {
							throw new RuntimeException("Invalid record: " + record.getBody());
						}
					})
					.map(o -> updateProgressMessage(o, counter, total, PROGRESS_MESSAGE_INTERVAL));

			this.mdStoreSqlBackend.saveRecords(outputVersion, stream);

			final long size = this.mdStoreSqlBackend.countRecords(outputVersion.getId());

			mdstoreManager.commitVersion(outputVersion.getId(), size);

			updateProgressMessage(size + "/" + size);

			this.clientFactory.getClient(DsmClient.class).updateApiAggregationInfo(this.api.getId(), this.outputMdId, size);
		} catch (final Throwable e) {
			mdstoreManager.abortVersion(outputVersion);
			throw e;
		} finally {
			mdstoreManager.endReading(inputVersion);
		}
	}

}
