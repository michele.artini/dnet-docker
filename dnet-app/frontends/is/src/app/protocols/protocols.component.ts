import { Component, Injectable } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Protocol, ProtocolParam } from '../common/is.model';
import { ISClient } from '../common/is.client';

export interface ProtocolDatasource {
	protocol: string;
	datasource: MatTableDataSource<ProtocolParam>;
}

@Component({
	selector: 'app-protocols',
	templateUrl: './protocols.component.html',
	styleUrls: []
})
export class ProtocolsComponent {
	protDatasources: ProtocolDatasource[] = [];
	colums: string[] = ['name', 'label', 'type', 'optional', 'hasSelFunction'];

	constructor(public client: ProtocolsClient) {
		this.client.loadProtocols((data: Protocol[]) =>
			data.forEach(p => {
				this.protDatasources.push({
					protocol: p.id,
					datasource: new MatTableDataSource(p.params)
				});
			})
		);
	}
}


@Injectable({
	providedIn: 'root'
})
export class ProtocolsClient extends ISClient {
	loadProtocols(onSuccess: Function): void {
		this.httpGet<Protocol[]>("/proxy/byType/datasource_manager/api/protocols/", onSuccess);
	}
}
