import { Component, Injectable, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ResourceType, SimpleResource } from '../common/is.model';
import { ISClient } from '../common/is.client';
import { HttpHeaders } from '@angular/common/http';

@Component({
	selector: 'app-xslt-tester',
	templateUrl: './xslt-tester.component.html',
	styleUrls: []
})
export class XsltTesterComponent {
	rules: SimpleResource[] = [];

	xmlOutput: string = ''

	xsltForm = new FormGroup({
		xmlIn: new FormControl('', [Validators.required]),
		rule: new FormControl('', [Validators.required])
	});

	constructor(public client: XsltTesterClient, public route: ActivatedRoute, public dialog: MatDialog) {
		this.client.loadXsltRules((data: SimpleResource[]) => this.rules = data);
	}

	transformXslt() {
		this.xmlOutput = '';
		this.client.transformXslt(this.xsltForm.get("rule")?.value!, this.xsltForm.get("xmlIn")?.value!, (data: string) => this.xmlOutput = data, this.xsltForm);
	}

}

@Injectable({
	providedIn: 'root'
})
export class XsltTesterClient extends ISClient {

	loadXsltRules(onSuccess: Function): void {
		this.httpGet<SimpleResource[]>("/proxy/byType/resource_manager/api/resources/byType/transformation_rule_xslt", onSuccess);
	}

	transformXslt(rule: string, xml: string, onSuccess: Function, relatedForm?: FormGroup): void {
		var headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
		this.httpPostWithOptions('/proxy/byType/wf_manager/api/mapping/xsltTransform?rule=' + encodeURIComponent(rule), xml, { headers, responseType: 'text' as 'json' }, onSuccess, relatedForm, true);
	}

}
