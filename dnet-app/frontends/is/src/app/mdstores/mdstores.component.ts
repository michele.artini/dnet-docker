import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MDStore, MDStoreRecord, MDStoreVersion } from '../common/is.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MdstoresClient } from './mdstores.client';

@Component({
	selector: 'app-mdstores',
	templateUrl: './mdstores.component.html',
	styleUrls: ['./mdstores.component.css']
})
export class MdstoresComponent implements OnInit {

	mdId?: string;
	mdstores: MDStore[] = [];
	searchText: string = '';

	constructor(public client: MdstoresClient, public route: ActivatedRoute, public dialog: MatDialog) { }

	ngOnInit() {
		this.route.params.subscribe(params => this.mdId = params['mdId']);
		this.reload();
	}

	reload() {
		if (this.mdId) {
			this.client.loadMDStore(this.mdId, (data: MDStore) => this.mdstores = [data]);
		} else {
			this.client.loadMDStores((data: MDStore[]) => this.mdstores = data);
		}
	}

	openVersionsDialog(md: MDStore): void {
		const dialogRef = this.dialog.open(MDStoreVersionsDialog, {
			data: md,
			width: '80%'
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) this.reload();
		});
	}

	openAddMdstoreDialog(): void {
		const dialogRef = this.dialog.open(AddMDStoreDialog, {
			data: {},
			width: '80%'
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) this.reload();
		});
	}

	createNewVersion(md: MDStore): void {
		this.client.prepareNewMDStoreVersion(md.id, (data: MDStoreVersion) => {
			md.numberOfVersions = md.numberOfVersions + 1;
			this.openVersionsDialog(md);
		});
	}

	deleteMdstore(md: MDStore) {
		if (confirm('Are you sure?')) {
			this.client.deleteMDStore(md.id, (data: void) => this.reload());
		}
	}
}

@Component({
	selector: 'app-mdstore-inspector',
	templateUrl: './mdstore-inspector.component.html',
	styleUrls: ['./mdstores.component.css']
})
export class MdstoreInspectorComponent implements OnInit {

	mdstore?: MDStore = undefined;
	version?: MDStoreVersion = undefined;
	records: MDStoreRecord[] = [];

	limit: number = 0;

	constructor(public client: MdstoresClient, public route: ActivatedRoute, public dialog: MatDialog) {
	}
	ngOnInit() {
		this.route.params.subscribe(params => {
			const versionId = params['versionId'];
			this.limit = params['limit'];

			this.client.loadMDStoreVersion(versionId, (data: MDStoreVersion) => {
				this.version = data;
				this.client.loadMDStore(this.version.mdstore, (data: MDStore) => {
					this.mdstore = data;
					this.client.loadMDStoreVersionRecords(versionId, this.limit, (data: MDStoreRecord[]) => {
						this.records = data;
					});
				});
			});
		});
	}
}

@Component({
	selector: 'mdstores-versions-dialog',
	templateUrl: './mdstores-versions-dialog.html',
	styleUrls: ['./mdstores.component.css']
})
export class MDStoreVersionsDialog {

	versions: MDStoreVersion[] = [];

	constructor(public dialogRef: MatDialogRef<MDStoreVersionsDialog>, @Inject(MAT_DIALOG_DATA) public data: any, public client: MdstoresClient, public router: Router) {
		this.reload();
	}

	reload() {
		this.client.loadMDStoreVersions(this.data.id, (res: MDStoreVersion[]) => this.versions = res);
	}

	openInspectorPage(version: MDStoreVersion): void {
		const url = this.router.serializeUrl(
			this.router.createUrlTree(['/mdrecords/' + encodeURIComponent(version.id) + '/50'])
		);
		window.open(url, '_blank');
	}

	resetReading(version: MDStoreVersion): void {
		this.client.resetReadingMDStoreVersion(version.id, (data: void) => version.readCount = 0);
	}

	commitVersion(version: MDStoreVersion): void {
		let size: number = parseInt(prompt("New Size", "0") || "0");
		if (size >= 0) {
			this.client.commitMDStoreVersion(version.id, size, (data: void) => {
				this.data.currentVersion = version.id;
				this.reload();
			});
		}
	}

	abortVersion(version: MDStoreVersion): void {
		this.client.abortMDStoreVersion(version.id, (data: void) => this.reload());
	}

	deleteVersion(version: MDStoreVersion): void {
		this.client.deleteMDStoreVersion(version.id, (data: void) => this.reload());
	}

	onNoClick(): void {
		this.dialogRef.close();
	}
}

@Component({
	selector: 'add-mdstore-dialog',
	templateUrl: './add-mdstore-dialog.html',
	styleUrls: ['./mdstores.component.css']
})
export class AddMDStoreDialog {

	newMdstoreForm = new FormGroup({
		format: new FormControl('', [Validators.required]),
		layout: new FormControl('', [Validators.required]),
		interpretation: new FormControl('', [Validators.required]),
		type: new FormControl('', [Validators.required]),
		dsName: new FormControl(''),
		dsId: new FormControl(''),
		apiId: new FormControl(''),
	});

	constructor(public dialogRef: MatDialogRef<MDStoreVersionsDialog>, @Inject(MAT_DIALOG_DATA) public data: any, public client: MdstoresClient) {

	}

	onSubmit(): void {
		let format: string = this.newMdstoreForm.get('format')?.value!;
		let layout: string = this.newMdstoreForm.get('layout')?.value!;
		let interpretation: string = this.newMdstoreForm.get('interpretation')?.value!;
		let type: string = this.newMdstoreForm.get('type')?.value!;
		let dsName: string = this.newMdstoreForm.get('dsName')?.value!;
		let dsId: string = this.newMdstoreForm.get('dsId')?.value!;
		let apiId: string = this.newMdstoreForm.get('apiId')?.value!;

		this.client.addMDStore(format, layout, interpretation, type, dsName, dsId, apiId, (data: void) => this.dialogRef.close(1), this.newMdstoreForm);
	}

	onNoClick(): void {
		this.dialogRef.close();
	}
}
