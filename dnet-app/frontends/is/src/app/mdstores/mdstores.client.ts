import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MDStore, MDStoreVersion, MDStoreRecord } from '../common/is.model';
import { FormGroup } from '@angular/forms';
import { ISClient } from '../common/is.client';

@Injectable({
	providedIn: 'root'
})
export class MdstoresClient extends ISClient {

	loadMDStores(onSuccess: Function): void {
		this.httpGet<MDStore[]>("/proxy/byType/mdstore_manager/api/mdstores/", onSuccess);
	}

	loadMDStore(mdId: string, onSuccess: Function): void {
		this.httpGet<MDStore>('/proxy/byType/mdstore_manager/api/mdstores/mdstore/' + encodeURIComponent(mdId), onSuccess);
	}

	addMDStore(format: string, layout: string, interpretation: string, type: string, dsName: string, dsId: string, apiId: string, onSuccess: Function, relatedForm?: FormGroup) {
		const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')

		let body = new HttpParams()
			.set('dsName', dsName)
			.set('dsId', dsId)
			.set('apiId', apiId);

		this.httpPostWithOptions('/proxy/byType/mdstore_manager/api/mdstores/new/'
			+ encodeURIComponent(format)
			+ '/'
			+ encodeURIComponent(layout)
			+ '/'
			+ encodeURIComponent(interpretation)
			+ '/'
			+ encodeURIComponent(type),
			body, { headers: headers }, onSuccess, relatedForm);
	}

	deleteMDStore(mdId: string, onSuccess: Function): void {
		this.httpDelete('/proxy/byType/mdstore_manager/api/mdstores/mdstore/' + encodeURIComponent(mdId), onSuccess);
	}

	prepareNewMDStoreVersion(mdId: string, onSuccess: Function): void {
		this.httpGet<MDStoreVersion>('/proxy/byType/mdstore_manager/api/mdstores/mdstore/' + encodeURIComponent(mdId) + '/newVersion', onSuccess);
	}

	commitMDStoreVersion(versionId: string, size: number, onSuccess: Function) {
		this.httpGet<any>('/proxy/byType/mdstore_manager/api/mdstores/version/' + encodeURIComponent(versionId) + '/commit/' + size, onSuccess);
	}

	abortMDStoreVersion(versionId: string, onSuccess: Function) {
		this.httpGet<any>('/proxy/byType/mdstore_manager/api/mdstores/version/' + encodeURIComponent(versionId) + '/abort', onSuccess);
	}

	deleteMDStoreVersion(versionId: string, onSuccess: Function) {
		this.httpDelete('/proxy/byType/mdstore_manager/api/mdstores/version/' + encodeURIComponent(versionId), onSuccess);
	}

	resetReadingMDStoreVersion(versionId: string, onSuccess: Function) {
		this.httpGet<any>('/proxy/byType/mdstore_manager/api/mdstores/version/' + encodeURIComponent(versionId) + '/resetReading', onSuccess);
	}

	loadMDStoreVersions(mdId: string, onSuccess: Function): void {
		this.httpGet<MDStoreVersion[]>('/proxy/byType/mdstore_manager/api/mdstores/mdstore/' + encodeURIComponent(mdId) + '/versions', onSuccess);
	}

	loadMDStoreVersion(versionId: string, onSuccess: Function): void {
		this.httpGet<MDStoreVersion>('/proxy/byType/mdstore_manager/api/mdstores/version/' + encodeURIComponent(versionId), onSuccess);
	}

	loadMDStoreVersionRecords(versionId: string, limit: number, onSuccess: Function): void {
		this.httpGet<MDStoreRecord[]>('/proxy/byType/mdstore_manager/api/mdstores/version/' + encodeURIComponent(versionId) + '/content/' + limit, onSuccess);
	}
}
