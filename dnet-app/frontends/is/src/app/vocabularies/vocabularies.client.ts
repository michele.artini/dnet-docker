import { Injectable, OnInit } from '@angular/core';
import { Vocabulary, VocabularyTerm } from '../common/is.model';
import { FormGroup } from '@angular/forms';
import { ISClient } from '../common/is.client';

@Injectable({
	providedIn: 'root'
})
export class VocabulariesClient extends ISClient {

	baseUrl = '/proxy/byType/vocabulary_manager/api';

	loadVocabularies(onSuccess: Function): void {
		this.httpGet<Vocabulary[]>(this.baseUrl + '/vocs/', onSuccess);
	}

	loadVocabulary(vocId: string, onSuccess: Function): void {
		this.httpGet<Vocabulary>(this.baseUrl + '/vocs/' + encodeURIComponent(vocId), onSuccess);
	}

	loadVocabularyTerms(vocId: string, onSuccess: Function): void {
		this.httpGet<VocabularyTerm[]>(this.baseUrl + '/vocs/' + encodeURIComponent(vocId) + '/terms', onSuccess);
	}

	saveVocabulary(voc: Vocabulary, onSuccess: Function, relatedForm?: FormGroup): void {
		this.httpPost(this.baseUrl + '/vocs/', voc, onSuccess, relatedForm);
	}

	saveVocabularyTerm(vocId: string, term: VocabularyTerm, onSuccess: Function, relatedForm?: FormGroup): void {
		this.httpPost(this.baseUrl + '/vocs/' + encodeURIComponent(vocId) + '/terms', term, onSuccess, relatedForm);
	}

	deleteVocabulary(vocId: string, onSuccess: Function): void {
		this.httpDelete(this.baseUrl + '/vocs/' + encodeURIComponent(vocId), onSuccess);
	}

	deleteVocabularyTerm(vocId: string, termCode: string, onSuccess: Function): void {
		this.httpDelete(this.baseUrl + '/vocs/'
			+ encodeURIComponent(vocId)
			+ '/terms/'
			+ encodeURIComponent(termCode)
			, onSuccess);
	}

}
