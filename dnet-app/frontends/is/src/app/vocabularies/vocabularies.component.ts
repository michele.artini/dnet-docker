import { Component, Inject, AfterViewInit, OnInit, ViewChild } from '@angular/core';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Vocabulary, VocabularyTerm, VocabularyTermSynonym } from '../common/is.model';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { VocabulariesClient } from './vocabularies.client';

@Component({
	selector: 'app-vocabularies',
	templateUrl: './vocabularies.component.html',
	styleUrls: []
})
export class VocabulariesComponent implements OnInit, AfterViewInit {
	vocsDatasource: MatTableDataSource<Vocabulary> = new MatTableDataSource<Vocabulary>([]);

	colums: string[] = ['id', 'name', 'description', 'buttons'];

	@ViewChild(MatSort) sort: MatSort | undefined

	constructor(public client: VocabulariesClient, public route: ActivatedRoute, public dialog: MatDialog) {
	}

	ngOnInit() {
		this.reload();
	}

	ngAfterViewInit() {
		if (this.sort) this.vocsDatasource.sort = this.sort;
	}

	reload() {
		this.client.loadVocabularies((data: Vocabulary[]) => this.vocsDatasource.data = data);
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value.trim().toLowerCase();
		this.vocsDatasource.filter = filterValue;
	}

	newVocabularyDialog(): void {
		const dialogRef = this.dialog.open(VocDialog, {
			data: { id: '', name: '', description: '' },
			width: '80%'
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) this.reload();
		});
	}

	editVocabularyDialog(vocabulary: Vocabulary): void {
		const dialogRef = this.dialog.open(VocDialog, {
			data: vocabulary,
			width: '80%'
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) this.reload();
		});
	}

	deleteVocabulary(vocabulary: Vocabulary): void {
		if (confirm("Are you sure?")) {
			this.client.deleteVocabulary(vocabulary.id, (data: void) => this.reload());
		}
	}

}

@Component({
	selector: 'app-vocabulary-editor',
	templateUrl: './vocabulary-editor.component.html',
	styleUrls: []
})
export class VocabularyEditorComponent implements OnInit, AfterViewInit {

	voc?: Vocabulary

	termsDatasource: MatTableDataSource<VocabularyTerm> = new MatTableDataSource<VocabularyTerm>([]);
	colums: string[] = ['code', 'name', 'encoding', 'synonyms', 'buttons'];

	@ViewChild(MatSort) sort: MatSort | undefined

	constructor(public client: VocabulariesClient, public route: ActivatedRoute, public dialog: MatDialog) {
	}

	ngOnInit() {
		this.reload();
	}

	ngAfterViewInit() {
		if (this.sort) this.termsDatasource.sort = this.sort;
	}

	reload() {
		this.route.queryParams.subscribe((params) => {
			this.client.loadVocabulary(params['id'], (data: Vocabulary) => this.voc = data);
			this.client.loadVocabularyTerms(params['id'], (data: VocabularyTerm[]) => this.termsDatasource.data = data);
		});
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value.trim().toLowerCase();
		this.termsDatasource.filter = filterValue;
	}

	newVocabularyTermDialog(): void {
		if (this.voc?.id) {
			const dialogRef = this.dialog.open(VocTermDialog, {
				data: { vocabulary: this.voc.id, code: '', name: '', encoding: 'OPENAIRE', synonyms: [] },
				width: '80%'
			});

			dialogRef.afterClosed().subscribe(result => {
				if (result) this.reload();
			});
		}

	}

	editVocabularyTermDialog(term: VocabularyTerm): void {
		const dialogRef = this.dialog.open(VocTermDialog, {
			data: term,
			width: '80%'
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) this.reload();
		});
	}

	deleteVocabularyTerm(term: VocabularyTerm): void {
		if (confirm("Are you sure?") && this.voc?.id && term.code) {
			this.client.deleteVocabularyTerm(this.voc.id, term.code, (data: void) => this.reload());
		}
	}

}

@Component({
	selector: 'voc-dialog',
	templateUrl: 'voc-dialog.html',
	styleUrls: []
})
export class VocDialog {
	vocForm = new FormGroup({});

	constructor(public dialogRef: MatDialogRef<VocDialog>, @Inject(MAT_DIALOG_DATA) public data: any, public client: VocabulariesClient) {
		if (data.id && data.id.length > 0) {
			this.vocForm.addControl('id', new FormControl({ value: data.id, disabled: true }));
		} else {
			this.vocForm.addControl('id', new FormControl("", [Validators.required]));
		}
		this.vocForm.addControl('name', new FormControl(data.name, [Validators.required]));
		this.vocForm.addControl('description', new FormControl(data.description));
	}

	onSubmit(): void {
		const voc = Object.assign({}, this.data, this.vocForm.value);
		this.client.saveVocabulary(voc, (data: void) => this.dialogRef.close(1), this.vocForm);
	}

	onNoClick(): void {
		this.dialogRef.close();
	}
}

@Component({
	selector: 'voc-term-dialog',
	templateUrl: 'voc-term-dialog.html',
	styleUrls: []
})
export class VocTermDialog {

	termForm = new FormGroup({
		code: new FormControl(''),
		name: new FormControl(''),
		encoding: new FormControl('OPENAIRE'),
		tmpSynonymTerm: new FormControl(''),
		tmpSynonymEncoding: new FormControl('OPENAIRE')
	});

	synonymsDatasource: MatTableDataSource<VocabularyTermSynonym> = new MatTableDataSource<VocabularyTermSynonym>([]);
	synonymColums: string[] = ['term', 'encoding', 'buttons'];
	@ViewChild(MatTable) synonymsTable: MatTable<VocabularyTermSynonym> | undefined;

	constructor(public dialogRef: MatDialogRef<VocDialog>, @Inject(MAT_DIALOG_DATA) public data: any, public client: VocabulariesClient) {
		this.termForm.get('code')?.setValue(data.code);
		this.termForm.get('name')?.setValue(data.name);
		this.termForm.get('encoding')?.setValue(data.encoding);
		this.synonymsDatasource.data = data.synonyms;
	}

	onSubmit(): void {
		console.log('SUBMIT');

		let oldCode = this.data.code;
		let voc = this.data.vocabulary;

		let term: VocabularyTerm = {
			code: this.termForm.get('code')?.value!,
			name: this.termForm.get('name')?.value!,
			encoding: this.termForm.get('encoding')?.value!,
			synonyms: this.synonymsDatasource.data,
			vocabulary: voc
		}

		this.client.saveVocabularyTerm(voc, term, (data: void) => {
			if (oldCode && oldCode != term.code) {
				this.client.deleteVocabularyTerm(voc, oldCode, (data: void) => this.dialogRef.close(1))
			} else { this.dialogRef.close(1) }
		}, this.termForm);
	}

	removeSynonym(pos: number) {
		this.synonymsDatasource.data.splice(pos, 1);
		this.synonymsTable?.renderRows();
	}

	isNewSynonymValid(): boolean {
		if (!this.termForm.get('tmpSynonymTerm')) return false;
		if (!this.termForm.get('tmpSynonymEncoding')) return false;
		if (this.termForm.get('tmpSynonymTerm')?.value?.trim().length == 0) return false;
		if (this.termForm.get('tmpSynonymEncoding')?.value?.trim().length == 0) return false;
		return true;
	}

	addSynonym() {
		this.synonymsDatasource.data.push({
			term: this.termForm.get('tmpSynonymTerm')?.value!,
			encoding: this.termForm.get('tmpSynonymEncoding')?.value!
		});
		this.termForm.get('tmpSynonymTerm')?.setValue('');
		this.termForm.get('tmpSynonymEncoding')?.setValue('OPENAIRE');
		this.synonymsTable?.renderRows();
	}

	onNoClick(): void {
		this.dialogRef.close();
	}
}
