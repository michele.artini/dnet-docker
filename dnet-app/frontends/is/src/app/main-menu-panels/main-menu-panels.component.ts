import { Component, Injectable, ViewChild } from '@angular/core';
import { KeyValue, ResourceType, WfSection } from '../common/is.model';
import { MatAccordion } from '@angular/material/expansion';
import { ISClient } from '../common/is.client';

@Component({
	selector: 'app-main-menu-panels',
	templateUrl: './main-menu-panels.component.html',
	styleUrls: ['./main-menu-panels.component.css']
})
export class MainMenuPanelsComponent {

	@ViewChild(MatAccordion)
	accordion!: MatAccordion;

	resTypes: ResourceType[] = [];
	wfSections: WfSection[] = [];

	runningServiceTypes = new Set<string>();

	constructor(public client: MainMenuPanelsClient) {
		client.availableServiceTypes((data: string[]) => {
			data.forEach((type) => this.runningServiceTypes.add(type));

			if (this.isEnabled('resource_manager')) {
				client.loadResourceTypes((data: ResourceType[]) => this.resTypes = data);
			}
			if (this.isEnabled('wf_manager')) {
				client.loadWfSections((data: WfSection[]) => this.wfSections = data);
			}
		});
	}

	isEnabled(serviceType: string) {
		return this.runningServiceTypes.has(serviceType);
	}
}

@Injectable({
	providedIn: 'root'
})
export class MainMenuPanelsClient extends ISClient {

	loadResourceTypes(onSuccess: Function) {
		this.httpGet<ResourceType[]>("/proxy/byType/resource_manager/api/resourceTypes", onSuccess)
	}

	loadWfSections(onSuccess: Function) {
		this.httpGet<WfSection[]>("/proxy/byType/wf_manager/api/sections", onSuccess)
	}

	availableServiceTypes(onSuccess: Function) {
		this.httpGet<string[]>("/ajax-is/service-types", onSuccess)
	}
}
