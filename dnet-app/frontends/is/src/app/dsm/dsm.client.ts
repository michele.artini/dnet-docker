import { Injectable } from '@angular/core';
import { Page, DsmConf, KeyValue, BrowseTerm, SimpleDatasource, WfRepoHi, WfConf, WfHistoryEntry, WfTemplateDesc } from '../common/is.model';
import { ISClient } from '../common/is.client';

@Injectable({
	providedIn: 'root'
})
export class DsmClient extends ISClient {

	baseUrl = '/proxy/byType/datasource_manager/api';

	dsmConf(onSuccess: Function) {
		this.httpGet<DsmConf>(this.baseUrl + '/conf', onSuccess);
	}

	dsmBrowsableFields(onSuccess: Function) {
		this.httpGet<KeyValue[]>(this.baseUrl + '/browse', onSuccess);
	}

	dsmBrowse(field: string, onSuccess: Function) {
		this.httpGet<BrowseTerm[]>(this.baseUrl + '/browse/' + encodeURIComponent(field), onSuccess);
	}

	dsmSearchByField(field: string, value: string, page: number, pageSize: number, onSuccess: Function) {
		this.httpGet<Page<SimpleDatasource>>(this.baseUrl + '/search/' + page + '/' + pageSize + '?field=' + encodeURIComponent(field) + '&value=' + encodeURIComponent(value), onSuccess);
	}

	dsmSearch(value: string, page: number, pageSize: number, onSuccess: Function) {
		this.httpGet<Page<SimpleDatasource>>(this.baseUrl + '/search/' + page + '/' + pageSize + '?value=' + encodeURIComponent(value), onSuccess);
	}

	dsmGetDs(id: string, onSuccess: Function) {
		this.httpGet<any>(this.baseUrl + '/ds/' + encodeURIComponent(id), onSuccess);
	}

	dsmGetApi(id: string, onSuccess: Function) {
		this.httpGet<any>(this.baseUrl + '/ds-api/' + encodeURIComponent(id), onSuccess);
	}

	dsmLoadWfs(apiId: string, onSuccess: Function) {
		this.httpGet<WfConf[]>('/proxy/byType/wf_manager/api/confs/byApi/' + encodeURIComponent(apiId), onSuccess);
	}

	dsmListRepoHiWfs(onSuccess: Function) {
		this.httpGet<WfRepoHi>('/proxy/byType/wf_manager/api/repo-his', onSuccess);
	}

	dsmGetWfTemplate(id: string, onSuccess: Function) {
		this.httpGet<WfTemplateDesc>('/proxy/byType/wf_manager/api/template/' + encodeURIComponent(id), onSuccess);
	}

	dsmRepoHiWf(wfId: string, dsId: string, dsName: string, apiId: string, onSuccess: Function) {
		let params = { 'dsId': dsId, 'dsName': dsName, 'apiId': apiId };
		this.httpPost('/proxy/byType/wf_manager/api/repo-hi/' + encodeURIComponent(wfId) + '/start', params, onSuccess);
	}

	dsmStartWfConf(wfConfId: string, onSuccess: Function) {
		this.httpGet<WfRepoHi>('/proxy/byType/wf_manager/api/conf/' + encodeURIComponent(wfConfId) + '/start', onSuccess);
	}

	dsmDestroyWfConf(wfConfId: string, onSuccess: Function) {
		this.httpGet<WfRepoHi>('/proxy/byType/wf_manager/api/conf/' + encodeURIComponent(wfConfId) + '/destroy', onSuccess);
	}

	dsmRecentWfsByDsId(dsId: string, onSuccess: Function) {
		this.httpGet<WfHistoryEntry[]>('/proxy/byType/wf_manager/api/history/byDsId/' + encodeURIComponent(dsId), onSuccess);
	}

	dsmRecentWfsByApiId(apiId: string, onSuccess: Function) {
		this.httpGet<WfHistoryEntry[]>('/proxy/byType/wf_manager/api/history/byApiId/' + encodeURIComponent(apiId), onSuccess);
	}

}
