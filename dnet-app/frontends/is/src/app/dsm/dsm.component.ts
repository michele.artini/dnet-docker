import { Component, Inject, Injectable, OnInit, ViewChild } from '@angular/core';
import { Page, BrowseTerm, SimpleDatasource, KeyValue, DsmConf, ProtocolParam, Api, ApiInsert, WfRepoHi, WfTemplate, WfConf, WfHistoryEntry, WfTemplateDesc } from '../common/is.model';
import { ActivatedRoute, Params } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { combineLatest } from 'rxjs';
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material/paginator';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { MatSelectChange } from '@angular/material/select';
import { DsmClient } from './dsm.client';
import { WfConfDialog, WfConfLauncherDialog, WfTemplateDialog } from '../wf-confs/wf-common.component';
import { WfHistoryDialog } from '../wf-history/wf-history.component';

@Component({
	selector: 'app-dsm-search',
	templateUrl: './dsm-search.component.html',
	styleUrls: []
})
export class DsmSearchComponent implements OnInit {
	searchText: string = '';

	constructor(public client: DsmClient, public dsmConfService: DsmConfService, public route: ActivatedRoute, public router: Router, public dialog: MatDialog) {
	}

	ngOnInit() {
		if (!this.dsmConfService.isReady()) {
			this.client.dsmConf((data: DsmConf) => this.dsmConfService.updateConf(data));
		}
	}

	search() {
		this.router.navigate(['/dsm/results/0/50'], {
			queryParams: { value: this.searchText }
		});
	}

	browseField(field: string, label: string) {
		const dialogRef = this.dialog.open(DsmBrowseDialog, {
			data: { field: field, label: label },
			width: '80%'
		});
	}

}

@Component({
	selector: 'app-dsm-results',
	templateUrl: './dsm-results.component.html',
	styleUrls: []
})
export class DsmResultsComponent implements OnInit {
	filterText: string = '';
	results: SimpleDatasource[] = [];
	nResults: number = 0;
	currPage: number = 0;
	nPages: number = 0;
	pageSize: number = 0;
	field?: string;
	value: string = '';

	constructor(public client: DsmClient, public route: ActivatedRoute, public router: Router, public dialog: MatDialog) {
	}

	ngOnInit(): void {

		combineLatest([this.route.params, this.route.queryParams],
			(params: Params, queryParams: Params) => ({ params, queryParams })
		).subscribe((res: { params: Params; queryParams: Params }) => {
			const { params, queryParams } = res;
			this.currPage = parseInt(params['page']);
			this.pageSize = parseInt(params['size']);
			this.field = queryParams['field'];
			this.value = queryParams['value'];
			this.reload();
		});
	}

	reload() {
		if (this.field) {
			this.client.dsmSearchByField(this.field, this.value, this.currPage, this.pageSize, (page: Page<SimpleDatasource>) => {
				this.results = page.content;
				this.nResults = page.totalElements;
				this.nPages = page.totalPages;
			});
		} else {
			this.client.dsmSearch(this.value, this.currPage, this.pageSize, (page: Page<SimpleDatasource>) => {
				this.results = page.content;
				this.nResults = page.totalElements;
				this.nPages = page.totalPages;
			});
		}
	}

	changePage(event: PageEvent) {
		let path = '/dsm/results/' + event.pageIndex + '/' + event.pageSize;
		let qp = this.field ?
			{ field: this.field, value: this.value } :
			{ value: this.value };

		this.router.navigate([path], {
			queryParams: qp
		});
	}

	openAddApiDialog(dsId: string, dsName: string) {
		const dialogRef = this.dialog.open(DsmEditApiDialog, {
			data: { dsId: dsId, dsName: dsName },
			width: '80%'
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) this.reload();
		});
	}

}

@Component({
	selector: 'app-dsm-api',
	templateUrl: './dsm-api.component.html',
	styleUrls: []
})
export class DsmApiComponent implements OnInit {

	apiId: string = '';
	api: any = {};
	ds: any = {};
	wfs: WfConf[] = [];

	constructor(public client: DsmClient, public route: ActivatedRoute, public router: Router, public dialog: MatDialog) {
	}

	ngOnInit(): void {
		combineLatest([this.route.params, this.route.queryParams],
			(params: Params, queryParams: Params) => ({ params, queryParams })
		).subscribe((res: { params: Params; queryParams: Params }) => {
			const { params, queryParams } = res;
			this.apiId = queryParams['id'];
			this.reload();
		});
	}

	reload() {
		if (this.apiId) {
			this.client.dsmGetApi(this.apiId, (api: any) => {
				this.api = api;
				this.client.dsmGetDs(api.datasource, (ds: any) => {
					this.ds = ds;
				});
				this.reloadWfs()
			});
		}
	}

	reloadWfs() {
		if (this.apiId) {
			this.client.dsmLoadWfs(this.apiId, (wfs: any[]) => {
				this.wfs = wfs;
			});
		}
	}

	openLaunchWfConfDialog(conf: WfConf): void {
		const wfDialogRef = this.dialog.open(WfConfLauncherDialog, {
			data: conf
		});

		wfDialogRef.componentInstance.newHistory.subscribe((data: WfHistoryEntry[]) => {
			const wHistoryfDialogRef = this.dialog.open(WfHistoryDialog, {
				data: {
					'confId': conf.id,
					'dsId': conf.dsId,
					'apiId': conf.apiId,
					'processId': (data.length > 0) ? data[0].processId : undefined
				}
			});
		});
	}

	destroyWfConf(wfConfId: string): void {
		if (confirm("Are you sure ?")) {
			this.client.dsmDestroyWfConf(wfConfId, (data: WfHistoryEntry) => {
				const wfDialogRef = this.dialog.open(WfHistoryDialog, {
					data: {
						'dsId': this.ds.id,
						'apiId': this.api.id,
						'processId': data.processId,
						'confId': undefined
					}
				});
			});
		}
	}

	openJournalDialog(confId: string | undefined, processId: string | undefined) {
		const wfDialogRef = this.dialog.open(WfHistoryDialog, {
			data: {
				'dsId': this.ds.id,
				'apiId': this.api.id,
				'confId': confId,
				'processId': processId
			}
		});
	}

	deleteApi() {
		alert('TODO DELETE API');
	}

	newAddWorkflowDialog() {
		this.client.dsmListRepoHiWfs((data: WfRepoHi[]) => {
			const dialogRef = this.dialog.open(DsmAddWorkflowDialog, {
				data: {
					'ds': this.ds,
					'api': this.api,
					'repohis': data
				},
				width: '60%'
			});

			dialogRef.afterClosed().subscribe(result => this.reloadWfs());
		});
	}

	newEditApiDialog() {
		const dialogRef = this.dialog.open(DsmEditApiDialog, {
			data: { dsId: this.ds.id, dsName: this.ds.officialname, api: this.api },
			width: '80%'
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) this.reload();
		});
	}

	newEditWfConfDialog(conf: WfConf) {
		const dialogRef = this.dialog.open(WfConfDialog, {
			data: conf,
			width: '80%'
		});
		dialogRef.afterClosed().subscribe(result => this.reloadWfs());
	}

	showWfTemplateModal(wfId: string): void {
		this.client.dsmGetWfTemplate(wfId, (data: WfTemplateDesc) => this.dialog.open(WfTemplateDialog, { data: data, width: '80%' }));
	}

}

@Component({
	selector: 'dsm-browse-dialog',
	templateUrl: 'dsm-browse-dialog.html',
	styleUrls: []
})
export class DsmBrowseDialog implements OnInit {

	datasource: MatTableDataSource<BrowseTerm> = new MatTableDataSource<BrowseTerm>([]);
	colums: string[] = ['name', 'total'];

	@ViewChild(MatSort) sort: MatSort | undefined

	constructor(public dialogRef: MatDialogRef<DsmBrowseDialog>, @Inject(MAT_DIALOG_DATA) public data: any, public client: DsmClient) {
	}

	ngOnInit(): void {
		this.client.dsmBrowse(this.data.field, (res: BrowseTerm[]) => this.datasource.data = res);
	}

	ngAfterViewInit() {
		if (this.sort) this.datasource.sort = this.sort;
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value.trim().toLowerCase();
		this.datasource.filter = filterValue;
	}

	onNoClick(): void {
		this.dialogRef.close();
	}
}


@Component({
	selector: 'dsm-edit-api-dialog',
	templateUrl: './dsm-edit-api.dialog.html',
	styleUrls: []
})
export class DsmEditApiDialog implements OnInit {
	selectedProtocol: string = '';
	apiPrefix: string = '';
	paramPrefix: string = '__PARAM_';
	selProtParams: ProtocolParam[] = [];
	insertMode: boolean = true;

	editApiForm: FormGroup = new FormGroup({});

	constructor(public dsmConfService: DsmConfService, public dialogRef: MatDialogRef<DsmEditApiDialog>, @Inject(MAT_DIALOG_DATA) public data: any, public client: DsmClient) {
		this.apiPrefix = 'api_________::' + data.dsId + '::';

		if (data.api && data.api.id) {
			this.insertMode = false;
		}

		this.editApiForm.addControl('apiIdSuffix', new FormControl('', [Validators.required]));

		let baseUrlPattern = Validators.pattern('^(http|https|ftp|file|sftp|jar|mongodb):\/\/.*');

		if (this.insertMode) {
			this.editApiForm.addControl('compatibility', new FormControl('', [Validators.required]));
			this.editApiForm.addControl('contentdescription', new FormControl('', [Validators.required]));
			this.editApiForm.addControl('protocol', new FormControl('', [Validators.required]));
			this.editApiForm.addControl('baseurl', new FormControl('', [Validators.required, baseUrlPattern]));
			this.editApiForm.addControl('metadataIdentifierPath', new FormControl('', [Validators.required]));
		} else {
			this.editApiForm.addControl('compatibility', new FormControl({ value: this.data.api.compatibility, disabled: true }, [Validators.required]));
			this.editApiForm.addControl('contentdescription', new FormControl({ value: this.data.api.contentdescription, disabled: true }, [Validators.required]));
			this.editApiForm.addControl('protocol', new FormControl({ value: this.data.api.protocol, disabled: true }, [Validators.required]));
			this.editApiForm.addControl('baseurl', new FormControl(this.data.api.baseurl, [Validators.required, baseUrlPattern]));
			this.editApiForm.addControl('metadataIdentifierPath', new FormControl(this.data.api.metadataIdentifierPath, [Validators.required]));
			this.configureSelectedProtocol(this.data.api.protocol);
		}

	}

	ngOnInit() {
		if (!this.dsmConfService.isReady()) {
			this.client.dsmConf((conf: DsmConf) => this.dsmConfService.updateConf(conf));
		}
	}

	onChangeProtocol(e: MatSelectChange): void {
		this.configureSelectedProtocol(e.value);
	}

	configureSelectedProtocol(protocol: string) {
		this.selectedProtocol = protocol;

		Object.keys(this.editApiForm.controls).forEach(k => {
			if (k.startsWith(this.paramPrefix)) {
				this.editApiForm.removeControl(k);
			}
		});

		if (this.dsmConfService.getProtocolsMap()[this.selectedProtocol]) {
			this.selProtParams = this.dsmConfService.getProtocolsMap()[this.selectedProtocol];
			this.selProtParams.forEach(p => {
				let validations: ValidatorFn[] = [];
				if (!p.optional) { validations.push(Validators.required); }
				if (p.type == 'INT') { validations.push(Validators.pattern('^[0-9]*$')); }
				this.editApiForm.addControl(this.paramPrefix + p.name, new FormControl('', validations));
			});
		} else {
			this.selProtParams = [];
		}
	}

	onSubmit(): void {
		if (this.insertMode) {
			let api: ApiInsert = {
				id: this.apiPrefix + this.editApiForm.get('apiIdSuffix')?.value,
				protocol: this.selectedProtocol,
				datasource: this.data.dsId,
				contentdescription: this.editApiForm.get('contentdescription')?.value,
				removable: true,
				compatibility: this.editApiForm.get('compatibility')?.value,
				metadataIdentifierPath: this.editApiForm.get('metadataIdentifierPath')?.value,
				baseurl: this.editApiForm.get('baseurl')?.value,
				apiParams: []
			};

			Object.keys(this.editApiForm.controls).forEach(k => {
				if (k.startsWith(this.paramPrefix)) {
					let val = this.editApiForm.get(k)?.value;
					if (val) {
						api.apiParams.push({
							param: k.substring(this.paramPrefix.length),
							value: (Array.isArray(val)) ? val.join() : val
						});
					}
				}
			});
			console.log(api);
			alert("TODO INSERT");
			//this.service.dsmAddApi(api, (data: void) => this.dialogRef.close(1), this.metadataForm);
		} else {
			alert("TODO UPDATE");
		}
	}

	onNoClick(): void {
		this.dialogRef.close();
	}
}

@Injectable({ providedIn: 'root' })
export class DsmConfService {

	private ready: boolean = false;
	private compatibilityLevels: string[] = [];
	private contentDescTypes: string[] = [];
	private browsableFields: KeyValue[] = [];
	private protocols: string[] = [];
	private protocolsMap: any = {};

	constructor() { }

	updateConf(conf: DsmConf) {
		this.compatibilityLevels = conf.compatibilityLevels;
		this.contentDescTypes = conf.contentDescTypes;
		this.browsableFields = conf.browsableFields;
		this.protocols = [];
		this.protocolsMap = {};
		conf.protocols.forEach((p) => {
			this.protocols.push(p.id);
			this.protocolsMap[p.id] = p.params;
		});
		this.ready = true;
	}

	isReady() {
		return this.ready;
	}

	getCompatibilityLevels() {
		return this.compatibilityLevels;
	}

	getContentDescTypes() {
		return this.contentDescTypes;
	}

	getBrowsableFields() {
		return this.browsableFields;
	}

	getProtocols() {
		return this.protocols;
	}

	getProtocolsMap() {
		return this.protocolsMap;
	}

}

@Component({
	selector: 'dsm-add-wf-dialog',
	templateUrl: 'add-wf-dialog.html',
	styleUrls: []
})
export class DsmAddWorkflowDialog {

	api: any = {};
	ds: any = {};
	repohis: WfRepoHi[] = [];

	constructor(public dialogRef: MatDialogRef<DsmAddWorkflowDialog>, @Inject(MAT_DIALOG_DATA) public data: any, public client: DsmClient, public dialog: MatDialog) {
		this.api = data.api;
		this.ds = data.ds;
		this.repohis = data.repohis;
	}

	startRepoHiWf(wfId: string): void {
		this.client.dsmRepoHiWf(wfId, this.ds.id, this.ds.officialname, this.api.id, (data: WfHistoryEntry) => {
			const wfDialogRef = this.dialog.open(WfHistoryDialog, {
				data: {
					'dsId': this.ds.id,
					'apiId': this.api.id,
					'processId': data.processId,
					'confId': undefined
				}
			});
		});
	}

	showWfTemplateModal(wf: WfRepoHi): void {
		const dialogRef = this.dialog.open(WfTemplateDialog, { data: wf, width: '80%' });
	}

	onNoClick(): void {
		this.dialogRef.close();
	}
}
