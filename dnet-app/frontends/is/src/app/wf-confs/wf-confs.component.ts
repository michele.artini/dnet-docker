import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { KeyValue, WfConf, WfHistoryEntry, WfParam, WfSection } from '../common/is.model';
import { WfHistoryDialog } from '../wf-history/wf-history.component';
import { WfConfsClient } from './wf-confs.client';
import { WfConfDialog, WfConfLauncherDialog } from './wf-common.component';
import { MatTableDataSource } from '@angular/material/table';

@Component({
	selector: 'app-wf-confs',
	templateUrl: './wf-confs.component.html',
	styleUrls: []
})
export class WfConfsComponent implements OnInit {
	section?: WfSection;
	confs: KeyValue[] = [];
	wfsDatasource: MatTableDataSource<KeyValue> = new MatTableDataSource<KeyValue>([]);

	colums: string[] = ['k', 'v'];

	constructor(public client: WfConfsClient, public route: ActivatedRoute, public router: Router, public dialog: MatDialog, public snackBar: MatSnackBar) {
	}

	ngOnInit() {
		this.route.params.subscribe(params => {
			let sectionId = params['section'];

			this.client.loadWfSections((data: WfSection[]) => this.section = data.find(s => s.id == sectionId));
			this.client.loadWfConfigurations(sectionId, (data: KeyValue[]) => {
				this.wfsDatasource.data = data;
			});
		});
	}

	openAddWfConfDialog(): void {
		const dialogRef = this.dialog.open(WfConfDialog, {
			data: {
				id: '',
				name: '',
				section: this.section?.id,
				enabled: true,
				priority: 75,
				workflow: '',
				schedulingEnabled: false,
				cronExpression: '0 30 12 1/1 * ?',
				cronMinInterval: 9600,
				details: new Map,
				configured: true,
				systemParams: new Map,
				userParams: new Map
			},
			width: '80%'
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result && this.section?.id) {
				this.client.loadWfConfigurations(this.section?.id, (data: KeyValue[]) => {
					this.wfsDatasource.data = data;
				});
			}
		});
	}
}

@Component({
	selector: 'wf-conf-single',
	templateUrl: 'wf-conf-single.html',
	styleUrls: []
})
export class WfSingleConfComponent implements OnInit, OnChanges {

	@Input() conf?: WfConf;
	prevConfId = '';

	historyEntries: WfHistoryEntry[] = [];

	constructor(public client: WfConfsClient, public route: ActivatedRoute, public router: Router, public dialog: MatDialog, public snackBar: MatSnackBar) {
	}

	ngOnInit(): void {
		this.route.params.subscribe(params => {
			let confId = params['conf'];
			this.client.loadWfConfiguration(confId, (conf: WfConf) => {
				this.conf = conf;
			});
			this.refresh();
		});
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (this.conf && this.conf.id != this.prevConfId) {
			this.prevConfId = this.conf.id;
			this.refresh();
		}
	}

	refresh() {
		if (this.conf) {
			this.client.loadWfHistoryForConf(this.conf?.id, (data: WfHistoryEntry[]) => this.historyEntries = data);
		}
	}

	launchWfConf() {
		const wfDialogRef = this.dialog.open(WfConfLauncherDialog, {
			data: this.conf
		});
		wfDialogRef.componentInstance.newHistory.subscribe((data: WfHistoryEntry[]) => {
			this.historyEntries = data;
		});
	}

	editConf() {
		const dialogRef = this.dialog.open(WfConfDialog, {
			data: this.conf,
			width: '80%'
		});
	}

	deleteConf() {
		if (this.conf?.destroyWf) {
			if (this.conf?.id && this.conf?.workflows) {
				this.client.startDestroyWfConfiguration(this.conf?.id, (data: WfHistoryEntry) => this.snackBar.open('Destroy Workflow launched, PLEASE WAIT !!!', 'INFO', { duration: 5000 }));
			}
		} else if (this.conf?.id) {
			this.client.deleteWfConfiguration(this.conf?.id, (data: void) => {
				this.snackBar.open('Configuration deleted !!!', 'INFO', { duration: 5000 });
				this.conf = undefined;
			});
		}
	}

	openWfHistoryDialog(wf: WfHistoryEntry): void {
		alert(1);
		const wfDialogRef = this.dialog.open(WfHistoryDialog, {
			data: {
				'confId': wf.wfConfId,
				'processId': wf.processId
			}
		});
	}

}
