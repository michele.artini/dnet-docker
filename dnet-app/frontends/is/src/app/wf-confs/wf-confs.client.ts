import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { WfHistoryEntry, SimpleResource, KeyValue, WfConf, WfSubscription, WfSection } from '../common/is.model';
import { FormGroup } from '@angular/forms';
import { ISClient } from '../common/is.client';

@Injectable({
	providedIn: 'root'
})
export class WfConfsClient extends ISClient {

	loadWfSections(onSuccess: Function) {
		this.httpGet<WfSection[]>("/proxy/byType/wf_manager/api/sections", onSuccess)
	}

	loadWfHistoryForConf(wfConfId: string, onSuccess: Function): void {
		this.httpGet<WfHistoryEntry[]>('/proxy/byType/wf_manager/api/history/byConf/' + encodeURIComponent(wfConfId), onSuccess);
	}

	loadWfConfigurations(sectionId: string, onSuccess: Function): void {
		this.httpGet<KeyValue[]>('/proxy/byType/wf_manager/api/sections/' + encodeURIComponent(sectionId), onSuccess);
	}

	loadWfConfiguration(id: string, onSuccess: Function): void {
		this.httpGet<WfConf>('/proxy/byType/wf_manager/api/conf/' + encodeURIComponent(id), onSuccess);
	}

	saveWfConfiguration(conf: WfConf, onSuccess: Function, relatedForm?: FormGroup): void {
		this.httpPost('/proxy/byType/wf_manager/api/conf', conf, onSuccess, relatedForm);
	}

	deleteWfConfiguration(id: string, onSuccess: Function): void {
		this.httpDelete('/proxy/byType/wf_manager/api/conf/' + encodeURIComponent(id), onSuccess);
	}

	startWfConfiguration(id: string, wfs: string[], onSuccess: Function): void {
		let opts = '';
		wfs.forEach(wf => {
			if (opts) { opts += '&' }
			opts += 'wf=' + encodeURIComponent(wf);
		});

		let url = '/proxy/byType/wf_manager/api/conf/' + encodeURIComponent(id) + '/start';
		if (opts) url += '?' + opts;

		this.httpGet<WfHistoryEntry[]>(url, onSuccess);
	}

	startDestroyWfConfiguration(id: string, onSuccess: Function): void {
		this.httpGet<WfHistoryEntry[]>('/proxy/byType/wf_manager/api/conf/' + encodeURIComponent(id) + '/destroy', onSuccess);
	}

	findProcess(id: string, onSuccess: Function): void {
		this.httpGet<WfHistoryEntry>('/proxy/byType/wf_manager/api/process/' + encodeURIComponent(id), onSuccess);
	}

	killProcess(id: string, onSuccess: Function): void {
		this.httpDelete('/proxy/byType/wf_manager/api/process/' + encodeURIComponent(id), onSuccess);
	}

	findWfSubscriptions(id: string, onSuccess: Function): void {
		this.httpGet<WfSubscription[]>('/proxy/byType/wf_manager/api/conf/' + encodeURIComponent(id) + '/subscriptions', onSuccess);
	}

	saveWfSubscriptions(id: string, subscriptions: WfSubscription[], onSuccess: Function, relatedForm?: FormGroup): void {
		this.httpPost('/proxy/byType/wf_manager/api/conf/' + encodeURIComponent(id) + '/subscriptions', subscriptions, onSuccess, relatedForm);
	}

	loadWfTemplates(onSuccess: Function): void {
		this.httpGet<SimpleResource[]>("/proxy/byType/resource_manager/api/resources/byType/wf_template", onSuccess);
	}

	loadWfTemplate(id: string, onSuccess: Function): void {
		const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
		this.httpGetWithOptions<string>("/proxy/byType/resource_manager/api/resources/byType/wf_template/" + encodeURIComponent(id) + '/content', {
			headers, responseType: 'text' as 'json'
		}, onSuccess);
	}

	loadWfTemplatesParams(wfs: string[], onSuccess: Function) {
		let opts = '';
		wfs.forEach(id => {
			if (opts) { opts += '&'; }
			opts += 'wf=' + encodeURIComponent(id);
		});

		this.httpGet<SimpleResource[]>('/proxy/byType/wf_manager/api/parameters?' + opts, onSuccess);
	}

}
