import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

declare const SwaggerUIBundle: any;

@Component({
	selector: 'app-swagger-ui',
	templateUrl: './swagger-ui.component.html',
	styleUrls: []
})
export class SwaggerUiComponent implements OnInit {

	constructor(public route: ActivatedRoute, public router: Router) {
	}

	ngOnInit(): void {
		this.route.params.subscribe(params => {
			let serviceId = params['id'];
			const ui = SwaggerUIBundle({
				dom_id: '#swagger-ui',
				layout: 'BaseLayout',
				presets: [
					SwaggerUIBundle.presets.apis,
					SwaggerUIBundle.SwaggerUIStandalonePreset
				],
				url: '/proxy/byId/' + encodeURIComponent(serviceId) + '/swagger-conf',
				docExpansion: 'none',
				operationsSorter: 'alpha',
				plugins: [
					// plugin to modify the original server url
					{
						statePlugins: {
							spec: {
								wrapActions: {
									updateJsonSpec: function (oriAction: any, system: any) {
										return (spec: any) => {
											spec.servers = [{ url: '/proxy/byId/' + encodeURIComponent(serviceId) }];
											return oriAction(spec)
										}
									}
								}
							}
						}
					}
				]
			});
		});
	}
}
