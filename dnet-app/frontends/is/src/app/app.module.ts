import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FilterPipe } from './common/filter.pipe';
import { XmlPipe } from './common/xml.pipe';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InfoComponent, ServiceInfoDialog } from './info/info.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatTreeModule } from '@angular/material/tree';
import { MatBadgeModule } from '@angular/material/badge';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input'
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select'
import { MatTableModule } from '@angular/material/table';
import { ProtocolsComponent } from './protocols/protocols.component';
import { MainMenuPanelsComponent } from './main-menu-panels/main-menu-panels.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { WfHistoryDialog, WfHistoryComponent, WfHistoryTableComponent, WfHistoryDetailsDialog, WfRuntimeGraphDialog } from './wf-history/wf-history.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSortModule } from '@angular/material/sort';
import { ResourcesComponent, ResContentDialog, ResCreateNewDialog, ResMetadataDialog } from './resources/resources.component'
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ContextsComponent, ContextViewerComponent } from './contexts/contexts.component';
import { VocabulariesComponent, VocabularyEditorComponent, VocDialog, VocTermDialog } from './vocabularies/vocabularies.component';
import { DsmSearchComponent, DsmResultsComponent, DsmApiComponent, DsmEditApiDialog, DsmBrowseDialog, DsmAddWorkflowDialog } from './dsm/dsm.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SpinnerHttpInterceptor } from './common/spinner.service';
import { MdstoresComponent, MdstoreInspectorComponent, MDStoreVersionsDialog, AddMDStoreDialog } from './mdstores/mdstores.component';
import { CleanerTesterComponent } from './cleaner-tester/cleaner-tester.component';
import { WfConfsComponent, WfSingleConfComponent } from './wf-confs/wf-confs.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatStepperModule } from '@angular/material/stepper';
import { IndexComponent } from './index/index.component';
import { OaiComponent } from './oai/oai.component';
import { SwaggerUiComponent } from './swagger/swagger-ui.component';
import { WfTemplateDialog, WfConfDialog, WfConfLauncherDialog } from './wf-confs/wf-common.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { OaiExplorerComponent } from './oai-explorer/oai-explorer.component';
import { XsltTesterComponent } from './xslt-tester/xslt-tester.component';

@NgModule({
	declarations: [
		AppComponent,
		FilterPipe,
		XmlPipe,
		MainMenuPanelsComponent,
		InfoComponent,
		ServiceInfoDialog,
		ProtocolsComponent,
		WfTemplateDialog,
		ResourcesComponent,
		ResContentDialog,
		ResCreateNewDialog,
		ResMetadataDialog,
		ContextsComponent,
		ContextViewerComponent,
		VocabulariesComponent,
		VocabularyEditorComponent,
		VocDialog,
		VocTermDialog,
		DsmSearchComponent,
		DsmResultsComponent,
		DsmApiComponent,
		DsmEditApiDialog,
		DsmBrowseDialog,
		DsmAddWorkflowDialog,
		MdstoresComponent,
		MdstoreInspectorComponent,
		MDStoreVersionsDialog,
		AddMDStoreDialog,
		CleanerTesterComponent,
		XsltTesterComponent,
		WfConfsComponent,
		WfConfDialog,
		WfSingleConfComponent,
		WfHistoryComponent,
		WfHistoryDialog,
		WfHistoryDetailsDialog,
		WfHistoryTableComponent,
		WfRuntimeGraphDialog,
		WfConfLauncherDialog,
		IndexComponent,
		OaiComponent,
		OaiExplorerComponent,
		SwaggerUiComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		HttpClientModule,
		LayoutModule,
		MatToolbarModule,
		MatButtonModule,
		MatSidenavModule,
		MatIconModule,
		MatListModule,
		MatTreeModule,
		MatBadgeModule,
		MatCardModule,
		MatFormFieldModule,
		MatInputModule,
		MatRadioModule,
		MatSelectModule,
		MatCheckboxModule,
		MatTableModule,
		MatExpansionModule,
		MatDialogModule,
		MatSortModule,
		ReactiveFormsModule,
		MatSnackBarModule,
		MatPaginatorModule,
		MatProgressSpinnerModule,
		MatTabsModule,
		MatStepperModule,
		MatButtonModule,
		MatIconModule,
		MatMenuModule,
		MatDatepickerModule,
		MatNativeDateModule,
	],
	providers: [{
		provide: HTTP_INTERCEPTORS,
		useClass: SpinnerHttpInterceptor,
		multi: true
	}],
	bootstrap: [AppComponent]
})
export class AppModule { }
