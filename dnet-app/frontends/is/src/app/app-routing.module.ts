import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InfoComponent } from './info/info.component';
import { ProtocolsComponent } from './protocols/protocols.component';
import { WfHistoryComponent } from './wf-history/wf-history.component';
import { ResourcesComponent } from './resources/resources.component';
import { VocabulariesComponent, VocabularyEditorComponent } from './vocabularies/vocabularies.component';
import { ContextViewerComponent, ContextsComponent } from './contexts/contexts.component';
import { DsmSearchComponent, DsmResultsComponent, DsmApiComponent } from './dsm/dsm.component';
import { MdstoreInspectorComponent, MdstoresComponent } from './mdstores/mdstores.component';
import { CleanerTesterComponent } from './cleaner-tester/cleaner-tester.component';
import { WfConfsComponent, WfSingleConfComponent } from './wf-confs/wf-confs.component';
import { IndexComponent } from './index/index.component';
import { OaiComponent } from './oai/oai.component';
import { SwaggerUiComponent } from './swagger/swagger-ui.component';
import { OaiExplorerComponent } from './oai-explorer/oai-explorer.component';
import { XsltTesterComponent } from './xslt-tester/xslt-tester.component';


const routes: Routes = [
	{ path: "", redirectTo: 'info', pathMatch: 'full' },
	{ path: "info", component: InfoComponent },
	{ path: "resources/:type", component: ResourcesComponent },
	{ path: "contexts", component: ContextsComponent },
	{ path: "vocabularies", component: VocabulariesComponent },
	{ path: "protocols", component: ProtocolsComponent },
	{ path: "wfs/:section", component: WfConfsComponent },
	{ path: "wfs/conf/:conf", component: WfSingleConfComponent },
	{ path: "wf_history", component: WfHistoryComponent },
	{ path: "ctx_viewer", component: ContextViewerComponent },
	{ path: "voc_editor", component: VocabularyEditorComponent },
	{ path: "dsm/search", component: DsmSearchComponent },
	{ path: "dsm/results/:page/:size", component: DsmResultsComponent },
	{ path: "dsm/api", component: DsmApiComponent },
	{ path: "mdstores", component: MdstoresComponent },
	{ path: "mdstores/:mdId", component: MdstoresComponent },
	{ path: "mdrecords/:versionId/:limit", component: MdstoreInspectorComponent },
	{ path: "cleaner", component: CleanerTesterComponent },
	{ path: "xslt", component: XsltTesterComponent },
	{ path: "index", component: IndexComponent },
	{ path: "oai", component: OaiComponent },
	{ path: "oai-explorer", component: OaiExplorerComponent },
	{ path: "swagger-ui/:id", component: SwaggerUiComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
