import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { ResourceType, SimpleResource } from '../common/is.model';
import { FormGroup } from '@angular/forms';
import { ISClient } from '../common/is.client';

@Injectable({
	providedIn: 'root'
})
export class ResourcesClient extends ISClient {

	baseUrl = "/proxy/byType/resource_manager/api";

	loadResourceType(id: string, onSuccess: Function) {
		this.httpGet<ResourceType>(this.baseUrl + '/resourceTypes/' + encodeURIComponent(id), onSuccess);
	}
	loadSimpleResources(type: string, onSuccess: Function): void {
		this.httpGet<SimpleResource[]>(this.baseUrl + '/resources/byType/' + encodeURIComponent(type), onSuccess);
	}

	loadSimpleResourceContent(id: any, onSuccess: Function): void {
		const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
		this.httpGetWithOptions<string>(this.baseUrl + '/resources/' + encodeURIComponent(id) + '/content', {
			headers, responseType: 'text' as 'json'
		}, onSuccess);
	}

	saveSimpleResourceMedatata(res: SimpleResource, onSuccess: Function, relatedForm?: FormGroup): void {
		this.httpPost(this.baseUrl + '/resources/' + encodeURIComponent(res.id) + '/metadata', res, onSuccess, relatedForm);
	}

	saveSimpleResourceContent(id: string, content: string, onSuccess: Function, relatedForm?: FormGroup): void {
		const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		let body = new HttpParams().set('content', content);
		this.httpPostWithOptions(this.baseUrl + '/resources/' + encodeURIComponent(id) + '/content', body, { headers: headers }, onSuccess, relatedForm);
	}

	addSimpleResource(name: string, type: string, subtype: string, description: string, content: string, onSuccess: Function, relatedForm?: FormGroup): void {
		const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		let body = new HttpParams()
			.set('name', name)
			.set('type', type)
			.set('subtype', subtype)
			.set('description', description)
			.set('content', content);
		this.httpPostWithOptions(this.baseUrl + '/resources/', body, { headers: headers }, onSuccess, relatedForm);
	}

	deleteSimpleResource(resourceId: string, onSuccess: Function): void {
		this.httpDelete(this.baseUrl + '/resources/' + encodeURIComponent(resourceId), onSuccess);
	}

}
