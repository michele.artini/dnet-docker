import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ResourceType, SimpleResource } from '../common/is.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ResourcesClient } from './resources.client';

@Component({
	selector: 'app-resources',
	templateUrl: './resources.component.html',
	styleUrls: []
})
export class ResourcesComponent implements OnInit {
	typeId: string = '';
	type?: ResourceType;
	resources: SimpleResource[] = [];
	searchText: string = '';

	constructor(public client: ResourcesClient, public route: ActivatedRoute, public dialog: MatDialog) {
	}

	ngOnInit() {
		this.route.params.subscribe(params => {
			this.typeId = params['type'];
			this.client.loadResourceType(this.typeId, (data: ResourceType) => this.type = data);
			this.reload()
		});
	}

	reload() {
		if (this.typeId) {
			this.client.loadSimpleResources(this.typeId, (data: SimpleResource[]) => this.resources = data);
		}
	}

	openNewDialog(): void {
		const dialogRef = this.dialog.open(ResCreateNewDialog, {
			data: this.type,
			width: '80%'
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) this.reload();
		});
	}

	openMetadataDialog(r: SimpleResource): void {
		const dialogRef = this.dialog.open(ResMetadataDialog, {
			data: r,
			width: '80%'
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) this.reload();
		});
	}

	openContentDialog(r: SimpleResource): void {
		this.client.loadSimpleResourceContent(r.id, (data: string) => {
			const dialogRef = this.dialog.open(ResContentDialog, {
				data: {
					id: r.id,
					contentType: this.type?.contentType,
					content: data
				},
				width: '80%'
			});

			dialogRef.afterClosed().subscribe(result => {
				if (result) this.reload();
			});
		});
	}

	deleteResource(r: SimpleResource) {
		if (confirm('Are you sure?')) {
			this.client.deleteSimpleResource(r.id, (data: void) => this.reload());
		}
	}
}


@Component({
	selector: 'res-content-dialog',
	templateUrl: 'content-dialog.html',
	styleUrls: []
})
export class ResContentDialog {
	contentFormControl = new FormControl('');

	contentForm = new FormGroup({
		content: this.contentFormControl
	});

	constructor(public dialogRef: MatDialogRef<ResContentDialog>, @Inject(MAT_DIALOG_DATA) public data: any, public client: ResourcesClient) {
		this.contentFormControl.setValue(data.content);
	}

	onSubmit(): void {
		let content = this.contentFormControl.value;
		if (content) {
			this.client.saveSimpleResourceContent(this.data.id, content, (data: void) => this.dialogRef.close(1), this.contentForm)
		}
	}

	onNoClick(): void {
		this.dialogRef.close();
	}
}

@Component({
	selector: 'res-metadata-dialog',
	templateUrl: 'metadata-dialog.html',
	styleUrls: []
})
export class ResMetadataDialog {
	metadataForm = new FormGroup({
		name: new FormControl('', [Validators.required]),
		subtype: new FormControl(''),
		description: new FormControl('')
	});

	constructor(public dialogRef: MatDialogRef<ResMetadataDialog>, @Inject(MAT_DIALOG_DATA) public data: any, public client: ResourcesClient) {
		this.metadataForm.get('name')?.setValue(data.name);
		if (data.subtype) {
			this.metadataForm.get('subtype')?.setValue(data.subtype);
		}
		if (data.description) {
			this.metadataForm.get('description')?.setValue(data.description);
		}
	}

	onSubmit(): void {
		const res = Object.assign({}, this.data, this.metadataForm.value);
		this.client.saveSimpleResourceMedatata(res, (data: void) => this.dialogRef.close(1), this.metadataForm);
	}

	onNoClick(): void {
		this.dialogRef.close();
	}
}

@Component({
	selector: 'res-new-dialog',
	templateUrl: 'new-dialog.html',
	styleUrls: []
})
export class ResCreateNewDialog {
	newResourceForm = new FormGroup({
		name: new FormControl('', [Validators.required]),
		subtype: new FormControl(''),
		description: new FormControl(''),
		content: new FormControl('', [Validators.required])
	});

	constructor(public dialogRef: MatDialogRef<ResCreateNewDialog>, @Inject(MAT_DIALOG_DATA) public data: any, public client: ResourcesClient) { }

	onSubmit(): void {
		let name: string = this.newResourceForm.get('name')?.value!;
		let type: string = this.data.id!;
		let subtype: string = this.newResourceForm.get('subtype')?.value!;
		let description: string = this.newResourceForm.get('description')?.value!;
		let content: string = this.newResourceForm.get('content')?.value!;

		this.client.addSimpleResource(name, type, subtype, description, content, (data: void) => this.dialogRef.close(1), this.newResourceForm);
	}
	onNoClick(): void {
		this.dialogRef.close();
	}
}
