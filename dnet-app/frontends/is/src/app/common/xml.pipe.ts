import { Pipe, PipeTransform } from "@angular/core";
import xmlFormat from 'xml-formatter';

@Pipe({
	name: 'xml'
})
export class XmlPipe implements PipeTransform {
	transform(value: string): string {
		return xmlFormat(value, {
			indentation: '  ',
			collapseContent: true,
			lineSeparator: '\n',
			throwOnFailure: false
		});
	}
}
