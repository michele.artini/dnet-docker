export interface Service {
	name: string,
	type: string,
	baseUrl: string,
	date: string,
	status: number // 0: NOT_VERIFIED, 1: OK, 2: NOT_RESPOND 
}

export interface ResourceType {
	id: string
	name: string
	contentType: string
	count: number
	simple: boolean
}

export interface KeyValue {
	k: string;
	v: any;
}

export interface BrowseTerm {
	term: string,
	name: string,
	total: number
}

export interface Module {
	group: string;
	name: string;
	versions: string[];
	files: string[];
}

export interface ProtocolParam {
	name: string
	label: string
	type: string
	optional: boolean
	hasSelFunction: boolean
}

export interface Protocol {
	id: string
	params: ProtocolParam[]
}

export interface SimpleResource {
	id: string,
	name: string,
	type: string,
	subtype?: string,
	description?: string,
	creationDate?: string,
	modificationDate?: string
}

export interface ContextParam {
	name: string,
	value: string
}

export interface Context {
	id: string,
	label: string,
	parameters: ContextParam[],
	nChilds: number,
	type: string
}

export interface ContextNode {
	id: string,
	label: string,
	parameters: ContextParam[],
	nChilds: number,
	claim: boolean,
	parent: string,
	populated?: boolean,
	childs?: ContextNode[]
}

export interface Vocabulary {
	id: string,
	name: string,
	description?: string
}

export interface VocabularyTermSynonym {
	term: string,
	encoding: string
}

export interface VocabularyTerm {
	code: string,
	vocabulary: string,
	name: string,
	encoding: string,
	synonyms: VocabularyTermSynonym[]
}

export interface Api {
	id: string,
	protocol: string,
	compliance: string,
	active: boolean,
	aggrDate: string,
	aggrTotal: number
}

export interface ApiParam {
	param: string,
	value: string
}

export interface ApiInsert {
	id: string,
	protocol: string,
	datasource: string,
	contentdescription: string,
	removable: boolean,
	compatibility: string,
	metadataIdentifierPath: string,
	baseurl: string,
	apiParams: ApiParam[]
};

export interface SimpleOrganization {
	name: string,
	country: string
}

export interface SimpleDatasource {
	id: string,
	name: string,
	otherName?: string,
	nsprefix: string,
	websiteUrl?: string,
	type: string,
	consenttermsofuse?: boolean,
	fulltextdownload?: boolean,
	collectedFrom: string,
	organizations: SimpleOrganization[],
	apis: Api[]
}

export interface Page<T> {
	content: T[],
	totalPages: number,
	totalElements: number,
	size: number,
	number: number
}

export interface DsmConf {
	compatibilityLevels: string[],
	contentDescTypes: string[],
	protocols: Protocol[],
	browsableFields: KeyValue[]
}

export interface MDStore {
	id: string,
	format: string,
	layout: string,
	type: string,
	interpretation: string,
	datasourceName: string,
	datasourceId: string,
	apiId: string,
	currentVersion: string,
	creationDate: string,
	lastUpdate: string,
	size: number,
	numberOfVersions: number,
	params: any
}

export interface MDStoreVersion {
	id: string,
	mdstore: string,
	writing: boolean,
	readCount: number,
	lastUpdate: string,
	size: number,
	params: any
}

export interface MDStoreRecord {
	id: string,
	originalId: string,
	encoding: string,
	body: string,
	dateOfCollection: string,
	dateOfTransformation: string,
	provenance: any
}

export interface WfSection {
	id: string,
	name: string,
	count: number
}

export interface WfConf {
	id: string,
	name: string,
	section?: string,
	details: Map<string, string>,
	priority: number,
	dsId?: string,
	dsName?: string,
	apiId?: string,
	enabled: boolean,
	configured: boolean,
	schedulingEnabled: boolean,
	cronExpression?: string,
	cronMinInterval?: number,
	workflows: string[],
	destroyWf?: string,
	systemParams: Map<string, string>,
	userParams: Map<string, string>
}

export interface WfParam {
	name: string,
	description?: string,
	type?: string,
	defaultValue?: any,
	required: boolean,
	validTerms: string[]
}

export interface WfHistoryEntry {
	processId: string,
	name: string,
	wfConfId: string,
	family: string,
	status: string,
	startDate: string,
	endDate: string,
	lastUpdate: string,
	dsId?: string,
	dsName?: string,
	dsApi?: string,
	inputParams: Map<string, string>,
	outputParams: Map<string, string>,
	graph: any
}

export interface WfSubscription {
	// TODO
}

export interface WfTemplate {
	parameters: any,
	graph: any,
	info: Map<string, string>
}

export interface WfTemplateDesc extends WfTemplate {
	id: string,
	name: string,
	description?: string
}

export interface WfRepoHi extends WfTemplateDesc {
	expectedEoscDsTypes: string[],
	expectedCompliances: string[]
}
