import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';
import { HttpInterceptor, HttpResponse } from '@angular/common/http';
import { HttpRequest } from '@angular/common/http';
import { HttpHandler } from '@angular/common/http';
import { HttpEvent } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class SpinnerService {
	visibility: BehaviorSubject<boolean>;

	constructor() { this.visibility = new BehaviorSubject(false); }

	show() { this.visibility.next(true); }
	hide() { this.visibility.next(false); }
}

@Injectable()
export class SpinnerHttpInterceptor implements HttpInterceptor {

	constructor(private spinnerService: SpinnerService) { }

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

		this.spinnerService.show();

		return next.handle(req)
			.pipe(tap((event: HttpEvent<any>) => {
				if (event instanceof HttpResponse) {
					this.spinnerService.hide();
				}
			}, (error) => {
				this.spinnerService.hide();
			}));
	}
}
