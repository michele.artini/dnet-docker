import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'searchFilter' })
export class FilterPipe implements PipeTransform {
	/**
	 * Pipe filters the list of elements based on the search text provided
	 *
	 * @param items list of elements to search in
	 * @param searchText search string
	 * @returns list of elements filtered by search text or []
	 */

	transform(items: any[], searchText: string): any[] {
		if (!items) return [];
		if (!searchText) return items;

		searchText = searchText.trim().toLocaleLowerCase();

		return items.filter(obj => {
			return Object.keys(obj).reduce((acc, curr) => {
				return acc || JSON.stringify(obj[curr]).toLowerCase().includes(searchText);
			}, false);
		});
	}
}
