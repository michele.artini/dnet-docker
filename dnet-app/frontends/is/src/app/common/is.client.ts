import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
	providedIn: 'root'
})
export class ISClient {

	constructor(public client: HttpClient, public snackBar: MatSnackBar) { }

	protected httpGet<T>(url: string, onSuccess: Function) {
		this.client.get<T>(url).subscribe({
			next: data => onSuccess(data),
			error: error => this.showError(error)
		});
	}

	protected httpGetWithOptions<T>(url: string, options: any, onSuccess: Function) {
		this.client.get<T>(url, options).subscribe({
			next: data => onSuccess(data),
			error: error => this.showError(error)
		});
	}

	protected httpDelete(url: string, onSuccess: Function) {
		this.client.delete<void>(url).subscribe({
			next: data => onSuccess(data),
			error: error => this.showError(error)
		});
	}

	protected httpPost(url: string, body: any, onSuccess: Function, relatedForm?: FormGroup) {
		this.client.post<void>(url, body).subscribe({
			next: data => onSuccess(data),
			error: error => this.showError(error, relatedForm)
		});
	}

	protected httpPostWithOptions(url: string, body: any, options: any, onSuccess: Function, relatedForm?: FormGroup, showStrackTrace?: boolean) {
		this.client.post<void>(url, body, options).subscribe({
			next: data => onSuccess(data),
			error: error => this.showError(error, relatedForm, showStrackTrace)
		});
	}

	protected showError(error: any, form?: FormGroup, showStrackTrace?: boolean) {

		console.log(error);

		const msg = this.errorMessage(error, showStrackTrace);

		if (form) {
			form.setErrors({ serverError: msg })
		} else if (this.snackBar) {
			this.snackBar.open(msg, 'ERROR', {
				duration: 5000,
			});
		} else {
			alert(msg);
		}
	}

	protected errorMessage(error: any, showStrackTrace?: boolean) {
		if (showStrackTrace && error.error) {
			return error.error.stacktrace ? error.error.stacktrace : JSON.parse(error.error).stacktrace;
		} else if (error.error) {
			return error.error.message ? error.error.message : JSON.parse(error.error).message;
		} else if (error.message) {
			return error.message;
		} else {
			return 'Generic server side error';
		}
	}

}
