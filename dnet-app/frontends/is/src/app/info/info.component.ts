import { Component, Inject, AfterViewInit, OnInit, ViewChild, Injectable } from '@angular/core';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Module, Service } from '../common/is.model';
import { ISClient } from '../common/is.client';

export interface KeyValueDatasource {
	name: string;
	datasource: MatTableDataSource<any>;
}

@Component({
	selector: 'app-info',
	templateUrl: './info.component.html',
	styleUrls: []
})
export class InfoComponent {

	servicesDatasource: MatTableDataSource<Service> = new MatTableDataSource<Service>([]);

	colums: string[] = ['ping', 'name', 'type', 'baseUrl', 'info', 'date'];

	@ViewChild(MatSort) sort: MatSort | undefined

	constructor(public client: InfoClient, public route: ActivatedRoute, public dialog: MatDialog) {
	}

	ngOnInit() {
		this.reload();
	}

	ngAfterViewInit() {
		if (this.sort) this.servicesDatasource.sort = this.sort;
	}

	reload() {
		this.client.availableServices((data: Service[]) => {
			data.forEach(service => service.status = 0);
			this.servicesDatasource.data = data;
		});
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value.trim().toLowerCase();
		this.servicesDatasource.filter = filterValue;
	}

	showInfoDialog(service: string) {
		this.client.infoService(service, (info: any[]) => {

			const dialogRef = this.dialog.open(ServiceInfoDialog, {
				data: {
					service: service,
					info: info
				},
				width: '80%'
			});

			dialogRef.afterClosed().subscribe(result => {
				if (result) this.reload();
			});
		});

	}

	ping(service: Service) {
		this.client.pingService(service.name, (status: number) => {
			service.status = status;
		});
	}

	pingAll() {
		this.servicesDatasource.data.forEach(service => service.status = 0);
		this.servicesDatasource.data.forEach(service => this.ping(service));
	}

}

@Component({
	selector: 'service-info-dialog',
	templateUrl: './info.dialog.html',
	styleUrls: []
})
export class ServiceInfoDialog {

	service: string = '';
	info: any[] = [];
	kvDatasources: KeyValueDatasource[] = [];
	moduleDatasource: MatTableDataSource<Module> = new MatTableDataSource<Module>([]);

	displayedKVColumns: string[] = ['k', 'v'];
	displayedModuleColumns: string[] = ['group', 'name', 'versions', 'files'];

	constructor(public dialogRef: MatDialogRef<ServiceInfoDialog>, @Inject(MAT_DIALOG_DATA) public data: any) {
		this.service = data.service;
		this.info = data.info;

		this.info.forEach(section => {
			if (section['name'] == 'Modules') {
				this.moduleDatasource.data = section['data'];
			} else {
				this.kvDatasources.push({
					name: section['name'],
					datasource: new MatTableDataSource(section['data'])
				});
			}
		});
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value.trim().toLowerCase();
		this.kvDatasources.forEach(s => s.datasource.filter = filterValue)
		this.moduleDatasource.filter = filterValue;
	}

}

@Injectable({
	providedIn: 'root'
})
export class InfoClient extends ISClient {

	availableServices(onSuccess: Function): void {
		this.httpGet<Service[]>("/ajax-is/services", onSuccess);
	}

	infoService(service: string, onSuccess: Function): void {
		this.httpGet<any>("/ajax-is/info/" + encodeURIComponent(service), onSuccess);
	}

	pingService(service: string, onSuccess: Function): void {
		this.httpGet<number>("/ajax-is/ping/" + encodeURIComponent(service), onSuccess);
	}

}
