import { Component, Injectable, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MDStore, MDStoreRecord, MDStoreVersion } from '../common/is.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ISClient } from '../common/is.client';

@Component({
	selector: 'app-index-confs',
	templateUrl: './index.component.html',
	styleUrls: []
})
export class IndexComponent implements OnInit {



	constructor(public client: IndexClient, public route: ActivatedRoute, public dialog: MatDialog) { }

	ngOnInit() { this.reload() }
	reload() {
		// TODO
	}



	openNewConfigurationDialog(): void {
		/*const dialogRef = this.dialog.open(AddIndexConfDialog, {
			data: {},
			width: '80%'
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) this.reload();
		});*/
	}

}

@Injectable({
	providedIn: 'root'
})
export class IndexClient extends ISClient {


}
