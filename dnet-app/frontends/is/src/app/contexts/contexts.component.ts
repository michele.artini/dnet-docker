import { Component, Inject, AfterViewInit, OnInit, ViewChild, Injectable } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Context, ContextParam, ContextNode } from '../common/is.model';
import { ActivatedRoute, Params } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ISClient } from '../common/is.client';


@Component({
	selector: 'app-contexts',
	templateUrl: './contexts.component.html',
	styleUrls: []
})
export class ContextsComponent implements AfterViewInit, OnInit {
	contextsDatasource: MatTableDataSource<Context> = new MatTableDataSource<Context>([]);

	colums: string[] = ['id', 'label', 'type', 'buttons'];

	@ViewChild(MatSort) sort: MatSort | undefined

	constructor(public client: ContextsClient, public route: ActivatedRoute, public dialog: MatDialog) {
	}

	ngOnInit() {
		this.reload();
	}

	ngAfterViewInit() {
		if (this.sort) this.contextsDatasource.sort = this.sort;
	}

	reload() {
		this.client.loadContexts((data: Context[]) => this.contextsDatasource.data = data);
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value.trim().toLowerCase();
		this.contextsDatasource.filter = filterValue;
	}


}

@Component({
	selector: 'app-context-editor',
	templateUrl: './context-viewer.component.html',
	styleUrls: []
})
export class ContextViewerComponent implements OnInit {

	constructor(public client: ContextsClient, public route: ActivatedRoute, public dialog: MatDialog) {
	}

	ngOnInit() {
		this.route.queryParams.subscribe((params) => {
			//	this.client.loadContext(params['id'], (data: Context) => this.context = data);
			//	this.client.loadContextCategories(params['id'], (data: ContextNode[]) => this.categories = data);
		});
	}


}

@Injectable({
	providedIn: 'root'
})
export class ContextsClient extends ISClient {

	loadContexts(onSuccess: Function): void {
		//this.httpGet<Context[]>('/proxy/byType/contexts-manager/', onSuccess);
	}

	loadContext(ctxId: string, onSuccess: Function): void {
		//this.httpGet<Context>('/proxy/byType/contexts-manager/' + encodeURIComponent(ctxId), onSuccess);
	}

	loadContextCategories(ctxId: string, onSuccess: Function): void {
		//this.httpGet<ContextNode[]>('/proxy/byType/contexts-manager/' + encodeURIComponent(ctxId) + '/categories', onSuccess);
	}

	loadContextConcepts(level: number, nodeId: string, onSuccess: Function): void {
		//this.httpGet<ContextNode[]>('/proxy/byType/contexts-manager/' + encodeURIComponent(level) + '/' + encodeURIComponent(nodeId) + '/concepts', onSuccess);
	}

}
