import { Injectable } from '@angular/core';
import { Context, ContextNode } from '../common/is.model';
import { ISClient } from '../common/is.client';

@Injectable({
	providedIn: 'root'
})
export class ContextsClient extends ISClient {

	loadContexts(onSuccess: Function): void {
		this.httpGet<Context[]>('/proxy/byType/contexts-manager/', onSuccess);
	}

	loadContext(ctxId: string, onSuccess: Function): void {
		this.httpGet<Context>('/proxy/byType/contexts-manager/' + encodeURIComponent(ctxId), onSuccess);
	}

	loadContextCategories(ctxId: string, onSuccess: Function): void {
		this.httpGet<ContextNode[]>('/proxy/byType/contexts-manager/' + encodeURIComponent(ctxId) + '/categories', onSuccess);
	}

	loadContextConcepts(level: number, nodeId: string, onSuccess: Function): void {
		this.httpGet<ContextNode[]>('/proxy/byType/contexts-manager/' + encodeURIComponent(level) + '/' + encodeURIComponent(nodeId) + '/concepts', onSuccess);
	}

}
