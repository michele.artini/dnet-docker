import { Component, Inject, Injectable, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MDStore, MDStoreRecord, MDStoreVersion } from '../common/is.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ISClient } from '../common/is.client';
import { combineLatest } from 'rxjs';

@Component({
	selector: 'oai-explorer',
	templateUrl: './oai-explorer.component.html',
	styleUrls: []
})
export class OaiExplorerComponent implements OnInit {

	oaiBaseUrl: string = ""
	page: number = 0;
	oaiResponse: any = {};
	currFormat?: string;

	constructor(public client: OaiExplorerClient, public router: Router, public route: ActivatedRoute, public dialog: MatDialog) { }

	ngOnInit() {
		combineLatest([this.route.params, this.route.queryParams],
			(params: Params, queryParams: Params) => ({ params, queryParams })
		).subscribe((res: { params: Params; queryParams: Params }) => {
			this.oaiBaseUrl = res.queryParams['baseUrl'];
			this.pageInfo();
		});
	}

	pageInfo() {
		this.oaiResponse = {};
		this.page = 0;
		this.client.callIdentifyVerb(this.oaiBaseUrl, (res: any) => {
			this.oaiResponse = res;
			this.page = 1;
		});
	}

	pageSets(token?: string) {
		this.oaiResponse = {};
		this.page = 0;
		this.client.callListSetsVerb(this.oaiBaseUrl, token, (res: any) => {
			this.oaiResponse = res;
			this.page = 2;
		});
	}

	pageFormats() {
		this.oaiResponse = {};
		this.page = 0;
		this.client.callListMdFormatsVerb(this.oaiBaseUrl, (res: any) => {
			this.oaiResponse = res;
			this.page = 3;
		});
	}

	pageRecords(set: string | undefined, format: string | undefined, token: string | undefined) {
		this.oaiResponse = {};
		this.page = 0;
		this.currFormat = format;
		this.client.callListRecordsVerb(this.oaiBaseUrl, set, format, token, (res: any) => {
			this.oaiResponse = res;
			this.page = 4;
		});
	}

	pageIdentifiers(set: string | undefined, format: string | undefined, token: string | undefined) {
		this.oaiResponse = {};
		this.page = 0;
		this.currFormat = format;
		this.client.callListIdentifiersVerb(this.oaiBaseUrl, set, format, token, (res: any) => {
			this.oaiResponse = res;
			this.page = 5;
		});
	}

	pageSingleRecord(id: string, format?: string) {
		this.oaiResponse = {};
		this.page = 0;
		this.currFormat = format;
		this.client.callGetRecordVerb(this.oaiBaseUrl, id, format, (res: any) => {
			this.oaiResponse = res;
			this.page = 6;
		});
	}
}

@Injectable({
	providedIn: 'root'
})
export class OaiExplorerClient extends ISClient {
	baseUrl = '/proxy/byType/datasource_manager/api';

	callIdentifyVerb(oaiBaseUrl: string, onSuccess: Function) {
		this.httpGet(this.baseUrl + '/oai-explorer/info?baseUrl=' + encodeURIComponent(oaiBaseUrl), onSuccess);
	}

	callListSetsVerb(oaiBaseUrl: string, token: string | undefined, onSuccess: Function) {
		let url = this.baseUrl + '/oai-explorer/sets?baseUrl=' + encodeURIComponent(oaiBaseUrl);
		if (token) { url += "&token=" + encodeURIComponent(token); }
		this.httpGet(url, onSuccess);
	}

	callListMdFormatsVerb(oaiBaseUrl: string, onSuccess: Function) {
		this.httpGet(this.baseUrl + '/oai-explorer/mdformats?baseUrl=' + encodeURIComponent(oaiBaseUrl), onSuccess);
	}

	callListRecordsVerb(oaiBaseUrl: string, set: string | undefined, format: string | undefined, token: string | undefined, onSuccess: Function) {
		let url = this.baseUrl + '/oai-explorer/records?baseUrl=' + encodeURIComponent(oaiBaseUrl);
		if (token) {
			url += "&token=" + encodeURIComponent(token);
		} else {
			if (set) { url += '&set=' + encodeURIComponent(set); }
			if (format) { url += '&format=' + encodeURIComponent(format); }
		}
		this.httpGet(url, onSuccess);
	}

	callListIdentifiersVerb(oaiBaseUrl: string, set: string | undefined, format: string | undefined, token: string | undefined, onSuccess: Function) {
		let url = this.baseUrl + '/oai-explorer/identifiers?baseUrl=' + encodeURIComponent(oaiBaseUrl);
		if (token) {
			url += "&token=" + encodeURIComponent(token);
		} else {
			if (set) { url += '&set=' + encodeURIComponent(set); }
			if (format) { url += '&format=' + encodeURIComponent(format); }
		}
		this.httpGet(url, onSuccess);
	}

	callGetRecordVerb(oaiBaseUrl: string, id: string, format: string | undefined, onSuccess: Function) {
		let url = this.baseUrl + '/oai-explorer/record?baseUrl=' + encodeURIComponent(oaiBaseUrl) + '&id=' + encodeURIComponent(id);

		if (format) {
			url += "&format=" + encodeURIComponent(format);
		}
		this.httpGet(url, onSuccess);
	}
}
