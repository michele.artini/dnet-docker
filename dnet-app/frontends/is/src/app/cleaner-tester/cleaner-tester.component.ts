import { Component, Injectable, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ResourceType, SimpleResource } from '../common/is.model';
import { ISClient } from '../common/is.client';
import { HttpHeaders } from '@angular/common/http';

@Component({
	selector: 'app-cleaner-tester',
	templateUrl: './cleaner-tester.component.html',
	styleUrls: []
})
export class CleanerTesterComponent {
	rules: SimpleResource[] = [];

	xmlOutput: string = ''

	cleanForm = new FormGroup({
		xmlIn: new FormControl('', [Validators.required]),
		rule: new FormControl('', [Validators.required])
	});

	constructor(public client: CleanerTesterClient, public route: ActivatedRoute, public dialog: MatDialog) {
		this.client.loadCleaningRules((data: SimpleResource[]) => this.rules = data);
	}

	clean() {
		this.client.testCleaning(this.cleanForm.get("rule")?.value!, this.cleanForm.get("xmlIn")?.value!, (data: string) => this.xmlOutput = data, this.cleanForm);
	}

}

@Injectable({
	providedIn: 'root'
})
export class CleanerTesterClient extends ISClient {

	loadCleaningRules(onSuccess: Function): void {
		this.httpGet<SimpleResource[]>("/proxy/byType/resource_manager/api/resources/byType/cleaning_rule", onSuccess);
	}

	testCleaning(rule: string, xml: string, onSuccess: Function, relatedForm?: FormGroup): void {
		var headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
		this.httpPostWithOptions('/proxy/byType/wf_manager/api/mapping/clean?rule=' + encodeURIComponent(rule), xml, { headers, responseType: 'text' as 'json' }, onSuccess, relatedForm, true);
	}

}
