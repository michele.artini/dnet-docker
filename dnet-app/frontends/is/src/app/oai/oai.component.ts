import { Component, Inject, Injectable, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MDStore, MDStoreRecord, MDStoreVersion } from '../common/is.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ISClient } from '../common/is.client';

@Component({
	selector: 'app-oai',
	templateUrl: './oai.component.html',
	styleUrls: []
})
export class OaiComponent implements OnInit {



	constructor(public client: OaiClient, public route: ActivatedRoute, public dialog: MatDialog) { }

	ngOnInit() { this.reload() }
	reload() {
		// TODO
	}
}

@Injectable({
	providedIn: 'root'
})
export class OaiClient extends ISClient {


}
