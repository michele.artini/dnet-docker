#!/bin/sh

export PUBLIC_PORT=8888
export PUBLIC_API_PORT=9999

export SPRING_BOOT_PORT=8080
export SOLR_PORT=8983

export PG_PORT=5432
export PG_USER=dnet
export PG_PASSWORD=ax45vs#1A
export PG_RESOURCES_DB=dnet_resources
export PG_DSM_DB=dnet_dsm
export PG_MDSTORES_DB=dnet_mdstores
export PG_WFS_DB=dnet_wfs
export PG_VOCS_DB=dnet_vocabularies
export PG_CONTEXTS_DB=dnet_contexts
export PG_MDSTORES_DATA_DB=dnet_mdstores_data
export PG_INDEX_DB=dnet_index_configs
export PG_OAI_DB=dnet_oai

#export COMPOSE_PROFILES=base,ui
export COMPOSE_PROFILES=base,mail,dsm,vocs,mdstores,wfs,index,contexts,oai,ui

docker-compose -f docker-compose.dev.yml up --force-recreate --build

